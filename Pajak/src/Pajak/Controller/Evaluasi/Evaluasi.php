<?php

namespace Pajak\Controller\Evaluasi;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class Evaluasi extends AbstractActionController {

    public function indexAction() {
        ini_set('memory_limit', '2048M');
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();

        $view = new ViewModel(array(
            'datauser' => $session,
            'data_pemda' => $ar_pemda
        ));
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function graphpajakAction() {
        
        $jenispajak = $this->Tools()->getService('RekeningTable')->getdataJenisObjekAsc();
        
        $s = "";
        $s .= "<script src='" . $this->getRequest()->getBasePath() . "/public/js/code/modules/data.js'></script>
                <script src='" . $this->getRequest()->getBasePath() . "/public/js/code/modules/export-data.js'></script>
                <table id='datatable'>
                    <thead>
                        <tr>
                            <th>Jenis Pajak</th>
                            <th>Realisasi</th>
                            <th>Ketetapan</th>
                            <th>Piutang</th>
                        </tr>
                    </thead>
                    <tbody>";
                    foreach($jenispajak as $v){
                        $v_r = $this->Tools()->getService('PendaftaranTable')->getRealisasiPerTahun(date('Y'), $v['s_idjenis']);
                        $v_k = $this->Tools()->getService('PendaftaranTable')->getKetetapanPerTahun(date('Y'), $v['s_idjenis']);
                        $piutang = $v_k['ketetapan'] - $v_r['realisasi'];
                        $s .= "<tr>
                                    <th>".$v['s_singkat']."</th>
                                    <td>".$v_r['realisasi']."</td>
                                    <td>".$v_k['ketetapan']."</td>
                                    <td>".$piutang."</td>
                                </tr>";
                    }
                $s .="        
                    </tbody>
                </table>
                <script type='text/javascript'>
                    Highcharts.chart('container', {
                        data: {
                            table: 'datatable'
                        },
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Data Realisasi, Ketetapan dan Piutang'
                        },
                        yAxis: {
                            allowDecimals: true,
                            title: {
                                text: 'Jumlah (Rp)'
                            },
                            labels: {
                                format: '{value:.0f}'
                            }
                        },
                        calculable: true,
                        colors: ['#52e550', '#6e76ef', '#fc4141'],
                        plotOptions: {
                            series: {
                                pointWidth: 50
                            }
                        },
                    });
                </script>";
        $data_render = array(
            "grid" => $s
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function graphpajakbulananAction() {

        $jenispajak = $this->Tools()->getService('RekeningTable')->getdataJenisObjekAsc();
        $recordspajak = array();
        foreach ($jenispajak as $v) {
            $recordspajak[] = "'" . $v['s_singkat'] . "'";
        }
        $pajak = implode(", ", $recordspajak);

        $q_ketetapan = $this->Tools()->getService('PendaftaranTable')->getKetetapanPerBulan(date('m'), date('Y'));
        //ketetapan
        $recordsketetapan = array();
        foreach ($q_ketetapan as $v_k) {
            $recordsketetapan[] = $v_k['ketetapan'];
        }
        
        $ketetapan = implode(", ", $recordsketetapan);
        // var_dump($ketetapan); exit();

        $s = "";
        $s .= "<script type='text/javascript'>
                Highcharts.chart('container1', {
                    chart: {
                        type: 'area'
                    },
                    title: {
                        text: 'Data Realisasi, Ketetapan dan Piutang'
                    },
                    xAxis: {
                        categories: [".$pajak."],
                        tickmarkPlacement: 'on',
                        title: {
                            enabled: false
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Jumlah (Rp)'
                        },
                        labels: {
                            formatter: function () {
                                return this.value / 1000;
                            }
                        }
                    },
                    tooltip: {
                        split: true,
                        // valueSuffix: ' millions'
                    },
                    plotOptions: {
                        area: {
                            stacking: 'normal',
                            lineColor: '#666666',
                            lineWidth: 1,
                            marker: {
                                lineWidth: 1,
                                lineColor: '#666666'
                            }
                        }
                    },
                    series: [{
                        name: 'Realisasi',
                        data: [".$ketetapan."]
                    }, {
                        name: 'Ketetapan',
                        data: [".$ketetapan."]
                    }, {
                        name: 'Piutang',
                        data: [".$ketetapan."]
                    }]
                });
                </script>
                ";

        $data_render = array(
            "grid1" => $s
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }
}
