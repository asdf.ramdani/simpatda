<?php

namespace Pajak\Controller\Migrasirekening;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class MigrasiRekening extends AbstractActionController
{
    public function indexAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();

        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }

        $view = new ViewModel();

        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function tambahAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $datadenda = $this->Tools()->getService('RekeningHistoryTable')->getDataDenda();

        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }

        $view = new ViewModel(array(
            // 'frm' => $form
            'datadenda' => $datadenda
        ));

        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridRekeningLamaAction()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $req->isGet();
        $post_data = $req->getPost();
        $parametercari = $req->getQuery();
        $base = new \Pajak\Model\Setting\RekeningBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2) {
            $base->page = $base->page + 1;
        }
        if ($base->direction == 1) {
            $base->page = $base->page - 1;
        }
        if ($base->page <= 0) {
            $base->page = 1;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('RekeningHistoryTable')->getGridCountRekening($base, $parametercari);

        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages) {
            $page = $total_pages;
        }
        $start = $limit * $page - $limit;
        if ($start < 0) {
            $start = 0;
        }
        $s = "";
        $data = $this->Tools()->getService('RekeningHistoryTable')->getGridDataRekening($base, $start, $parametercari);
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td>" . $row['korek'] . "</td>";
            $s .= "<td>" . $row['s_namakorek'] . "</td>";
            if ($post_data['rekbaru'] == 'true') {
                $s .= "<td><a href='#' onclick='pilihRekening2(" . $row['s_idkorek'] . ");return false;' class='btn btn-xs btn-primary'><span class='glyph-icon icon-hand-o-up'> Pilih </a></td>";
            } else {
                $s .= "<td><a href='#' onclick='pilihRekening(" . $row['s_idkorek'] . ");return false;' class='btn btn-xs btn-primary'><span class='glyph-icon icon-hand-o-up'> Pilih </a></td>";
            }
            $s .= "</tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => 10,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function pilihRekeningAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataRekening = $this->Tools()->getService('RekeningHistoryTable')->getdataRekeningLama($data_get['s_idkorek']);

        if ($dataRekening['s_sub3korek'] == null) {
            if ($dataRekening['s_sub2korek'] == null) {
                if ($dataRekening['s_sub1korek'] == null) {
                    $korek = $dataRekening['s_tipekorek'] . '.' . $dataRekening['s_kelompokkorek'] . '.' . $dataRekening['s_jeniskorek'] . '.' . $dataRekening['s_objekkorek'] . '.' . $dataRekening['s_rinciankorek'];
                } else {
                    $korek = $dataRekening['s_tipekorek'] . '.' . $dataRekening['s_kelompokkorek'] . '.' . $dataRekening['s_jeniskorek'] . '.' . $dataRekening['s_objekkorek'] . '.' . $dataRekening['s_rinciankorek'] . '.' . $dataRekening['s_sub1korek'];
                }
            } else {
                $korek = $dataRekening['s_tipekorek'] . '.' . $dataRekening['s_kelompokkorek'] . '.' . $dataRekening['s_jeniskorek'] . '.' . $dataRekening['s_objekkorek'] . '.' . $dataRekening['s_rinciankorek'] . '.' . $dataRekening['s_sub1korek'] . '.' . $dataRekening['s_sub2korek'];
            }
        } else {
            $korek = $dataRekening['s_tipekorek'] . '.' . $dataRekening['s_kelompokkorek'] . '.' . $dataRekening['s_jeniskorek'] . '.' . $dataRekening['s_objekkorek'] . '.' . $dataRekening['s_rinciankorek'] . '.' . $dataRekening['s_sub1korek'] . '.' . $dataRekening['s_sub2korek'] . '.' . $dataRekening['s_sub3korek'];
        }

        if ($dataRekening['s_sub3korek_baru'] == null) {
            if ($dataRekening['s_sub2korek_baru'] == null) {
                if ($dataRekening['s_sub1korek_baru'] == null) {
                    $korek_baru = $dataRekening['s_tipekorek_baru'] . '.' . $dataRekening['s_kelompokkorek_baru'] . '.' . $dataRekening['s_jeniskorek_baru'] . '.' . $dataRekening['s_objekkorek_baru'] . '.' . $dataRekening['s_rinciankorek_baru'];
                } else {
                    $korek_baru = $dataRekening['s_tipekorek_baru'] . '.' . $dataRekening['s_kelompokkorek_baru'] . '.' . $dataRekening['s_jeniskorek_baru'] . '.' . $dataRekening['s_objekkorek_baru'] . '.' . $dataRekening['s_rinciankorek_baru'] . '.' . $dataRekening['s_sub1korek_baru'];
                }
            } else {
                $korek_baru = $dataRekening['s_tipekorek_baru'] . '.' . $dataRekening['s_kelompokkorek_baru'] . '.' . $dataRekening['s_jeniskorek_baru'] . '.' . $dataRekening['s_objekkorek_baru'] . '.' . $dataRekening['s_rinciankorek_baru'] . '.' . $dataRekening['s_sub1korek_baru'] . '.' . $dataRekening['s_sub2korek_baru'];
            }
        } else {
            $korek_baru = $dataRekening['s_tipekorek_baru'] . '.' . $dataRekening['s_kelompokkorek_baru'] . '.' . $dataRekening['s_jeniskorek_baru'] . '.' . $dataRekening['s_objekkorek_baru'] . '.' . $dataRekening['s_rinciankorek_baru'] . '.' . $dataRekening['s_sub1korek_baru'] . '.' . $dataRekening['s_sub2korek_baru'] . '.' . $dataRekening['s_sub3korek_baru'];
        }

        $data = array(
            't_idkorek' => $dataRekening['s_idkorek'],
            't_korek' => $korek,
            't_namakorek' => $dataRekening['s_namakorek'],
            't_tarifpajak' => $dataRekening['s_persentarifkorek'],
            't_tarifdasarkorek' => number_format($dataRekening['s_tarifdasarkorek'], 0, ',', '.'),
            't_tglawalkorek' => $dataRekening['s_tglawalkorek'],
            't_tglakhirkorek' => $dataRekening['s_tglakhirkorek'],
            's_jenisdenda' => $dataRekening['s_jenisdenda'],
            't_idkorek_baru' => $dataRekening['s_idkorek_baru'],
            't_korek_baru' => $korek_baru,
            't_namakorek_baru' => $dataRekening['s_namakorek_baru'],
            't_tglawalkorek_baru' => $dataRekening['s_tglawalkorek_baru'],
            't_tglakhirkorek_baru' => $dataRekening['s_tglakhirkorek_baru'],
            't_jenisdenda_baru' => $dataRekening['s_jenisdenda_baru']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function simpanRekeninngBaruAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $frm = new \Pajak\Form\Setting\RekeningFrm($this->Tools()->getService('ObjekTable')->getcomboIdJenisRek());
        $req = $this->getRequest();

        if ($req->isPost()) {
            $kb = new \Pajak\Model\Setting\RekeningBase();
            $frm->setInputFilter($kb->getInputFilter());
            $post_data = $req->getPost();
            $datarek_lama = $this->Tools()->getService('RekeningTable')->getdataRekeningId($post_data['t_idkorek_lama']);
            $t_korek_baru = explode(".", $post_data['t_korek_baru']);

            $post = array(
                's_idkorek' => $post_data['s_idkorek'],
                's_jenisobjek' => $datarek_lama['s_jenisobjek'],
                's_tipekorek' => $t_korek_baru[0],
                's_kelompokkorek' => $t_korek_baru['1'],
                's_jeniskorek' => $t_korek_baru['2'],
                's_objekkorek' => $t_korek_baru['3'],
                's_rinciankorek' => $t_korek_baru['4'],
                's_sub1korek' => (isset($t_korek_baru['5'])) ? $t_korek_baru['5'] : null,
                's_sub2korek' => (isset($t_korek_baru['6'])) ? $t_korek_baru['6'] : null,
                's_sub3korek' => (isset($t_korek_baru['7'])) ? $t_korek_baru['7'] : null,
                's_namakorek' => $post_data['s_namakorek'],
                's_persentarifkorek' => $datarek_lama['s_persentarifkorek'],
                's_tarifdasarkorek' => $datarek_lama['s_tarifdasarkorek'],
                's_voldasarkorek' => $datarek_lama['s_voldasarkorek'],
                's_tariftambahkorek' => $datarek_lama['s_tariftambahkorek'],
                's_tglawalkorek' => date('Y-m-d', strtotime($post_data['s_tglawalkorek'])),
                's_tglakhirkorek' => date('Y-m-d', strtotime($post_data['s_tglakhirkorek']))
            );

            $frm->setData($post);
            if ($frm->isValid()) {
                $kb->exchangeArray($frm->getData());
                
                $this->Tools()->getService('RekeningTable')->savedata($kb, $session);
                if ($post_data['s_idkorek'] == '') {
                    $lastId = $this->Tools()->getService('RekeningHistoryTable')->getRekeningIdmax();
                } else {
                    $lastId['max'] = $post_data['s_idkorek'];
                }

                $data_rek_histori = [
                    's_idkorek_lama' => $post_data['t_idkorek_lama'],
                    's_idkorek_baru' => $lastId['max']
                ];
                $this->Tools()->getService('RekeningHistoryTable')->save($data_rek_histori);
                $dataObjek =  $this->Tools()->getService('RekeningHistoryTable')->getdataObjek($post_data['t_idkorek_lama']);
                $dataRekObjek = [];
                $dataObjekRek = [];

                if ($dataObjek->count() > 0) {
                    foreach ($dataObjek as $val) {
                        $dataRekObjek[] = [
                            't_idobjek' => $val['t_idobjek'],
                            't_idkorek_lama' => $post_data['t_idkorek_lama'],
                            't_idkorek_baru' => $lastId['max'],
                        ];

                        $dataObjekRek[] = [
                            't_idobjek' => $val['t_idobjek'],
                            't_korekobjek' => $lastId['max'],
                        ];
                    }

                    foreach ($dataRekObjek as $val) {
                        $this->Tools()->getService('RekeningHistoryObjekTabel')->save($val);
                    }

                    foreach ($dataObjekRek as $val) {
                        $this->Tools()->getService('RekeningHistoryTable')->updateKorekObjek($val);
                    }
                }

                $response = [
                    't_idkorek' => $lastId['max'],
                    'message' => 'Berhasil di simpan'
                ];
                return $this->getResponse()->setContent(\Zend\Json\Json::encode($response));
            }
        }
    }

    public function dataGridMigrasirekAction()
    {

        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $req->isGet();
        $parametercari = $req->getQuery();
        $base = new \Pajak\Model\Setting\RekeningBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2) {
            $base->page = $base->page + 1;
        }
        if ($base->direction == 1) {
            $base->page = $base->page - 1;
        }
        if ($base->page <= 0) {
            $base->page = 1;
        }
        $page = $base->page;
        $limit = $base->rows;

        $count = $this->Tools()->getService('RekeningHistoryTable')->getGridCountMigrasiRekening($base, $parametercari);

        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages) {
            $page = $total_pages;
        }
        $start = $limit * $page - $limit;
        if ($start < 0) {
            $start = 0;
        }
        $s = "";
        $data = $this->Tools()->getService('RekeningHistoryTable')->getGridMigrasiRekening($base, $start, $parametercari);
        // var_dump($data); die;
        $no = 0;
        foreach ($data as $key => $row) {
            $no = $no+1;
            $s .= "<tr>";
            $s .= "<td>" . $no . "</td>";
            $s .= "<td>" . $row['korek'] . "</td>";
            $s .= "<td>" . $row['s_namakorek'] . "</td>";
            $s .= "<td>" . $row['s_tglakhirkorek'] . "</td>";
            $s .= "<td>" . $row['korek_baru'] . "</td>";
            $s .= "<td>" . $row['s_namakorek_baru'] . "</td>";
            $s .= "<td>" . $row['s_tglakhirkorek_baru'] . "</td>";
            $s .= "</tr>";
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator1($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "grid" => $s,
            "rows" => 10,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages,
            "paginatore" => $datapaging['paginatore'],
            "akhirdata" => $datapaging['akhirdata'],
            "awaldata" => $datapaging['awaldata'],
        );
        
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataGridObjekAction()
    {

        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $req_post = $req->getPost();
        $parametercari = $req->getQuery();
        
        $base = new \Pajak\Model\Setting\RekeningBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2) {
            $base->page = $base->page + 1;
        }
        if ($base->direction == 1) {
            $base->page = $base->page - 1;
        }
        if ($base->page <= 0) {
            $base->page = 1;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('RekeningHistoryObjekTabel')->getGridCountObjek($base, $req_post['t_idrek'], $parametercari);

        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages) {
            $page = $total_pages;
        }
        $start = $limit * $page - $limit;
        if ($start < 0) {
            $start = 0;
        }
        $s = "";
        $data = $this->Tools()->getService('RekeningHistoryObjekTabel')->getGridDataObjek($base, $start, $req_post['t_idrek'], $parametercari);
        
        $no = 0;
        foreach ($data as $row) {
            // var_dump($row); die;
            $no = $no+1;
            $s .= "<tr>";
            $s .= "<td>" . $no . "</td>";
            $s .= "<td>" . $row['t_namaobjekpj'] . "</td>";
            $s .= "<td>" . $row['t_alamatobjekpj'] . "</td>";
            $s .= "<td>" . $row['korek'] . "</td>";
            $s .= "<td>" . $row['s_namakorek'] . "</td>";
            $s .= "</tr>";
        }

        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginatore4($base->rows, $count, $page, $total_pages, $start, $req_post['t_idrek']);
        $data_render = array(
            "grid" => $s,
            "rows" => 10,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages,
            "paginatore" => $datapaging['paginatore'],
            "akhirdata" => $datapaging['akhirdata'],
            "awaldata" => $datapaging['awaldata'],
        );

        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataGridkorekobjekAction()
    {

        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $req->isGet();
        $parametercari = $req->getQuery();
        $base = new \Pajak\Model\Setting\RekeningBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2) {
            $base->page = $base->page + 1;
        }
        if ($base->direction == 1) {
            $base->page = $base->page - 1;
        }
        if ($base->page <= 0) {
            $base->page = 1;
        }
        $page = $base->page;
        $limit = $base->rows;

        $count = $this->Tools()->getService('RekeningHistoryObjekTabel')->getGridCountMigrasiRekening($base, $parametercari);

        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages) {
            $page = $total_pages;
        }
        $start = $limit * $page - $limit;
        if ($start < 0) {
            $start = 0;
        }
        $s = "";
        $data = $this->Tools()->getService('RekeningHistoryObjekTabel')->getGridMigrasiRekening($base, $start, $parametercari);
        
        $no = 0;
        foreach ($data as $row) {
            $no = $no+1;
            $s .= "<tr>";
            $s .= "<td>" . $no . "</td>";
            $s .= "<td>" . $row['t_namaobjekpj'] . "</td>";
            $s .= "<td>" . $row['korek_baru'] . "</td>";
            $s .= "<td>" . $row['s_namakorek_baru'] . "</td>";
            $s .= "<td>" . $row['s_tglakhirkorek_baru'] . "</td>";
            $s .= "<td>" . $row['korek_lama'] . "</td>";
            $s .= "<td>" . $row['s_namakorek_lama'] . "</td>";
            $s .= "<td>" . $row['s_tglakhirkorek_lama'] . "</td>";
            $s .= "</tr>";
        }

        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator5($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "grid" => $s,
            "rows" => 10,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages,
            "paginatore" => $datapaging['paginatore'],
            "akhirdata" => $datapaging['akhirdata'],
            "awaldata" => $datapaging['awaldata'],
        );
        
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }
}
