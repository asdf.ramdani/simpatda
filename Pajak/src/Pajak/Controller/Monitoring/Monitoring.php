<?php

namespace Pajak\Controller\Monitoring;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class Monitoring extends AbstractActionController {

    public function indexAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $view = new ViewModel(array(
        ));
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function CariObjekPajakAction() {
        /** Cari Data Objek Pajak
         * param int $npwpd
         * author Miftahul Huda <miftahul06gmail.com>
         * date 29/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        // Mengambil Data WP, OP dan Transaksi
        $data = $this->Tools()->getService('ObjekTable')->getObjekPajakbyNPWPD($data_get['npwpd']);
        $wp = $data->current();
        $datawp = " <div class='row'>
                    <div class='col-sm-12'>
                        <label class='col-sm-1'>Nama</label>
                        <div class='col-sm-11'>
                             " . $wp['t_nama'] . "
                        </div>
                    </div>
                    <div class='col-sm-12'>
                        <label class='col-sm-1'>Alamat</label>
                        <div class='col-sm-11'>
                             " . $wp['t_alamat'] . "
                            ,Desa/Kel. " . $wp['s_namakel'] . "
                            ,Kec. " . $wp['s_namakec'] . "
                            ,Kab. " . $wp['t_kabupaten'] . "
                        </div>
                    </div>";

        $dataobjek = "<div class='panel'>
                        <div class='panel-body'>
                            <h3 class='title-hero' style='color:blue;'>
                                Data Objek Pajak
                            </h3>
                            <div class='example-box-wrapper'>
                                <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size:11px; color:black'>
                                        <thead class='cf'>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>NIOP</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Nama</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Alamat</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataobjekpajak = $this->Tools()->getService('ObjekTable')->getObjekPajakbyNPWPD($data_get['npwpd']);
        $i = 1;
        foreach ($dataobjekpajak as $row) {
            $dataobjek .= "                 <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i++ . "</td>
                                                <td data-title='NIOP' style='text-align:center'>" . $row['t_nop'] . "</td>
                                                <td data-title='Nama'>" . $row['t_namaobjek'] . "</td>
                                                <td data-title='Alamat'>" . $row['t_alamatobjek'] . ", " . $row['s_namakelobjek'] . ", " . $row['s_namakecobjek'] . "</td>
                                                <td data-title='#' style='text-align:center'><input type='button' class='btn btn-xs btn-warning' value='Transaksi' onclick='bukatransaksibyobjek(" . $row['t_idobjek'] . ")'></td>
                                            </tr>";
        }
        $dataobjek .= "                 </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div></div>";

        $data_render = array(
            "datawp" => $datawp,
            "dataobjek" => $dataobjek
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function CariTransaksiByobjekAction() {
        /** Cari Transaksi By Objek Pajak
         * param int $npwpd
         * author Miftahul Huda <miftahul06gmail.com>
         * date 29/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        // Mengambil Data WP, OP dan Transaksi
        $op = $this->Tools()->getService('ObjekTable')->getObjekPajakbyIdObjek($data_get['idobjek']);
        $dataop = " <div class='col-sm-12'>
                        <label class='col-sm-1'>NIOP</label>
                        <div class='col-sm-11'>
                            : " . $op['t_nop'] . "
                        </div>
                    </div>
                    <div class='col-sm-12'>
                        <label class='col-sm-1'>Nama</label>
                        <div class='col-sm-11'>
                            : " . $op['t_namaobjek'] . "
                        </div>
                    </div>
                    <div class='col-sm-12'>
                        <label class='col-sm-1'>Alamat</label>
                        <div class='col-sm-11'>
                            : " . $op['t_alamatobjek'] . "
                            ,Desa/Kel. " . $op['s_namakelobjek'] . "
                            ,Kec. " . $op['s_namakecobjek'] . "
                            ,Kab. " . $op['t_kabupatenobjek'] . "
                        </div>
                    </div>";

        if ($op['t_jenisobjek'] == 4 || $op['t_jenisobjek'] == 8) {
            $datatransaksi = "  <div class='example-box-wrapper'>
                                <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Penetapan (Tgl.)</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>#</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jumlah (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
            $datatransaksipajak = $this->Tools()->getService('ObjekTable')->getTransaksibyIdObjek($data_get['idobjek']);
            $i = 1;
            foreach ($datatransaksipajak as $row) {
                $t_tglpenetapan = (!empty($row['t_tglpenetapan']) ? date('d-m-Y', strtotime($row['t_tglpenetapan'])) : '-');
                $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-');
                $t_tgldendapembayaran = (!empty($row['t_tgldendapembayaran']) ? date('d-m-Y', strtotime($row['t_tgldendapembayaran'])) : '-');
                $t_tglbayardenda = (!empty($row['t_tglbayardenda']) ? date('d-m-Y', strtotime($row['t_tglbayardenda'])) : '-');
                $datatransaksi .= "         <tr data-toggle='collapse' data-target='#accordion" . $row['t_idtransaksi'] . "' class='clickable'>
                                                <td data-title='No.' style='text-align:center'>" . $i++ . "</td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl.' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                <td data-title='Tgl. Penetapan' style='text-align:center'>" . $t_tglpenetapan . "</td>
                                                <td data-title='Tgl. Pembayaran' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jumlah' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                                <td data-title='Detail' style='text-align:right'><button type='button' class='btn btn-xs btn-warning'> Detail</button></td>
                                            </tr>
                                            <tr id='accordion" . $row['t_idtransaksi'] . "' class='collapse'>
                                                <td style='background-color: #C6E746'></td>
                                                <td style='background-color: #C6E746'>STPD</td>
                                                <td style='text-align:center; background-color: #C6E746'>" . $t_tgldendapembayaran . "</td>
                                                <td style='background-color: #C6E746'></td>
                                                <td style='text-align:right; background-color: #C6E746'>" . number_format($row['t_jmlhdendapembayaran'], 0, ',', '.') . "</td>
                                                <td style='background-color: #C6E746'></td>
                                                <td style='text-align:center; background-color: #C6E746'>" . $t_tglbayardenda . "</td>
                                                <td style='text-align:right; background-color: #C6E746'>" . number_format($row['t_jmlhbayardenda'], 0, ',', '.') . "</td>
                                                <td style='background-color: #C6E746'></td>
                                            </tr>";
            }
            $datatransaksi .= "             </tbody>
                                    </table>
                                </div>
                            </div>";
        } else {
            $datatransaksi = "  <div class='example-box-wrapper'>
                                <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>#</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jumlah (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
            $datatransaksipajak = $this->Tools()->getService('ObjekTable')->getTransaksibyIdObjek($data_get['idobjek']);
            $i = 1;
            foreach ($datatransaksipajak as $row) {
                $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-');
                $t_tgldendapembayaran = (!empty($row['t_tgldendapembayaran']) ? date('d-m-Y', strtotime($row['t_tgldendapembayaran'])) : '-');
                $t_tglbayardenda = (!empty($row['t_tglbayardenda']) ? date('d-m-Y', strtotime($row['t_tglbayardenda'])) : '-');
                $datatransaksi .= "         <tr data-toggle='collapse' data-target='#accordion" . $row['t_idtransaksi'] . "' class='clickable'>
                                                <td data-title='No.' style='text-align:center'>" . $i++ . "</td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jumlah' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                                <td data-title='Detail' style='text-align:right'><button type='button' class='btn btn-xs btn-warning'> Detail</button></td>
                                            </tr>
                                            <tr id='accordion" . $row['t_idtransaksi'] . "' class='collapse'>
                                                <td colspan='8'> 
                                                    <table class='table table-striped table-condensed table-bordered cf' style='font-size: 11px'>
                                                        <tr>
                                                            <td style='text-align:center; background-color: #C6E746'>Jenis Ketetapan</td>
                                                            <td style='text-align:center; background-color: #C6E746' colspan='2'>Penetapan</td>
                                                            <td style='text-align:center; background-color: #C6E746' colspan='2'>Pembayaaran</td>
                                                        </tr>
                                                        <tr>
                                                            <td>STPD</td>
                                                            <td style='text-align:center;'>" . $t_tgldendapembayaran . "</td>
                                                            <td style='text-align:right;'>" . number_format($row['t_jmlhdendapembayaran'], 0, ',', '.') . "</td>
                                                            <td style='text-align:center;'>" . $t_tglbayardenda . "</td>
                                                            <td style='text-align:right;'>" . number_format($row['t_jmlhbayardenda'], 0, ',', '.') . "</td>
                                                        </tr>";
                // SKPDKB
                $dataSKPDKB = $this->Tools()->getService('SkpdkbTable')->getDataSKPDKB($row['t_idtransaksi']);
                $t_tglpendataanSKPDKB = ($dataSKPDKB['t_tglpendataan'] != null ? date('d-m-Y', strtotime($dataSKPDKB['t_tglpendataan'])) : '-');
                $t_tglpembayaranSKPDKB = ($dataSKPDKB['t_tglpembayaran'] != null ? date('d-m-Y', strtotime($dataSKPDKB['t_tglpembayaran'])) : '-');
                $datatransaksi .= "                     <tr>
                                                            <td style=''>" . $dataSKPDKB['t_jenisketetapan'] . "</td>
                                                            <td style='text-align:center;'>" . $t_tglpendataanSKPDKB . "</td>
                                                            <td style='text-align:right;'>" . number_format($dataSKPDKB['t_totalpajak'], 0, ',', '.') . "</td>
                                                            <td style='text-align:center;'>" . $t_tglpembayaranSKPDKB . "</td>
                                                            <td style='text-align:right;'>" . number_format($dataSKPDKB['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                                        </tr>";

                // SKPDKBT
                $dataSKPDKBT = $this->Tools()->getService('SkpdkbtTable')->getDataSKPDKBT($row['t_idtransaksi']);
                $t_tglpendataanSKPDKBT = ($dataSKPDKBT['t_tglpendataan'] != null ? date('d-m-Y', strtotime($dataSKPDKBT['t_tglpendataan'])) : '-');
                $t_tglpembayaranSKPDKBT = ($dataSKPDKBT['t_tglpembayaran'] != null ? date('d-m-Y', strtotime($dataSKPDKBT['t_tglpembayaran'])) : '-');
                $datatransaksi .= "                     <tr>
                                                            <td style=''>" . $dataSKPDKBT['t_jenisketetapan'] . "</td>
                                                            <td style='text-align:center;'>" . $t_tglpendataanSKPDKBT . "</td>
                                                            <td style='text-align:right;'>" . number_format($dataSKPDKBT['t_totalpajak'], 0, ',', '.') . "</td>
                                                            <td style='text-align:center;'>" . $t_tglpembayaranSKPDKBT . "</td>
                                                            <td style='text-align:right;'>" . number_format($dataSKPDKBT['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                                        </tr>";

                // SKPDLB
                $dataSKPDLB = $this->Tools()->getService('SkpdlbTable')->getDataSKPDLB($row['t_idtransaksi']);
                $t_tglpendataanSKPDLB = ($dataSKPDLB['t_tglpendataan'] != null ? date('d-m-Y', strtotime($dataSKPDLB['t_tglpendataan'])) : '-');
                $t_tglpembayaranSKPDLB = ($dataSKPDLB['t_tglpembayaran'] != null ? date('d-m-Y', strtotime($dataSKPDLB['t_tglpembayaran'])) : '-');
                $datatransaksi .= "                     <tr>
                                                            <td style=''>" . $dataSKPDLB['t_jenisketetapan'] . "</td>
                                                            <td style='text-align:center;'>" . $t_tglpendataanSKPDLB . "</td>
                                                            <td style='text-align:right;'>" . number_format($dataSKPDLB['t_totalpajak'], 0, ',', '.') . "</td>
                                                            <td style='text-align:center;'>" . $t_tglpembayaranSKPDLB . "</td>
                                                            <td style='text-align:right;'>" . number_format($dataSKPDLB['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                                        </tr>";

                // SKPDN
                $dataSKPDN = $this->Tools()->getService('SkpdnTable')->getDataSKPDN($row['t_idtransaksi']);
                $t_tglpendataanSKPDN = ($dataSKPDN['t_tglpendataan'] != null ? date('d-m-Y', strtotime($dataSKPDN['t_tglpendataan'])) : '-');
                $datatransaksi .= "                     <tr>
                                                            <td style=''>" . $dataSKPDN['t_jenisketetapan'] . "</td>
                                                            <td style='text-align:center;'>" . $t_tglpendataanSKPDN . "</td>
                                                            <td style='text-align:right;'>" . number_format($dataSKPDN['t_totalpajak'], 0, ',', '.') . "</td>
                                                            <td style='text-align:center;'></td>
                                                            <td style='text-align:right;'></td>
                                                        </tr>";

                $datatransaksi .= "                 </table>
                                                </td>
                                            </tr>";
            }
            $datatransaksi .= "             </tbody>
                                    </table>
                                </div>
                            </div>";
        }

        $data_render = array(
            "dataop" => $dataop,
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

}
