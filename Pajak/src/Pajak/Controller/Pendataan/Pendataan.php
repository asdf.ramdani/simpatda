<?php

namespace Pajak\Controller\Pendataan;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Pajak\Form\Pendataan\PendataanFrm;
use Pajak\Model\Pendataan\PendataanBase;

class Pendataan extends AbstractActionController {

    public function indexAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $form = new \Pajak\Form\Pendaftaran\PendaftaranFrm($this->Tools()->getService('PendaftaranTable')->getcomboIdKecamatan(), null);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $s_idjenis = $this->getEvent()->getRouteMatch()->getParam('s_idjenis');
        $jenisobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjekId($s_idjenis);
        $ar_rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningByIdJenisObjek($s_idjenis);
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $ar_kecamatan = $this->Tools()->getService('KecamatanTable')->getdata();
        $recordskecamatan = array();
        foreach ($ar_kecamatan as $ar_kecamatan) {
            $recordskecamatan[] = $ar_kecamatan;
        }
        $view = new ViewModel(array(
            'form' => $form,
            's_idjenis' => $s_idjenis,
            'jenisobjek' => $jenisobjek,
            'ar_rekening' => $ar_rekening,
            'ar_kecamatan' => $recordskecamatan,
            'ar_ttd0' => $recordspejabat,
            "idjenis" => $this->params('s_idjenis'),
        ));
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridWpAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $post = $req->getPost();
        $base = new \Pajak\Model\Pendaftaran\PendaftaranBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('PendaftaranTable')->getGridCountWp($base, $allParams['s_idjenis'], $post);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('PendaftaranTable')->getGridDataWp($base, $start, $allParams['s_idjenis'], $post);
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        if ($count == 0) {
            $s .= "<tr><td colspan='11'>Tidak ada data</td></tr>";
        } else {
            foreach ($data as $row) {
                $s .= "<tr>";
                $s .= "<td data-title='No'><center>" . $counter . "</center></td>";
                $s .= "<td data-title='NPWPD'><center style='color:red; font-size:12px; font-weight:bold'>" . $row['t_npwpd'] . "</center></td>";
                $s .= "<td data-title='Nama WP'>" . $row['t_nama'] . "</td>";
                $s .= "<td data-title='NIOP'>" . $row['t_nop'] . "</td>";
                $s .= "<td data-title='Nama OP'>" . $row['t_namaobjek'] . "</td>";
                $s .= "<td data-title='Alamat OP'>" . $row['t_alamatobjek'] . "</td>";
                $s .= "<td data-title='Kelurahan OP'>" . $row['s_namakelobjek'] . "</td>";
                $s .= "<td data-title='Kecamatan OP'>" . $row['s_namakecobjek'] . "</td>";
                $s .= "<td data-title='Nama Pemilik'>" . $row['t_namapemilik'] . "</td>";
                $s .= "<td data-title='Rekening'>" . $row['korek'] . "<br>" . $row['s_namakorek'] . "</td>";
                if ($row['t_status_wp'] == 't') {
                    $cetakKartudata = "<button class='btn btn-primary btn-xs btn-flat' type='button' onclick='bukaCetakKartudata($row[t_idobjek])'><span class='glyph-icon icon-print'></span> Kartu Data</button>";
                    if ($allParams['s_idjenis'] == 4) {
                        $s .= "<td data-title='#'><center>" . $cetakKartudata . "<a href='form_pagereklame?t_idobjek=$row[t_idobjek]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Pendataan</a></center></td>";
                    } else if ($allParams['s_idjenis'] == 6) {
                        $s .= "<td data-title='#'><center>" . $cetakKartudata . "<a href='form_pageminerba?t_idobjek=$row[t_idobjek]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Pendataan</a></center></td>";
                        // } else if ($allParams['s_idjenis'] == 7) {
                        // 	$s .= "<td data-title='#'><center><a href='form_pageparkir?t_idobjek=$row[t_idobjek]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Pendataan</a></center></td>";        
                    } else if ($allParams['s_idjenis'] == 8) {
                        $s .= "<td data-title='#'><center>" . $cetakKartudata . "<a href='form_pageair?t_idobjek=$row[t_idobjek]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Pendataan</a></center></td>";
                        // } else if ($allParams['s_idjenis'] == 9) {
                        // 	$s .= "<td data-title='#'><center><a href='form_pagewalet?t_idobjek=$row[t_idobjek]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Pendataan</a></center></td>";
                    } else if ($allParams['s_idjenis'] == 5) {
                        $s .= "<td data-title='#'><center>" . $cetakKartudata . "<a href='form_pageppj?t_idobjek=$row[t_idobjek]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Pendataan</a></center></td>";
                    } else {
                        $s .= "<td data-title='#'><center>" . $cetakKartudata . "<a href='form_pagedefault?t_idobjek=$row[t_idobjek]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Pendataan</a></center></td>";
                    }
                } else {
                    $s .= "<td data-title='#'><center><span class='btn btn-xs btn-flat btn-danger'><i class='glyph-icon icon-minus-circle'></i> OP TUTUP</span></center></td>";
                }
                $s .= "</tr>";
                $counter++;
            }
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "gridwp" => $s,
            "rowswp" => $base->rows,
            "countwp" => $count,
            "pagewp" => $page,
            "startwp" => $start,
            "total_halamanwp" => $total_pages,
            "paginatorewp" => $datapaging['paginatore'],
            "akhirdatawp" => $datapaging['akhirdata'],
            "awaldatawp" => $datapaging['awaldata']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataGridSudahAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $post = $req->getPost();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $base = new PendataanBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('PendataanTable')->getGridCountSudah($base, $allParams['s_idjenis'], $post);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('PendataanTable')->getGridDataSudah($base, $start, $allParams['s_idjenis'], $post);
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        if ($count == 0) {
            $s .= "<tr><td colspan='17'>Tidak ada data</td></tr>";
        } else {
            foreach ($data as $row) {
                $is_esptpd = ($row['is_esptpd'] != 0 ? '<span class="btn btn-xs btn-success">WP</span><br><b style="color:#db403c">e-SPTPD</b>' : '<span class="btn btn-xs btn-info">' . $ar_pemda->s_namasingkatinstansi . '</span><br><b style="color:#3850b8">SIMPATDA</b>');
                if ($row['t_kodebayar'] == '') {
                    $status_bayar = ($row['t_jmlhpajak']!=0) ? '<b style="color:blue;"><i class="glyph-icon icon-check"></i> BELUM DITETAPKAN</b>' : '<b style="color:blue;">NIHIL</b>';
                } else {
                    $status_bayar = (!empty($row['t_tglpembayaran'])) ? '<b style="color:GREEN;"><i class="glyph-icon icon-money"></i> LUNAS<br>Tgl. ' . date('d-m-Y', strtotime($row['t_tglpembayaran'])) . '</b>' : '<b style="color:red;"><i class="glyph-icon icon-money"></i> BELUM BAYAR</b>';
                }
                $s .= "<tr>";
                $s .= "<td data-title='No'><center>" . $counter . "</center></td>";
                $edit = "";
                $hapus = "";
                $operator = "";
                if (empty($row['t_tglpembayaran'])) {
                    if ($row['t_jenispajak'] == 4) {
                        $edit = "<a href='form_pagereklameedit?t_idtransaksi=$row[t_idtransaksi]' class='btn btn-warning btn-xs btn-flat' title='Edit'><i class='glyphicon glyphicon-pencil'></i></a>";
                    } elseif ($row['t_jenispajak'] == 6) {
                        $edit = "<a href='form_pageminerbaedit?t_idtransaksi=$row[t_idtransaksi]' class='btn btn-warning btn-xs btn-flat' title='Edit'><i class='glyphicon glyphicon-pencil'></i></a>";
                    } elseif ($row['t_jenispajak'] == 8) {
                        $edit = "<a href='form_pageairedit?t_idtransaksi=$row[t_idtransaksi]' class='btn btn-warning btn-xs btn-flat' title='Edit'><i class='glyphicon glyphicon-pencil'></i></a>";
                    } else if ($allParams['s_idjenis'] == 5) {
                        $edit = "<a href='form_pageppjedit?t_idtransaksi=$row[t_idtransaksi]' class='btn btn-warning btn-xs btn-flat' title='Edit'><i class='glyphicon glyphicon-pencil'></i></a>";
                    } else {
                        $edit = "<a href='form_pagedefaultedit?t_idtransaksi=$row[t_idtransaksi]' class='btn btn-warning btn-xs btn-flat' title='Edit'><i class='glyphicon glyphicon-pencil'></i></a>";
                    }
                    $hapus = "";
                    if ($session['s_akses'] == 2) {
                        $hapus = "<a href='#' onclick='hapus(" . $row['t_idtransaksi'] . ");return false;' class='btn btn-danger btn-xs btn-flat' title='Hapus'><i class='glyph-icon icon-trash'></i></a>";
                        $operator = $row['s_nama'];
                    }
                }

                if ($row['s_jenispungutan'] == 'Retribusi') {
                    $cetak = "<button onclick='bukaCetakSKRD($row[t_idtransaksi])' target='_blank' class='btn btn-primary btn-xs' title='Cetak SKRD'><i class='glyph-icon icon-print'></i></button>
                          <a href='cetakkodebayar?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' class='btn btn-primary btn-xs' title='Cetak Kode Bayar'><i class='glyph-icon icon-print'></i></a>";
                } elseif ($row['s_jenispungutan'] == 'Pajak') {
                    if ($row['t_jenispajak'] == 4 || $row['t_jenispajak'] == 8) {
                        $modelcetak = "<button onclick='bukaCetakSKPD($row[t_idtransaksi])' target='_blank' class='btn btn-primary btn-xs' title='Cetak SKPD'><i class='glyph-icon icon-print'></i></button>";
                    } else {
                        $modelcetak = "<button onclick='bukaCetakSPTPD($row[t_idtransaksi])' target='_blank' class='btn btn-primary btn-xs' title='Cetak SPTPD'><i class='glyph-icon icon-print'></i></button>";
                    }
//
//                if (($allParams['s_idjenis'] == 4) || ($allParams['s_idjenis'] == 8)) {
//                    $cetak = "$modelcetak <button onclick='bukaCetakNPPD($row[t_idtransaksi])' target='_blank' class='btn btn-primary btn-xs' title='Cetak Nota Perhitungan Pajak Daerah'><i class='glyph-icon icon-print'></i></button>";
//                } else {
//                    $cetak = $modelcetak;
//                }
                    $cetak = $modelcetak;
                    // if ($row['t_jenispajak'] == 5):
                    //     $cetak .= " <a href='cetaklampiranppj?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' class='btn btn-primary btn-xs' title='Cetak Lampiran PPJ'><i class='glyph-icon icon-print'></i></a>";
                    // endif;
                    if ($row['t_jenispajak'] == 4 || $row['t_jenispajak'] == 8) {
                        if (!empty($row['t_tglpenetapan'])) {
                            $cetak .= " <a href='cetakkodebayar?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' class='btn btn-primary btn-xs' title='Cetak Kode Bayar'><i class='glyph-icon icon-print'></i></a>";
                        } else {
                            $cetak .= "";
                        }
                    } else {
                        $cetak .= " <a href='cetakkodebayar?&t_idtransaksi=$row[t_idtransaksi]' target='_blank' class='btn btn-primary btn-xs' title='Cetak Kode Bayar'><i class='glyph-icon icon-print'></i></a>";
                    }
                }
                $s .= "<td data-title='#'><center> 
                    $cetak
                    $edit
                    $hapus
                    <br>
                    $operator
                    </center></td>";
                $s .= "<td data-title='No. SPTPD'><center style='color:red; font-size:12px; font-weight:bold'>" . $row['t_nourut'] . "</center></td>";
                $s .= "<td data-title='Tanggal Pendataan'><center>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</center></td>";
                $s .= "<td data-title='NPWPD'><center style='color:red; font-size:12px; font-weight:bold'>" . $row['t_npwpd'] . "</center></td>";
                $s .= "<td data-title='Nama WP'>" . $row['t_nama'] . "</td>";
                $s .= "<td data-title='NIOP'><center>" . $row['t_nop'] . "</center></td>";
                $s .= "<td data-title='Nama Objek'>" . $row['t_namaobjek'] . "</td>";
                $s .= "<td data-title='Masa Pajak' class='text-center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . '<br>s/d<br>' . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>";
                if ($row['t_jenispajak'] == 4) {
                    $s .= "<td data-title='Naskah'>" . $row['t_naskah'] . "</td>";
                    $s .= "<td data-title='Lokasi'>" . $row['t_lokasi'] . "</td>";
                    $s .= "<td data-title='Kelurahan'>" . $row['s_namakelobjek'] . "</td>";
                    $s .= "<td data-title='Kecamatan'>" . $row['s_namakecobjek'] . "</td>";
                }
                $s .= "<td data-title='Rekening'>" . $row['korek'] . "<br>" . $row['s_namakorek'] . "</td>";
                $s .= "<td data-title='Jumlah Pajak' style='color:black; text-align: right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>";
                $s .= "<td data-title='KODE BAYAR' style='text-align: center;color:blue;'><b>" . $row['t_kodebayar'] . "</b><BR>" . $status_bayar . "</td>";
                $s .= "<td data-title='Jumlah Pajak' style='text-align: center'>" . $is_esptpd . "</td>";
                $s .= "</tr>";
                $counter++;
            }
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator1($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "gridsudah" => $s,
            "rowssudah" => $base->rows,
            "countsudah" => $count,
            "pagesudah" => $page,
            "startsudah" => $start,
            "total_halamansudah" => $total_pages,
            "paginatoresudah" => $datapaging['paginatore'],
            "akhirdatasudah" => $datapaging['akhirdata'],
            "awaldatasudah" => $datapaging['awaldata'],
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataGridMasahabisAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $post = $req->getPost();
        $base = new PendataanBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('PendataanTable')->getGridCountMasahabis($base, $allParams['s_idjenis'], $post);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('PendataanTable')->getGridDataMasahabis($base, $start, $allParams['s_idjenis'], $post);
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        if ($count == 0) {
            $s .= "<tr><td colspan='11'>Tidak ada data</td></tr>";
        } else {
            foreach ($data as $row) {
                $s .= "<tr>";
                $s .= "<td data-title='No'><center>" . $counter . "</center></td>";
                $s .= "<td data-title='No. Urut'><center><b style='color:red'>" . $row['t_nourut'] . "</b></center></td>";
                $s .= "<td data-title='Tanggal Pendataan'><center>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</center></td>";
                $s .= "<td data-title='NPWPD'><center><b style='color:red'>" . $row['t_npwpd'] . "</b></center></td>";
                $s .= "<td data-title='Nama'>" . $row['t_nama'] . "</td>";
                $s .= "<td data-title='NIOP'><center>" . $row['t_nop'] . "</center></td>";
                $s .= "<td data-title='Nama Objek'>" . $row['t_namaobjek'] . "</td>";
                $s .= "<td data-title='Masa Pajak'><center>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</center></td>";
                $s .= "<td data-title='Jumlah Pajak' style='color:black; text-align: right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>";
                $s .= "<td data-title='#'><center> <button onclick='bukaCetakHimbauan($row[t_idtransaksi])' target='_blank' class='btn btn-primary btn-xs' title='Cetak Himbauan'><i class='glyph-icon icon-print'></i> Himbauan</button></center></td>";
                $s .= "</tr>";
                $counter++;
            }
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator2($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "grid2" => $s,
            "rows2" => $base->rows,
            "count2" => $count,
            "page2" => $page,
            "start2" => $start,
            "total_halaman2" => $total_pages,
            "paginatore2" => $datapaging['paginatore'],
            "akhirdata2" => $datapaging['akhirdata'],
            "awaldata2" => $datapaging['awaldata']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataGridBelumLaporAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new PendataanBase();
        $base->exchangeArray($allParams);
        $data = $this->Tools()->getService('PendataanTable')->getGridDataBelumLapor($base, $allParams['s_idjenis'], $allParams['page'], $allParams['rows']);
        $s = "";
        $counter = 1;
        $namapajak = '';
        if ($data->count() == 0) {
            $s .= "<tr><td colspan='11'>Tidak ada data</td></tr>";
        } else {
            foreach ($data as $row) {
                $s .= "<tr>";
                $s .= "<td style='text-align:center'>" . $counter . "</td>";
                $s .= "<td style='text-align:center'><input type='checkbox' name='idobjekdbelumlapor' id='idobjekdbelumlapor' value='" . $row['t_idobjek'] . "'/></td>";
                $s .= "<td style='text-align:center'><b style='color:red; font-size:12px;'>" . $row['t_npwpdwp'] . "</b></td>";
                $s .= "<td>" . $row['t_namawp'] . "</td>";
                $s .= "<td style='text-align:center'>" . $row['t_nop'] . "</td>";
                $s .= "<td>" . $row['t_namaobjek'] . "</td>";
                $s .= "<td>" . $row['t_alamatlengkapobjek'] . "</td>";
                $s .= "<td>" . $row['t_notelpobjek'] . "</td>";
                $s .= "<td>" . $row['t_nohpobjek'] . "</td>";
                $s .= "</tr>";
                $counter++;
                $namapajak = $row['s_namajenis'];
            }
        }

        $s .= "<script type='text/javascript'>
                    $(document).ready(function () {
                        $('.tableBelumLapor tr').click(function (event) {
                            if (event.target.type !== 'checkbox') {
                                $(':checkbox', this).trigger('click');
                            }
                        }); 
                    });
                </script>";
        $htmlcount = '';
        $bulan = (int) $allParams['page'];
        $abulan = ['1' => 'Januari', '2' => 'Februari', '3' => 'Maret', '4' => 'April', '5' => 'Mei', '6' => 'Juni', '7' => 'Juli', '8' => 'Agustus', '9' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
        $htmlcount .= "<b style='color:red; animation: blinker 1s linear infinite;'>" . count($data) . " OP $namapajak Belum Melaporkan SPTPD Pada Masa Pajak " . $abulan[$bulan] . " " . $allParams['rows'] . "</b>";
        $data_render = array(
            "gridbelumlapor" => $s,
            "jumlah" => $htmlcount
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function FormPageinputteguranAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\TeguranlaporanFrm();
        if ($req->isGet()) {
            $idobjek = $req->getQuery()->get('idobjekbelumlapor');
            $bulanmasapajak = $req->getQuery()->get('bulanmasapajak');
            $tahunmasapajak = $req->getQuery()->get('tahunmasapajak');
            $s_idjenis = $req->getQuery()->get('s_idjenis');
            $abulan = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
            $masapajak = $abulan[$bulanmasapajak];

            $datamauditegur = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjekArray($idobjek);
            $maxnoteguran = $this->Tools()->getService('PendaftaranTable')->getNoteguran();
            $noteguran = (int) $maxnoteguran['t_noteguran'] + 1;

            $jenispajak = $this->Tools()->getService('PendaftaranTable')->JenisPajak($s_idjenis);
//            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new \Pajak\Model\Pendataan\TeguranBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                for ($i = 0; count($post['t_noteguran']) > $i; $i++) {
                    $this->Tools()->getService('TeguranTable')->simpansuratteguran($base, $post, $session, $i);
                }
                return $this->redirect()->toRoute("teguranlaporan", array("controllers" => "Teguranlaporan", "action" => "index"));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datamauditegur' => $datamauditegur,
            'noteguran' => $noteguran,
            'bulanpajak' => $bulanmasapajak,
            'masapajak' => $masapajak,
            'tahunpajak' => $tahunmasapajak,
            'jenispajak' => $jenispajak['s_namajenis']
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }

        $belumditetapkan = $this->Tools()->getService('PenetapanTable')->getBelumDitetapkan();
        $data = array(
            'belumditetapkan' => $belumditetapkan,
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagedefaultAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new PendataanFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            // $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            // $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));
            $data->t_tglpendataan = date('d-m-Y');
            if ($datapendataansebelumnya['t_berdasarmasa'] == 'No') {
                $t_berdasarmasa = 'Tidak Berdasar Masa';
            } else {
                $t_berdasarmasa = 'Berdasar Masa';
            }
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax(date('Y'));
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
//                var_dump($post);
//                die();
                if ($post['t_jenispajak'] == 2 && $post['t_berdasarmasa'] == 'Tidak Berdasar Masa') {
                    $sudahditetapkan = null;
                } else {
                    if (!empty($post['t_idtransaksi'])) {
                        $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]);
                    } else {
                        $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                        if ($sudahditetapkan['t_idkorek'] == 417) {
                            $sudahditetapkan = null;
                        } else {
                            $sudahditetapkan = $sudahditetapkan;
                        }
                    }
                }

//                var_dump($sudahditetapkan1); exit();
                if (empty($sudahditetapkan)) {
                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                    $this->Tools()->getService('PendataanTable')->simpanpendataanself($base, $session, $post);
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
                    $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $this->flashMessenger()->addMessage('<div class="alert alert-danger"><i class="glyph-icon icon-exclamation-triangle"></i> Data WP [' . $datapendaftaran['t_nama'] . '] untuk Bulan ' . $abulan[date('m', strtotime($base->t_masaawal))] . ' Tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah direkam!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                }
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            't_berdasarmasa' => $t_berdasarmasa
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagedefaulteditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new PendataanFrm();
        if ($req->isGet()) {
            $message = '';
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_tglpendataan = date('d-m-Y', strtotime($data->t_tglpendataan));
            $data->t_masaawal = date('d-m-Y', strtotime($data->t_masaawal));
            $data->t_masaakhir = date('d-m-Y', strtotime($data->t_masaakhir));
            $data->t_jmlhpajak = number_format($data->t_jmlhpajak, 0, ',', '.');
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');

            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            if ($datapendaftaran['t_jenisobjek'] == 2) {
                if ($datatransaksi['s_idkorek'] == 18) {
                    $t_berdasarmasa = 'Tidak Berdasar Masa';
                } else {
                    $t_berdasarmasa = 'Berdasar Masa';
                }
            } else {
                $t_berdasarmasa = null;
            }
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            't_berdasarmasa' => $t_berdasarmasa
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageppjAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $combo_tariflistrik = $this->Tools()->getService('DetailPpjTable')->getDataTarifListrik();
        $form = new \Pajak\Form\Pendataan\PendataanppjFrm($combo_tariflistrik);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            // $data->t_masaawal = date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month '));
            // $data->t_masaakhir = date('d-m-Y', strtotime(date('t-m-Y') . ' last day of previous month '));
            $data->t_tglpendataan = date('d-m-Y');
            if ($datapendataansebelumnya['t_berdasarmasa'] == 'No') {
                $t_berdasarmasa = 'Tidak Berdasar Masa';
            } else {
                $t_berdasarmasa = 'Berdasar Masa';
            }
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax(date('Y'));
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
//                 var_dump($post);exit();
                $base->exchangeArray($form->getData());
                // ppj bisa melakukan pendataan lebih dari 1 kali
                if ($post['t_jenispajak'] == 2 || $post['t_jenispajak'] == 5 || $post['t_berdasarmasa'] == 'Tidak Berdasar Masa') {
                    $sudahditetapkan = null;
                } else {
                    if (!empty($post['t_idtransaksi']))
                        $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]);
                    else
                        $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                }

                if (empty($sudahditetapkan)) {
                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                    $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanself($base, $session, $post);
                    $this->Tools()->getService('DetailPpjTable')->simpanpendataanppj($post, $dataparent);
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
                    $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $this->flashMessenger()->addMessage('<div class="alert alert-danger"><i class="glyph-icon icon-exclamation-triangle"></i> Data WP [' . $datapendaftaran['t_nama'] . '] untuk Bulan ' . $abulan[date('m', strtotime($base->t_masaawal))] . ' Tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah direkam!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                }
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            't_berdasarmasa' => $t_berdasarmasa,
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageppjeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $combo_tariflistrik = $this->Tools()->getService('DetailPpjTable')->getDataTarifListrik();
        $form = new \Pajak\Form\Pendataan\PendataanppjFrm($combo_tariflistrik);
        if ($req->isGet()) {
            $message = '';
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningPPJ();
            $recordsrekening = array();
            foreach ($rekening as $rekening) {
                $recordsrekening[] = $rekening;
            }
            $data->t_tglpendataan = date('d-m-Y', strtotime($data->t_tglpendataan));
            $data->t_masaawal = date('d-m-Y', strtotime($data->t_masaawal));
            $data->t_masaakhir = date('d-m-Y', strtotime($data->t_masaakhir));
            $data->t_jmlhpajak = number_format($data->t_jmlhpajak, 0, ',', '.');
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');

            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $detailppj = $this->Tools()->getService('DetailPpjTable')->getDetailByIdTransaksi($id);
            // $lampiranppj_a = $this->Tools()->getService('LampiranPpjTable')->getLampiranByIdTransaksi($id, '1');
            // $lampiranppj_b = $this->Tools()->getService('LampiranPpjTable')->getLampiranByIdTransaksi($id, '2');
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_iddetailppj = $detailppj->current()['t_iddetailppj'];
            $data->t_jmlhkwh = number_format($detailppj->current()['t_jmlhkwh'], 0, ',', '.');
            $data->t_golongantarif = $detailppj->current()['t_golongantarif'];
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'detailppj' => $detailppj,
            'rekeningppj' => $recordsrekening,
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagereklameAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboid_sudutpandang = $this->Tools()->getService('ReklameTable')->comboidSudutpandang();
        $comboid_zona = $this->Tools()->getService('ReklameTable')->comboidZona();
        $comboid_jenisreklame = $this->Tools()->getService('ReklameTable')->comboidJenisReklame();
        $comboid_jenislokasi = $this->Tools()->getService('ReklameTable')->comboidJenislokasi();
        $comboid_lebarjalan = $this->Tools()->getService('ReklameTable')->comboidLebarjalan();
        $comboid_ketinggian = $this->Tools()->getService('ReklameTable')->comboidKetinggian();
        $form = new \Pajak\Form\Pendataan\PendataanreklameFrm($comboid_jenisreklame, $comboid_sudutpandang, $comboid_zona, $comboid_jenislokasi, $comboid_lebarjalan, $comboid_ketinggian);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $ar_wilayah1 = $this->Tools()->getService('ReklameTable')->getDataZonaWIlayah(1);
            $ar_wilayah2 = $this->Tools()->getService('ReklameTable')->getDataZonaWIlayah(2);
            $ar_wilayah3 = $this->Tools()->getService('ReklameTable')->getDataZonaWIlayah(3);
            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_tglpendataan = date('d-m-Y');
            $data->t_masaawal = date('d-m-Y', strtotime(date('d-m-Y') . ' first day of previous month'));
            $data->t_masaakhir = date('d-m-Y', strtotime(date('d-m-Y') . ' first day of previous month'));
            $data->t_tglpenetapan = date('d-m-Y', strtotime(date('d-m-Y')));
            $message = '';
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax(date('Y'));
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $basedetailreklame = new \Pajak\Model\Pendataan\DetailreklameBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
//            var_dump($post);
//            die();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $basedetailreklame->exchangeArray($form->getData());
                $data = $this->Tools()->getService('PendataanTable')->simpanpendataanofficial($base, $session, $post);
                $this->Tools()->getService('DetailreklameTable')->simpanpendataanreklame($basedetailreklame, $data);

                $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                // $this->flashMessenger()->addMessage('Pendataan Pajak Telah Tersimpan!');
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'data_wilayah1' => $ar_wilayah1,
            'data_wilayah2' => $ar_wilayah2,
            'data_wilayah3' => $ar_wilayah3,
            'message' => $message
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagereklameeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $message = '';
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_tglpendataan = date('d-m-Y', strtotime($data->t_tglpendataan));
            $data->t_masaawal = date('d-m-Y', strtotime($data->t_masaawal));
            $data->t_masaakhir = date('d-m-Y', strtotime($data->t_masaakhir));
            $data->t_tglpenetapan = date('d-m-Y', strtotime($data->t_tglpenetapan));
            $data->t_jmlhpajak = number_format($data->t_jmlhpajak, 0, ',', '.');
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');

            $datatransaksi = $this->Tools()->getService('PendataanTable')->getPendataanReklameByIdTransaksi($id);
            $data->t_iddetailreklame = $datatransaksi['t_iddetailreklame'];
            $data->t_jenisreklame = $datatransaksi['t_jenisreklame'];
            $data->t_dasarperhitungan = $datatransaksi['t_dasarperhitungan'];
            $data->t_naskah = $datatransaksi['t_naskah'];
            $data->t_lokasi = $datatransaksi['t_lokasi'];
            $data->t_panjang = $datatransaksi['t_panjang'];
            $data->t_lebar = $datatransaksi['t_lebar'];
            $data->t_luas = number_format($datatransaksi['t_panjang'] * $datatransaksi['t_lebar'], 2, '.', '.');
            $data->t_tinggi = $datatransaksi['t_tinggi'];
            $data->t_jumlah = $datatransaksi['t_jumlah'];
            $data->t_jangkawaktu = $datatransaksi['t_jangkawaktu'];
            $data->t_tipewaktu = $datatransaksi['t_tipewaktu'];
            $data->t_tarifreklame = $datatransaksi['t_tarifreklame'];
            $data->t_kelasjalan = $datatransaksi['t_kelasjalan'];
            $data->t_jenisselebaran = $datatransaksi['t_jenisselebaran'];
            $data->t_jeniskendaraan = $datatransaksi['t_jeniskendaraan'];
            $data->t_jenissuara = $datatransaksi['t_jenissuara'];
            $data->t_jenisfilm = $datatransaksi['t_jenisfilm'];
            $data->t_jenisperagaan = $datatransaksi['t_jenisperagaan'];
            $data->t_nsl = number_format($datatransaksi['t_nsl'], 0, ',', '.');
            $data->t_nsr = number_format($datatransaksi['t_nsr'], 0, ',', '.');
            $data->t_sudutpandang = $datatransaksi['t_sudutpandang'];
            $data->t_pemasangan = $datatransaksi['t_pemasangan'];
            $data->t_konstruksi = $datatransaksi['t_konstruksi'];
            $data->t_bentukreklame = $datatransaksi['t_bentukreklame'];
            $data->t_jenislokasi = $datatransaksi['t_jenislokasi'];
            $data->t_lebarjalan = $datatransaksi['t_lebarjalan'];
            $data->t_ketinggian = $datatransaksi['t_ketinggian'];

            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = strtoupper($datatransaksi['s_namakorek']);
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
//            var_dump($data);
//            die();
            $comboid_sudutpandang = $this->Tools()->getService('ReklameTable')->comboidSudutpandang();
            $comboid_zona = $this->Tools()->getService('ReklameTable')->comboidZona();
            $comboid_jenisreklame = $this->Tools()->getService('ReklameTable')->comboidJenisReklame();
            $comboid_jenislokasi = $this->Tools()->getService('ReklameTable')->comboidJenislokasi();
            $comboid_lebarjalan = $this->Tools()->getService('ReklameTable')->comboidLebarjalan();
            $comboid_ketinggian = $this->Tools()->getService('ReklameTable')->comboidKetinggian();
            $form = new \Pajak\Form\Pendataan\PendataanreklameFrm($comboid_jenisreklame, $comboid_sudutpandang, $comboid_zona, $comboid_jenislokasi, $comboid_lebarjalan, $comboid_ketinggian);
            $form->bind($data);
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            't_jenisreklame' => $data->t_jenisreklame
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageparkirAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanparkirFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningParkir();
            $recordsrekening = array();
            foreach ($rekening as $rekening) {
                $recordsrekening[] = $rekening;
            }
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_tglpendataan = date('d-m-Y');
            // $data->t_masaawal = date('d-m-Y', strtotime(date('d-m-Y') . ' first day of previous month'));
            // $data->t_masaakhir = date('d-m-Y', strtotime(date('d-m-Y') . ' last day of previous month'));
//            $data->t_masaawal = date('01-m-Y');
//            $data->t_masaakhir = date('01-m-Y');
            $message = '';
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax(date('Y'));
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());

                if (!empty($post['t_idtransaksi'])) {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]);
                } else {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                }

                if (empty($sudahditetapkan)) {
                    $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanself($base, $session, $post);
                    $this->Tools()->getService('DetailparkirTable')->simpanpendataanparkir($post, $dataparent);

                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
                    $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $this->flashMessenger()->addMessage('<div class="alert alert-danger"><i class="glyph-icon icon-exclamation-triangle"></i> Data WP [' . $datapendaftaran['t_nama'] . '] untuk Bulan ' . $abulan[date('m', strtotime($base->t_masaawal))] . ' Tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                }
            }
        }

        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'ar_ttd0' => $recordspejabat,
            'message' => $message,
            'rekeningparkir' => $recordsrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageparkireditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanparkirFrm();
        $datadetail = null;
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailparkirTable')->getPendataanParkirByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailparkirTable')->getDetailParkirByIdTransaksi($id);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningParkir();
        $recordsrekening = array();
        foreach ($rekening as $rekening) {
            $recordsrekening[] = $rekening;
        }

        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'rekeningparkir' => $recordsrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageminerbaAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanminerbaFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningMineral();
            $recordsrekening = array();
            foreach ($rekening as $rekening) {
                $recordsrekening[] = $rekening;
            }
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_tglpendataan = date('d-m-Y');
//             $data->t_masaawal = date('d-m-Y', strtotime(date('d-m-Y') . ' first day of previous month'));
//             $data->t_masaakhir = date('d-m-Y', strtotime(date('d-m-Y') . ' last day of previous month'));
//            $data->t_masaawal = date('01-m-Y');
//            $data->t_masaakhir = date('01-m-Y');
            $message = '';
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax(date('Y'));
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                if (!empty($post['t_idtransaksi'])) {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]);
                } else {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                    if ($sudahditetapkan['t_idkorek'] == $base->t_idkorek) {
                        $sudahditetapkan = $sudahditetapkan;
                    } else {
                        $sudahditetapkan = null;
                    }
                }
//                 $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
//                     var_dump($sudahditetapkan); exit();
                if (empty($sudahditetapkan)) {
                    $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanself($base, $session, $post);
//                     var_dump($dataparent->t_idtransaksi); exit();
//                    $this->Tools()->getService('DetailminerbaTable')->simpanpendataanminerba($post, $dataparent);
                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
//                     $data = $this->Tools()->getService('PendaftaranTable')->getPendaftaranId($id);
//                     $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
//                     $data->t_tglpendataan = date('d-m-Y');
//                     $data->t_idwp = $datapendaftaran['t_idwp'];
//                     $message = 'Data WP untuk bulan ' . date('m', strtotime($base->t_masaawal)) . ' tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan';
//                     $form->bind($data);
                    $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $this->flashMessenger()->addMessage('<div class="alert alert-danger"><i class="glyph-icon icon-exclamation-triangle"></i> Data WP [' . $datapendaftaran['t_nama'] . '] untuk Bulan ' . $abulan[date('m', strtotime($base->t_masaawal))] . ' Tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                }
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'rekeningmineral' => $recordsrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageminerbaeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanminerbaFrm();
        $datadetail = null;
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailminerbaTable')->getPendataanMinerbaByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_volume = number_format($datatransaksi['t_volume'], 0, ',', '.');
            $data->t_dasarpengenaan = number_format($datatransaksi['t_dasarpengenaan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailminerbaTable')->getDetailMinerbaByIdTransaksi($id);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $rekening = $this->Tools()->getService('RekeningTable')->getdataRekeningMineral();
        $recordsrekening = array();
        foreach ($rekening as $rekening) {
            $recordsrekening[] = $rekening;
        }

        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'rekeningmineral' => $recordsrekening
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageairAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $cmbZona = $this->Tools()->getService('AirTable')->getcomboZona();
        $cmbKelompok = $this->Tools()->getService('AirTable')->getcomboKelompok();
        $form = new \Pajak\Form\Pendataan\PendataanairFrm($cmbZona, $cmbKelompok);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnyaABT($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $data->t_tglpendataan = date('d-m-Y');
            // $data->t_masaawal = (!empty($datapendataansebelumnya['t_masaawal'])) ? date('d-m-Y', strtotime($datapendataansebelumnya['t_masaawal'] . ' first day of next month ')) : date('d-m-Y', strtotime(date('01-m-Y') . ' first day of previous month'));
            // $data->t_masaakhir = (!empty($datapendataansebelumnya['t_masaakhir'])) ? date('d-m-Y', strtotime($datapendataansebelumnya['t_masaakhir'] . ' last day of next month ')) : date('d-m-Y', strtotime(date('t-m-Y') . 'last day of previous month'));
            $data->t_tglpenetapan = date('d-m-Y');
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax(date('Y'));
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $basedetailair = new \Pajak\Model\Pendataan\DetailairBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $basedetailair->exchangeArray($form->getData());

                // $sudahditetapkan = (!empty($post['t_idtraksaksi'])) ? $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]) : $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                if (!empty($post['t_idtransaksi'])) {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]);
                } else {
                    $sudahditetapkan = $this->Tools()->getService('PendataanTable')->getPendataanMasapajakAwal($base);
                }

                if (empty($sudahditetapkan)) {
                    $data = $this->Tools()->getService('PendataanTable')->simpanpendataanofficial($base, $session, $post);
                    $this->Tools()->getService('DetailairTable')->simpanpendataanair($basedetailair, $data);

                    $this->flashMessenger()->addMessage('<div class="alert alert-success"><i class="glyph-icon icon-check-circle"></i> Pendataan Pajak Telah Tersimpan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
//                    $data = $this->Tools()->getService('PendaftaranTable')->getPendaftaranId($id);
//                    $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
//                    $data->t_tglpendataan = date('d-m-Y');
//                    $data->t_idwp = $datapendaftaran['t_idwp'];
//                    $message = 'Data WP untuk bulan ' . date('m', strtotime($base->t_masaawal)) . ' tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan';
//                    $form->bind($data);
                    $abulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
                    $this->flashMessenger()->addMessage('<div class="alert alert-danger"><i class="glyph-icon icon-exclamation-triangle"></i> Data WP [' . $datapendaftaran['t_nama'] . '] untuk Bulan ' . $abulan[date('m', strtotime($base->t_masaawal))] . ' Tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan!</div>');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                }
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPageaireditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $cmbZona = $this->Tools()->getService('AirTable')->getcomboZona();
        $cmbKelompok = $this->Tools()->getService('AirTable')->getcomboKelompok();
        $form = new \Pajak\Form\Pendataan\PendataanairFrm($cmbZona, $cmbKelompok);
        $datadetail = null;
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailairTable')->getPendataanairByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_tglpenetapan = date('d-m-Y', strtotime($datatransaksi['t_tglpenetapan']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $data->t_hargadasarair = number_format($datatransaksi['s_tarifdasarkorek'], 0, ',', '.');
            $datadetail = $this->Tools()->getService('DetailairTable')->getDetailAirByIdTransaksi($id);
            // var_dump($datadetail); exit();
            $data->t_idair = $datadetail['t_idair'];
            $data->t_zona = $datadetail['t_zona'];
            $data->t_kodekelompok = $datadetail['t_kodekelompok'];
            $data->t_kodejenis = $datadetail['t_kodejenis'];
            $data->t_volumem3 = number_format($datadetail['t_volume'], 0, ',', '.');
            $data->t_volume = $datadetail['t_volume'];
            $data->t_fna = $datadetail['t_fna'];
            $data->t_perhitungan = $datadetail['t_perhitungan'];
            $data->t_totalnpa = number_format($datadetail['t_totalnpa'], 0, ',', '.');
            $data->t_tarifdasarkorek = $datadetail['t_tarifdasarkorek'];
            $data->t_volumeair0 = number_format($datadetail['t_volumeair0'], 0, ',', '.');
            $data->t_hargadasar0 = number_format($datadetail['t_hargadasar0'], 0, ',', '.');
            $data->t_tarif0 = $datadetail['t_tarif0'];
            $data->t_jumlah0 = number_format($datadetail['t_jumlah0'], 0, ',', '.');
            $data->t_volumeair1 = number_format($datadetail['t_volumeair1'], 0, ',', '.');
            $data->t_hargadasar1 = number_format($datadetail['t_hargadasar1'], 0, ',', '.');
            $data->t_tarif1 = $datadetail['t_tarif1'];
            $data->t_jumlah1 = number_format($datadetail['t_jumlah1'], 0, ',', '.');
            $data->t_volumeair2 = number_format($datadetail['t_volumeair2'], 0, ',', '.');
            $data->t_hargadasar2 = number_format($datadetail['t_hargadasar2'], 0, ',', '.');
            $data->t_tarif2 = $datadetail['t_tarif2'];
            $data->t_jumlah2 = number_format($datadetail['t_jumlah2'], 0, ',', '.');
            $data->t_volumeair3 = number_format($datadetail['t_volumeair3'], 0, ',', '.');
            $data->t_hargadasar3 = number_format($datadetail['t_hargadasar3'], 0, ',', '.');
            $data->t_tarif3 = $datadetail['t_tarif3'];
            $data->t_jumlah3 = number_format($datadetail['t_jumlah3'], 0, ',', '.');
            $data->t_volumeair4 = number_format($datadetail['t_volumeair4'], 0, ',', '.');
            $data->t_hargadasar4 = number_format($datadetail['t_hargadasar4'], 0, ',', '.');
            $data->t_tarif4 = $datadetail['t_tarif4'];
            $data->t_jumlah4 = number_format($datadetail['t_jumlah4'], 0, ',', '.');
            $data->t_volumeair5 = number_format($datadetail['t_volumeair5'], 0, ',', '.');
            $data->t_hargadasar5 = number_format($datadetail['t_hargadasar5'], 0, ',', '.');
            $data->t_tarif5 = $datadetail['t_tarif5'];
            $data->t_jumlah5 = number_format($datadetail['t_jumlah5'], 0, ',', '.');
            $data->t_perhitungan = $datadetail['t_perhitungan'];
            $data->t_jenissumur = $datadetail['t_jenissumur'];
            $data->t_sumurke = $datadetail['t_sumurke'];
            $data->t_lokasisumur = $datadetail['t_lokasisumur'];
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }

        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagewaletAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanwaletFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $data->t_tarifdasarkorek = number_format($datapendataansebelumnya['s_tarifdasarkorek'], 0, ',', '.');
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax(date('Y'));
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $data->t_tglpendataan = date('d-m-Y');
            $data->t_masaawal = date('01-m-Y', strtotime(date('d-m-Y') . ' first day of previous month'));
            $data->t_masaakhir = date('t-m-Y', strtotime(date('d-m-Y') . ' last day of previous month'));
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $sudahditetapkan = (!empty($post['t_idtransaksi'])) ? $this->Tools()->getService('PendataanTable')->getPendataanSeMasa($base, $post["t_idtransaksi"]) : null;
                if (empty($sudahditetapkan)) {
                    $this->Tools()->getService('PendataanTable')->simpanpendataanwalet($base, $session, $post);

                    $this->flashMessenger()->addMessage('Pendataan Pajak Telah Tersimpan!');
                    return $this->redirect()->toRoute("pendataan", array(
                                "controllers" => "Pendataan",
                                "action" => "index",
                                "page" => $req->getPost()['t_jenisobjekpajak']
                    ));
                } else {
                    $id = $base->t_idobjek;
                    $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
                    $data = $this->Tools()->getService('PendaftaranTable')->getPendaftaranId($id);
                    $datapendaftaran['t_tgldaftar'] = date('d-m-Y', strtotime($datapendaftaran['t_tgldaftar']));
                    $data->t_tglpendataan = date('d-m-Y');
                    $data->t_idwp = $datapendaftaran['t_idwp'];
                    $message = 'Data WP untuk bulan ' . date('m', strtotime($base->t_masaawal)) . ' tahun ' . date('Y', strtotime($base->t_masaawal)) . ' sudah pernah ditetapkan';
                    $form->bind($data);
                }
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagewaleteditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanwaletFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_tarifdasarkorek = number_format($data->t_tarifdasarkorek, 0, ',', '.');
            $data->t_dasarpengenaan = number_format($data->t_dasarpengenaan, 0, ',', '.');
            $message = '';

            $datatransaksi = $this->Tools()->getService('PendataanTable')->getDataPendataanID($id);
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }

        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // RETRIBUSI

    /**
     * author : Roni Mustapa
     * email : ronimustapa@gmail.com
     * created : 09/04/2019
     */
    public function FormPagekekayaandaerahAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('KekayaandaerahTable')->getcomboIdKlasifikasi();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataankekayaandaerahFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailkekayaanTable')->simpandetailkekayaan($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagekebersihanAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('DetailkebersihanTable')->getcomboIdKlasifikasi();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataankebersihanFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                // var_dump($dataparent); exit();
                $this->Tools()->getService('DetailkebersihanTable')->simpandetailkebersihan($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepasarAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('DetailpasarTable')->getcomboIdKlasifikasi();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataanpasarFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                // var_dump($dataparent); exit();
                $this->Tools()->getService('DetailpasarTable')->simpandetailpasar($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepasargrosirAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('DetailpasargrosirTable')->getcomboIdKlasifikasi();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataanpasargrosirFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                // var_dump($dataparent); exit();
                $this->Tools()->getService('DetailpasargrosirTable')->simpandetailpasargrosir($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagetempatparkirAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboJeniskendaraan = $this->Tools()->getService('DetailtempatparkirTable')->getcomboIdJeniskendaraan();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataantempatparkirFrm($comboJeniskendaraan);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
            //jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                // var_dump($dataparent); exit();
                $this->Tools()->getService('DetailtempatparkirTable')->simpandetailtempatparkir($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    /*
      EDIT RETRIBUSI
     */

    public function FormPagekebersihaneditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('DetailkebersihanTable')->getcomboIdKlasifikasi();
        $form = new \Pajak\Form\Pendataan\PendataankebersihanFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailkebersihanTable')->getPendataanKebersihanByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrumahdinas = $datatransaksi['t_idkebersihan'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_klasifikasi = $datatransaksi['t_idklasifikasi'];
            $data->t_kategori = $datatransaksi['t_idtarif'];
            $data->t_jmlhbln = $datatransaksi['t_jmlhbln'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            // $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepasareditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('DetailpasarTable')->getcomboIdKlasifikasi();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataanpasarFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailpasarTable')->getPendataanPasarByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrumahdinas = $datatransaksi['t_idpasar'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_klasifikasi = $datatransaksi['t_idklasifikasi'];
            $data->t_jenisbangunan = $datatransaksi['t_jenisbangunan'];
            $data->t_panjang = $datatransaksi['t_panjang'];
            $data->t_lebar = $datatransaksi['t_lebar'];
            $data->t_luas = $datatransaksi['t_luas'];
            $data->t_jmlhbln = $datatransaksi['t_jmlhbln'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            // $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagekekayaandaeraheditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('KekayaandaerahTable')->getcomboIdKlasifikasi();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataankekayaandaerahFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailkekayaanTable')->getPendataanKekayaanByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrumahdinas = $datatransaksi['t_idpasar'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_klasifikasi = $datatransaksi['t_idklasifikasi'];
            $data->t_luastanah = $datatransaksi['t_luastanah'];
            $data->t_luasbangunan = $datatransaksi['t_luasbangunan'];
            $data->t_jmlhbln = $datatransaksi['t_jmlhbln'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            // $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepasargrosireditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboKlasifikasi = $this->Tools()->getService('DetailpasargrosirTable')->getcomboIdKlasifikasi();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataanpasargrosirFrm($comboKlasifikasi);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailpasargrosirTable')->getPendataanPasargrosirByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrumahdinas = $datatransaksi['t_idpasar'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_klasifikasi = $datatransaksi['t_idklasifikasi'];
            $data->t_jenisbangunan = $datatransaksi['t_jenisbangunan'];
            $data->t_panjang = $datatransaksi['t_panjang'];
            $data->t_lebar = $datatransaksi['t_lebar'];
            $data->t_luas = $datatransaksi['t_luas'];
            $data->t_jmlhbln = $datatransaksi['t_jmlhbln'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            // $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagetempatparkireditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $comboJeniskendaraan = $this->Tools()->getService('DetailtempatparkirTable')->getcomboIdJeniskendaraan();
        // var_dump($comboKlasifikasi); exit();
        $form = new \Pajak\Form\Pendataan\PendataantempatparkirFrm($comboJeniskendaraan);
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailtempatparkirTable')->getPendataanTempatparkirByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrumahdinas = $datatransaksi['t_idpasar'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_jeniskendaraan = $datatransaksi['t_jeniskendaraan'];
            $data->t_jmlhkendaraan = $datatransaksi['t_jumlah'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            // $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagerumahdinasAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanrumahdinasFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailrumahdinasTable')->simpandetailrumahdinas($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagerumahdinaseditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanrumahdinasFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailrumahdinasTable')->getPendataanRumahDinasByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrumahdinas = $datatransaksi['t_idrumahdinas'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_panjang = $datatransaksi['t_panjang'];
            $data->t_lebar = $datatransaksi['t_lebar'];
            $data->t_luas = $datatransaksi['t_luas'];
            $data->t_jmlhbln = $datatransaksi['t_jmlhbln'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepanggungreklameAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanpanggungreklameFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailpanggungreklameTable')->simpandetailpanggungreklame($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagepanggungreklameeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataanpanggungreklameFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailpanggungreklameTable')->getPendataanPanggungReklameByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrpangrek = $datatransaksi['t_idrpangrek'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_panjang = $datatransaksi['t_panjang'];
            $data->t_lebar = $datatransaksi['t_lebar'];
            $data->t_luas = $datatransaksi['t_luas'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            $data->t_jmlhbln = $datatransaksi['t_jmlhbln'];
            $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagetanahreklameAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataantanahreklameFrm($this->Tools()->getService('DetailtanahreklameTable')->getcomboIdTanahReklame());
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailtanahreklameTable')->simpandetailtanahreklame($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagetanahreklameeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataantanahreklameFrm($this->Tools()->getService('DetailtanahreklameTable')->getcomboIdTanahReklame());
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailtanahreklameTable')->getPendataanTanahReklameByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrpangrek = $datatransaksi['t_idrpangrek'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_panjang = $datatransaksi['t_panjang'];
            $data->t_lebar = $datatransaksi['t_lebar'];
            $data->t_luas = $datatransaksi['t_luas'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            $data->t_jmlhblnhari = $datatransaksi['t_jmlhblnhari'];
            $data->t_lokasitanah = $datatransaksi['t_lokasitanah'];
            $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function CariTarifpipaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        // $t_volume = $data_get['t_tarifdasarkorek'] * $data_get['t_jmlhsumur'];
        $data_tarifpipa = $this->Tools()->getService('AirTable')->getdataTarifpipaId($data_get['t_kodekelompok']);

        if ($data_get['t_idkorek'] == 103) {
            $t_volume = $data_tarifpipa['s_pipa1'] * $data_get['t_jmlhsumur'];
        } elseif ($data_get['t_idkorek'] == 104) {
            $t_volume = $data_tarifpipa['s_pipa2'] * $data_get['t_jmlhsumur'];
        } elseif ($data_get['t_idkorek'] == 105) {
            $t_volume = $data_tarifpipa['s_pipa3'] * $data_get['t_jmlhsumur'];
        } elseif ($data_get['t_idkorek'] == 106) {
            $t_volume = $data_tarifpipa['s_pipa4'] * $data_get['t_jmlhsumur'];
        } elseif ($data_get['t_idkorek'] == 107) {
            $t_volume = $data_tarifpipa['s_pipa5'] * $data_get['t_jmlhsumur'];
        } elseif ($data_get['t_idkorek'] == 108) {
            $t_volume = $data_tarifpipa['s_pipa6'] * $data_get['t_jmlhsumur'];
        } elseif ($data_get['t_idkorek'] == 109) {
            $t_volume = $data_tarifpipa['s_pipa7'] * $data_get['t_jmlhsumur'];
        }

        $data = array(
            't_volume' => $t_volume,
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function CariKodeJenisAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data_kodejenis = $this->Tools()->getService('AirTable')->getdataKodeJenisId($data_get['t_kodekelompok']);
        $data_jenis .= '<option value="">-- Pilih Jenis --</option>';
        foreach ($data_kodejenis as $row):
            $data_jenis .= '<option value="' . $row['s_nourut'] . '">' . $row['s_nourut'] . ' [ ' . $row['s_deskripsi'] . ' ]</option>';
        endforeach;
        $dataToRender = [
            't_kodejenis' => $data_jenis,
        ];
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($dataToRender));
    }

    public function caritariftanahreklameAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('DetailtanahreklameTable')->getDataId($data_get['t_lokasitanah']);
        $data_render = array(
            "t_tarifdasar" => number_format($data['s_tarif'], 0, ",", "."),
            "s_satuan" => $data['s_satuan']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function FormPagetanahlainAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataantanahlainFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailtanahlainTable')->simpandetailtanahlain($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagetanahlaineditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataantanahlainFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailtanahlainTable')->getPendataanTanahLainByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_idrpangrek = $datatransaksi['t_idrpangrek'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_tarifpajak = $datatransaksi['s_persentarifkorek'];
            $data->t_panjang = $datatransaksi['t_panjang'];
            $data->t_lebar = $datatransaksi['t_lebar'];
            $data->t_luas = $datatransaksi['t_luas'];
            $data->t_tarifdasar = number_format($datatransaksi['t_tarifdasar'], 0, ',', '.');
            $data->t_jmlhbln = $datatransaksi['t_jmlhbln'];
            $data->t_potongan = number_format($datatransaksi['t_potongan'], 0, ',', '.');
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagegedungAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataangedungFrm();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idobjek');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdObjek($id);
            $tarifgedung = $this->Tools()->getService('DetailgedungTable')->gettarifgedung();
            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';
//jika pernah melakukan pendataan sebelumnya
            $datapendataansebelumnya = $this->Tools()->getService('PendataanTable')->getPendataanSebelumnya($id);
            $data->t_idkorek = $datapendataansebelumnya['s_idkorek'];
            $data->t_korek = $datapendataansebelumnya['korek'];
            $data->t_namakorek = $datapendataansebelumnya['s_namakorek'];
            $data->t_tarifpajak = $datapendataansebelumnya['s_persentarifkorek'];
            $datapendataan = $this->Tools()->getService('PendataanTable')->getPendataanMax($id);
            $data->t_nourut = (int) $datapendataan['t_nourut'] + 1;
            $form->bind($data);
        }
        if ($this->getRequest()->isPost()) {
            $base = new PendataanBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());
                $dataparent = $this->Tools()->getService('PendataanTable')->simpanpendataanretribusi($base, $session, $post);
                $this->Tools()->getService('DetailgedungTable')->simpandetailgedung($post, $dataparent);
                return $this->redirect()->toRoute("pendataan", array(
                            "controllers" => "Pendataan",
                            "action" => "index",
                            "page" => $req->getPost()['t_jenisobjekpajak']
                ));
            }
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }

        $recordsgedung = array();
        foreach ($tarifgedung as $tarifgedung) {
            $recordsgedung[] = $tarifgedung;
        }
        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'tarifgedung' => $recordsgedung
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormPagegedungeditAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Pendataan\PendataangedungFrm();
        $datadetail = null;
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idtransaksi');
            $datapendaftaran = $this->Tools()->getService('PendaftaranTable')->getPendaftaranbyIdTransaksi($id);
//            $data = $this->Tools()->getService('ObjekTable')->getDataObjekById($id);
            $data = $this->Tools()->getService('PendataanTable')->getPendataanId($id);
            $data->t_idobjek = $datapendaftaran['t_idobjek'];
            $data->t_jenisobjek = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $data->t_jenispajak = $datapendaftaran['t_jenisobjek']; // jangan diubah
            $message = '';

            $datatransaksi = $this->Tools()->getService('DetailgedungTable')->getPendataanGedungByIdTransaksi($id);
            $data->t_idtransaksi = $datatransaksi['t_idtransaksi'];
            $data->t_nourut = $datatransaksi['t_nourut'];
            $data->t_periodepajak = $datatransaksi['t_periodepajak'];
            $data->t_tglpendataan = date('d-m-Y', strtotime($datatransaksi['t_tglpendataan']));
            $data->t_masaawal = date('d-m-Y', strtotime($datatransaksi['t_masaawal']));
            $data->t_masaakhir = date('d-m-Y', strtotime($datatransaksi['t_masaakhir']));
            $data->t_idkorek = $datatransaksi['s_idkorek'];
            $data->t_korek = $datatransaksi['korek'];
            $data->t_namakorek = $datatransaksi['s_namakorek'];
            $data->t_jmlhpajak = number_format($datatransaksi['t_jmlhpajak'], 0, ',', '.');
            $form->bind($data);
            $datadetail = $this->Tools()->getService('DetailgedungTable')->getDetailGedungByIdTransaksi($id);
        }
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $tarifgedung = $this->Tools()->getService('DetailgedungTable')->gettarifgedung();
        $recordsgedung = array();
        foreach ($tarifgedung as $tarifgedung) {
            $recordsgedung[] = $tarifgedung;
        }

        $view = new ViewModel(array(
            'form' => $form,
            'datapendaftaran' => $datapendaftaran,
            'message' => $message,
            'ar_ttd0' => $recordspejabat,
            'datadetail' => $datadetail,
            'tarifgedung' => $recordsgedung
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function caritarifgedungAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('DetailgedungTable')->gettarifgedungId($data_get['t_idtarifgedung']);
        $data_render = array(
            "t_tarif" => number_format($data['s_tarif'], 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretgedungAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_tarif = str_ireplace(".", "", $data_get['t_tarif']);
        $t_total = $t_tarif * $data_get['t_jumlah'];
        $data_render = array(
            "t_total" => number_format($t_total, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungtotalretgedungAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jmlhpajak = str_ireplace(".", "", $data_get['t_total1']) + str_ireplace(".", "", $data_get['t_total2']) + str_ireplace(".", "", $data_get['t_total3']) +
                str_ireplace(".", "", $data_get['t_total4']) + str_ireplace(".", "", $data_get['t_total5']) + str_ireplace(".", "", $data_get['t_total6']) +
                str_ireplace(".", "", $data_get['t_total7']);
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hapusAction() {
        /** Hapus Pendataan
         * @param int $s_idkorek
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $this->Tools()->getService('PendataanTable')->hapusPendataan($this->params('s_idjenis'));
        return $this->getResponse();
    }

    public function comboKelurahanCamatAction() {
        $frm = new \Pajak\Form\Pendaftaran\PendaftaranFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Pajak\Model\Pendaftaran\PendaftaranBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->Tools()->getService('PendaftaranTable')->getByKecamatan($ex);
                $opsi = "";
                $opsi .= "<option value=''>Silahkan Pilih</option>";
                foreach ($data as $r) {
                    $opsi .= "<option value='" . $r['s_kodekel'] . "'>" . str_pad($r['s_kodekel'], 2, "0", STR_PAD_LEFT) . " || " . $r['s_namakel'] . "</option>";
                    // $opsi .= "<option value='" . $r['s_idkel'] . "'>" . str_pad($r['s_kodekel'], 2, "0", STR_PAD_LEFT) . " || " . $r['s_namakel'] . "</option>";
                }
                $res->setContent($opsi);
            }
        }
        return $res;
    }

    public function cetakkodebayarAction() {
        /** Cetak Kode Bayar
         * @param int $t_idwp
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 02/01/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
// Mengambil Data WP
        $data = $this->Tools()->getService('PembayaranTable')->getDataPembayaranID($data_get['t_idtransaksi']);

// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'data' => $data,
            'ar_pemda' => $ar_pemda,
        ));
        $pdf->setOption("paperSize", "potrait");
        return $pdf;
    }

    public function cetakpendataanAction() {
        /** Cetak Pendataan
         * @param string $tglcetak Tanggal Mencetak Dokumen
         * @param date('d-m-Y') $tglpendataan0 Tanggal Minimal Pendataan
         * @param date('d-m-Y') $tglpendataan1 Tanggal Maximal Pendataan
         * @param int $t_kecamatan 
         * @param int $t_kelurahan  
         * @param int $t_idkorek
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $data = $this->Tools()->getService('PendataanTable')->getDataPendataan($data_get->tglpendataan0, $data_get->tglpendataan1, $data_get->t_kecamatan, $data_get->t_kelurahan, $data_get->t_idkorek);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'tglpendataan0' => $data_get->tglpendataan0,
            'tglpendataan1' => $data_get->tglpendataan1,
            'tglcetak' => $data_get->tglcetak,
            'ar_pemda' => $ar_pemda
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }

    public function dataPendataanAction() {
        /** Mendapatkan Data Pendataan
         * @param int $page
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 05/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
// Mengambil Data WP, OP dan Transaksi
        $data = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
        $data_render = array(
            "idtransaksi" => $data['t_idtransaksi'],
            "namawp" => strtoupper($data['t_nama']),
            "alamatwp" => strtoupper($data['t_alamat_lengkapwp']),
            "namaop" => strtoupper($data['t_namaobjek']),
            "alamatop" => strtoupper($data['t_alamatlengkapobjek']),
            "tglpendataan" => date('d-m-Y', strtotime($data['t_tglpendataan'])),
            "tglketetapan" => (!empty($data['t_tglpenetapan'])) ? date('d-m-Y', strtotime($data['t_tglpenetapan'])) : '',
            "jenispajak" => strtoupper($data['s_namajenis']),
            "idjenispajak" => $data['t_jenisobjek'],
            "t_idobjek" => $data['t_idobjek']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataWPObjekAction() {
        /** Mendapatkan Data WP Objek
         * @param int $page
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 20/06/2019
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        // Mengambil Data WP, OP dan Transaksi
        $data = $this->Tools()->getService('PendataanTable')->getDataWPObjek($data_get['idobjek']);
        $data_render = array(
            "idobjek" => $data['t_idobjek'],
            "namawp" => $data['t_nama'],
            "alamatwp" => $data['t_alamat_lengkapwp'],
            "namaop" => $data['t_namaobjek'],
            "alamatop" => $data['t_alamatlengkapobjek'],
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cetakkartudataAction() {
        /** Cetak Kartu Data
         * @param int $idobjek
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 20/06/2019
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data Jenis Pajak
        $datajenispajak = $this->Tools()->getService('PendaftaranTable')->getDataJenisPajak($data_get['idobjek']);
        // Mengambil Data WP
        $data = $this->Tools()->getService('PendaftaranTable')->getPendaftaranIDObjek($data_get['idobjek']);
        // Mengambil Data Penetapan dan Pembayaran
        $dataarr = array();
        for ($i = 1; $i <= 12; $i++) {
            $datatransaksi = $this->Tools()->getService('PendaftaranTable')->getTransaksi($i, $data_get['periode'], $data_get['idobjek']);
            if ($datatransaksi == false) {
                $datatransaksi = array(
                    "t_tglpendataan" => null,
                    "t_jmlhpajak" => null,
                    "t_tglpembayaran" => null,
                    "t_jmlhpembayaran" => null
                );
            } else {
                $datatransaksi = $datatransaksi;
            }
            $dataarr[] = array_merge($datatransaksi);
        }
        $datatransaksireklame = $this->Tools()->getService('PendaftaranTable')->getTransaksireklame($data_get['periode'], $data_get['idobjek']);
        $datatransaksicatering = $this->Tools()->getService('PendaftaranTable')->getTransaksicatering($data_get['periode'], $data_get['idobjek']);
        $datatransaksiminerba = $this->Tools()->getService('PendaftaranTable')->getTransaksiminerba($data_get['periode'], $data_get['idobjek']);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_mengetahui = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttdmengetahui']);
        $ar_diperiksa = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttddiperiksa']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'datatransaksi' => $dataarr,
            'datatransaksireklame' => $datatransaksireklame,
            'datatransaksicatering' => $datatransaksicatering,
            'datatransaksiminerba' => $datatransaksiminerba,
            'ar_pemda' => $ar_pemda,
            'ar_mengetahui' => $ar_mengetahui,
            'ar_diperiksa' => $ar_diperiksa,
            "datajenispajak" => $datajenispajak,
            'periodepajak' => $data_get['periode']
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetaksptpdhotelAction() {
        /** Cetak SPTPD Hotel
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningByJenis(1);

        // var_dump($rekening);
        // exit();
// Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
// Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = (!empty($data_get['ttd0'])) ? $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']) : '';


        // $view = new ViewModel(array(
        // 'data' => $datasekarang,
        // 'datasebelumnya' => $datasebelumnya,
        // 'ar_pemda' => $ar_pemda,
        // 'ar_ttd' => $ar_ttd,
        // 'rekening' => $rekening,
        // ));
        // return $view;

        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "Letter");
        return $pdf;
    }

    public function cetaksptpdrestoranAction() {
        /** Cetak SPTPD Restoran
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningByJenis(2);
// Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
// Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = (!empty($data_get['ttd0'])) ? $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']) : '';
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "Letter");
        return $pdf;
    }

    public function cetaksptpdhiburanAction() {
        /** Cetak SPTPD Hiburan
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningSubByJenis(3);
// Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
// Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = (!empty($data_get['ttd0'])) ? $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']) : '';
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "Letter");
        return $pdf;
    }

    public function cetaksptpdreklameAction() {
        /** Cetak SPTPD Reklame
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
// Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
        $datareklame = $this->Tools()->getService('DetailreklameTable')->getDetailReklameByIdTransaksi($data_get['idtransaksi']);
// Mengambil Data Sebelumnya
// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = (!empty($data_get['ttd0'])) ? $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']) : '';
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datareklame' => $datareklame,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
        ));
        $pdf->setOption("paperSize", "legal");
        return $pdf;
    }

    public function cetaksptpdppjAction() {
        /** Cetak SPTPD PPJ
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningSubByJenis(5);
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
        $detailppjsekarang = $this->Tools()->getService('DetailPpjTable')->getPendataanByIdTransaksi($data_get['idtransaksi']);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
        $detailppjsebelumnya = $this->Tools()->getService('DetailPpjTable')->getPendataanByIdTransaksi($datasebelumnya['t_idtransaksi']);
        // Mengambil data Detail Minerba
        // $datadetailppj = $this->Tools()->getService('PendataanTable')->getDataDetailPPJ($data_get['idtransaksi']);
        // $t_tarifdasarkorek = $datadetailppj->current();
        // Mengambil Data Pemda
        $petugas_penerima = $this->Tools()->getService('PejabatTable')->getPejabatId($datasekarang['t_operatorpendataan']);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = (!empty($data_get['ttd0'])) ? $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']) : '';
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'detailppj' => $detailppjsekarang,
            'detailppjsebelum' => $detailppjsebelumnya,
            // 'data_detailppj' => $datadetailppj,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'rekening' => $rekening,
            't_tarifdasarkorek' => $t_tarifdasarkorek['s_persentarifkorek'],
            'petugas_penerima' => $petugas_penerima
        ));
        $pdf->setOption("paperSize", "Letter");
        return $pdf;
    }

    public function cetaklampiranppjAction() {
        /** Cetak SPTPD PPJ
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['t_idtransaksi']);
        $detailppjsekarang = $this->Tools()->getService('DetailPpjTable')->getPendataanByIdTransaksi($data_get['t_idtransaksi']);
        $lampiranppj_a = $this->Tools()->getService('LampiranPpjTable')->getLampiranByIdTransaksi($data_get['t_idtransaksi'], 1);
        $lampiranppj_b = $this->Tools()->getService('LampiranPpjTable')->getLampiranByIdTransaksi($data_get['t_idtransaksi'], 2);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'detailppj' => $detailppjsekarang,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'lampiranppj_a' => $lampiranppj_a,
            'lampiranppj_b' => $lampiranppj_b,
        ));
        $pdf->setOption("paperSize", "legal");
        return $pdf;
    }

    public function cetaksptpdminerbaAction() {
        /** Cetak SPTPD Minerba
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
        // Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
        // Mengambil data Detail Minerba
        $datadetailminerba = $this->Tools()->getService('PendataanTable')->getDataDetailMinerba($data_get['idtransaksi']);

        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = (!empty($data_get['ttd0'])) ? $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']) : '';
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'dataminerba' => $datadetailminerba,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
        ));
        $pdf->setOption("paperSize", "Letter");
        return $pdf;
    }

    public function cetaksptpdparkirAction() {
        /** Cetak SPTPD Parkir
         * @param int $idtransaksi
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 31/01/2019
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningByJenis(7);
//// Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
//// Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
//// Mengambil Data Pemda
// Mengambil Data WP, OP dan Transaksi
        // $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
// Mengambil Data Sebelumnya
        // $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
// Mengambil data Detail Minerba
        // $datadetailparkir = $this->Tools()->getService('PendataanTable')->getDataDetailParkir($data_get['idtransaksi']);

        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = (!empty($data_get['ttd0'])) ? $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']) : '';
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            // 'dataparkir' => $datadetailparkir,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "Letter");
        return $pdf;
    }

    public function cetaksptpdabtAction() {
        /** Cetak SPTPD ABT
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
// Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
// Mengambil Data Detail Air
        $dataair = $this->Tools()->getService('DetailAirTable')->getDetailAirByIdTransaksi($data_get['idtransaksi']);
// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = (!empty($data_get['ttd0'])) ? $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']) : '';
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'dataair' => $dataair,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
        ));
        $pdf->setOption("paperSize", "legal");
        return $pdf;
    }

    public function cetaksptpdwaletAction() {
        /** Cetak SPTPD Walet
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 04/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $rekening = $this->Tools()->getService('RekeningTable')->getRekeningByJenis(9);
// Mengambil Data WP, OP dan Transaksi
        $datasekarang = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['idtransaksi']);
// Mengambil Data Sebelumnya
        $datasebelumnya = $this->Tools()->getService('PendataanTable')->getDataPendataanSebelumnya($datasekarang['t_jenisobjek'], $datasekarang['t_idwpobjek'], $datasekarang['t_masaawal']);
// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = (!empty($data_get['ttd0'])) ? $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']) : '';
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $datasekarang,
            'datasebelumnya' => $datasebelumnya,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
            'rekening' => $rekening,
        ));
        $pdf->setOption("paperSize", "Letter");
        return $pdf;
    }

    public function tentukanMasaAction() {
        /** tentukan Masa Pajak Akhir
         * @param int $t_masaawal
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 06/02/2019
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();

        $t_masaakhir = date('t', strtotime($data_get['t_masaawal'])) . '-' . date('m', strtotime($data_get['t_masaawal'])) . '-' . date('Y', strtotime($data_get['t_masaawal']));

        $data_render = array(
            "t_masaakhir" => $t_masaakhir
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungpajakAction() {
        /** Hitung Pajak Default
         * @param int $t_dasarpengenaan
         * @param int $t_tarifpajak
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 13/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        if ($data_get['t_jenispajak'] == 2) { // restoran
//            if ($data_get['t_idkorek'] == 21) { // katering
//                $datarek = $this->Tools()->getService('RekeningTable')->getdataRekeningId($data_get['t_idkorek']);
//                $t_dasarpengenaan = str_ireplace(".", "", $data_get['t_dasarpengenaan']);
//                $t_tarifpajak = $datarek['s_persentarifkorek'];
//                $t_jmlhpajak = ($t_dasarpengenaan * $t_tarifpajak / 100);
//            } else {
            $t_dasarpengenaan = str_ireplace(".", "", $data_get['t_dasarpengenaan']);
//            if ($t_dasarpengenaan < 3000000) {
//                $t_tarifpajak = 0;
//                $t_jmlhpajak = ($t_dasarpengenaan * $t_tarifpajak / 100);
//            } else {
//                $t_tarifpajak = 10;
            $t_tarifpajak = $data_get['t_tarifpajak'];
            $t_jmlhpajak = ($t_dasarpengenaan * $t_tarifpajak / 100);
//            }
//            }
            $t_jmlhkenaikan = 0;
            if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
                $t_jmlhkenaikan = ($t_jmlhpajak * $data_get['t_tarifkenaikan'] / 100);
                $t_jmlhpajak = $t_jmlhpajak + $t_jmlhkenaikan;
            }
        } else {
            $t_dasarpengenaan = str_ireplace(".", "", $data_get['t_dasarpengenaan']);
            $t_tarifpajak = $data_get['t_tarifpajak'];
            $t_jmlhpajak = ($t_dasarpengenaan * $t_tarifpajak / 100);
            $t_jmlhkenaikan = 0;
            if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
                $t_jmlhkenaikan = ($t_jmlhpajak * $data_get['t_tarifkenaikan'] / 100);
                $t_jmlhpajak = $t_jmlhpajak + $t_jmlhkenaikan;
            }
        }
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", "."),
            "t_tarifpajak" => $t_tarifpajak
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function jangkawaktureklameAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();

        $data_render = array(
            "t_jenisreklame" => $data_get->t_jenisreklame
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungpajakreklameAction() {
        /** Hitung Pajak Reklame
         * @param int $t_dasarpengenaan
         * @param int $t_tarifpajak
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 17/01/2018
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
//        var_dump($data_get); exit();
        $data = $this->Tools()->getService('ReklameTable')->getDataJenisReklame($data_get->t_jenisreklame);
        $data_korek = $this->Tools()->getService('RekeningTable')->getdataRekeningId($data['s_idkorek']);
        $data_kelasjalan = $this->Tools()->getService('ReklameTable')->getIndexKelasjalan($data_get->t_jenislokasi);
        $data_muka = $this->Tools()->getService('ReklameTable')->getIndexMuka($data_get->t_sudutpandang);
        $data_lebarjalan = $this->Tools()->getService('ReklameTable')->getIndexLebarjalan($data_get->t_lebarjalan);
        $data_ketinggian = $this->Tools()->getService('ReklameTable')->getIndexKetinggian($data_get->t_ketinggian);

        $luas = ($data_get->t_panjang * $data_get->t_lebar);
        $luas_reklame = ($luas == 0) ? 1 : round($luas);
        $jmlh_reklame = (!empty($data_get->t_jumlah)) ? $data_get->t_jumlah : 1;

        $t_konstruksi = ($data_get->t_jenisreklame == 7 || $data_get->t_jenisreklame == 8) ? null : $data_get->t_konstruksi;
        $data_tarif = $this->Tools()->getService('ReklameTable')->getDataTarifReklame($data_get->t_jenisreklame, $data_get->t_pemasangan, $t_konstruksi);
//        var_dump($data_tarif['s_hargasatuan1']); exit();

        if ($data_get->t_jenisreklame == 1 || $data_get->t_jenisreklame == 2 || $data_get->t_jenisreklame == 3 || $data_get->t_jenisreklame == 4 || $data_get->t_jenisreklame == 5 || $data_get->t_jenisreklame == 6 || $data_get->t_jenisreklame == 7 || $data_get->t_jenisreklame == 8) {
            $tipewaktu = '';
            $jangkawaktu = $data_get->t_jangkawaktu;
            if ($data_get->t_pemasangan == 1) {
                $nsl = $data_kelasjalan['s_indekskelasjalan'] + $data_muka['s_indekssudutpandang'] + $data_lebarjalan['s_indekslebarjalan'] + $data_ketinggian['s_indexketinggian'];
            } else {
                $nsl = 0.5;
            }

            if ($luas_reklame <= 5) {
                $harga_satuan = $data_tarif['s_hargasatuan1'];
            } elseif ($luas_reklame > 5 && $luas_reklame <= 15) {
                $harga_satuan = $data_tarif['s_hargasatuan2'];
            } elseif ($luas_reklame > 15 && $luas_reklame <= 25) {
                $harga_satuan = $data_tarif['s_hargasatuan3'];
            } elseif ($luas_reklame > 25 && $luas_reklame <= 35) {
                $harga_satuan = $data_tarif['s_hargasatuan4'];
            } else {
                $harga_satuan = $data_tarif['s_hargasatuan5'];
            }

            $nsr = $nsl * $luas_reklame * $jmlh_reklame * $jangkawaktu * $harga_satuan;
        } elseif ($data_get->t_jenisreklame == 9 || $data_get->t_jenisreklame == 10 || $data_get->t_jenisreklame == 18 || $data_get->t_jenisreklame == 19) {
            $tipewaktu = 'Minggu';
            $jangkawaktu = $data_get->t_jangkawaktutext;
            $harga_satuan = $data_tarif['s_hargasatuan1'];
            $nsl = 1;
            $nsr = $nsl * $luas_reklame * $jmlh_reklame * $jangkawaktu * $harga_satuan;
        } elseif ($data_get->t_jenisreklame == 11 || $data_get->t_jenisreklame == 12 || $data_get->t_jenisreklame == 13) {
            $tipewaktu = 'Bulan';
            $jangkawaktu = $data_get->t_jangkawaktutext;
            $harga_satuan = $data_tarif['s_hargasatuan1'];
            $nsl = 1;
            $nsr = $nsl * $luas_reklame * $jmlh_reklame * $jangkawaktu * $harga_satuan;
        } elseif ($data_get->t_jenisreklame == 14 || $data_get->t_jenisreklame == 15 || $data_get->t_jenisreklame == 16 || $data_get->t_jenisreklame == 17) {
            $tipewaktu = 'Tahun';
            $jangkawaktu = $data_get->t_jangkawaktutext;
            $harga_satuan = $data_tarif['s_hargasatuan1'];
            $nsl = 1;
            $nsr = $nsl * $luas_reklame * $jmlh_reklame * $jangkawaktu * $harga_satuan;
        } elseif ($data_get->t_jenisreklame == 20 || $data_get->t_jenisreklame == 21 || $data_get->t_jenisreklame == 22) {
            $tipewaktu = 'Hari';
            $jangkawaktu = $data_get->t_jangkawaktutext;
            $harga_satuan = $data_tarif['s_hargasatuan1'];
            $nsl = 1;
            $nsr = $nsl * $luas_reklame * $jmlh_reklame * $jangkawaktu * $harga_satuan;
        }

        $jumlahpajak = $nsr * $data_korek['s_persentarifkorek'] / 100;

        $data_render = array(
            "t_satuanreklame" => $data['s_satuan'],
            "t_jenisreklame" => $data_get->t_jenisreklame,
            "t_tipewaktu" => $tipewaktu,
            "t_luas" => number_format($luas, 2, ".", "."),
            "t_nsr" => number_format($nsr, 0, ",", "."),
            "t_nsl" => $nsl,
            "t_jmlhpajak" => number_format($jumlahpajak, 0, ",", "."),
            "t_idkorek" => $data_korek['s_idkorek'],
            "t_korek" => $data_korek['korek'],
            "t_namakorek" => strtoupper($data_korek['s_namakorek']),
            "t_tarifpajak" => $data_korek['s_persentarifkorek'],
            "t_tarifdasarkorek" => number_format($data_korek['s_tarifdasarkorek'], 0, ",", "."),
            "t_hargasatuan" => number_format($harga_satuan, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungpajakairAction() {
        /** Hitung Pajak Air
         * @param int $t_volume
         * @param int $t_tarifpajak
         * @author Roni Mustapa <ronimustapa@gmail.com>
         * @date 13/09/2017
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
//        $t_volume = str_ireplace(".", "", $data_get['t_volume']);
        $t_volume = $data_get['t_volume'];
        if ($this->getRequest()->isPost() && $t_volume > 0):
            $datatarif = $this->Tools()->getService('AirTable')->getDataIdTarif($data_get->t_zona, $data_get->t_kodekelompok, $data_get->t_kodejenis);
//        var_dump($datatarif['s_nilai1']); exit();

            if ($data_get['t_fna'] == 0 || $data_get['t_fna'] == NULL):

                $snilai1 = 0;
                $snilai2 = 0;
                $snilai3 = 0;
                $snilai4 = 0;
                $snilai5 = 0;
                $snilai6 = 0;
                if ($t_volume <= 50) {
                    $t_volume1 = $t_volume;
                    $t_volume2 = "0";
                    $t_volume3 = "0";
                    $t_volume4 = "0";
                    $t_volume5 = "0";
                    $t_volume6 = "0";
//                    $snilai1 = $t_volume * $datatarif['s_nilai1'];
                    $snilai1 = $t_volume * $datatarif['s_nilai1'] * $data_get['t_tarifpajak'] / 100;
                    $t_tarifpajak1 = $data_get['t_tarifpajak'];
                    $t_tarifpajak2 = "0";
                    $t_tarifpajak3 = "0";
                    $t_tarifpajak4 = "0";
                    $t_tarifpajak5 = "0";
                    $t_tarifpajak6 = "0";
                    $t_jmlhpajak = number_format($snilai1, 0, ",", ".");
                    $datatarif1 = $datatarif['s_nilai1'];
                    $datatarif2 = '0';
                    $datatarif3 = '0';
                    $datatarif4 = '0';
                    $datatarif5 = '0';
                    $datatarif6 = '0';
//                    $nilaiperuntukan = $datatarif1;
                    $datatarif_fna = $snilai1;
                    $volume_pro = $t_volume1;
                } elseif ($t_volume >= 51 && $t_volume <= 500) {
                    $t_volume1 = 50;
                    $t_volume2 = $t_volume - 50;
                    $t_volume3 = "0";
                    $t_volume4 = "0";
                    $t_volume5 = "0";
                    $t_volume6 = "0";
//                    $snilai1 = $t_volume1 * $datatarif['s_nilai1'];
//                    $snilai2 = $t_volume2 * $datatarif['s_nilai2'];
                    $snilai1 = $t_volume1 * $datatarif['s_nilai1'] * $data_get['t_tarifpajak'] / 100;
                    $snilai2 = $t_volume2 * $datatarif['s_nilai2'] * $data_get['t_tarifpajak'] / 100;
                    $t_jmlhpajak = number_format($snilai1 + $snilai2, 0, ",", ".");
                    $t_tarifpajak1 = $data_get['t_tarifpajak'];
                    $t_tarifpajak2 = $data_get['t_tarifpajak'];
                    $t_tarifpajak3 = "0";
                    $t_tarifpajak4 = "0";
                    $t_tarifpajak5 = "0";
                    $t_tarifpajak6 = "0";
                    $datatarif1 = $datatarif['s_nilai1'];
                    $datatarif2 = $datatarif['s_nilai2'];
                    $datatarif3 = '0';
                    $datatarif4 = '0';
                    $datatarif5 = '0';
                    $datatarif6 = '0';
//                    $nilaiperuntukan = $datatarif2;
                    $datatarif_fna = $snilai1 + $snilai2;
                    $volume_pro = $t_volume2;
                } elseif ($t_volume >= 501 && $t_volume <= 1000) {
                    $t_volume1 = 50;
                    $t_volume2 = 450;
                    $t_volume3 = $t_volume - 500;
                    $t_volume4 = "0";
                    $t_volume5 = "0";
                    $t_volume6 = "0";
//                    $snilai1 = $t_volume1 * $datatarif['s_nilai1'];
//                    $snilai2 = $t_volume2 * $datatarif['s_nilai2'];
//                    $snilai3 = $t_volume3 * $datatarif['s_nilai3'];
                    $snilai1 = $t_volume1 * $datatarif['s_nilai1'] * $data_get['t_tarifpajak'] / 100;
                    $snilai2 = $t_volume2 * $datatarif['s_nilai2'] * $data_get['t_tarifpajak'] / 100;
                    $snilai3 = $t_volume3 * $datatarif['s_nilai3'] * $data_get['t_tarifpajak'] / 100;
                    $t_jmlhpajak = number_format($snilai1 + $snilai2 + $snilai3, 0, ",", ".");
                    $t_tarifpajak1 = $data_get['t_tarifpajak'];
                    $t_tarifpajak2 = $data_get['t_tarifpajak'];
                    $t_tarifpajak3 = $data_get['t_tarifpajak'];
                    $t_tarifpajak4 = "0";
                    $t_tarifpajak5 = "0";
                    $t_tarifpajak6 = "0";
                    $datatarif1 = $datatarif['s_nilai1'];
                    $datatarif2 = $datatarif['s_nilai2'];
                    $datatarif3 = $datatarif['s_nilai3'];
                    $datatarif4 = '0';
                    $datatarif5 = '0';
                    $datatarif6 = '0';
//                    $nilaiperuntukan = $datatarif3;
                    $datatarif_fna = $snilai1 + $snilai2 + $snilai3;
                    $volume_pro = $t_volume3;
                } elseif ($t_volume >= 1001 && $t_volume <= 2500) {
                    $t_volume1 = 50;
                    $t_volume2 = 450;
                    $t_volume3 = 500;
                    $t_volume4 = $t_volume - 1000;
                    $t_volume5 = "0";
                    $t_volume6 = "0";
//                    $snilai1 = $t_volume1 * $datatarif['s_nilai1'];
//                    $snilai2 = $t_volume2 * $datatarif['s_nilai2'];
//                    $snilai3 = $t_volume3 * $datatarif['s_nilai3'];
//                    $snilai4 = str_ireplace(".", "", $t_volume4) * $datatarif['s_nilai4'];
                    $snilai1 = $t_volume1 * $datatarif['s_nilai1'] * $data_get['t_tarifpajak'] / 100;
                    $snilai2 = $t_volume2 * $datatarif['s_nilai2'] * $data_get['t_tarifpajak'] / 100;
                    $snilai3 = $t_volume3 * $datatarif['s_nilai3'] * $data_get['t_tarifpajak'] / 100;
                    $snilai4 = str_ireplace(".", "", $t_volume4) * $datatarif['s_nilai4'] * $data_get['t_tarifpajak'] / 100;
                    $t_jmlhpajak = number_format($snilai1 + $snilai2 + $snilai3 + $snilai4, 0, ",", ".");
                    $t_tarifpajak1 = $data_get['t_tarifpajak'];
                    $t_tarifpajak2 = $data_get['t_tarifpajak'];
                    $t_tarifpajak3 = $data_get['t_tarifpajak'];
                    $t_tarifpajak4 = $data_get['t_tarifpajak'];
                    $t_tarifpajak5 = "0";
                    $t_tarifpajak6 = "0";
                    $datatarif1 = $datatarif['s_nilai1'];
                    $datatarif2 = $datatarif['s_nilai2'];
                    $datatarif3 = $datatarif['s_nilai3'];
                    $datatarif4 = $datatarif['s_nilai4'];
                    $datatarif5 = '0';
                    $datatarif6 = '0';
//                    $nilaiperuntukan = $datatarif4;
                    $datatarif_fna = $snilai1 + $snilai2 + $snilai3 + $snilai4;
                    $volume_pro = $t_volume4;
                } elseif ($t_volume >= 2501 && $t_volume <= 5000) {
                    $t_volume1 = 50;
                    $t_volume2 = 450;
                    $t_volume3 = 500;
                    $t_volume4 = 1500;
                    $t_volume5 = $t_volume - 2500;
                    $t_volume6 = "0";
//                    $snilai1 = $t_volume1 * $datatarif['s_nilai1'];
//                    $snilai2 = $t_volume2 * $datatarif['s_nilai2'];
//                    $snilai3 = $t_volume3 * $datatarif['s_nilai3'];
//                    $snilai4 = $t_volume4 * $datatarif['s_nilai4'];
//                    $snilai5 = str_ireplace(".", "", $t_volume5) * $datatarif['s_nilai5'];
                    $snilai1 = $t_volume1 * $datatarif['s_nilai1'] * $data_get['t_tarifpajak'] / 100;
                    $snilai2 = $t_volume2 * $datatarif['s_nilai2'] * $data_get['t_tarifpajak'] / 100;
                    $snilai3 = $t_volume3 * $datatarif['s_nilai3'] * $data_get['t_tarifpajak'] / 100;
                    $snilai4 = $t_volume4 * $datatarif['s_nilai4'] * $data_get['t_tarifpajak'] / 100;
                    $snilai5 = str_ireplace(".", "", $t_volume5) * $datatarif['s_nilai5'] * $data_get['t_tarifpajak'] / 100;
                    $t_jmlhpajak = number_format($snilai1 + $snilai2 + $snilai3 + $snilai4 + $snilai5, 0, ",", ".");
                    $t_tarifpajak1 = $data_get['t_tarifpajak'];
                    $t_tarifpajak2 = $data_get['t_tarifpajak'];
                    $t_tarifpajak3 = $data_get['t_tarifpajak'];
                    $t_tarifpajak4 = $data_get['t_tarifpajak'];
                    $t_tarifpajak5 = $data_get['t_tarifpajak'];
                    $t_tarifpajak6 = "0";
                    $datatarif1 = $datatarif['s_nilai1'];
                    $datatarif2 = $datatarif['s_nilai2'];
                    $datatarif3 = $datatarif['s_nilai3'];
                    $datatarif4 = $datatarif['s_nilai4'];
                    $datatarif5 = $datatarif['s_nilai5'];
                    $datatarif6 = '0';
//                    $nilaiperuntukan = $datatarif5;
                    $datatarif_fna = $snilai1 + $snilai2 + $snilai3 + $snilai4 + $snilai5;
                    $volume_pro = $t_volume5;
                } elseif ($t_volume > 5000) {
                    $t_volume1 = 50;
                    $t_volume2 = 450;
                    $t_volume3 = 500;
                    $t_volume4 = 1500;
                    $t_volume5 = 2500;
                    $t_volume6 = $t_volume - 5000;
//                    $snilai1 = $t_volume1 * $datatarif['s_nilai1'];
//                    $snilai2 = $t_volume2 * $datatarif['s_nilai2'];
//                    $snilai3 = $t_volume3 * $datatarif['s_nilai3'];
//                    $snilai4 = $t_volume4 * $datatarif['s_nilai4'];
//                    $snilai5 = $t_volume5 * $datatarif['s_nilai5'];
//                    $snilai6 = str_ireplace(".", "", $t_volume6) * $datatarif['s_nilai6'];
                    $snilai1 = $t_volume1 * $datatarif['s_nilai1'] * $data_get['t_tarifpajak'] / 100;
                    $snilai2 = $t_volume2 * $datatarif['s_nilai2'] * $data_get['t_tarifpajak'] / 100;
                    $snilai3 = $t_volume3 * $datatarif['s_nilai3'] * $data_get['t_tarifpajak'] / 100;
                    $snilai4 = $t_volume4 * $datatarif['s_nilai4'] * $data_get['t_tarifpajak'] / 100;
                    $snilai5 = $t_volume5 * $datatarif['s_nilai5'] * $data_get['t_tarifpajak'] / 100;
                    $snilai6 = str_ireplace(".", "", $t_volume6) * $datatarif['s_nilai6'] * $data_get['t_tarifpajak'] / 100;
                    $t_jmlhpajak = number_format($snilai1 + $snilai2 + $snilai3 + $snilai4 + $snilai5 + $snilai6, 0, ",", ".");
                    $t_tarifpajak1 = $data_get['t_tarifpajak'];
                    $t_tarifpajak2 = $data_get['t_tarifpajak'];
                    $t_tarifpajak3 = $data_get['t_tarifpajak'];
                    $t_tarifpajak4 = $data_get['t_tarifpajak'];
                    $t_tarifpajak5 = $data_get['t_tarifpajak'];
                    $t_tarifpajak6 = $data_get['t_tarifpajak'];
                    $datatarif1 = $datatarif['s_nilai1'];
                    $datatarif2 = $datatarif['s_nilai2'];
                    $datatarif3 = $datatarif['s_nilai3'];
                    $datatarif4 = $datatarif['s_nilai4'];
                    $datatarif5 = $datatarif['s_nilai5'];
                    $datatarif6 = $datatarif['s_nilai6'];
//                    $nilaiperuntukan = $datatarif5;
                    $datatarif_fna = $snilai1 + $snilai2 + $snilai3 + $snilai4 + $snilai5 + $snilai6;
                    $volume_pro = $t_volume6;
                }

                $t_jmlhkenaikan = 0;
                if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
                    $t_jmlhkenaikan = ($t_jmlhpajak * $data_get['t_tarifkenaikan'] / 100);
                    $t_jmlhpajak = $t_jmlhpajak + $t_jmlhkenaikan;
                }
//                $sumberair = $this->Tools()->getService('AirTable')->getFNA(0, $data_get['t_sumberair']);
//                $cekungan = $this->Tools()->getService('AirTable')->getFNA(1, $data_get['t_cekungan']);
//                $jaringanpdam = $this->Tools()->getService('AirTable')->getFNA(2, $data_get['t_jaringanpdam']);
//                $kualitasair = $this->Tools()->getService('AirTable')->getFNA(3, $data_get['t_kualitasair']);
//                $areapengaruh = $this->Tools()->getService('AirTable')->getFNA(4, $data_get['t_areapengaruh']);
//                $tingkatkerusakan = $this->Tools()->getService('AirTable')->getFNA(5, $data_get['t_tingkatkerusakan']);
            endif;
            //perhitungan Kaimana
            # 1. FNA = ((sumberairx20%) + (cekunganx20%) + (pdamx20%) + (kualitasx40%) x 60% ) + ( (peruntukanx40%) + (areapengaruhx20%) + (kerusakanx40%) x 40% )
            # 2. HDA = FNA x HAB
            # 3. NPA = Volume x HDA
            # 4. Pajak = NPA x 20%

            if ($data_get['t_fna'] != NULL || $data_get['t_fna'] > 0):
                $FNA = $data_get['t_fna'];
            else:
                $FNA = $datatarif_fna;
//                $FNA = (( ($sumberair['s_nilai'] * 0.2) + ($cekungan['s_nilai'] * 0.2) + ($jaringanpdam['s_nilai'] * 0.2) + ($kualitasair['s_nilai'] * 0.4) ) * 0.6) + (( ($nilaiperuntukan * 0.4) + ($areapengaruh['s_nilai'] * 0.2) + ($tingkatkerusakan['s_nilai'] * 0.4) ) * 0.4 );
            endif;
//            $HAB = $data_get['t_tarifdasarkorek'];
//            $HDA = $nilaiperuntukan;
//            $NPA = $volume_pro * $HDA;
            $NPA = $FNA;
            $pajak = $FNA;
//            $pajak = $FNA * ($data_get['t_tarifpajak'] / 100);

        endif;
        $data_render = array(
            "t_volumeair0" => number_format($t_volume1, 0, ',', '.'),
            "t_volumeair1" => number_format($t_volume2, 0, ',', '.'),
            "t_volumeair2" => number_format($t_volume3, 0, ',', '.'),
            "t_volumeair3" => number_format($t_volume4, 0, ',', '.'),
            "t_volumeair4" => number_format($t_volume5, 0, ',', '.'),
            "t_volumeair5" => number_format($t_volume6, 0, ',', '.'),
            "t_hargadasar0" => number_format($datatarif1, 0, ',', '.'),
            "t_hargadasar1" => number_format($datatarif2, 0, ',', '.'),
            "t_hargadasar2" => number_format($datatarif3, 0, ',', '.'),
            "t_hargadasar3" => number_format($datatarif4, 0, ',', '.'),
            "t_hargadasar4" => number_format($datatarif5, 0, ',', '.'),
            "t_hargadasar5" => number_format($datatarif6, 0, ',', '.'),
            "t_tarif0" => $t_tarifpajak1,
            "t_tarif1" => $t_tarifpajak2,
            "t_tarif2" => $t_tarifpajak3,
            "t_tarif3" => $t_tarifpajak4,
            "t_tarif4" => $t_tarifpajak5,
            "t_tarif5" => $t_tarifpajak6,
            "t_jumlah0" => number_format($snilai1, 0, ',', '.'),
            "t_jumlah1" => number_format($snilai2, 0, ',', '.'),
            "t_jumlah2" => number_format($snilai3, 0, ',', '.'),
            "t_jumlah3" => number_format($snilai4, 0, ',', '.'),
            "t_jumlah4" => number_format($snilai5, 0, ',', '.'),
            "t_jumlah5" => number_format($snilai6, 0, ',', '.'),
            "t_jmlhpajak" => number_format($pajak, 0, ',', '.'),
            "t_volumem3" => number_format($t_volume, 0, ',', '.'),
            "t_fna" => $FNA,
            "t_totalnpa" => number_format($NPA, 0, ',', '.'),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", "."),
//            'perhitungan' => " ( Rp. " . number_format($FNA, 0, ',', '.') . " (Total NPA)  x " . $data_get['t_tarifpajak'] . "% (Tarif Pajak) )",
//            'perhitungan' => " ( " . number_format($NPA, 0, ',', '.') . " (NPA) = " . number_format($volume_pro, 0, ',', '.') . " (Volume) x  ( " . number_format($HDA, 0, ',', '.') . " (HDA) = " . number_format($FNA, 2, ',', '.') . " (FNA) ) x " . $data_get['t_tarifpajak'] . "% (Tarif Pajak)",
//            'perhitungan' => " ( " . number_format($NPA, 0, ',', '.') . " (NPA) = " . number_format($t_volume, 0, ',', '.') . " (Volume) x  ( " . number_format($HDA, 0, ',', '.') . " (HDA) = " . number_format($FNA, 2, ',', '.') . " (FNA) x " . number_format($HAB, 0, ',', '.') . " (HAB) ) ) x " . $data_get['t_tarifpajak'] . "% (Tarif Pajak)",
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    /*
     * HITUNG RETRIBUSI
     * AUTHOR : RONI MUSTAPA
     * EMAIL : ronimustapa@gmail.com
     * created : 12/04/2019
     */

    public function hitungretribusikekayaanAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatarif = $this->Tools()->getService('KekayaandaerahTable')->getDataKlasifikasi($data_get['t_klasifikasi']);
        // var_dump($datatarif['s_nilailuastanah']); exit();
        $t_luastanah = $data_get['t_jmlhbln'] * $data_get['t_luastanah'] * $datatarif['s_nilailuastanah'];
        $t_luasbangunan = $data_get['t_jmlhbln'] * $data_get['t_luasbangunan'] * $datatarif['s_nilailuasbangunan'];

        $t_jmlhpajak = $t_luastanah + $t_luasbangunan;
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_nilailuastanah" => number_format($datatarif['s_nilailuastanah'], 0, ",", "."),
            "t_nilailuasbangunan" => number_format($datatarif['s_nilailuasbangunan'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cariKlasifikasiTarifKebersihanAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataKlasifikasi = $this->Tools()->getService('DetailkebersihanTable')->getDataKlasifikasiKategori($data_get['t_klasifikasi']);
        $opsi = "";
        $opsi .= "<option value=''>Silahkan Pilih</option>";
        foreach ($dataKlasifikasi as $r) {
            $opsi .= "<option value='" . $r['s_idtarif'] . "'>" . str_pad($r['s_idklasifikasi'], 2, "0", STR_PAD_LEFT) . " || " . $r['s_keterangan'] . "</option>";
        }
        $data_render = array(
            'res' => $opsi,
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretribusikebersihanAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatarif = $this->Tools()->getService('DetailkebersihanTable')->getDataKlasifikasiTarif($data_get['t_kategori']);
        // var_dump($datatarif['s_nilailuastanah']); exit();
        $t_jmlhpajak = $data_get['t_jmlhbln'] * $datatarif['s_tarifdasar'];
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_tarifdasar" => number_format($datatarif['s_tarifdasar'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cariKlasifikasiTarifPasarAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataKlasifikasi = $this->Tools()->getService('DetailpasarTable')->getDataKlasifikasiKategori($data_get['t_klasifikasi']);
        $opsi = "";
        $opsi .= "<option value=''>Silahkan Pilih</option>";
        foreach ($dataKlasifikasi as $r) {
            $opsi .= "<option value='" . $r['s_idtarif'] . "'>" . str_pad($r['s_idklasifikasi'], 2, "0", STR_PAD_LEFT) . " || " . $r['s_keterangan'] . "</option>";
        }
        $data_render = array(
            'res' => $opsi,
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretribusipasarAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatarif = $this->Tools()->getService('DetailpasarTable')->getDataKlasifikasiTarif($data_get['t_jenisbangunan']);
        // var_dump($datatarif['s_nilailuastanah']); exit();
        $t_luas = $data_get['t_panjang'] * $data_get['t_lebar'];
        $t_jmlhpajak = $data_get['t_jmlhbln'] * $t_luas * $datatarif['s_tarifdasar'];
        $data_render = array(
            "t_tipewaktu" => $datatarif['s_satuan'],
            "t_luas" => $t_luas,
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_tarifdasar" => number_format($datatarif['s_tarifdasar'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cariKlasifikasiTarifPasargrosirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataKlasifikasi = $this->Tools()->getService('DetailpasargrosirTable')->getDataKlasifikasiKategori($data_get['t_klasifikasi']);
        $opsi = "";
        $opsi .= "<option value=''>Silahkan Pilih</option>";
        foreach ($dataKlasifikasi as $r) {
            $opsi .= "<option value='" . $r['s_idtarif'] . "'>" . str_pad($r['s_idklasifikasi'], 2, "0", STR_PAD_LEFT) . " || " . $r['s_keterangan'] . "</option>";
        }
        $data_render = array(
            'res' => $opsi,
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretribusipasargrosirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatarif = $this->Tools()->getService('DetailpasargrosirTable')->getDataKlasifikasiTarif($data_get['t_jenisbangunan']);
        // var_dump($datatarif['s_nilailuastanah']); exit();
        $t_luas = $data_get['t_panjang'] * $data_get['t_lebar'];
        $t_jmlhpajak = $data_get['t_jmlhbln'] * $t_luas * $datatarif['s_tarifdasar'];
        $data_render = array(
            "t_tipewaktu" => $datatarif['s_satuan'],
            "t_luas" => $t_luas,
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_tarifdasar" => number_format($datatarif['s_tarifdasar'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretribusitempatparkirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatarif = $this->Tools()->getService('DetailtempatparkirTable')->getDataTarif($data_get['t_jeniskendaraan']);
        $t_jmlhpajak = $data_get['t_jmlhkendaraan'] * $datatarif['s_tarifdasar'];
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_tarifdasar" => number_format($datatarif['s_tarifdasar'], 0, ",", "."),
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretribusirumahdinasAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_luas = $data_get['t_panjang'] * $data_get['t_lebar'];
        $datatarif = $this->Tools()->getService('RumahdinasTable')->getDataRange($t_luas);
        $t_jmlhpajak = ($t_luas * $datatarif['s_tarif'] * $data_get['t_jmlhbln']) - str_ireplace(".", "", $data_get['t_potongan']);
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_tarifdasar" => number_format($datatarif['s_tarif'], 0, ",", "."),
            "t_luas" => $t_luas
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungretpangrekAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_luas = $data_get['t_panjang'] * $data_get['t_lebar'];
        $t_tarifdasar = str_ireplace(".", "", $data_get['t_tarifdasar']);
        $t_jmlhpajak = ($t_luas * $t_tarifdasar * $data_get['t_jmlhbln']) - str_ireplace(".", "", $data_get['t_potongan']);
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_luas" => $t_luas
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungrettanahlainAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_luas = $data_get['t_panjang'] * $data_get['t_lebar'];
        $t_tarifdasar = str_ireplace(".", "", $data_get['t_tarifdasar']);
        $t_jmlhpajak = ($t_luas * $t_tarifdasar * $data_get['t_jmlhbln']) - str_ireplace(".", "", $data_get['t_potongan']);
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_luas" => $t_luas
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function hitungrettanrekAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_luas = $data_get['t_panjang'] * $data_get['t_lebar'];
        $t_jmlhblnhari = $data_get['t_jmlhblnhari'];
        $t_tarifdasar = (int) str_ireplace(".", "", $data_get['t_tarifdasar']);
        $t_jmlhpajak = ($t_luas * $t_tarifdasar * $t_jmlhblnhari) - str_ireplace(".", "", $data_get['t_potongan']);
        $data_render = array(
            "t_jmlhpajak" => number_format($t_jmlhpajak, 0, ",", "."),
            "t_luas" => $t_luas
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function CariPendaftaranByObjekAction() {
        /** Cari Transaksi By Objek Pajak
         * @param int $npwpd
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 29/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
// Mengambil Data WP, OP dan Transaksi
        $op = $this->Tools()->getService('ObjekTable')->getObjekPajakbyIdObjek($data_get['idobjek']);
        $dataop = " <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2'>NPWPD</label>
                        <div class='col-sm-10'>
                            : " . $op['t_npwpd'] . "
                        </div>
                    </div>
                    <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2'>Nama WP</label>
                        <div class='col-sm-10'>
                            : " . $op['t_nama'] . "
                        </div>
                    </div>
                    <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2'>Alamat WP</label>
                        <div class='col-sm-10'>
                            : " . $op['t_alamat'] . "
                            ,Desa/Kel. " . $op['s_namakel'] . "
                            ,Kec. " . $op['s_namakec'] . "
                            ,Kab. " . $op['t_kabupaten'] . "
                        </div>
                    </div>
                    <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2'>NIOP</label>
                        <div class='col-sm-10'>
                            : " . $op['t_nop'] . "
                        </div>
                    </div>
                    <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2'>Nama OP</label>
                        <div class='col-sm-10'>
                            : " . $op['t_namaobjek'] . "
                        </div>
                    </div>
                    <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2'>Alamat OP</label>
                        <div class='col-sm-10'>
                            : " . $op['t_alamatobjek'] . "
                            ,Desa/Kel. " . $op['s_namakelobjek'] . "
                            ,Kec. " . $op['s_namakecobjek'] . "
                            ,Kab. " . $op['t_kabupatenobjek'] . "
                        </div>
                    </div>
                    <div class='col-sm-12' style='font-size:11px'>
                        <label class='col-sm-2 control-label'>Periode Pajak</label>
                        <div class='col-sm-2'>
                            <input type='text' class='form-control' name='periodepajak' id='periodepajak'>
                        </div>
                        <div class='col-sm-1'>
                            <input type='button' class='btn btn-sm btn-primary' value='Cari' onclick='CariPendataanByObjek(" . $op['t_idobjek'] . ");'>
                        </div>
                    </div>";
        $datatransaksi = "";
        $data_render = array(
            "dataop" => $dataop,
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function CariPendataanByObjekAction() {
        /** Cari Pendataan By Objek Pajak
         * @param int $idobjek
         * @param int $periodepajak
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 29/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $abulan = ['1' => 'Januari', '2' => 'Februari', '3' => 'Maret', '4' => 'April', '5' => 'Mei', '6' => 'Juni', '7' => 'Juli', '8' => 'Agustus', '9' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
        $datatransaksi = "  <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataawal = $this->Tools()->getService('ObjekTable')->getPendataanAwalbyIdObjek($data_get['idobjek']);
        // $tombolTeguranDanSKPDJ = "<td data-title='#' style='text-align:right'><a href='javascript:void(0)' class='btn btn-primary btn-xs' onclick='bukaCetakSuratTeguran($dataparameterTeguran, $data_get[idobjek])'><i class='glyph-icon icon-print'></i> Surat Teguran</href> <a href='javascript:void(0)' class='btn btn-warning btn-xs' onclick='pilihskpdjabatan($dataparameter)'><i class='glyph-icon icon-hand-o-up'></i> SKPD Jabatan</href></td>";
        $txt_bulan = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
        if ($dataawal['t_idkorek'] == 18) {//katering
            $rowKatering = $this->Tools()->getService('ObjekTable')->getPendataanKatering($data_get['periodepajak'], $data_get['idobjek']);
            $i = 1;
            foreach ($rowKatering as $row) {
                $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
                $datatransaksi .= "         <tr>
                                                <td data-title='Masa Pajak' style='text-align:left'>" . $txt_bulan[date('m', strtotime($row['t_masaawal']))] . "</td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                                    
                                            </tr>";
            }
        } else {
            for ($i = 1; $i <= 12; $i++) {
                // $row = $this->Tools()->getService('ObjekTable')->getPendataanbyMasa($i, $data_get['periodepajak'], $data_get['idobjek']);
                // pendataan selain katering ada yg lebih dari 1 kali
                $row = $this->Tools()->getService('ObjekTable')->getPendataanbyMasaHistori($i, $data_get['periodepajak'], $data_get['idobjek']);
                if ($row == false || $row->count() == 0) {
                    // $today = (int) date('m');
                    $tglpembanding = $data_get['periodepajak'] . "-" . str_pad($i, 2, '0', STR_PAD_LEFT) . "-01";
                    if ($dataawal['t_masaawal'] <= date('Y-m-t', strtotime($tglpembanding)) && date('Y-m-d') >= date('Y-m-t', strtotime($tglpembanding))) { //jika belum melaporkan pajak dan pernah melaporkan sebelumnya
                        $dataparameter = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01";
                        $dataparameterTeguran = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01";
                        $datatransaksi .= "     <tr>
                                                <td data-title='Masa Pajak' style='text-align:left'>" . $abulan[$i] . "</td>
                                                <td data-title='Kode Rekening'><b style='color:red'>Belum Melakukan Pelaporan Pajak</b></td>
                                                <td data-title='Tgl' style='text-align:center'></td>
                                                <td data-title='Masa Pajak' style='text-align:center'></td>
                                                <td data-title='Pajak' style='text-align:right'></td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>-</td>
                                                <td data-title='#' style='text-align:right'>
                                                <a href='javascript:void(0)' class='btn btn-primary btn-xs' onclick='bukaCetakSuratTeguran($dataparameterTeguran, $data_get[idobjek])'><i class='glyph-icon icon-print'></i> Surat Teguran</href> <a href='javascript:void(0)' class='btn btn-warning btn-xs' onclick='pilihskpdjabatan($dataparameter)'><i class='glyph-icon icon-hand-o-up'></i> SKPD Jabatan</href></td>
                                                </tr>";
                    } else { // data sebelum wp daftar dan dilakukan pendataan sama sekali
                        $datatransaksi .= "     <tr>
                                                <td data-title='Masa Pajak' style='text-align:left'>" . $abulan[$i] . "</td>
                                                <td data-title='Kode Rekening'>-</td>
                                                <td data-title='Tgl' style='text-align:center'>-</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>-</td>
                                                <td data-title='Pajak' style='text-align:right'>-</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>-</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>-</td>
                                            </tr>";
                    }
                } else {
                    // selain katering ada ppj, bisa pendataan leboh dari 1 kali
                    foreach ($row as $row) {
                        $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
                        $datatransaksi .= "
                        <tr>
                            <td data-title='Masa Pajak' style='text-align:left'>" . $abulan[$i] . "</td>
                            <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                            <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                            <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                            <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                            <td data-title='Tgl. Bayar' style='text-align:center'>" . $t_tglpembayaran . "</td>
                            <td data-title='Jml. Bayar' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                        </tr>";
                    }
                }
            }
        }
        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function CariPendataanByObjekOfficialAction() {
        /** Cari Pendataan By Objek Pajak
         * @param int $idobjek
         * @param int $periodepajak
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 29/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatransaksi = "  <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataawal = $this->Tools()->getService('ObjekTable')->getPendataanAwalbyIdObjek($data_get['idobjek']);
        if ($dataawal['t_idkorek'] == 18) {//katering
            $rowKatering = $this->Tools()->getService('ObjekTable')->getPendataanKatering($data_get['periodepajak'], $data_get['idobjek']);
            $i = 1;
            foreach ($rowKatering as $row) {
                $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
                $datatransaksi .= "         <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i++ . "</td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                                    
                                            </tr>";
            }
        } else {
            for ($i = 1; $i <= 12; $i++) {
                $row = $this->Tools()->getService('ObjekTable')->getPendataanbyMasa($i, $data_get['periodepajak'], $data_get['idobjek']);
                if ($row == false) {
//                $today = (int) date('m');
                    $tglpembanding = $data_get['periodepajak'] . "-" . str_pad($i, 2, '0', STR_PAD_LEFT) . "-01";
                    if ($dataawal['t_masaawal'] <= date('Y-m-t', strtotime($tglpembanding)) && date('Y-m-d') >= date('Y-m-t', strtotime($tglpembanding))) { //jika belum melaporkan pajak dan pernah melaporkan sebelumnya
                        $dataparameter = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01";
                        $dataparameterTeguran = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01";
                        $datatransaksi .= "     <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Kode Rekening'><b style='color:red'>Belum dilakukan pendataan</b></td>
                                                <td data-title='Tgl' style='text-align:center'></td>
                                                <td data-title='Masa Pajak' style='text-align:center'></td>
                                                <td data-title='Pajak' style='text-align:right'></td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>" . $dataawal['s_jenisobjek'] . "</td>";

                        $datatransaksi .= "<td data-title='#' style='text-align:center'></td>";

                        $datatransaksi .= "</tr>";
                    } else { // data sebelum wp daftar dan dilakukan pendataan sama sekali
                        $datatransaksi .= "     <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Kode Rekening'>-</td>
                                                <td data-title='Tgl' style='text-align:center'>-</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>-</td>
                                                <td data-title='Pajak' style='text-align:right'>-</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>-</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>-</td>
                                            </tr>";
                    }
                } else {
                    $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
                    $datatransaksi .= "         <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                            </tr>";
                }
            }
        }
        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function CariPendataanReklameByObjekAction() {
        /** Cari Pendataan By Objek Pajak
         * @param int $idobjek
         * @param int $periodepajak
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 29/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatransaksi = "  <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Penetapan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataHistory = $this->Tools()->getService('ObjekTable')->getHistoryReklame($data_get['idobjek']);
        $i = 1;
        foreach ($dataHistory as $row) {
            $t_tglpenetapan = (!empty($row['t_tglpenetapan']) ? date('d-m-Y', strtotime($row['t_tglpenetapan'])) : '-'); // returns true
            $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
            $datatransaksi .= " <tr>
                                <td data-title='No.' style='text-align:center'>" . $i++ . "</td>
                                <td data-title='Kode Rekening'>" . $row['korek'] . "<br>" . $row['s_namakorek'] . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpenetapan . "</td>
                                <td data-title='Tgl' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                            </tr>";
        }

        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function CariPendataanAirByObjekAction() {
        /** Cari Pendataan By Objek Pajak
         * @param int $idobjek
         * @param int $periodepajak
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 29/11/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $datatransaksi = "  <div class='remove-columns'>
                                    <table class='table table-bordered table-striped table-condensed cf' style='font-size : 11px; color:black'>
                                        <thead class='cf' style='background-color:blue'>
                                            <tr>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>No.</th>
                                                <th colspan='4' style='background-color: #00BCA4; color: white; text-align:center'>Pendataan</th>
                                                <th rowspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Penetapan</th>
                                                <th colspan='2' style='background-color: #00BCA4; color: white; text-align:center'>Pembayaran</th>
                                            </tr>
                                            <tr>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Kode Rekening</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Masa Pajak</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Pajak (Rp.)</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Tgl.</th>
                                                <th style='background-color: #00BCA4; color: white; text-align:center'>Jml. (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>";
        $dataawal = $this->Tools()->getService('ObjekTable')->getPendataanAwalbyIdObjek($data_get['idobjek']);
        for ($i = 1; $i <= 12; $i++) {
            $row = $this->Tools()->getService('ObjekTable')->getPendataanbyMasa($i, $data_get['periodepajak'], $data_get['idobjek']);
            if ($row == false) {
                $tglpembanding = $data_get['periodepajak'] . "-" . str_pad($i, 2, '0', STR_PAD_LEFT) . "-01";
                if ($dataawal['t_masaawal'] <= date('Y-m-t', strtotime($tglpembanding)) && date('Y-m-d') >= date('Y-m-t', strtotime($tglpembanding))) { //jika belum melaporkan pajak dan pernah melaporkan sebelumnya
                    $dataparameter = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01";
                    $dataparameterTeguran = $data_get['periodepajak'] . "" . str_pad($i, 2, '0', STR_PAD_LEFT) . "01" . str_pad($data_get['idobjek'], 9, '0', STR_PAD_LEFT);
                    $datatransaksi .= "     <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Kode Rekening'><b style='color:red'>Belum Melakukan Penetapan</b></td>
                                                <td data-title='Tgl' style='text-align:center'></td>
                                                <td data-title='Masa Pajak' style='text-align:center'></td>
                                                <td data-title='Pajak' style='text-align:right'></td>
                                                <td data-title='Tgl. Penetapan' style='text-align:center'></td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'></td>
                                                <td data-title='#' style='text-align:right'><a href='javascript:void(0)' class='btn btn-primary btn-xs' onclick='bukaCetakSuratTeguran($dataparameterTeguran)'><i class='glyph-icon icon-print'></i> Surat Teguran</href> <a href='javascript:void(0)' class='btn btn-warning btn-xs' onclick='pilihskpdjabatan($dataparameter)'><i class='glyph-icon icon-hand-o-up'></i> SKPD Jabatan</href></td>
                                            </tr>";
                } else { // data sebelum wp daftar dan dilakukan pendataan sama sekali
                    $datatransaksi .= "     <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Kode Rekening'>-</td>
                                                <td data-title='Tgl' style='text-align:center'>-</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>-</td>
                                                <td data-title='Pajak' style='text-align:right'>-</td>
                                                <td data-title='Tgl. Penetapan' style='text-align:center'>-</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>-</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>-</td>
                                            </tr>";
                }
            } else {
                $t_tglpembayaran = (!empty($row['t_tglpembayaran']) ? date('d-m-Y', strtotime($row['t_tglpembayaran'])) : '-'); // returns true
                $datatransaksi .= "         <tr>
                                                <td data-title='No.' style='text-align:center'>" . $i . "</td>
                                                <td data-title='Kode Rekening'>" . $row['korek'] . " - " . $row['s_namakorek'] . "</td>
                                                <td data-title='Tgl' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</td>
                                                <td data-title='Masa Pajak' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "</td>
                                                <td data-title='Pajak' style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>
                                                <td data-title='Tgl. Penetapan' style='text-align:center'>" . date('d-m-Y', strtotime($row['t_tglpenetapan'])) . "</td>
                                                <td data-title='Tgl. Bayar' style='text-align:center'>" . $t_tglpembayaran . "</td>
                                                <td data-title='Jml. Bayar' style='text-align:right'>" . number_format($row['t_jmlhpembayaran'], 0, ',', '.') . "</td>
                                            </tr>";
            }
        }
        $datatransaksi .= "             </tbody>
                                    </table>
                                </div>";
        $data_render = array(
            "datatransaksi" => $datatransaksi
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataGridRekeningAction() {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $req->isGet();
        $parametercari = $req->getQuery();
        $base = new \Pajak\Model\Setting\RekeningBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('RekeningTable')->getGridCountRekening($base, $parametercari);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $s = "";
        $data = $this->Tools()->getService('RekeningTable')->getGridDataRekening($base, $start, $parametercari);
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td>" . $row['korek'] . "</td>";
            $s .= "<td>" . $row['s_namakorek'] . "</td>";
            $s .= "<td>" . $row['s_persentarifkorek'] . "</td>";
            $s .= "<td><a href='#' onclick='pilihRekening(" . $row['s_idkorek'] . ");return false;' class='btn btn-xs btn-primary'><span class='glyph-icon icon-hand-o-up'> Pilih </a></td>";
            $s .= "</tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => 10,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function pilihRekeningAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataRekening = $this->Tools()->getService('RekeningTable')->getDataRekeningId($data_get['s_idkorek']);
        if ($dataRekening['t_berdasarmasa'] == 'Yes') {
            $t_berdasarmasa = "Berdasar Masa";
        } else {
            $t_berdasarmasa = "Tidak Berdasar Masa";
        }
        $opsi = "";
        if ($dataRekening['s_jenisobjek'] == 4) {
            $dataReklame = $this->Tools()->getService('ReklameTable')->getDataReklameByIdRekening($data_get['s_idkorek']);
            $opsi .= "<option value=''>Silahkan Pilih</option>";
            foreach ($dataReklame as $row) {
                $opsi .= "<option value='" . $row['s_kodejenis'] . "'>" . $row['s_namareklame'] . "</option>";
            }
        }
        $data = array(
            't_idkorek' => $dataRekening['s_idkorek'],
            't_korek' => $dataRekening['korek'],
            't_namakorek' => $dataRekening['s_namakorek'],
            't_tarifpajak' => $dataRekening['s_persentarifkorek'],
            't_tarifdasarkorek' => $dataRekening['s_tarifdasarkorek'], //number_format($dataRekening['s_tarifdasarkorek'], 0, ',', '.'),
            't_jenisreklame' => $opsi,
            't_berdasarmasa' => $t_berdasarmasa,
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function caritarifminerbaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataRekening = $this->Tools()->getService('RekeningTable')->getDataRekeningId($data_get['t_idkorek']);
        $data = array(
            't_hargapasaran' => number_format($dataRekening['s_tarifdasarkorek'], 0, ",", "."),
            't_tarifpersen' => $dataRekening['s_persentarifkorek']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungpajakminerbaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        // var_dump(str_ireplace(",", ".", $data_get['t_volume'])); exit();
//        $t_jumlah = str_ireplace(",", ".", $data_get['t_volume']) * str_ireplace(".", "", $data_get['t_hargapasaran']);
        $t_dasarpengenaan = str_ireplace(",", ".", $data_get['t_volume']) * str_ireplace(".", "", $data_get['t_hargapasaran']);
//        $t_pajak = $t_jumlah * $data_get['t_tarifpersen'] / 100;
        $data = array(
//            't_jumlah' => number_format($t_jumlah, 0, ",", "."),
//            't_pajak' => number_format($t_pajak, 0, ",", ".")
            't_dasarpengenaan' => number_format($t_dasarpengenaan, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalpajakminerbaAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jumlah1']) + str_ireplace(".", "", $data_get['t_jumlah2']) + str_ireplace(".", "", $data_get['t_jumlah3']) + str_ireplace(".", "", $data_get['t_jumlah4']) + str_ireplace(".", "", $data_get['t_jumlah5']) + str_ireplace(".", "", $data_get['t_jumlah6']) + str_ireplace(".", "", $data_get['t_jumlah7']) + str_ireplace(".", "", $data_get['t_jumlah8']) + str_ireplace(".", "", $data_get['t_jumlah9']) + str_ireplace(".", "", $data_get['t_jumlah10']);
        $t_jmlhpajak = str_ireplace(".", "", $data_get['t_pajak1']) + str_ireplace(".", "", $data_get['t_pajak2']) + str_ireplace(".", "", $data_get['t_pajak3']) + str_ireplace(".", "", $data_get['t_pajak4']) + str_ireplace(".", "", $data_get['t_pajak5']) + str_ireplace(".", "", $data_get['t_pajak6']) + str_ireplace(".", "", $data_get['t_pajak7']) + str_ireplace(".", "", $data_get['t_pajak8']) + str_ireplace(".", "", $data_get['t_pajak9']) + str_ireplace(".", "", $data_get['t_pajak10']);
        $t_pajak = $t_jmlhpajak;

        $t_jmlhkenaikan = 0;
        if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
            $t_jmlhkenaikan = ($t_pajak * $data_get['t_tarifkenaikan'] / 100);
            $t_pajak = $t_pajak + $t_jmlhkenaikan;
        }
        $data = array(
            // 't_jmlhpajak' => number_format($t_jmlhpajak, 0, ",", ".")

            't_dasarpengenaan' => number_format($t_jumlah, 0, ",", "."),
            't_jmlhpajak' => number_format($t_pajak, 0, ",", "."),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungpajakppjplnAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();

        $t_nilailistrik_rt = str_ireplace(".", "", $data_get['t_nilailistrik_rt']);
        $t_tarif_rt = str_ireplace(".", "", $data_get['t_tarif_rt']);
        $t_pajakterhutang_rt = $t_nilailistrik_rt * $t_tarif_rt / 100;

        $t_nilailistrik_nonrt = str_ireplace(".", "", $data_get['t_nilailistrik_nonrt']);
        $t_tarif_nonrt = str_ireplace(".", "", $data_get['t_tarif_nonrt']);
        $t_pajakterhutang_nonrt = $t_nilailistrik_nonrt * $t_tarif_nonrt / 100;

        $t_nilailistrik_industri = str_ireplace(".", "", $data_get['t_nilailistrik_industri']);
        $t_tarif_industri = str_ireplace(".", "", $data_get['t_tarif_industri']);
        $t_pajakterhutang_industri = $t_nilailistrik_industri * $t_tarif_industri / 100;

        $t_jmlhpajak = $t_pajakterhutang_rt + $t_pajakterhutang_nonrt + $t_pajakterhutang_industri;
        $data = array(
            't_pajakterhutang_rt' => number_format($t_pajakterhutang_rt, 0, ",", "."),
            't_pajakterhutang_nonrt' => number_format($t_pajakterhutang_nonrt, 0, ",", "."),
            't_pajakterhutang_industri' => number_format($t_pajakterhutang_industri, 0, ",", "."),
            't_jmlhpajak' => number_format($t_jmlhpajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungpajakppjAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();

        $ar_tarifppjnonpln = (!empty($data_get['t_golongantarif'])) ? $this->Tools()->getService('DetailPpjTable')->getDataTarifListrikById($data_get['t_golongantarif']) : array();
        $t_jmlhkwh = str_ireplace(".", "", $data_get['t_jmlhkwh']);
        $t_tarifpajak = str_ireplace(".", "", $data_get['t_tarifpajak']);

        $t_dasarpengenaan = $t_jmlhkwh * $ar_tarifppjnonpln['s_biaya_pakai2'];
        $t_jmlhpajak = ($t_dasarpengenaan * $t_tarifpajak / 100);
        $data = array(
//            't_dasarpengenaan' => $t_perdabaru,

            't_dasarpengenaan' => number_format($t_dasarpengenaan, 0, ",", "."),
            't_jmlhpajak' => number_format($t_jmlhpajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalpajakppjAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $tarif_pajak = 0;
        $t_subtotalpajak0 = str_ireplace(".", "", $data_get['subtotalpajak0']);
        $t_subtotalpajak1 = str_ireplace(".", "", $data_get['subtotalpajak1']);
        $t_subtotalpajak2 = str_ireplace(".", "", $data_get['subtotalpajak2']);
        $t_jmlhpajak = ($t_subtotalpajak0 + $t_subtotalpajak1 + $t_subtotalpajak2);
        $t_pajak = $t_jmlhpajak;

        $t_jmlhkenaikan = 0;
        if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
            $t_jmlhkenaikan = ($t_pajak * $data_get['t_tarifkenaikan'] / 100);
            $t_pajak = $t_pajak + $t_jmlhkenaikan;
        }
        $data = array(
            // 't_jmlhpajak' => number_format($t_jmlhpajak, 0, ",", "."),
            't_tarifdasar' => $tarif_pajak,
            // 't_dasarpengenaan' => number_format($t_jumlah, 0, ",", "."),
            't_jmlhpajak' => number_format($t_pajak, 0, ",", "."),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function caritarifparkirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataRekening = $this->Tools()->getService('RekeningTable')->getDataRekeningId($data_get['t_idkorek']);
        $data = array(
//            't_hargapasaran' => number_format($dataRekening['s_tarifdasarkorek'], 0, ",", "."),
            't_tarifpersen' => $dataRekening['s_persentarifkorek']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungpajakparkirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jmlh_kendaraan']) * str_ireplace(".", "", $data_get['t_hargadasar']);
        $t_pajak = $t_jumlah * $data_get['t_tarifpersen'] / 100;
        $data = array(
            't_jumlah' => number_format($t_jumlah, 0, ",", "."),
            't_pajak' => number_format($t_pajak, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtotalpajakparkirAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_jumlah1']) + str_ireplace(".", "", $data_get['t_jumlah2']) + str_ireplace(".", "", $data_get['t_jumlah3']) + str_ireplace(".", "", $data_get['t_jumlah4']) + str_ireplace(".", "", $data_get['t_jumlah5']) + str_ireplace(".", "", $data_get['t_jumlah6']) + str_ireplace(".", "", $data_get['t_jumlah7']);
        $t_jmlhpajak = str_ireplace(".", "", $data_get['t_pajak1']) + str_ireplace(".", "", $data_get['t_pajak2']) + str_ireplace(".", "", $data_get['t_pajak3']) + str_ireplace(".", "", $data_get['t_pajak4']) + str_ireplace(".", "", $data_get['t_pajak5']) + str_ireplace(".", "", $data_get['t_pajak6']) + str_ireplace(".", "", $data_get['t_pajak7']);
        $t_pajak = $t_jmlhpajak;
        $t_jmlhkenaikan = 0;
        if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
            $t_jmlhkenaikan = ($t_pajak * $data_get['t_tarifkenaikan'] / 100);
            $t_pajak = $t_pajak + $t_jmlhkenaikan;
        }
        $data = array(
            // 't_jmlhpajak' => number_format($t_jmlhpajak, 0, ",", "."),
            't_dasarpengenaan' => number_format($t_jumlah, 0, ",", "."),
            't_jmlhpajak' => number_format($t_pajak, 0, ",", "."),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungpajakwaletAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $t_jumlah = str_ireplace(".", "", $data_get['t_nilaiperolehan']) * str_ireplace(".", "", $data_get['t_tarifdasarkorek']);
        $t_pajak = $t_jumlah * $data_get['t_tarifpajak'] / 100;

        $t_jmlhkenaikan = 0;
        if (!empty($data_get['t_tarifkenaikan']) || $data_get['t_tarifkenaikan'] != 0) {
            $t_jmlhkenaikan = ($t_pajak * $data_get['t_tarifkenaikan'] / 100);
            $t_pajak = $t_pajak + $t_jmlhkenaikan;
        }
        $data = array(
            't_dasarpengenaan' => number_format($t_jumlah, 0, ",", "."),
            't_jmlhpajak' => number_format($t_pajak, 0, ",", "."),
            "t_jmlhkenaikan" => number_format($t_jmlhkenaikan, 0, ",", ".")
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function cetakhimbauanAction() {
        /** Cetak Himbauan
         * @param int $t_idwp
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 02/01/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
// Mengambil Data WP
        $data = $this->Tools()->getService('PendataanTable')->getDataPendataanID($data_get['t_idtransaksi']);

// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'data' => $data,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd
        ));
        $pdf->setOption("paperSize", "potrait");
        return $pdf;
    }

    public function cetaknppdselfAction() {
        /** Cetak NPPD Self Assesment
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 22/01/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
// Mengambil Data WP, OP dan Transaksi
        $data = $this->Tools()->getService('PendataanTable')->getDataNppdObjek($data_get['idwpobjeknppd']);
        $datanppd = $this->Tools()->getService('PendataanTable')->getDataNppdSelf($data_get['idwpobjeknppd']);
// Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'datanppd' => $datanppd,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }

    public function cetaknppdofficialAction() {
        /** Cetak NPPD Official Assesment
         * @param int $idtransaksi
         * @author Miftahul Huda <miftahul06@gmail.com>
         * @date 22/01/2016
         */
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP, OP dan Transaksi
        $data = $this->Tools()->getService('PendataanTable')->getDataNppdObjek($data_get['idwpobjeknppd']);
        $datanppd = $this->Tools()->getService('PendataanTable')->getDataNppdOfficial($data_get['idwpobjeknppd']);
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'datanppd' => $datanppd,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }

    public function pilihskpdjabatanAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data_render = array(
            "t_tglpendataan" => date('01-m-Y', strtotime($data_get['parameter'] . '+1 month')),
            "t_masaawal" => date('d-m-Y', strtotime($data_get['parameter'])),
            "t_masaakhir" => date('t-m-Y', strtotime($data_get['parameter'])),
            "t_tarifkenaikan" => 25
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataSuratTeguranAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('ObjekTable')->getObjekPajakbyIdObjek($data_get['idobjekwp']);
        $data_render = array(
            "idobjekwp" => $data_get['idobjekwp'],
            "t_masaawal" => date('d-m-Y', strtotime($data_get['parameter'])),
            "t_masaakhir" => date('t-m-Y', strtotime($data_get['parameter'])),
            "namawp" => $data['t_nama'],
            "alamatwp" => $data['t_alamatlengkapobjek'],
            "jenispajak" => $data['s_namajenis'],
            "idjenispajak" => $data['t_jenisobjek'],
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cetaksuratteguranAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $data = $this->Tools()->getService('ObjekTable')->getObjekPajakbyIdObjek($data_get['idobjekwp']);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'tglpendataan0' => $data_get->tglpendataan0,
            'tglpendataan1' => $data_get->tglpendataan1,
            'masapajak' => date('d-m-Y', strtotime($data_get->masaawal)) . ' s/d ' . date('d-m-Y', strtotime($data_get->masaakhir)),
            'nomor_surat' => $data_get->nomorsrt,
            'tglcetak' => $data_get->tglcetak,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
        ));
        $pdf->setOption("paperSize", "potrait");
        return $pdf;
    }

    public function cetakskpdAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP
        if ($data_get['jenisobjek'] == 4) {
            $data = $this->Tools()->getService('PenetapanTable')->getDataPenetapanReklame($data_get['idtransaksi']);

            if ($data['t_jenisreklame'] == 1 || $data['t_jenisreklame'] == 7 || $data['t_jenisreklame'] == 11) {
                $jeniskelas = $data['t_kelasjalan'];
                $tipewaktu = 'Tahun';
            } else if ($data['t_jenisreklame'] == 2) {
                $jeniskelas = $data['t_kelasjalan'];
                $tipewaktu = 'Bulan';
            } else if ($data['t_jenisreklame'] == 4) {
                $jeniskelas = $data['t_jenisselebaran'];
                $tipewaktu = 'Bulan';
            } else if ($data['t_jenisreklame'] == 5) {
                $jeniskelas = $data['t_jeniskendaraan'];
                $tipewaktu = 'Bulan';
            } else if ($data['t_jenisreklame'] == 6) {
                $jeniskelas = null;
                $tipewaktu = 'Bulan';
            } else if ($data['t_jenisreklame'] == 8) {
                $jeniskelas = $data['t_jenissuara'];
                $tipewaktu = 'Menit';
            } else if ($data['t_jenisreklame'] == 9) {
                $jeniskelas = $data['t_jenisfilm'];
                $tipewaktu = 'Menit';
            } else if ($data['t_jenisreklame'] == 10) {
                $jeniskelas = $data['t_jenisperagaan'];
                $tipewaktu = 'Minggu';
            } else {
                $jeniskelas = $data['t_kelasjalan'];
                $tipewaktu = 'Bulan';
            }

            $data_reklame = $this->Tools()->getService('ReklameTable')->getDataTarifReklame($data['t_jenisreklame'], $jeniskelas, null);
            $ar_jenisreklame = $this->Tools()->getService('ReklameTable')->getDataJenisReklame($data['t_jenisreklame']);
            // $tarif_nspr = $this->Tools()->getService('ReklameTable')->getTarifNSPR($data['t_klasifikasi_jalan']);
            // $tarif_kawasan = $this->Tools()->getService('ReklameTable')->getDataZona($data['t_wilayah']);
            // $tarif_tinggi = $this->Tools()->getService('ReklameTable')->getTarifTinggi($data['t_tinggi']);
        } elseif ($data_get['jenisobjek'] == 8) {
            $data = $this->Tools()->getService('PenetapanTable')->getDataPenetapanABT($data_get['idtransaksi']);
        }
        //    var_dump($peruntukan_air);
        //    exit();
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd0 = (!empty($data_get['ttd0'])) ? $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']) : '';
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'ar_pemda' => $ar_pemda,
            'ar_ttd0' => $ar_ttd0,
            'jenisobjek' => $data_get['jenisobjek'],
            'ar_jenisreklame' => $ar_jenisreklame,
            'data_reklame' => $data_reklame,
            'tipewaktu' => $tipewaktu
        ));
        $pdf->setOption("paperSize", "legal");
        return $pdf;
    }

    public function cetakskrdAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        // Mengambil Data WP
        $data = $this->Tools()->getService('PendataanTable')->getPendataanRetribusi($data_get['idtransaksi']);
        if ($data_get['jenisobjek'] == 10) {
            $datadetail = $this->Tools()->getService('PendataanTable')->getSewaDinas($data_get['idtransaksi']);
        } elseif ($data_get['jenisobjek'] == 11) {
            $datadetail = $this->Tools()->getService('PendataanTable')->getPanggungReklame($data_get['idtransaksi']);
        } elseif ($data_get['jenisobjek'] == 12) {
            $datadetail = $this->Tools()->getService('PendataanTable')->getTanahReklame($data_get['idtransaksi']);
        } elseif ($data_get['jenisobjek'] == 13) {
            $datadetail = $this->Tools()->getService('PendataanTable')->getTanahLain($data_get['idtransaksi']);
        } elseif ($data_get['jenisobjek'] == 14) {
            $datadetail = $this->Tools()->getService('PendataanTable')->getGedungOlahraga($data_get['idtransaksi']);
        }
        // Mengambil Data Pemda
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd0 = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttd0']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'datadetail' => $datadetail,
            'ar_pemda' => $ar_pemda,
            'ar_ttd0' => $ar_ttd0,
            'jenisobjek' => $data_get['jenisobjek']
        ));
        $pdf->setOption("paperSize", "A4");
        return $pdf;
    }

    public function cetakdaftarreklameAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new PendataanBase();
        $base->exchangeArray($allParams);
        $data = $this->Tools()->getService('PendataanTable')->getDaftarReklame($base);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'ar_pemda' => $ar_pemda
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }

    public function formattglAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $tlg = (int) substr($data_get->tgl, 0, 2);

        $bln = (int) substr($data_get->tgl, 2, 2);
        if ($bln < 1) {
            $bln = 1;
        } elseif ($bln > 12) {
            $bln = 12;
        }

        $thn = substr($data_get->tgl, 4, 5);
        $tglfix = str_pad($tlg, 2, "0", STR_PAD_LEFT) . "-" . str_pad($bln, 2, "0", STR_PAD_LEFT) . "-20" . $thn;
        if ($tlg < 1) {
            $tglfix = "01-" . str_pad($bln, 2, "0", STR_PAD_LEFT) . "-20" . $thn;
        } elseif ($tlg > 28) {
            $tglfix0 = "28-" . str_pad($bln, 2, "0", STR_PAD_LEFT) . "-20" . $thn;
            $tglfix = date('t-m-Y', strtotime($tglfix0));
        }
        $data = array(
            'tgl' => $tglfix
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function hitungtglAction() {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $tlg = (int) substr($data_get->t_masaawal, 0, 2);

        $bln = (int) substr($data_get->t_masaawal, 2, 2);
        if ($bln < 1) {
            $bln = 1;
        } elseif ($bln > 12) {
            $bln = 12;
        }

        $thn = substr($data_get->t_masaawal, 4, 5);
        $t_masaawal = str_pad($tlg, 2, "0", STR_PAD_LEFT) . "-" . str_pad($bln, 2, "0", STR_PAD_LEFT) . "-20" . $thn;
        if ($tlg < 1) {
            $t_masaawal = "01-" . str_pad($bln, 2, "0", STR_PAD_LEFT) . "-20" . $thn;
        } elseif ($tlg > 28) {
            $t_masaawal0 = "28-" . str_pad($bln, 2, "0", STR_PAD_LEFT) . "-20" . $thn;
            $t_masaawal = date('t-m-Y', strtotime($t_masaawal0));
        }

        $t_masaakhir = date('t-m-Y', strtotime($t_masaawal));
        $data = array(
            't_masaakhir' => $t_masaakhir,
            't_masaawal' => $t_masaawal
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function cetakregisterpajakAction() {
        $req = $this->getRequest();
        // $data_get = $req->getPost();
        $data_get = $req->getQuery();
        // var_dump($data_get);exit();
        // tglcetak
        // tglinput0
        // tglinput1
        // s_idjenis
        // t_rekening
        // t_kecamatan
        // t_kelurahan
        // t_statusbayar
        // t_viabayar
        // diperiksa
        // mengetahui
        // formatcetak
        $data = $this->Tools()->getService('PendataanTable')->getDaftarRegisterPajak(
                $data_get['tglinput0']
                , $data_get['tglinput1']
                , $data_get['s_idjenis']
                , $data_get['t_rekening']
                , $data_get['t_kecamatan']
                , $data_get['t_kelurahan']
                , $data_get['t_statusbayar']
                , $data_get['t_viabayar']);
        $ar_jenis = $this->Tools()->getService('ObjekTable')->getJenisObjek($data_get['s_idjenis']);
        $ar_rekening = (!empty($data_get['t_rekening'])) ? $this->Tools()->getService('RekeningTable')->getdataRekeningId($data_get['t_rekening']) : NULL;
        $ar_kecamatan = (!empty($data_get['t_kecamatan'])) ? $this->Tools()->getService('KecamatanTable')->getDataId($data_get['t_kecamatan']) : NULL;
        $ar_kelurahan = (!empty($data_get['t_kelurahan'])) ? $this->Tools()->getService('KelurahanTable')->getDataId($data_get['t_kelurahan']) : NULL;
        $ar_mengetahui = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['mengetahui']);
        $ar_diperiksa = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['diperiksa']);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();

        if ($data_get['formatcetak'] == 'pdf') {
            $pdf = new \LosPdf\View\Model\PdfModel();
            $pdf->setVariables(array(
                'data' => $data,
                'data_jenis' => $ar_jenis,
                'data_rekening' => (!empty($ar_rekening)) ? 'Rekening : ' . $ar_rekening['korek'] . ' - ' . $ar_rekening['s_namakorek'] . '<br>' : '',
                'data_kecamatan' => (!empty($ar_kecamatan)) ? 'Kecamatan : ' . $ar_kecamatan->s_namakec . '<br>' : '',
                'data_kelurahan' => (!empty($ar_kelurahan)) ? 'Kelurahan ' . $ar_kelurahan->s_namakel . '<br>' : '',
                'ar_pemda' => $ar_pemda,
                'ar_mengetahui' => $ar_mengetahui,
                'ar_diperiksa' => $ar_diperiksa,
                'tglpencetakan' => $data_get['tglinput0'] . ' s.d. ' . $data_get['tglinput1'],
                'formatcetak' => $data_get->formatcetak
            ));
            $pdf->setOption("paperSize", "legal-L");
            return $pdf;
        } elseif ($data_get['formatcetak'] == 'excel') {
            $view = new ViewModel(array(
                'data' => $data,
                'data_jenis' => $ar_jenis,
                'data_rekening' => (!empty($ar_rekening)) ? 'Rekening : ' . $ar_rekening['korek'] . ' - ' . $ar_rekening['s_namakorek'] . '<br>' : '',
                'data_kecamatan' => (!empty($ar_kecamatan)) ? 'Kecamatan : ' . $ar_kecamatan->s_namakec . '<br>' : '',
                'data_kelurahan' => (!empty($ar_kelurahan)) ? 'Kelurahan ' . $ar_kelurahan->s_namakel . '<br>' : '',
                'ar_pemda' => $ar_pemda,
                'ar_mengetahui' => $ar_mengetahui,
                'ar_diperiksa' => $ar_diperiksa,
                'tglpencetakan' => $data_get['tglinput0'] . ' s.d. ' . $data_get['tglinput1'],
                'formatcetak' => $data_get->formatcetak
            ));
            $data = array('nilai' => '3');
            $this->layout()->setVariables($data);
            return $view;
        }
    }

    public function cetakpendterimadimukaAction() {
        $req = $this->getRequest();
        // $data_get = $req->getPost();
        $data_get = $req->getQuery();

        $ar_diperiksa = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['diperiksa']);
        $ar_mengetahui = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['mengetahui']);
        $data = $this->Tools()->getService('PendataanTable')->getDataTransaksiByPerMasaAkhirpajak($data_get['tglcetak'], $data_get['periodepajak'], $data_get['s_idjenispenetapan']);
        // var_dump($data);exit();
        $ar_jenis = $this->Tools()->getService('ObjekTable')->getJenisObjek($data_get['s_idjenispenetapan']);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            'data_jenis' => $ar_jenis,
            'ar_pemda' => $ar_pemda,
            'ar_diperiksa' => $ar_diperiksa,
            'ar_mengetahui' => $ar_mengetahui,
            'tglcetak' => $data_get['tglcetak'],
            'periode' => $data_get['periodepajak']
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }

    public function cetakdaftarblmlaporAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $datajenispajak = $this->Tools()->getService('PendataanTable')->getJenisPajak($data_get->s_idjenis);
        $data = $this->Tools()->getService('PendataanTable')->getdaftarbelumlapor($data_get->idobjekbelumlapor);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'datajenispajak' => $datajenispajak,
            'data' => $data,
            'bulan' => $data_get->bulanmasapajak,
            'tahun' => $data_get->tahunmasapajak,
            'ar_pemda' => $ar_pemda
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }

    public function cetakexportpendataanAction() {
        $req = $this->getRequest();
        // $data_get = $req->getPost();
        $data_get = $req->getQuery();
//        var_dump($data_get); exit();
        $data = $this->Tools()->getService('PendataanTable')->getDataExport($data_get->s_idjenis, $data_get);
        $ar_jenis = $this->Tools()->getService('ObjekTable')->getJenisObjek($data_get->s_idjenis);

        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        if ($data_get->formatcetak == 'pdf') {
            $pdf = new \LosPdf\View\Model\PdfModel();
            $pdf->setVariables(array(
                'data' => $data,
                'ar_jenis' => $ar_jenis,
                'ar_pemda' => $ar_pemda,
                'formatcetak' => $data_get->formatcetak
            ));
            $pdf->setOption("paperSize", "legal-L");
            return $pdf;
        } elseif ($data_get->formatcetak == 'excel') {
            $view = new ViewModel(array(
                'data' => $data,
                'ar_jenis' => $ar_jenis,
                'ar_pemda' => $ar_pemda,
                'formatcetak' => $data_get->formatcetak
            ));
            $data = array('nilai' => '3');
            $this->layout()->setVariables($data);
            return $view;
        }
    }

}
