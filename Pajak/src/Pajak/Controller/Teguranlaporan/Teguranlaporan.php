<?php

namespace Pajak\Controller\Teguranlaporan;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Pajak\Form\Pendataan\PendataanFrm;
use Pajak\Model\Pendataan\PendataanBase;
use Zend\Barcode\Barcode;

class Teguranlaporan extends AbstractActionController {

    public function indexAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $form = new \Pajak\Form\Pendaftaran\PendaftaranFrm($this->Tools()->getService('PendaftaranTable')->getcomboIdKecamatan(), null);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $s_idjenis = $this->getEvent()->getRouteMatch()->getParam('s_idjenis');
        $jenisobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjekId($s_idjenis);
        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }

        $dataRekOff = $this->Tools()->getService('RekeningTable')->getdataRekOff();
        $ar_kecamatan = $this->Tools()->getService('KecamatanTable')->getdata();
        $recordskecamatan = array();
        foreach ($ar_kecamatan as $ar_kecamatan) {
            $recordskecamatan[] = $ar_kecamatan;
        }
        $descPendaftaran = $this->Tools()->getService('PendaftaranTable')->getDescTablePendaftaran();
        $recordspendaftaran = array();
        foreach ($descPendaftaran as $descPendaftaran) {
            $recordspendaftaran[] = $descPendaftaran;
        }
        $descPendaftaranOP = $this->Tools()->getService('PendaftaranTable')->getDescTablePendaftaranop();
        $recordspendaftaranOP = array();
        foreach ($descPendaftaranOP as $descPendaftaranOP) {
            $recordspendaftaranOP[] = $descPendaftaranOP;
        }
        $descTransaksi = $this->Tools()->getService('PenetapanTable')->getDescTableTransaksi();
        $recordstransaksi = array();
        foreach ($descTransaksi as $descTransaksi) {
            $recordstransaksi[] = $descTransaksi;
        }
        $dataobjekOff = $this->Tools()->getService('RekeningTable')->getdataJenisObjekOff();
        $recordsdataobjekOff = array();
        foreach ($dataobjekOff as $dataobjekOff) {
            $recordsdataobjekOff[] = $dataobjekOff;
        }
        $view = new ViewModel(array(
            'form' => $form,
            's_idjenis' => $s_idjenis,
            'jenisobjek' => $jenisobjek,
            'ar_ttd0' => $recordspejabat,
            'dataRekOff' => $dataRekOff,
            'dataobjekOff' => $recordsdataobjekOff,
            'ar_kecamatan' => $recordskecamatan,
            'descPendaftaran' => $recordspendaftaran,
            'descPendaftaranOP' => $recordspendaftaranOP,
            'descTransaksi' => $recordstransaksi,
        ));
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }

        $belumditetapkan = $this->Tools()->getService('PenetapanTable')->getBelumDitetapkan();
        $data = array(
            'belumditetapkan' => $belumditetapkan,
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridSuratTeguranAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $post = $req->getPost();
        $base = new \Pajak\Model\Pendataan\TeguranBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('TeguranTable')->getGridCountSuratTeguran($base, $post);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('TeguranTable')->getGridDataSuratTeguran($base, $start, $post);
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td><center>" . $counter . "</center></td>";
            $s .= "<td><input type='checkbox' name='t_idteguran' id='t_idteguran' value='" . $row['t_idteguran'] . "'/></td>";
            $s .= "<td><center style='color:red; font-size:12px; font-weight:bold'>" . $row['t_nop'] . "</center></td>";
            $s .= "<td>" . $row['t_namaobjek'] . "</td>";
            $s .= "<td>" . $row['t_alamatlengkapobjek'] . "</td>";
            $s .= "<td>" . $row['t_npwpdwp'] . "</td>";
            $s .= "<td>" . $row['t_namawp'] . "</td>";
            $s .= "<td>" . $row['s_namajenis'] . "</td>";
            $s .= "<td>" . $row['t_bulanpajak'] . " / ". $row['t_tahunpajak'] . "</td>";
            $s .= "<td><center><b style='color:red'>" . $row['t_noteguran'] . "<br>" . date('d-m-Y', strtotime($row['t_tglteguran'])) . "</b></center></td>";
            $s .= "</tr>";
            $counter++;
        }
        $s .= "<script type='text/javascript'>
                    $(document).ready(function () {
                        $('.tableTeguran tr').click(function (event) {
                            if (event.target.type !== 'checkbox') {
                                $(':checkbox', this).trigger('click');
                            }
                        }); 
                    });
                </script>";
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "gridwp" => $s,
            "rowswp" => $base->rows,
            "countwp" => $count,
            "pagewp" => $page,
            "startwp" => $start,
            "total_halamanwp" => $total_pages,
            "paginatorewp" => $datapaging['paginatore'],
            "akhirdatawp" => $datapaging['akhirdata'],
            "awaldatawp" => $datapaging['awaldata']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cetaksuratteguranAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $data = $this->Tools()->getService('ObjekTable')->getObjekPajakbyIdTeguranArray($data_get['idobjekdbelumlapor']);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttdmasalteguran']);
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'dataall' => $data,
            'tglpendataan0' => $data_get->tglpendataan0,
            'tglpendataan1' => $data_get->tglpendataan1,
            'tglcetak' => $data_get->tglcetak,
            'ar_pemda' => $ar_pemda,
            'ar_ttd' => $ar_ttd,
        ));
        $pdf->setOption("paperSize", "potrait");
        return $pdf;
    }
    
    public function cetakdaftarteguranAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $data = $this->Tools()->getService('ObjekTable')->getObjekPajakbyIdTeguranArray($data_get['idobjekdbelumlapor']);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get['ttdmasalteguran']);
        
        if ($data_get->formatcetak == 'pdf') {
            $pdf = new \LosPdf\View\Model\PdfModel();
            $pdf->setVariables(array(
                'dataall' => $data,
                'tglpendataan0' => $data_get->tglpendataan0,
                'tglpendataan1' => $data_get->tglpendataan1,
                'tglcetak' => $data_get->tglcetak,
                'ar_pemda' => $ar_pemda,
                'ar_ttd' => $ar_ttd,
                'formatcetak' => $data_get->formatcetak
            ));
            $pdf->setOption("paperSize", "legal-l");
            return $pdf;
        }elseif ($data_get->formatcetak == 'excel') {
            $view = new ViewModel(array(
                'dataall' => $data,
                'tglpendataan0' => $data_get->tglpendataan0,
                'tglpendataan1' => $data_get->tglpendataan1,
                'tglcetak' => $data_get->tglcetak,
                'ar_pemda' => $ar_pemda,
                'ar_ttd' => $ar_ttd,
                'formatcetak' => $data_get->formatcetak
            ));
            $data = array('nilai' => '3');
            $this->layout()->setVariables($data);
            return $view;
        }
        
    }

}
