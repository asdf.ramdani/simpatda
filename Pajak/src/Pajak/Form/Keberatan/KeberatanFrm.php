<?php

namespace Pajak\Form\Keberatan;

use Zend\Form\Form;

class KeberatanFrm extends Form {

    public function __construct() {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 't_idkeberatan',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idkeberatan',
            )
        ));        
        
        $this->add(array(
            'name' => 't_idketetapan',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idketetapan',
            )
        ));        
        
        $this->add(array(
            'name' => 't_idwpobjek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idwpobjek',
            )
        ));        
        
        $this->add(array(
            'name' => 't_jenispajak',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_jenispajak',
            )
        ));

        $this->add(array(
            'name' => 't_jenisketetapan',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenisketetapan',
                'class' => 'form-control',
                'required' => true,
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => array(
                    '2' => 'SKPD',
                    '5' => 'SKPDKB',
                    '6' => 'SKPDKBT',
                    '10' => 'SKPDT'
                ),
                'disable_inarray_validator' => true, 
            )
        ));

        $this->add(array(
            'name' => 't_tglketetapankeberatan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglketetapankeberatan',
                'class' => 'form-control input-mask bootstrap-datepicker',
                'required' => true,
                'readonly' => true,
                'value' => date('d-m-Y'),
                'data-inputmask' => "'mask':'99-99-9999'"
            )
        ));

        $this->add(array(
            'name' => 't_nilaipengurangan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nilaipengurangan',
                'class' => 'form-control',
//                'onchange' => 'hitungketetapan()'
            ),
//            'options' => array(
//                'empty_option' => 'Silahkan Pilih',
//                'value_options' => array(
//                    '0' => '0%',
//                    '15' => '15%',
//                    '20' => '20%',
//                    '25' => '25%',
//                    '30' => '30%'
//                ),
//                'disable_inarray_validator' => true, 
//            )
        ));

        $this->add(array(
            'name' => 't_tglverifikasi',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglverifikasi',
                'class' => 'form-control bootstrap-datepicker',
                'readonly' => true,
                'value' => date('d-m-Y'),
            )
        ));
        
		$this->add(array(
            'name' => 't_nomorsk',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nomorsk',
                'class' => 'form-control',
                'required' => true
            )
        ));
		
        $this->add(array(
            'name' => 't_alasankeberatan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_alasankeberatan',
                'class' => 'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 't_jmlhpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlhpajak',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right; background:blue; color: white; padding: 7px 10px; height:40px; font-size: 16px; font-weight:bolder'
            )
        ));

        $this->add(array(
            'name' => 't_jmlhpengurangan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlhpengurangan',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right; background:orange; color: white; padding: 7px 10px; height:40px; font-size: 16px; font-weight:bolder'
            )
        ));
        
        $this->add(array(
            'name' => 't_jmlhditetapkan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlhditetapkan',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right; background:green; color: white; padding: 7px 10px; height:40px; font-size: 16px; font-weight:bolder'
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Simpan',
                'id' => 'submit',
                'class' => "btn btn-warning btn-block"
            )
        ));
    }

}
