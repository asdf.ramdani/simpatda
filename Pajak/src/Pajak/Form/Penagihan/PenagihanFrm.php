<?php

namespace Pajak\Form\Penagihan;

use Zend\Form\Form;

class PenagihanFrm extends Form {

    public function __construct() {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 't_tglpiutang',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglpiutang',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true,
                'readonly' => true,
                'value' => date('d-m-Y')
            )
        ));

        $this->add(array(
            'name' => 't_idpiutang',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idpiutang',
            )
        ));     
        
        $this->add(array(
            'name' => 't_tahunpiutang',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tahunpiutang',
                'class' => 'form-control',
                'readonly' => true,
                'value' => date('Y')-1
            )
        ));
        
        $this->add(array(
            'name' => 't_jenispajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jenispajak',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'color:blue;'
            )
        ));
        
        $this->add(array(
            'name' => 'Piutangsubmit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Tetapkan Piutang',
                'id' => 'Piutangsubmit',
                'class' => "btn btn-warning btn-block"
            )
        ));
    }

}
