<?php

namespace Pajak\Form\Pendataan;

use Zend\Form\Form;
use Zend\Form\Element\Select;

class PendataanairFrm extends Form {

    public function __construct($comboid_zona = null, $comboid_kelompok = null) { 
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 't_idtransaksi',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idtransaksi',
            )
        ));
        
        $this->add(array(
            'name' => 't_idair',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idair',
            )
        ));
        $this->add(array(
            'name' => 't_tarifdasarkorek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_tarifdasarkorek',
            )
        ));

        $this->add(array(
            'name' => 't_perhitungan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_perhitungan',
                'class' => 'form-control',
                'readonly' => true,
            )
        ));

        $this->add(array(
            'name' => 't_idobjek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idobjek',
            )
        ));

        $this->add(array(
            'name' => 't_idkorek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idkorek',
            )
        ));

        $this->add(array(
            'name' => 't_jenispajak',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_jenispajak',
            )
        ));

        $this->add(array(
            'name' => 't_operatorpendataan',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_operatorpendataan',
            )
        ));

        $this->add(array(
            'name' => 't_nourut',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nourut',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));

        $this->add(array(
            'name' => 't_periodepajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_periodepajak',
                'class' => 'form-control',
                'value' => date('Y'),
                'onchange' => 'CariPendataanByObjek();',
                'onblur' => 'CariPendataanByObjek();'
            )
        ));

        $this->add(array(
            'name' => 't_tglpendataan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglpendataan',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_masaawal',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_masaawal',
                'class' => 'form-control', //bootstrap-datepicker 
				// 'onchange' => 'tentukanMasa();',
                'required' => true,
                // 'readonly' => true,
                'placeholder' => 'ddmmyy',
                'onKeyPress' => "return numbersonly(this, event);",
                'maxlength' => '6',
            )
        ));

        $this->add(array(
            'name' => 't_masaakhir',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_masaakhir',
                'class' => 'form-control', //bootstrap-datepicker 
                // 'onchange' => 'tentukanMasa();',
                'required' => true,
                'readonly' => true,
                'placeholder' => 'ddmmyy',
                'onKeyPress' => "return numbersonly(this, event);",
                'maxlength' => '6',
            )
        ));

        $this->add(array(
            'name' => 't_korek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_korek',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_namakorek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namakorek',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_tarifpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarifpajak',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));

        
        $this->add(array(
            'name' => 't_hargadasarair',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_hargadasarair',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));
        
        $this->add(array(
            'name' => 't_jmlhpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlhpajak',
                'class' => 'form-control',
                'required' => true,
//                'readonly' => true,
                'style' => 'text-align:right; background:#000099; color: white; padding: 7px 10px; height:40px; font-size: 16px; font-weight:bolder',
                'onchange' => 'this.value = formatCurrency(this.value);',
                'onblur' => 'this.value = formatCurrency(this.value);',
                'onkeyup' => 'this.value = formatCurrency(this.value);',
                'onKeyPress' => "return numbersonly(this, event);",
            )
        ));
        
        $this->add(array(
            'name' => 't_jenissumur',
            'type' => Select::class,
            'attributes' => array(
                'id' => 't_jenissumur',
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => '-- Pilih Jenis Sumur --',
                'value_options' => array(
                    1 => '1 || Sumur Bor',
                    2 => '2 || Sumur Pantek'
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_sumurke',
            'type' => 'number',
            'attributes' => array(
                'id' => 't_sumurke',
                'class' => 'form-control',
                'required' => true,
                'value' => 1
            )
        ));
        
        $this->add(array(
            'name' => 't_lokasisumur',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_lokasisumur',
                'class' => 'form-control',
            )
        ));
        
        $this->add(array(
            'name' => 't_zona',
            'type' => Select::class,
            'attributes' => array(
                'id' => 't_zona',
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => '-- Pilih Zona --',
                'value_options' => $comboid_zona,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_kodekelompok',
            'type' => Select::class,
            'attributes' => array(
                'id' => 't_kodekelompok',
                'class' => 'form-control',
                'onchange' => 'CariKodeJenis()'
            ),
            'options' => array(
                'empty_option' => '-- Pilih Kelompok --',
                'value_options' => $comboid_kelompok,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_kodejenis',
            'type' => Select::class,
            'attributes' => array(
                'id' => 't_kodejenis',
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => '-- Pilih Jenis --',
                'value_options' => array(),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_volume',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_volume',
                'class' => 'form-control',
                'required' => true,
                'style' => 'text-align:right',
                'onchange' => 'hitungpajakair();',
                'onblur' => 'hitungpajakair();',
                'onkeyup' => 'hitungpajakair();'
            )
        ));
        
        $this->add(array(
            'name' => 't_fna',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_fna',
                'class' => 'form-control',
                'value' => 0,
                'style' => 'text-align:right',
            )
        ));
        
        $this->add(array(
            'name' => 't_volumem3',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_volumem3',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));
        
        $this->add(array(
            'name' => 't_totalnpa',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_totalnpa',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));
        
        $this->add(array(
            'name' => 't_volumeair0',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_volumeair0',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_hargadasar0',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_hargadasar0',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_tarif0',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarif0',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_jumlah0',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlah0',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_volumeair1',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_volumeair1',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_hargadasar1',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_hargadasar1',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_tarif1',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarif1',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_jumlah1',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlah1',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_volumeair2',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_volumeair2',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_hargadasar2',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_hargadasar2',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_tarif2',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarif2',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_jumlah2',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlah2',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_volumeair3',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_volumeair3',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_hargadasar3',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_hargadasar3',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_tarif3',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarif3',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_jumlah3',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlah3',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_volumeair4',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_volumeair4',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_hargadasar4',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_hargadasar4',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_tarif4',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarif4',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_jumlah4',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlah4',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_volumeair5',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_volumeair5',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_hargadasar5',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_hargadasar5',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_tarif5',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarif5',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_jumlah5',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlah5',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));
        
        $this->add(array(
            'name' => 't_volumeair6',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_volumeair6',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_hargadasar6',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_hargadasar6',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_tarif6',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarif6',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_jumlah6',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlah6',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
            )
        ));

        $this->add(array(
            'name' => 't_tglpenetapan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglpenetapan',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 'Pendataansubmit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Simpan',
                'id' => 'Pendataansubmit',
                'class' => "btn btn-warning btn-block"
            )
        ));
    }

}
