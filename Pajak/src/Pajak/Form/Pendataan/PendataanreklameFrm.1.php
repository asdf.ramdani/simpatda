<?php

namespace Pajak\Form\Pendataan;

use Zend\Form\Form;

class PendataanreklameFrm extends Form {

    public function __construct($comboid_jenisrek = null, $comboid_sudutpandang = null, $comboid_zona = null, $comboid_klasifikasi_jalan = null) {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 't_idtransaksi',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idtransaksi',
            )
        ));
        $this->add(array(
            'name' => 't_iddetailreklame',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_iddetailreklame',
            )
        ));

        $this->add(array(
            'name' => 't_idobjek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idobjek',
            )
        ));

        $this->add(array(
            'name' => 't_idkorek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idkorek',
            )
        ));

        $this->add(array(
            'name' => 't_jenispajak',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_jenispajak',
            )
        ));

        $this->add(array(
            'name' => 't_operatorpendataan',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_operatorpendataan',
            )
        ));

        $this->add(array(
            'name' => 't_nourut',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nourut',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));

        $this->add(array(
            'name' => 't_nspr',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nspr',
                'class' => 'form-control',
                'readonly' => true,
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_njopr',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_njopr',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
                'onchange' => 'hitungpajakreklame();this.value = formatCurrency(this.value);',
                'onblur' => 'hitungpajakreklame();this.value = formatCurrency(this.value);',
                'onkeyup' => 'hitungpajakreklame();this.value = formatCurrency(this.value);',
                'onKeyPress' => "return numbersonly(this, event);",
            )
        ));

        $this->add(array(
            'name' => 't_nsr',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nsr',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));

        $this->add(array(
            'name' => 't_periodepajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_periodepajak',
                'class' => 'form-control',
                'value' => date('Y')
            )
        ));

        $this->add(array(
            'name' => 't_tglpendataan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglpendataan',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_masaawal',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_masaawal',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true,
                // 'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_masaakhir',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_masaakhir',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true,
                // 'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_korek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_korek',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_namakorek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namakorek',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_jenisreklame',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenisreklame',
                'class' => 'form-control',
                'onchange' => 'pilihRekening();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_jenisrek,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jenisreklameusaha',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenisreklameusaha',
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
//                    1 => 'Identitas Usaha',
                    1 => '1 || Produk Usaha (Non Rokok)',
                    2 => '2 || Produk Usaha (Rokok)',
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

		$this->add(array(
            'name' => 't_bentukreklame',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_bentukreklame',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
//                    1 => 'Identitas Usaha',
                    1 => '1 || Satu Muka',
                    2 => '2 || Dua Muka',
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
		
        $this->add(array(
            'name' => 't_naskah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_naskah',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_lokasi',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_lokasi',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_panjang',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_panjang',
                'class' => 'form-control',
                'required' => true,
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_lebar',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_lebar',
                'class' => 'form-control',
                'required' => true,
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_tinggi',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tinggi',
                'class' => 'form-control',
                'required' => true,
                'onchange' => 'hitungpajakreklame();'
            )
        ));
        
        $this->add(array(
            'name' => 't_arah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_arah',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_sudutpandang',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_sudutpandang',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_sudutpandang,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jumlah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlah',
                'class' => 'form-control',
                'required' => true,
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_jangkawaktu',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jangkawaktu',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_tarifreklame',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarifreklame',
                'class' => 'form-control',
                'required' => true,
                'style' => 'text-align:right',
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_tipewaktu',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_tipewaktu',
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
                   0 => 'Harian',
                   1 => 'Bulanan',
                   2 => 'Tahunan',
//                    3 => 'Triwulanan',
//                    4 => 'Semesteran',
//                    5 => 'Tahunan',
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_klasifikasi_jalan',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_klasifikasi_jalan',
                'class' => 'form-control',
                'required' => true
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_klasifikasi_jalan,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_wilayah',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_wilayah',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_zona,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));


//        $this->add(array(
//            'name' => 't_tarifpajak',
//            'type' => 'text',
//            'attributes' => array(
//                'id' => 't_tarifpajak',
//                'class' => 'form-control',
//                'required' => true,
//                'readonly' => true,
//                'style' => 'text-align:right'
//            )
//        ));

        $this->add(array(
            'name' => 't_jmlhpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlhpajak',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right; background:#000099; color: white; padding: 7px 10px; height:40px; font-size: 16px; font-weight:bolder'
            )
        ));

        $this->add(array(
            'name' => 't_tglpenetapan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglpenetapan',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true
            )
        ));

//        $this->add(array(
//            'name' => 't_kecamatanrek',
//            'type' => 'Zend\Form\Element\Select',
//            'attributes' => array(
//                'id' => 't_kecamatanrek',
//                'class' => 'form-control',
//                'onchange' => 'comboKelurahanCamat();'
//            ),
//            'options' => array(
//                'empty_option' => 'Silahkan Pilih',
//                'value_options' => $comboid_kecamatan,
//                'disable_inarray_validator' => true, // <-- disable
//            )
//        ));
//
//        $this->add(array(
//            'name' => 't_kelurahanrek',
//            'type' => 'Zend\Form\Element\Select',
//            'attributes' => array(
//                'id' => 't_kelurahanrek',
//                'class' => 'form-control'
//            ),
//            'options' => array(
//                'empty_option' => 'Silahkan Pilih',
//                'value_options' => $comboid_kelurahan,
//                'disable_inarray_validator' => true, // <-- disable
//            )
//        ));

        $this->add(array(
            'name' => 'Pendataansubmit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Simpan',
                'id' => 'Pendataansubmit',
                'class' => "btn btn-warning btn-block"
            )
        ));
    }

}
