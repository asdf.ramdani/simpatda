<?php

namespace Pajak\Form\Pendataan;

use Zend\Form\Form;

class PendataanreklameFrm extends Form {

    public function __construct($comboid_jenisrek = null, 
            $comboid_sudutpandang = null, 
            $comboid_zona = null, 
            $comboid_jenislokasi = null, 
            $comboid_lebarjalan = null,
            $comboid_ketinggian = null) {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 't_idtransaksi',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idtransaksi',
            )
        ));
        $this->add(array(
            'name' => 't_iddetailreklame',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_iddetailreklame',
            )
        ));

        $this->add(array(
            'name' => 't_idobjek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idobjek',
            )
        ));

        $this->add(array(
            'name' => 't_idkorek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idkorek',
            )
        ));

        $this->add(array(
            'name' => 't_jenispajak',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_jenispajak',
            )
        ));

        $this->add(array(
            'name' => 't_operatorpendataan',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_operatorpendataan',
            )
        ));

        $this->add(array(
            'name' => 't_nourut',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nourut',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));

        $this->add(array(
            'name' => 't_nspr',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nspr',
                'class' => 'form-control',
                'readonly' => true,
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_njopr',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_njopr',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right',
                'onchange' => 'hitungpajakreklame();this.value = formatCurrency(this.value);',
                'onblur' => 'hitungpajakreklame();this.value = formatCurrency(this.value);',
                'onkeyup' => 'hitungpajakreklame();this.value = formatCurrency(this.value);',
                'onKeyPress' => "return numbersonly(this, event);",
            )
        ));

        $this->add(array(
            'name' => 't_hargasatuan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_hargasatuan',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));
        
        $this->add(array(
            'name' => 't_nsl',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nsl',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));
        
        $this->add(array(
            'name' => 't_nsr',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nsr',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));

        $this->add(array(
            'name' => 't_periodepajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_periodepajak',
                'class' => 'form-control',
                'value' => date('Y')
            )
        ));

        $this->add(array(
            'name' => 't_tglpendataan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglpendataan',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_masaawal',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_masaawal',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_masaakhir',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_masaakhir',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_korek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_korek',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_namakorek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namakorek',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_jenisreklame',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenisreklame',
                'class' => 'form-control',
                'onchange' => 'jangkawaktureklame();hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_jenisrek,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_naskah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_naskah',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_lokasi',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_lokasi',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_panjang',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_panjang',
                'class' => 'form-control',
                'required' => true,
                'value' => 1,
                'onkeyup' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_lebar',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_lebar',
                'class' => 'form-control',
                'required' => true,
                'value' => 1,
                'onkeyup' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_luas',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luas',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
                'onkeyup' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_tinggi',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tinggi',
                'class' => 'form-control',
                'value' => 1,
                'required' => true,
                'onchange' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_arah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_arah',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_sudutpandang',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_sudutpandang',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
//                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_sudutpandang,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jumlah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlah',
                'class' => 'form-control',
                'required' => true,
                'value' => 1,
                'onkeyup' => 'hitungpajakreklame();'
            )
        ));

        $this->add(array(
            'name' => 't_tarifreklame',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarifreklame',
                'class' => 'form-control',
                'required' => true,
                'style' => 'text-align:right',
                'readonly' => true
            )
        ));
        
        $this->add(array(
            'name' => 't_tarifdasarkorek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarifdasarkorek',
                'class' => 'form-control',
                'required' => true,
                'style' => 'text-align:right',
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_jangkawaktu',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jangkawaktu',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
//                'empty_option' => 'Silahkan Pilih',
                'value_options' => [],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jangkawaktutext',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jangkawaktutext',
                'class' => 'form-control',
//                'required' => true,
                'value' => 1,
                'onkeyup' => 'hitungpajakreklame();'
            )
        ));
        
        $this->add(array(
            'name' => 't_jenislokasi',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenislokasi',
                'class' => 'form-control',
//                'required' => true,
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => '-- Pilih Jenis Lokasi --',
                'value_options' => $comboid_jenislokasi,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_lebarjalan',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_lebarjalan',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
//                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_lebarjalan,
//                array(
//                    1 => '<= 10',
//                    2 => '> 10 - 15',
//                    3 => '> 15 - 20',
//                    4 => '> 20'
//                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_ketinggian',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_ketinggian',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'value_options' => $comboid_ketinggian,
//                array(
//                    1 => '<= 10',
//                    2 => '> 10 - 11',
//                    3 => '> 11 - 13',
//                    4 => '> 13'
//                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_pemasangan',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_pemasangan',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'value_options' => array(
                    1 => 'Outdoor',
                    2 => 'Indoor'
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_konstruksi',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_konstruksi',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => '-- Pilih Konstruksi --',
                'value_options' => array(
                    1 => 'Menempel',
                    2 => 'Tersendiri'
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_bentukreklame',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_bentukreklame',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'value_options' => array(
                    1 => 'Persegi/Kubus',
                    2 => 'Segitiga',
                    3 => 'Lingkaran',
                    4 => 'Silinder'
                ),
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_jenisselebaran',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenisselebaran',
                'class' => 'form-control',
//                'required' => true,
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
                    1 => '01 || Berwarna',
                    2 => '02 || Tidak Berwarna'
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_jeniskendaraan',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jeniskendaraan',
                'class' => 'form-control',
//                'required' => true,
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
                    1 => '01 || Roda Dua',
                    2 => '02 || Roda Empat'
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_jenissuara',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenissuara',
                'class' => 'form-control',
//                'required' => true,
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
                    1 => '01 || Pemancar Televisi',
                    2 => '02 || Pemancar Radio',
                    3 => '03 || Alat Pengeras Suara Lainnya'
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jenisfilm',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenisfilm',
                'class' => 'form-control',
//                'required' => true,
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
                    1 => '01 || Slide Bersuara',
                    2 => '02 || Slide Tidak Bersuara',
                    3 => '01 || Film Bersuara',
                    4 => '02 || Film Tidak Bersuara'
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_jenisperagaan',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenisperagaan',
                'class' => 'form-control',
//                'required' => true,
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
                    1 => '01 || Peragaan dengan Suara',
                    2 => '02 || Peragaan tidak dengan Suara'
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_wilayah',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_wilayah',
                'class' => 'form-control',
                'onchange' => 'hitungpajakreklame();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_zona,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_satuanreklame',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_satuanreklame',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));
        
        $this->add(array(
            'name' => 't_tarifpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarifpajak',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));

        $this->add(array(
            'name' => 't_jmlhpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlhpajak',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right; background:#000099; color: white; padding: 7px 10px; height:40px; font-size: 14pt; font-weight:bolder'
            )
        ));

        $this->add(array(
            'name' => 't_tglpenetapan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglpenetapan',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true
            )
        ));

//        $this->add(array(
//            'name' => 't_kecamatanrek',
//            'type' => 'Zend\Form\Element\Select',
//            'attributes' => array(
//                'id' => 't_kecamatanrek',
//                'class' => 'form-control',
//                'onchange' => 'comboKelurahanCamat();'
//            ),
//            'options' => array(
//                'empty_option' => 'Silahkan Pilih',
//                'value_options' => $comboid_kecamatan,
//                'disable_inarray_validator' => true, // <-- disable
//            )
//        ));
//
//        $this->add(array(
//            'name' => 't_kelurahanrek',
//            'type' => 'Zend\Form\Element\Select',
//            'attributes' => array(
//                'id' => 't_kelurahanrek',
//                'class' => 'form-control'
//            ),
//            'options' => array(
//                'empty_option' => 'Silahkan Pilih',
//                'value_options' => $comboid_kelurahan,
//                'disable_inarray_validator' => true, // <-- disable
//            )
//        ));

        $this->add(array(
            'name' => 'Pendataansubmit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Simpan',
                'id' => 'Pendataansubmit',
                'class' => "btn btn-warning btn-block"
            )
        ));
    }

}
