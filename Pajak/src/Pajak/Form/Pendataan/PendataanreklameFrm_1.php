<?php

namespace Pajak\Form\Pendataan;

use Zend\Form\Form;

class PendataanreklameFrm extends Form {

    public function __construct($comboid_jenisrek = null, $comboid_sudutpandang = null, $comboid_kelompokjalan = null) {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 't_idtransaksi',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idtransaksi',
            )
        ));
        $this->add(array(
            'name' => 't_iddetailreklame',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_iddetailreklame',
            )
        ));

        $this->add(array(
            'name' => 't_idobjek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idobjek',
            )
        ));

        $this->add(array(
            'name' => 't_idkorek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idkorek',
            )
        ));

        $this->add(array(
            'name' => 't_jenispajak',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_jenispajak',
            )
        ));

        $this->add(array(
            'name' => 't_operatorpendataan',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_operatorpendataan',
            )
        ));

        $this->add(array(
            'name' => 't_nourut',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nourut',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));
        $this->add(array(
            'name' => 't_nsr',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nsr',
                'class' => 'form-control',
                'readonly' => true,
            )
        ));

        $this->add(array(
            'name' => 't_periodepajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_periodepajak',
                'class' => 'form-control',
                'value' => date('Y')
            )
        ));

        $this->add(array(
            'name' => 't_tglpendataan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglpendataan',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_masaawal',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_masaawal',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_masaakhir',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_masaakhir',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_korek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_korek',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_namakorek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namakorek',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_jenisreklame',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenisreklame',
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_jenisrek,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        $this->add(array(
            'name' => 't_jeniskendaraan',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jeniskendaraan',
                'class' => 'form-control',
                'required' => true
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
                    'Kendaraan Tidak Bermotor',
                    'Kendaraan Bermotor',
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        $this->add(array(
            'name' => 't_jenisfilmslide',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenisfilmslide',
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
                    0 => 'Film',
                    1 => 'Slide (Dengan Suara)',
                    2 => 'Slide (Tanpa Suara)',
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jenisreklameusaha',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenisreklameusaha',
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
                    1 => 'Identitas Usaha',
                    2 => 'Produk Usaha (Non Rokok)',
                    3 => 'Produk Usaha (Rokok)',
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_naskah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_naskah',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_lokasi',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_lokasi',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_panjang',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_panjang',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_lebar',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_lebar',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_arah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_arah',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_sudutpandang',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_sudutpandang',
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_sudutpandang,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jumlah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jumlah',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_jangkawaktu',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jangkawaktu',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 't_tarifreklame',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarifreklame',
                'class' => 'form-control',
                'required' => true,
                'style' => 'text-align:right',
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_tipewaktu',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_tipewaktu',
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
                    0 => 'Harian',
                    1 => 'Mingguan',
                    2 => 'Bulanan',
                    3 => 'Triwulanan',
                    4 => 'Semesteran',
                    5 => 'Tahunan',
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jenislokasi',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jenislokasi',
                'class' => 'form-control',
                'required' => true
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
//                'value_options' => $comboid_kelompokjalan,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_kelompokjalan',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_kelompokjalan',
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_kelompokjalan,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));


//        $this->add(array(
//            'name' => 't_tarifpajak',
//            'type' => 'text',
//            'attributes' => array(
//                'id' => 't_tarifpajak',
//                'class' => 'form-control',
//                'required' => true,
//                'readonly' => true,
//                'style' => 'text-align:right'
//            )
//        ));

        $this->add(array(
            'name' => 't_jmlhpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlhpajak',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right; background:#000099; color: white; padding: 7px 10px; height:40px; font-size: 16px; font-weight:bolder'
            )
        ));

        $this->add(array(
            'name' => 't_tglpenetapan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglpenetapan',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true
            )
        ));

//        $this->add(array(
//            'name' => 't_kecamatanrek',
//            'type' => 'Zend\Form\Element\Select',
//            'attributes' => array(
//                'id' => 't_kecamatanrek',
//                'class' => 'form-control',
//                'onchange' => 'comboKelurahanCamat();'
//            ),
//            'options' => array(
//                'empty_option' => 'Silahkan Pilih',
//                'value_options' => $comboid_kecamatan,
//                'disable_inarray_validator' => true, // <-- disable
//            )
//        ));
//
//        $this->add(array(
//            'name' => 't_kelurahanrek',
//            'type' => 'Zend\Form\Element\Select',
//            'attributes' => array(
//                'id' => 't_kelurahanrek',
//                'class' => 'form-control'
//            ),
//            'options' => array(
//                'empty_option' => 'Silahkan Pilih',
//                'value_options' => $comboid_kelurahan,
//                'disable_inarray_validator' => true, // <-- disable
//            )
//        ));

        $this->add(array(
            'name' => 'Pendataansubmit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Simpan',
                'id' => 'Pendataansubmit',
                'class' => "btn btn-warning btn-block"
            )
        ));
    }

}
