<?php

namespace Pajak\Form\Setting;

use Zend\Form\Form;

class ReklameFrm extends Form {
    
    public function __construct() {
        parent::__construct();
        
        $this->setAttribute("method", "post");
        
        $this->add(array(
            'name' => 's_idreklame',
            'type' => 'hidden',
            'attributes' => array(             
                'id'=>'s_idreklame'
            )
        ));
        
        $this->add(array(
            'name' => 's_namareklame',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_namareklame',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_ukuranreklame',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_ukuranreklame',
                'class'=>'form-control',
                'value' => '1',
                'readonly' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_satuanreklame',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_satuanreklame',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_lokminggu1',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_lokminggu1',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_lokbulan1',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_lokbulan1',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_loktahun1',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_loktahun1',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_lokminggu2',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_lokminggu2',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_lokbulan2',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_lokbulan2',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_loktahun2',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_loktahun2',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_sewaminggu',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_sewaminggu',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_sewabulan',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_sewabulan',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_sewatahun',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_sewatahun',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'type' => 'submit',
            'name' => 'simpan',
            'attributes' => array(
                'value' => 'Simpan',
                'class' => 'btn btn-primary btn-sm',
            ),
        ));        
    }
    
}