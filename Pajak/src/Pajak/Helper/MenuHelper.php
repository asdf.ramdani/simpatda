<?php

namespace Pajak\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MenuHelper extends AbstractHelper implements ServiceLocatorAwareInterface {

    protected $tbl;

    public function __invoke() {
        return $this;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function GetDataPemda() {
        $ar_pemda = $this->getTable("PemdaTable")->getdata();
        return $ar_pemda;
    }
    
    public function getDataActivity() {
        $res = $this->getTable("ActivityTable")->getdata();
        return $res;
    }
    
    public function loginActivity() {
        $message = 'Melakukan Login ke Aplikasi SIMPATDA';
        $res = $this->getTable("ActivityTable")->saveLogin($message);
        return $res;
    }
    
    public function simpanActivity($session) {
        $message = 'Melakukan Rekam Data';
        $res = $this->getTable("ActivityTable")->save($message, $session);
        return $res;
    }
    
    public function merubahActivity($session) {
        $message = 'Melakukan Perubahan Data';
        $res = $this->getTable("ActivityTable")->save($message, $session);
        return $res;
    }
    
    public function hapusActivity($session) {
        $message = 'Melakukan Penghapusan Data';
        $res = $this->getTable("ActivityTable")->save($message, $session);
        return $res;
    }
    
    public function melihatActivity($session) {
        $message = 'Melakukan Lihat Data';
        $res = $this->getTable("ActivityTable")->save($message, $session);
        return $res;
    }
    
    public function getTable($tbl_service) {
        $this->tbl = $this->getServiceLocator()->getServiceLocator()->get($tbl_service);
        return $this->tbl;
    }
    
    

}
