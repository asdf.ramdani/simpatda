<?php

namespace Pajak\Model\Bphtb;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class BphtbTable extends AbstractTableGateway {

    protected $table = '';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->initialize();
    }

    public function getRealisasi($tgl1, $tgl2) {
        $awaltahun = date("Y-01-01", strtotime($tgl1));
        $akhirtahun = date("Y-12-31", strtotime($tgl2));
        $tglreal1 = date("Y-m-d", strtotime($tgl1));
        $tglreal2 = date("Y-m-d", strtotime($tgl2));
        $tahun = date("Y", strtotime($tgl1));
        $sql = "SELECT
	( SELECT SUM ( t_nilaipembayaranspt ) FROM view_sspd_pembayaran 
            WHERE t_tanggalpembayaran >= '".$awaltahun."' 
                AND t_tanggalpembayaran < '".$tglreal1."' 
                AND EXTRACT ( 'YEAR' FROM t_tanggalpembayaran ) = '".$tahun."') AS bphtb_sebelum,
	( SELECT SUM ( t_nilaipembayaranspt ) FROM view_sspd_pembayaran 
            WHERE t_tanggalpembayaran >= '".$tglreal1."' 
                AND t_tanggalpembayaran <= '".$tglreal2."' 
                AND EXTRACT ( 'YEAR' FROM t_tanggalpembayaran ) = '".$tahun."') AS bphtb_sekarang,
	SUM ( t_nilaipembayaranspt )  AS bphtb_sesudah
	FROM view_sspd_pembayaran 
            WHERE t_tanggalpembayaran >= '".$awaltahun."' 
                AND t_tanggalpembayaran <= '".$akhirtahun."'
                AND EXTRACT ( 'YEAR' FROM t_tanggalpembayaran ) = '".$tahun."' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();
        return $res;
    }
    
    public function getRealisasiSub($tgl1, $tgl2) {
        $awaltahun = date("Y-01-01", strtotime($tgl1));
        $akhirtahun = date("Y-12-31", strtotime($tgl2));
        $tglreal1 = date("Y-m-d", strtotime($tgl1));
        $tglreal2 = date("Y-m-d", strtotime($tgl2));
        $tahun = date("Y", strtotime($tgl1));
        $sql = "SELECT
	( SELECT SUM ( t_nilaipembayaranspt ) FROM view_sspd_pembayaran 
            WHERE t_iddetailsptbphtb != 15
                AND t_tanggalpembayaran >= '".$awaltahun."' 
                AND t_tanggalpembayaran < '".$tglreal1."' 
                AND EXTRACT ( 'YEAR' FROM t_tanggalpembayaran ) = '".$tahun."') AS bphtbsub0_sebelum,
        ( SELECT SUM ( t_nilaipembayaranspt ) FROM view_sspd_pembayaran 
            WHERE t_iddetailsptbphtb = 15
                AND t_tanggalpembayaran >= '".$awaltahun."' 
                AND t_tanggalpembayaran < '".$tglreal1."' 
                AND EXTRACT ( 'YEAR' FROM t_tanggalpembayaran ) = '".$tahun."') AS bphtbsub1_sebelum,
	( SELECT SUM ( t_nilaipembayaranspt ) FROM view_sspd_pembayaran 
            WHERE t_iddetailsptbphtb != 15
                AND t_tanggalpembayaran >= '".$tglreal1."' 
                AND t_tanggalpembayaran <= '".$tglreal2."' 
                AND EXTRACT ( 'YEAR' FROM t_tanggalpembayaran ) = '".$tahun."') AS bphtbsub0_sekarang,
	( SELECT SUM ( t_nilaipembayaranspt ) FROM view_sspd_pembayaran 
            WHERE t_iddetailsptbphtb = 15
                AND t_tanggalpembayaran >= '".$tglreal1."' 
                AND t_tanggalpembayaran <= '".$tglreal2."' 
                AND EXTRACT ( 'YEAR' FROM t_tanggalpembayaran ) = '".$tahun."') AS bphtbsub1_sekarang,
	( SELECT SUM ( t_nilaipembayaranspt ) FROM view_sspd_pembayaran 
            WHERE t_iddetailsptbphtb != 15
                AND t_tanggalpembayaran >= '".$awaltahun."' 
                AND t_tanggalpembayaran <= '".$akhirtahun."'
                AND EXTRACT ( 'YEAR' FROM t_tanggalpembayaran ) = '".$tahun."')  AS bphtbsub0_sesudah,
        SUM ( t_nilaipembayaranspt )  AS bphtbsub1_sesudah
	FROM view_sspd_pembayaran 
            WHERE t_iddetailsptbphtb = 15
                AND t_tanggalpembayaran >= '".$awaltahun."' 
                AND t_tanggalpembayaran <= '".$akhirtahun."'
                AND EXTRACT ( 'YEAR' FROM t_tanggalpembayaran ) = '".$tahun."' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();
        return $res;
    }

    public function getRealisasiSdBulanini($tglakhir) {
        $tgl = date('Y-m-d', strtotime($tglakhir));
        $tahun = date('Y', strtotime($tglakhir));
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("view_sspd_pembayaran");
        $select->columns(array(
            "bphtb_sdbulaiini" => new \Zend\Db\Sql\Expression("SUM(t_nilaipembayaranspt)"),
        ));
        $where = new Where();
        $where->literal("t_tanggalpembayaran is not null");
        $where->literal("EXTRACT('YEAR' from t_tanggalpembayaran) = '" . $tahun . "' ");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

}
