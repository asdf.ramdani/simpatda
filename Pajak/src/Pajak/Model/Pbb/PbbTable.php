<?php

namespace Pajak\Model\Pbb;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class PbbTable extends AbstractTableGateway {

    protected $table = '';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->initialize();
    }

    public function getRealisasi($tgl1, $tgl2) {
        $awaltahun = date("Y-01-01", strtotime($tgl1));
        $akhirtahun = date("Y-12-31", strtotime($tgl2));
        $tahun = date("Y", strtotime($tgl1));
        $sql = "SELECT (SUM( JML_SPPT_YG_DIBAYAR)-SUM( DENDA_SPPT)) AS PBB_SESUDAH,
            SUM( DENDA_SPPT) AS PBBDENDA_SESUDAH
            FROM PEMBAYARAN_SPPT
            WHERE TO_CHAR(TGL_PEMBAYARAN_SPPT, 'YYYY-MM-DD') >= '".$awaltahun."' 
              AND TO_CHAR(TGL_PEMBAYARAN_SPPT, 'YYYY-MM-DD') <= '".$akhirtahun."' 
              AND TO_CHAR(TGL_PEMBAYARAN_SPPT, 'YYYY') = '".$tahun."' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();
        return $res;
    }
    
    public function getRealisasiSdBulanlalu($tgl1, $tgl2) {
        $awaltahun = date("Y-01-01", strtotime($tgl1));
        $tglreal1 = date("Y-m-d", strtotime($tgl1));
        $tahun = date("Y", strtotime($tgl1));
        $sql = "SELECT (SUM( JML_SPPT_YG_DIBAYAR)-SUM( DENDA_SPPT)) AS PBB_SEBELUM,
            SUM( DENDA_SPPT) AS PBBDENDA_SEBELUM
            FROM PEMBAYARAN_SPPT
            WHERE TO_CHAR(TGL_PEMBAYARAN_SPPT, 'YYYY-MM-DD') >= '".$awaltahun."' 
              AND TO_CHAR(TGL_PEMBAYARAN_SPPT, 'YYYY-MM-DD') < '".$tglreal1."' 
              AND TO_CHAR(TGL_PEMBAYARAN_SPPT, 'YYYY') = '".$tahun."' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();
        return $res;
    }
    
    public function getRealisasiBulanini($tgl1, $tgl2) {
        $tglreal1 = date("Y-m-d", strtotime($tgl1));
        $tglreal2 = date("Y-m-d", strtotime($tgl2));
        $tahun = date("Y", strtotime($tgl1));
        $sql = "SELECT (SUM( JML_SPPT_YG_DIBAYAR)-SUM( DENDA_SPPT)) AS PBB_SEKARANG,
            SUM( DENDA_SPPT) AS PBBDENDA_SEKARANG
            FROM PEMBAYARAN_SPPT
            WHERE TO_CHAR(TGL_PEMBAYARAN_SPPT, 'YYYY-MM-DD') >= '".$tglreal1."' 
              AND TO_CHAR(TGL_PEMBAYARAN_SPPT, 'YYYY-MM-DD') <= '".$tglreal2."' 
              AND TO_CHAR(TGL_PEMBAYARAN_SPPT, 'YYYY') = '".$tahun."' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();
        return $res;
    }
    

}
