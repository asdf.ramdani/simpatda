<?php

namespace Pajak\Model\Pembukuan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class PembukuanTable extends AbstractTableGateway {

    protected $table = 't_transaksi';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new \Pajak\Model\Penagihan\PenagihanBase());
        $this->initialize();
    }

    public function getDataPembukuanID($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek", "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_tglpendataan", "t_jmlhpajak", "t_nourut", "t_tgljatuhtempo"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "d.s_idkorek = c.t_idkorek", array(
            "korek", "s_namakorek", "s_persentarifkorek", "s_namajenis"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getdataWpbyId($t_idwp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $where = new Where();
        $where->equalTo('a.t_idwp', $t_idwp);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getdataWpObjekbyId($t_idwp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_nop", "t_namaobjek", "s_namajenis"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('b.t_idwp', $t_idwp);
        $select->where($where);
        $select->order('s_idjenis asc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataBukuWP($t_periodepajak, $t_idwpobjek, $month) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_transaksi");
        $select->columns(array(
            't_nourut',
            't_tglpendataan', 't_jmlhpajak', 't_tgljatuhtempo', 't_jmlhpembayaran',
            't_tglpembayaran', 't_jmlhdendapembayaran', 't_tgldendapembayaran',
            't_jmlhbayardenda', 't_tglbayardenda', 't_jmlhbulandendapembayaran'
        ));
        $where = new Where();
        $where->equalTo('t_idwpobjek', $t_idwpobjek);
        // $where->literal("extract(month from t_masaawal) ='" . str_pad($month, 2, '0', STR_PAD_LEFT) . "' and extract(year from t_masaawal) ='" . $t_periodepajak . "'");
        $where->literal("extract(month from t_tglpendataan) ='" . str_pad($month, 2, '0', STR_PAD_LEFT) . "' and extract(year from t_tglpendataan) ='" . $t_periodepajak . "'");
        $select->where($where);
        $select->order('t_idtransaksi asc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataBelumbayar($t_periodepajak, $t_idwpobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_transaksi");
        $select->columns(array('*'));
        $where = new Where();
        $where->equalTo('t_idwpobjek', $t_idwpobjek);
        $where->literal("t_periodepajak <= '" . $t_periodepajak . "' ");
        $where->isNull("t_tglpembayaran");
        $select->where($where);
        $select->order('t_periodepajak asc');
        // $select->order('t_idtransaksi asc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getdataRealisasi____($tgl, $idrealisasi) {
        $tglreal = explode("-", $tgl);
        $bulan = date('m', strtotime($tgl));
        $tahun = date('Y', strtotime($tgl));

        $sql = "SELECT
	C .korek,
	C .s_namakorek,
	C .s_rinciankorek,
	(
		SELECT
			CASE
                WHEN C .s_kelompokkorek = '' THEN        
                (
                    SELECT
					SUM (s_targetjumlah)
				FROM
					s_targetdetail d
				LEFT JOIN s_target e ON e.s_idtarget = d.s_idtargetheader
				LEFT JOIN view_rekening f ON f.s_idkorek = d.s_targetrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				
				AND e.s_idtarget = $idrealisasi
				AND d.s_idtargetheader = $idrealisasi
                )
                WHEN C .s_jeniskorek = '' THEN        
                (
                    SELECT
					SUM (s_targetjumlah)
				FROM
					s_targetdetail d
				LEFT JOIN s_target e ON e.s_idtarget = d.s_idtargetheader
				LEFT JOIN view_rekening f ON f.s_idkorek = d.s_targetrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				
				AND e.s_idtarget = $idrealisasi
				AND d.s_idtargetheader = $idrealisasi
                )
                WHEN C .s_objekkorek = '' THEN        
                (
                    SELECT
					SUM (s_targetjumlah)
				FROM
					s_targetdetail d
				LEFT JOIN s_target e ON e.s_idtarget = d.s_idtargetheader
				LEFT JOIN view_rekening f ON f.s_idkorek = d.s_targetrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				
				AND e.s_idtarget = $idrealisasi
				AND d.s_idtargetheader = $idrealisasi
                )
		WHEN C .s_rinciankorek = '00' THEN
			(
				SELECT
					SUM (s_targetjumlah)
				FROM
					s_targetdetail d
				LEFT JOIN s_target e ON e.s_idtarget = d.s_idtargetheader
				LEFT JOIN view_rekening f ON f.s_idkorek = d.s_targetrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND f.s_objekkorek = C .s_objekkorek
				AND e.s_idtarget = $idrealisasi
				AND d.s_idtargetheader = $idrealisasi
			)
		ELSE
			(
				SELECT
					s_targetjumlah
				FROM
					s_targetdetail d
				LEFT JOIN s_target e ON e.s_idtarget = d.s_idtargetheader
				WHERE
					d.s_targetrekening = C .s_idkorek
				AND e.s_idtarget=$idrealisasi and d.s_idtargetheader=$idrealisasi
			)
		END AS jml
	) AS targetjumlah,
	(
		SELECT
			CASE
                WHEN C.s_kelompokkorek = '' THEN
                (
                    SELECT
					COALESCE(SUM (t_jmlhpembayaran),0)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idkorek
				WHERE
					f.s_tipekorek = C .s_tipekorek
				
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) < '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
                )+(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				
				AND EXTRACT (MONTH FROM A .t_tglsetor) < '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
                WHEN C .s_jeniskorek = '' THEN
			(
				SELECT
					COALESCE(SUM (t_jmlhpembayaran),0)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idkorek
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) < '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			)
+
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				
				AND EXTRACT (MONTH FROM A .t_tglsetor) < '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
                 WHEN C .s_objekkorek = '' THEN
			(
				SELECT
					COALESCE(SUM (t_jmlhpembayaran),0)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idkorek
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) < '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			)
+
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				
				AND EXTRACT (MONTH FROM A .t_tglsetor) < '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
		WHEN C .s_rinciankorek = '00' THEN
			(
				SELECT
					COALESCE(SUM (t_jmlhpembayaran),0)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idkorek OR f.s_jenisobjek = A .t_jenispajak
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND f.s_objekkorek = C .s_objekkorek
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) < '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			)
+
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND f.s_objekkorek = C .s_objekkorek
				AND EXTRACT (MONTH FROM A .t_tglsetor) < '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
                        WHEN c.s_tipekorek||c.s_kelompokkorek||c.s_jeniskorek||c.s_objekkorek='41407' THEN
                (
                    SELECT
					coalesce(SUM (t_jmlhbayardenda),0)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorekdenda
				WHERE
					f.s_idkorek = C.s_idkorek
				
				AND extract(month from A.t_tglbayardenda) < '" . $bulan . "'
                                    AND extract(year from A.t_tglbayardenda) = '" . $tahun . "'
                ) + (
                SELECT
					coalesce(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idrekening
				WHERE
					f.s_idkorek = C.s_idkorek
				
				AND extract(month from A.t_tglsetor) < '" . $bulan . "'
                                    AND extract(year from A.t_tglsetor) = '" . $tahun . "'
                                        AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
                )
		ELSE
			(
				SELECT
					COALESCE(SUM (t_jmlhpembayaran),0)
				FROM
					t_transaksi A
				WHERE
					C .s_idkorek = A .t_idkorek
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) < '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			)
+ 
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A					
				WHERE
					C .s_idkorek = A .t_idrekening
				AND EXTRACT (MONTH FROM A .t_tglsetor) < '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
		END AS jml
	) AS real_bulanlalu,
	(
		SELECT
			CASE
                WHEN C.s_kelompokkorek = '' THEN
                (
                    SELECT
					COALESCE(SUM (t_jmlhpembayaran),0)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idkorek
				WHERE
					f.s_tipekorek = C .s_tipekorek
				
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) = '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
                )+(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				
				AND EXTRACT (MONTH FROM A .t_tglsetor) = '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
                WHEN C .s_jeniskorek = '' THEN
			(
				SELECT
					COALESCE(SUM (t_jmlhpembayaran),0)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idkorek
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) = '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			)
+
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				
				AND EXTRACT (MONTH FROM A .t_tglsetor) = '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
                WHEN C .s_objekkorek = '' THEN
			(
				SELECT
					COALESCE(SUM (t_jmlhpembayaran),0)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idkorek
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) = '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			)
+
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				
				AND EXTRACT (MONTH FROM A .t_tglsetor) = '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
		WHEN C .s_rinciankorek = '00' THEN
			(
				SELECT
					COALESCE(SUM (t_jmlhpembayaran),0)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idkorek OR f.s_jenisobjek = A .t_jenispajak
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND f.s_objekkorek = C .s_objekkorek
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) = '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			)
+
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND f.s_objekkorek = C .s_objekkorek
				AND EXTRACT (MONTH FROM A .t_tglsetor) = '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
                        WHEN c.s_tipekorek||c.s_kelompokkorek||c.s_jeniskorek||c.s_objekkorek='41408' THEN
                (
                    SELECT
					coalesce(SUM (t_jmlhbayardenda),0)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorekdenda
				WHERE
					f.s_idkorek = C.s_idkorek
				
				AND extract(month from A.t_tglbayardenda) = '" . $bulan . "'
                                    AND extract(year from A.t_tglbayardenda) = '" . $tahun . "'
                ) + (
                SELECT
					coalesce(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idrekening
				WHERE
					f.s_idkorek = C.s_idkorek
				
				AND extract(month from A.t_tglsetor) = '" . $bulan . "'
                                    AND extract(year from A.t_tglsetor) = '" . $tahun . "'
                                        AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
                )
		ELSE
			(
				SELECT
					COALESCE(SUM (t_jmlhpembayaran),0)
				FROM
					t_transaksi A
				WHERE
					C .s_idkorek = A .t_idkorek
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) = '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			)
+ 
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A					
				WHERE
					C .s_idkorek = A .t_idrekening
				AND EXTRACT (MONTH FROM A .t_tglsetor) = '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
		END AS jml
	) AS real_bulanini,
	(
		SELECT
			CASE
                WHEN C .s_kelompokkorek = '' THEN
			(
				SELECT
					COALESCE(SUM (t_jmlhpembayaran),0) 
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idkorek
				WHERE
					f.s_tipekorek = C .s_tipekorek
				
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) <= '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			)
+
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				
				AND EXTRACT (MONTH FROM A .t_tglsetor) <= '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
                WHEN C .s_jeniskorek = '' THEN
			(
				SELECT
					COALESCE(SUM (t_jmlhpembayaran),0) 
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idkorek
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) <= '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			)
+
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				
				AND EXTRACT (MONTH FROM A .t_tglsetor) <= '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
                WHEN C .s_objekkorek = '' THEN
			(
				SELECT
					COALESCE(SUM (t_jmlhpembayaran),0) 
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idkorek
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) <= '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			)
+
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				
				AND EXTRACT (MONTH FROM A .t_tglsetor) <= '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
		WHEN C .s_rinciankorek = '00' THEN
			(
				SELECT
					COALESCE(SUM (t_jmlhpembayaran),0) 
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idkorek OR f.s_jenisobjek = A .t_jenispajak
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND f.s_objekkorek = C .s_objekkorek
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) <= '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			)
+
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A .t_idrekening
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND f.s_objekkorek = C .s_objekkorek
				AND EXTRACT (MONTH FROM A .t_tglsetor) <= '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
                        WHEN c.s_tipekorek||c.s_kelompokkorek||c.s_jeniskorek||c.s_objekkorek='41407' THEN
                (
                    SELECT
					coalesce(SUM (t_jmlhbayardenda),0)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorekdenda
				WHERE
					f.s_idkorek = C.s_idkorek
				
				AND extract(month from A.t_tglbayardenda) <= '" . $bulan . "'
                                    AND extract(year from A.t_tglbayardenda) = '" . $tahun . "'
                ) + (
                SELECT
					coalesce(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idrekening
				WHERE
					f.s_idkorek = C.s_idkorek
				
				AND extract(month from A.t_tglsetor) <= '" . $bulan . "'
                                    AND extract(year from A.t_tglsetor) = '" . $tahun . "'
                                        AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
                )
		ELSE
			(
				SELECT
					COALESCE(SUM (A.t_jmlhpembayaran),0)
				FROM
					t_transaksi A					
				WHERE
					C .s_idkorek = A .t_idkorek
				AND EXTRACT (MONTH FROM A .t_tglpembayaran) <= '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglpembayaran) = '" . $tahun . "'
			) 
+ 
(
				SELECT
					COALESCE(SUM (t_jumlahsetor),0)
				FROM
					t_setoranlain A					
				WHERE
					C .s_idkorek = A .t_idrekening
				AND EXTRACT (MONTH FROM A .t_tglsetor) <= '" . $bulan . "'
				AND EXTRACT (YEAR FROM A .t_tglsetor) = '" . $tahun . "'
				AND A.t_viasetor = '1'
				AND A.t_issetorandeleted = 0
			)
		END AS jml
	) AS real_sdbulanini
FROM
	view_rekening C
ORDER BY
	C .korek";
//        die($sql);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getdataKasHarian($tgl, $idrealisasi) {
        $tglreal = explode("-", $tgl);
        $tahun = date('Y', strtotime($tgl));
        $tgl_db = date('Y-m-d', strtotime($tgl));
        $tgl_dby = date('Y-m-d', strtotime($tgl . '- 1 day'));
        $sql = "SELECT
	C.korek,
	C.s_namakorek,
        C.s_objekkorek,
	C.s_rinciankorek,
	(
		SELECT
			CASE
		WHEN C.s_objekkorek = '00' THEN
			(
				SELECT
					SUM (s_targetjumlah)
				FROM
					s_targetdetail d
				LEFT JOIN s_target e ON e.s_idtarget = d.s_idtargetheader
				LEFT JOIN view_rekening f ON f.s_idkorek = d.s_targetrekening
				WHERE
					f.s_tipekorek = C.s_tipekorek
				AND f.s_kelompokkorek = C.s_kelompokkorek
				AND f.s_jeniskorek = C.s_jeniskorek
				AND e.s_tahuntarget = '" . $tglreal[2] . "'
				AND d.s_idtargetheader = " . $idrealisasi . "
			)
                WHEN C.s_rinciankorek = '00' THEN
			(
				SELECT
					SUM (s_targetjumlah)
				FROM
					s_targetdetail d
				LEFT JOIN s_target e ON e.s_idtarget = d.s_idtargetheader
				LEFT JOIN view_rekening f ON f.s_idkorek = d.s_targetrekening
				WHERE
					f.s_tipekorek = C.s_tipekorek
				AND f.s_kelompokkorek = C.s_kelompokkorek
				AND f.s_jeniskorek = C.s_jeniskorek
				AND f.s_objekkorek = C.s_objekkorek
				AND e.s_tahuntarget = '" . $tglreal[2] . "'
				AND d.s_idtargetheader = " . $idrealisasi . "
			)
		ELSE
			(
				SELECT
					s_targetjumlah
				FROM
					s_targetdetail d
				LEFT JOIN s_target e ON e.s_idtarget = d.s_idtargetheader
				WHERE
					d.s_targetrekening = C .s_idkorek
				AND e.s_tahuntarget = '" . $tglreal[2] . "' and d.s_idtargetheader=" . $idrealisasi . "
			)
		END AS jml
	) AS targetjumlah,
	(
		SELECT
			CASE
		WHEN C.s_objekkorek = '00' THEN
			(
				SELECT
					SUM (t_jmlhpembayaran)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek
				WHERE
					f.s_tipekorek = C.s_tipekorek
				AND f.s_kelompokkorek = C.s_kelompokkorek
				AND f.s_jeniskorek = C.s_jeniskorek
				AND A.t_tglpembayaran <= '" . $tgl_dby . "'
                                AND EXTRACT(YEAR FROM A.t_tglpembayaran) = '".$tahun."'
                                AND A.t_viapembayaran=1
			)
                WHEN C.s_rinciankorek = '00' THEN
			(
				SELECT
					SUM (t_jmlhpembayaran)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek
				WHERE
                                f.s_tipekorek = C.s_tipekorek
				AND f.s_kelompokkorek = C.s_kelompokkorek
				AND f.s_jeniskorek = C.s_jeniskorek
				AND f.s_objekkorek = C.s_objekkorek
				AND A.t_tglpembayaran <= '" . $tgl_dby . "'
                                AND EXTRACT(YEAR FROM A.t_tglpembayaran) = '".$tahun."'
                                AND A.t_viapembayaran=1
			)
                WHEN c.s_tipekorek||c.s_kelompokkorek||c.s_jeniskorek||c.s_objekkorek='41106' THEN
			(
				SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb
                                LEFT JOIN t_transaksi A on bb.t_idtransaksi = A.t_idtransaksi
                                LEFT JOIN view_rekening f on f.s_idkorek = bb.t_idkorek
				WHERE
                                f.s_tipekorek = C.s_tipekorek
				AND f.s_kelompokkorek = C.s_kelompokkorek
				AND f.s_jeniskorek = C.s_jeniskorek
				AND f.s_objekkorek = C.s_objekkorek
				AND f.s_rinciankorek = C.s_rinciankorek
				AND A.t_tglpembayaran <= '" . $tgl_dby . "'
                                AND EXTRACT(YEAR FROM A.t_tglpembayaran) = '".$tahun."'
                                AND A.t_viapembayaran=1
                                AND A.t_jenispajak=6
			)
                WHEN c.s_tipekorek||c.s_kelompokkorek||c.s_jeniskorek||c.s_objekkorek='41407' THEN
                (
                    SELECT
					SUM (t_jmlhbayardenda)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorekdenda
				WHERE
					f.s_idkorek = C.s_idkorek
				
				AND A.t_tglbayardenda <= '" . $tgl_dby . "'
                )
		ELSE
			(
				SELECT
					SUM (t_jmlhpembayaran)
				FROM
					t_transaksi A
				WHERE
					C.s_idkorek = A.t_idkorek
				AND A.t_tglpembayaran <= '" . $tgl_dby . "'
                                AND EXTRACT(YEAR FROM A.t_tglpembayaran) = '".$tahun."'
                                AND A.t_viapembayaran=1
			)
		END AS jml
	) AS real_sdharilalu,
	(
		SELECT
			CASE
		WHEN C.s_objekkorek = '00' THEN
			(
				SELECT
					SUM (t_jmlhpembayaran)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek
				WHERE
					f.s_tipekorek = C.s_tipekorek
				AND f.s_kelompokkorek = C.s_kelompokkorek
				AND f.s_jeniskorek = C.s_jeniskorek
				AND A.t_tglpembayaran = '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM A.t_tglpembayaran) = '".$tahun."'
                                AND A.t_viapembayaran=1
			)
                        WHEN C.s_rinciankorek = '00' THEN
			(
				SELECT
					SUM (t_jmlhpembayaran)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek
				WHERE
					f.s_tipekorek = C.s_tipekorek
				AND f.s_kelompokkorek = C.s_kelompokkorek
				AND f.s_jeniskorek = C.s_jeniskorek
				AND f.s_objekkorek = C.s_objekkorek
				AND A.t_tglpembayaran = '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM A.t_tglpembayaran) = '".$tahun."'
                                AND A.t_viapembayaran=1
			)
                        WHEN c.s_tipekorek||c.s_kelompokkorek||c.s_jeniskorek||c.s_objekkorek='41106' THEN
			(
				SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb
                                LEFT JOIN t_transaksi A on bb.t_idtransaksi = A.t_idtransaksi
                                LEFT JOIN view_rekening f on f.s_idkorek = bb.t_idkorek
				WHERE
                                f.s_tipekorek = C.s_tipekorek
				AND f.s_kelompokkorek = C.s_kelompokkorek
				AND f.s_jeniskorek = C.s_jeniskorek
				AND f.s_objekkorek = C.s_objekkorek
				AND f.s_rinciankorek = C.s_rinciankorek
				AND A.t_tglpembayaran = '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM A.t_tglpembayaran) = '".$tahun."'
                                AND A.t_viapembayaran=1
                                AND A.t_jenispajak=6
			)
                        WHEN c.s_tipekorek||c.s_kelompokkorek||c.s_jeniskorek||c.s_objekkorek='41407' THEN
                (
                    SELECT
					SUM (t_jmlhbayardenda)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorekdenda
				WHERE
					f.s_idkorek = C.s_idkorek
				AND A.t_tglbayardenda = '" . $tgl_db . "'
                )
		ELSE
			(
				SELECT
					SUM (t_jmlhpembayaran)
				FROM
					t_transaksi A
				WHERE
					C.s_idkorek = A.t_idkorek
				AND A.t_tglpembayaran = '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM A.t_tglpembayaran) = '".$tahun."'
                                AND A.t_viapembayaran=1
			)
		END AS jml
	) AS real_hariini,
	(
		SELECT
			CASE
                
		WHEN C.s_objekkorek = '00' THEN
			(
				SELECT
					SUM (t_jmlhpembayaran)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek
				WHERE
					f.s_tipekorek = C.s_tipekorek
				AND f.s_kelompokkorek = C.s_kelompokkorek
				AND f.s_jeniskorek = C.s_jeniskorek
				AND A.t_tglpembayaran <= '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM A.t_tglpembayaran) = '".$tahun."'
                                AND A.t_viapembayaran=1
			)
		WHEN C.s_rinciankorek = '00' THEN
			(
				SELECT
					SUM (t_jmlhpembayaran)
				FROM
					t_transaksi a
				LEFT JOIN view_rekening f ON f.s_idkorek = a.t_idkorek
				WHERE
					f.s_tipekorek = C.s_tipekorek
				AND f.s_kelompokkorek = C.s_kelompokkorek
				AND f.s_jeniskorek = C.s_jeniskorek
				AND f.s_objekkorek = C.s_objekkorek
				AND a.t_tglpembayaran <= '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM A.t_tglpembayaran) = '".$tahun."'
                                AND A.t_viapembayaran=1
			)
                        WHEN c.s_tipekorek||c.s_kelompokkorek||c.s_jeniskorek||c.s_objekkorek='41106' THEN
			(
				SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb
                                LEFT JOIN t_transaksi A on bb.t_idtransaksi = A.t_idtransaksi
                                LEFT JOIN view_rekening f on f.s_idkorek = bb.t_idkorek
				WHERE
                                f.s_tipekorek = C.s_tipekorek
				AND f.s_kelompokkorek = C.s_kelompokkorek
				AND f.s_jeniskorek = C.s_jeniskorek
				AND f.s_objekkorek = C.s_objekkorek
				AND f.s_rinciankorek = C.s_rinciankorek
				AND A.t_tglpembayaran <= '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM A.t_tglpembayaran) = '".$tahun."'
                                AND A.t_viapembayaran=1
                                AND A.t_jenispajak=6
			)
                WHEN c.s_tipekorek||c.s_kelompokkorek||c.s_jeniskorek||c.s_objekkorek='41407' THEN
                (
                    SELECT
					SUM (t_jmlhbayardenda)
				FROM
					t_transaksi A
				LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorekdenda
				WHERE
					f.s_idkorek = C.s_idkorek
				AND A.t_tglbayardenda <= '" . $tgl_db . "'
                )
		ELSE
			(
				SELECT
					SUM (t_jmlhpembayaran)
				FROM
					t_transaksi A
				WHERE
					C .s_idkorek = A .t_idkorek
				AND A .t_tglpembayaran <= '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM A.t_tglpembayaran) = '".$tahun."'
                                AND A.t_viapembayaran=1
			)
		END AS jml
	) AS real_sdhariini,
	(SELECT
			CASE
		WHEN C .s_objekkorek = '00' THEN
			(
				SELECT
					SUM (t_jmlhsbd)
				FROM
					t_setorbankdetail a
				LEFT JOIN t_setorbankheader b ON b.t_idsbh=a.t_idsbh
				LEFT JOIN view_rekening f ON f.s_idkorek = a.t_idkoreksbd
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND b.t_tglsbh <= '" . $tgl_dby . "'
                                AND EXTRACT(YEAR FROM b.t_tglsbh) = '".$tahun."'
                                AND b.t_issbhdeleted = 0
			)
		WHEN C .s_rinciankorek = '00' THEN
			(
				SELECT
					SUM (t_jmlhsbd)
				FROM
					t_setorbankdetail a
				LEFT JOIN t_setorbankheader b ON b.t_idsbh=a.t_idsbh
				LEFT JOIN view_rekening f ON f.s_idkorek = a.t_idkoreksbd
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND f.s_objekkorek = C .s_objekkorek
				AND b.t_tglsbh <= '" . $tgl_dby . "'
                                AND EXTRACT(YEAR FROM b.t_tglsbh) = '".$tahun."'
                                AND b.t_issbhdeleted = 0
			)
		ELSE
			(
				SELECT
					SUM (t_jmlhsbd)
				FROM
					t_setorbankdetail A
					LEFT JOIN t_setorbankheader B ON B.t_idsbh=A.t_idsbh
				WHERE
					C .s_idkorek = A .t_idkoreksbd
				AND B.t_tglsbh <= '" . $tgl_dby . "'
                                AND EXTRACT(YEAR FROM b.t_tglsbh) = '".$tahun."'
                                AND b.t_issbhdeleted = 0    
			)
		END AS jml
	) AS pengeluaran_sdharilalu,
(SELECT
			CASE
		WHEN C .s_objekkorek = '00' THEN
			(
				SELECT
					SUM (t_jmlhsbd)
				FROM
					t_setorbankdetail a
				LEFT JOIN t_setorbankheader b ON b.t_idsbh=a.t_idsbh
				LEFT JOIN view_rekening f ON f.s_idkorek = a.t_idkoreksbd
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND b.t_tglsbh = '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM b.t_tglsbh) = '".$tahun."'
                                AND b.t_issbhdeleted = 0
			)
		WHEN C .s_rinciankorek = '00' THEN
			(
				SELECT
					SUM (t_jmlhsbd)
				FROM
					t_setorbankdetail a
				LEFT JOIN t_setorbankheader b ON b.t_idsbh=a.t_idsbh
				LEFT JOIN view_rekening f ON f.s_idkorek = a.t_idkoreksbd
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND f.s_objekkorek = C .s_objekkorek
				AND b.t_tglsbh = '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM b.t_tglsbh) = '".$tahun."'
                                AND b.t_issbhdeleted = 0
			)
		ELSE
			(
				SELECT
					SUM (t_jmlhsbd)
				FROM
					t_setorbankdetail A
					LEFT JOIN t_setorbankheader B ON B.t_idsbh=A.t_idsbh
				WHERE
					C .s_idkorek = A .t_idkoreksbd
				AND B.t_tglsbh = '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM b.t_tglsbh) = '".$tahun."'
                                AND b.t_issbhdeleted = 0
			)
		END AS jml
	) AS pengeluaran_hariini,
(SELECT
			CASE
		WHEN C .s_objekkorek = '00' THEN
			(
				SELECT
					SUM (t_jmlhsbd)
				FROM
					t_setorbankdetail a
				LEFT JOIN t_setorbankheader b ON b.t_idsbh=a.t_idsbh
				LEFT JOIN view_rekening f ON f.s_idkorek = a.t_idkoreksbd
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND b.t_tglsbh <= '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM b.t_tglsbh) = '".$tahun."'
                                AND b.t_issbhdeleted = 0
			)
		WHEN C .s_rinciankorek = '00' THEN
			(
				SELECT
					SUM (t_jmlhsbd)
				FROM
					t_setorbankdetail a
				LEFT JOIN t_setorbankheader b ON b.t_idsbh=a.t_idsbh
				LEFT JOIN view_rekening f ON f.s_idkorek = a.t_idkoreksbd
				WHERE
					f.s_tipekorek = C .s_tipekorek
				AND f.s_kelompokkorek = C .s_kelompokkorek
				AND f.s_jeniskorek = C .s_jeniskorek
				AND f.s_objekkorek = C .s_objekkorek
				AND b.t_tglsbh <= '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM b.t_tglsbh) = '".$tahun."'
                                AND b.t_issbhdeleted = 0
			)
		ELSE
			(
				SELECT
					SUM (t_jmlhsbd)
				FROM
					t_setorbankdetail A
					LEFT JOIN t_setorbankheader B ON B.t_idsbh=A.t_idsbh
				WHERE
					C.s_idkorek = A.t_idkoreksbd
				AND B.t_tglsbh <= '" . $tgl_db . "'
                                AND EXTRACT(YEAR FROM b.t_tglsbh) = '".$tahun."'
                                AND b.t_issbhdeleted = 0
			)
		END AS jml
	) AS pengeluaran_sdhariini
FROM
	view_rekening C
        WHERE C. s_tipekorek||C. s_kelompokkorek||C. s_jeniskorek='411'
        OR C. s_tipekorek||C. s_kelompokkorek||C. s_jeniskorek||C. s_objekkorek='41407'
ORDER BY
	C.korek asc";
//        die($sql);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getdataBKU__($bulan, $tahun) {
        $sql = "SELECT * FROM ((
	SELECT
		t0.t_tglpembayaran AS tglbayar,
		t0.t_jmlhpembayaran AS pembayaran,
		0 AS penyetoran,
		t_rek.korek,
		t_rek.s_namakorek
	FROM
		t_transaksi t0
	LEFT JOIN view_rekening t_rek ON t_rek.s_idkorek = t0.t_idkorek
	WHERE
		t0.t_tglpembayaran IS NOT NULL
		AND t0.t_viapembayaran = 1
	ORDER BY t0.t_tglpembayaran
)
UNION
	(
		SELECT
			t0.t_tglbayardenda AS tglbayar,
			t0.t_jmlhbayardenda AS pembayaran,
			0 AS penyetoran,
			t_rekdenda.korek,
			t_rekdenda.s_namakorek
		FROM
			t_transaksi t0
		LEFT JOIN view_rekening t_rekdenda ON t_rekdenda.s_idkorek = t0.t_idkorekdenda
		WHERE
			t0.t_tglbayardenda IS NOT NULL
			AND t0.t_viapembayarandenda = 1
		ORDER BY t0.t_tglbayardenda
	)
UNION
(
SELECT 
bb.t_tglsbh AS tglbayar,
0 AS pembayaran,
sum(aa.t_jmlhsbd) AS penyetoran,
t_rek.korek,
'SETOR '||t_rek.s_namakorek
FROM t_setorbankdetail aa 
LEFT JOIN t_setorbankheader bb ON aa.t_idsbh=bb.t_idsbh
LEFT JOIN view_rekening t_rek ON t_rek.s_idkorek=aa.t_idkoreksbd
WHERE bb.t_issbhdeleted = 0
GROUP BY bb.t_tglsbh, t_rek.korek, t_rek.s_namakorek
ORDER BY bb.t_tglsbh
)) AS bku WHERE extract(month from tglbayar)::int = " . $bulan . " AND extract(year from tglbayar)='" . $tahun . "' ORDER BY tglbayar";
//        die($sql);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getdataBKU($bulan, $tahun) {
        $sql = "SELECT * FROM ((
	SELECT
		t0.t_tglpembayaran AS tglbayar,
		t0.t_jmlhpembayaran AS pembayaran,
		t0.t_jmlhpembayaran AS penyetoran,
		t_rek.korek,
		t_rek.s_namakorek
	FROM
		t_transaksi t0
	LEFT JOIN view_rekening t_rek ON t_rek.s_idkorek = t0.t_idkorek
	WHERE
		t0.t_tglpembayaran IS NOT NULL
		AND t0.t_viapembayaran = 2
	ORDER BY t0.t_tglpembayaran
)
UNION
	(
		SELECT
			t0.t_tglbayardenda AS tglbayar,
			t0.t_jmlhbayardenda AS pembayaran,
			t0.t_jmlhbayardenda AS penyetoran,
			t_rekdenda.korek,
			t_rekdenda.s_namakorek
		FROM
			t_transaksi t0
		LEFT JOIN view_rekening t_rekdenda ON t_rekdenda.s_idkorek = t0.t_idkorekdenda
		WHERE
			t0.t_tglbayardenda IS NOT NULL
			AND t0.t_viapembayarandenda = 2
		ORDER BY t0.t_tglbayardenda
	)
UNION
(
SELECT 
bb.t_tglsbh AS tglbayar,
0 AS pembayaran,
sum(aa.t_jmlhsbd) AS penyetoran,
t_rek.korek,
'SETOR '||t_rek.s_namakorek
FROM t_setorbankdetail aa 
LEFT JOIN t_setorbankheader bb ON aa.t_idsbh=bb.t_idsbh
LEFT JOIN view_rekening t_rek ON t_rek.s_idkorek=aa.t_idkoreksbd
WHERE bb.t_issbhdeleted = 0
GROUP BY bb.t_tglsbh, t_rek.korek, t_rek.s_namakorek
ORDER BY bb.t_tglsbh
)) AS bku WHERE pembayaran <> 0 and penyetoran <> 0 and extract(month from tglbayar)::int = " . $bulan . " AND extract(year from tglbayar)='" . $tahun . "' ORDER BY tglbayar";
//        die($sql);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getdataBKUTotal($bulan, $tahun) {
        $sql = "select 
			(
			select sum(t0.t_jmlhpembayaran) FROM t_transaksi t0 
			where extract(month from t0.t_tglpembayaran)::int < " . $bulan . "
			and extract(year from t0.t_tglpembayaran) = '" . $tahun . "'
			) as total_sdbulanlalu,
			(
			select sum(t0.t_jmlhpembayaran) FROM t_transaksi t0 
			where extract(month from t0.t_tglpembayaran)::int <= " . $bulan . "
			and extract(year from t0.t_tglpembayaran) = '" . $tahun . "'
			) as total_sdbulanini,
			(
			select sum(t0.t_jmlhpembayaran) FROM t_transaksi t0 
			where extract(month from t0.t_tglpembayaran)::int < " . $bulan . "
			and extract(year from t0.t_tglpembayaran) = '" . $tahun . "'
			) as total_setor_sdbulanlalu,
			(
			select sum(t0.t_jmlhpembayaran) FROM t_transaksi t0 
			where extract(month from t0.t_tglpembayaran)::int <= " . $bulan . "
			and extract(year from t0.t_tglpembayaran) = '" . $tahun . "'
			) as total_setor_sdbulanini";
//        die($sql);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();
        return $res;
    }

    public function getdataBKUTotal__($bulan, $tahun) {
        $sql = "select 
			(
			select sum(t0.t_jmlhpembayaran) FROM t_transaksi t0 
			where extract(month from t0.t_tglpembayaran)::int < " . $bulan . "
			and extract(year from t0.t_tglpembayaran) = '" . $tahun . "'
			) as total_sdbulanlalu,
			(
			select sum(t0.t_jmlhpembayaran) FROM t_transaksi t0 
			where extract(month from t0.t_tglpembayaran)::int <= " . $bulan . "
			and extract(year from t0.t_tglpembayaran) = '" . $tahun . "'
			) as total_sdbulanini,
			(
			select sum(t0.t_jmlhsbd) from t_setorbankdetail t0
			LEFT JOIN t_setorbankheader t1 ON t0.t_idsbh=t1.t_idsbh
			where extract(month from t1.t_tglsbh)::int < " . $bulan . " AND extract(year from t1.t_tglsbh) = '" . $tahun . "'
			) as total_setor_sdbulanlalu,
			(
			select sum(t0.t_jmlhsbd) from t_setorbankdetail t0
			LEFT JOIN t_setorbankheader t1 ON t0.t_idsbh=t1.t_idsbh
			where extract(month from t1.t_tglsbh)::int <= " . $bulan . " AND extract(year from t1.t_tglsbh) = '" . $tahun . "'
			) as total_setor_sdbulanini";
//        die($sql);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();
        return $res;
    }

    public function getdataBpps($tgl1, $tgl2, $viabayar) {
        $tgl1 = date('Y-m-d', strtotime($tgl1));
        $tgl2 = date('Y-m-d', strtotime($tgl2));
        if ($viabayar != NULL):
            $whereviabayar = " and t0.t_viapembayaran::text='$viabayar' ";
        endif;
        $sql = "select (select aa.s_namakorek from view_rekening aa where aa.s_jenisobjek=t1.s_jenisobjek and aa.s_rinciankorek='00' ) korek_parent, 
            t1.s_namakorek korek_child, t_nopembayaran, t2.t_namaobjek, t3.t_npwpd, 
            t0.t_jmlhpembayaran, t1.s_jenisobjek, t0.t_kodebayar from t_transaksi t0
left join view_rekening t1 ON t1.s_idkorek=t0.t_idkorek
left join view_wpobjek t2 ON t2.t_idobjek=t0.t_idwpobjek
left join view_wp t3 ON t3.t_idwp=t2.t_idwp
where t_jmlhpembayaran != 0 and t_tglpembayaran between '" . $tgl1 . "' and '" . $tgl2 . "' " . $whereviabayar . "
order by t0.t_kodebayar, t1.s_tipekorek, t1.s_kelompokkorek, t1.s_jeniskorek, t1.s_objekkorek, t1.s_rinciankorek";
//        die($sql);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getdataBppsRinci($bulan, $viabayar, $jenispajak) {
        if ($viabayar != NULL):
            $whereviabayar = " and t0.t_viapembayaran::text='$viabayar' ";
        endif;
        if ($jenispajak != NULL):
            $wherejenispajak = " and t0.t_jenispajak = $jenispajak ";
        endif;
        $sql = "SELECT
	t0.*
FROM
	t_transaksi t0
WHERE
	EXTRACT (
		MONTH
		FROM
			t0.t_tglpembayaran
	) :: INT = $bulan
AND EXTRACT (YEAR FROM t0.t_tglpembayaran) :: INT = " . date('Y') . "
$wherejenispajak";
//        die($sql);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getdataBppsRinciBulanLalu($bulan, $viabayar, $jenispajak) {
        if ($viabayar != NULL):
            $whereviabayar = " and t0.t_viapembayaran::text='$viabayar' ";
        endif;
        if ($jenispajak != NULL):
            $wherejenispajak = " and t0.t_jenispajak = $jenispajak ";
        endif;
        $sql = "SELECT
	sum(t0.t_jmlhpembayaran) as jumlah
FROM
	t_transaksi t0
WHERE
	EXTRACT (
		MONTH
		FROM
			t0.t_tglpembayaran
	) :: INT < $bulan
AND EXTRACT (YEAR FROM t0.t_tglpembayaran) :: INT = " . date('Y') . "
$wherejenispajak";
//        die($sql);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();
        return $res;
    }

    public function getdataBppsRinciBulanIni($bulan, $viabayar, $jenispajak) {
        if ($viabayar != NULL):
            $whereviabayar = " and t0.t_viapembayaran::text='$viabayar' ";
        endif;
        if ($jenispajak != NULL):
            $wherejenispajak = " and t0.t_jenispajak = $jenispajak ";
        endif;
        $sql = "SELECT
	sum(t0.t_jmlhpembayaran) as jumlah
FROM
	t_transaksi t0
WHERE
	EXTRACT (
		MONTH
		FROM
			t0.t_tglpembayaran
	) :: INT <= $bulan
AND EXTRACT (YEAR FROM t0.t_tglpembayaran) :: INT = " . date('Y') . "
$wherejenispajak";
//        die($sql);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();
        return $res;
    }

    public function getdataRekapPenerimaan($tgl1, $tgl2, $viabayar) {
        $tgl1 = date('Y-m-d', strtotime($tgl1));
        $tgl2 = date('Y-m-d', strtotime($tgl2));
        if ($viabayar != NULL):
            $whereviabayar = " and t0.t_viapembayaran::text='$viabayar' ";
            $whereviabayardenda = " and t0.t_viapembayarandenda::text='$viabayar' ";
        endif;
        $sql = "SELECT
	*
FROM
	(
		SELECT
			t_rek.korek, t_rek.s_namakorek,
			CASE
		WHEN s_tipekorek || s_kelompokkorek || s_jeniskorek || s_objekkorek = '41407' THEN
			(
				SELECT
					SUM (t_jmlhbayardenda)
				FROM
					t_transaksi t0
				LEFT JOIN view_rekening t1 ON t1.s_idkorek = t0.t_idkorekdenda
				WHERE
					t1.s_tipekorek = t_rek.s_tipekorek
				AND t1.s_kelompokkorek = t_rek.s_kelompokkorek
				AND t1.s_jeniskorek = t_rek.s_jeniskorek
				AND t1.s_objekkorek = t_rek.s_objekkorek
				AND t1.s_rinciankorek = t_rek.s_rinciankorek
				AND t0.t_tglbayardenda BETWEEN '" . $tgl1 . "'
				AND '" . $tgl2 . "' $whereviabayardenda
			)
		ELSE
			(
				SELECT
					SUM (t_jmlhpembayaran)
				FROM
					t_transaksi t0
				LEFT JOIN view_rekening t1 ON t1.s_idkorek = t0.t_idkorek
				WHERE
					t1.s_tipekorek = t_rek.s_tipekorek
				AND t1.s_kelompokkorek = t_rek.s_kelompokkorek
				AND t1.s_jeniskorek = t_rek.s_jeniskorek
				AND t1.s_objekkorek = t_rek.s_objekkorek
				AND t0.t_tglpembayaran BETWEEN '" . $tgl1 . "'
				AND '" . $tgl2 . "' $whereviabayar
			)
	 END AS jumlah
		FROM
			view_rekening t_rek
		WHERE
			s_rinciankorek = '00'
		OR (
			s_rinciankorek != '00'
			AND s_tipekorek || s_kelompokkorek || s_jeniskorek || s_objekkorek = '41407'
		)
	) AS rekap
WHERE jumlah is not null
order by korek";

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getdataBPP($bulan, $tahun) {

        $sql = "select * from ((
select 
t0.t_tglpembayaran as tglbayar,
t0.t_kodebayar as nobukti,
t_rek.korek,
t_rek.s_namakorek as uraian,
t0.t_jmlhpembayaran as penerimaan,
(select aa.t_jmlhsbd from t_setorbankdetail aa where aa.t_idtransaksi=t0.t_idtransaksi and aa.t_idkoreksbd=t0.t_idkorek) as penyetoran,
(select t_nosbh from t_setorbankheader where t_idsbh=(select aa.t_idsbh from t_setorbankdetail aa where aa.t_idtransaksi=t0.t_idtransaksi and aa.t_idkoreksbd=t0.t_idkorek)) as nosts,
(select t_tglsbh from t_setorbankheader where t_idsbh=(select aa.t_idsbh from t_setorbankdetail aa where aa.t_idtransaksi=t0.t_idtransaksi and aa.t_idkoreksbd=t0.t_idkorek)) as tglsetor
from t_transaksi t0
left join view_rekening t_rek ON t_rek.s_idkorek=t0.t_idkorek
where extract(month from t0.t_tglpembayaran)::int = " . $bulan . " AND extract(year from t0.t_tglpembayaran)::int = " . $tahun . "
)
UNION
(
select 
t0.t_tglbayardenda as tglbayar,
t0.t_kodebayar as nobukti,
t_rek.korek,
t_rek.s_namakorek as uraian,
t0.t_jmlhbayardenda as penerimaan,
(select aa.t_jmlhsbd from t_setorbankdetail aa where aa.t_idtransaksi=t0.t_idtransaksi and aa.t_idkoreksbd=t0.t_idkorekdenda) as penyetoran,
(select bb.t_nosbh from t_setorbankdetail aa left join t_setorbankheader bb ON aa.t_idsbh=bb.t_idsbh where aa.t_idtransaksi=t0.t_idtransaksi and aa.t_idkoreksbd=t0.t_idkorekdenda) as nosts,
(select t_tglsbh from t_setorbankheader where t_idsbh=(select aa.t_idsbh from t_setorbankdetail aa where aa.t_idtransaksi=t0.t_idtransaksi and aa.t_idkoreksbd=t0.t_idkorekdenda)) as tglsetor
from t_transaksi t0
left join view_rekening t_rek ON t_rek.s_idkorek=t0.t_idkorekdenda
where extract(month from t0.t_tglbayardenda)::int = " . $bulan . " AND extract(year from t0.t_tglbayardenda)::int = " . $tahun . "
)) as bpp order by tglbayar";
//        die($sql);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getdataRealisasiObjek($t_periodepajak, $t_kecamatanobjek, $t_kelurahanobjek) {
        $where_kec = "";
        if (!empty($t_kecamatanobjek)) {
            $where_kec .= "and b.t_kecamatanobjek = $t_kecamatanobjek";
        }

        $where_kel = "";
        if (!empty($t_kelurahanobjek)) {
            $where_kel .= "and b.t_kelurahanobjek = $t_kelurahanobjek";
        }
        $sql = "select korek, s_namakorek, s_rinciankorek,
                    CASE 
                    WHEN C .s_rinciankorek = '00' THEN
                    (
                        select sum(t_jmlhpajak) as jmlhpajak
                            from t_transaksi a
                            left join t_wpobjek b on b.t_idobjek = a.t_idwpobjek
                            left join view_rekening f on f.s_idkorek = a.t_idkorek
                            where f.s_tipekorek = c.s_tipekorek
                            and f.s_kelompokkorek = c.s_kelompokkorek
                            and f.s_jeniskorek = c.s_jeniskorek
                            and f.s_objekkorek = c.s_objekkorek
                            and a.t_periodepajak = '" . $t_periodepajak . "' $where_kec $where_kel
                    )
                    WHEN c.s_tipekorek||c.s_kelompokkorek||c.s_jeniskorek||c.s_objekkorek='41407' THEN
                    (
                        select sum(t_jmlhdendapembayaran) as jmlhdenda
                            from t_transaksi a
                            left join t_wpobjek b on b.t_idobjek = a.t_idwpobjek
                            where c.s_idkorek = a.t_idkorekdenda
                            and extract(year from a.t_tgldendapembayaran) = '" . $t_periodepajak . "' $where_kec $where_kel
                    )
                    ELSE
                    (
                            select sum(t_jmlhpajak) as jmlhpajak
                            from t_transaksi a
                            left join t_wpobjek b on b.t_idobjek = a.t_idwpobjek
                            where c.s_idkorek = a.t_idkorek
                            and a.t_periodepajak = '" . $t_periodepajak . "' $where_kec $where_kel
                    ) END as penetapan,
                    
                    CASE
                    WHEN C .s_rinciankorek = '00' THEN
                    (
                        select sum(t_jmlhpembayaran) as jmlhbayar
                            from t_transaksi a
                            left join t_wpobjek b on b.t_idobjek = a.t_idwpobjek
                            left join view_rekening f on f.s_idkorek = a.t_idkorek
                            where f.s_tipekorek = c.s_tipekorek
                            and f.s_kelompokkorek = c.s_kelompokkorek
                            and f.s_jeniskorek = c.s_jeniskorek
                            and f.s_objekkorek = c.s_objekkorek
                            and a.t_periodepajak = '" . $t_periodepajak . "' $where_kec $where_kel
                    )
                    WHEN c.s_tipekorek||c.s_kelompokkorek||c.s_jeniskorek||c.s_objekkorek='41407' THEN
                    (
                        select sum(t_jmlhbayardenda) as jmlhdenda
                            from t_transaksi a
                            left join t_wpobjek b on b.t_idobjek = a.t_idwpobjek
                            where c.s_idkorek = a.t_idkorekdenda
                            and extract(year from a.t_tglbayardenda) = '" . $t_periodepajak . "' $where_kec $where_kel
                    )
                    ELSE
                    (
                            select sum(t_jmlhpembayaran) as jmlhbayar
                            from t_transaksi a
                            left join t_wpobjek b on b.t_idobjek = a.t_idwpobjek
                            where c.s_idkorek = a.t_idkorek
                            and a.t_periodepajak = '" . $t_periodepajak . "' $where_kec $where_kel
                    ) END as realisasi
                from view_rekening c order by korek";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getdataKetetapanSetoran($tgl, $jenisobj, $korekid, $kecamatan) {

        if (!empty($jenisobj)):
            $wherejenisobj = " where b.s_idjenis = " . $jenisobj . " ";
        endif;
        if ($korekid != NULL):
            $wherekorekid = " and c.t_idkorek = $korekid ";
            $wherekorekobjek = " where idkorek_objek = $korekid ";
        endif;
        if (!empty($kecamatan)):
            $wherekecamatan = " and b.t_kecamatanobjek = " . $kecamatan . " ";
        endif;
        $tglmasa = explode("-", $tgl);
        $periode = $tglmasa[2];
        $sql = "select * from (select t_nama, s_namajenis, t_npwpd, t_nop, t_namaobjek,
                (select tt.t_idkorek from t_transaksi tt where tt.t_idwpobjek=b.t_idobjek limit 1 ) as idkorek_objek,
		(
			select sum(t_jmlhpajak) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '01' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as data_jan, 
		(
			select sum(t_jmlhpembayaran) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '01' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as bayar_jan, 
		(
			select sum(t_jmlhpajak) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '02' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as data_feb, 
		(
			select sum(t_jmlhpembayaran) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '02' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as bayar_feb, 
		(
			select sum(t_jmlhpajak) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '03' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as data_mar, 
		(
			select sum(t_jmlhpembayaran) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '03' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as bayar_mar, 
		(
			select sum(t_jmlhpajak) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '04' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as data_apr, 
		(
			select sum(t_jmlhpembayaran) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '04' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as bayar_apr, 
		(
			select sum(t_jmlhpajak) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '05' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as data_mei, 
		(
			select sum(t_jmlhpembayaran) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '05' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as bayar_mei, 
		(
			select sum(t_jmlhpajak) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '06' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as data_jun, 
		(
			select sum(t_jmlhpembayaran) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '06' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as bayar_jun, 
		(
			select sum(t_jmlhpajak) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '07' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as data_jul, 
		(
			select sum(t_jmlhpembayaran) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '07' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as bayar_jul, 
		(
			select sum(t_jmlhpajak) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '08' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as data_agu, 
		(
			select sum(t_jmlhpembayaran) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '08' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as bayar_agu, 
		(
			select sum(t_jmlhpajak) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '09' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as data_sep, 
		(
			select sum(t_jmlhpembayaran) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '09' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as bayar_sep, 
		(
			select sum(t_jmlhpajak) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '10' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as data_okt, 
		(
			select sum(t_jmlhpembayaran) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '10' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as bayar_okt, 
		(
			select sum(t_jmlhpajak) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '11' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as data_nov, 
		(
			select sum(t_jmlhpembayaran) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '11' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as bayar_nov, 
		(
			select sum(t_jmlhpajak) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '12' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as data_des, 
		(
			select sum(t_jmlhpembayaran) from t_transaksi c
			where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_tglpendataan) = '12' and extract(year from t_tglpendataan) = '" . $periode . "'
		) as bayar_des
                from view_wp a
                left join view_wpobjek b on a.t_idwp = b.t_idwp
                
                " . $wherejenisobj . "
                order by b.s_idjenis asc ) ketset $wherekorekobjek ";

        // if (!empty($jenisobj)):
        // $wherejenisobj = " where b.s_idjenis = " . $jenisobj . " ";
        // endif;
        // if ($korekid != NULL):
        // $wherekorekid = " and c.t_idkorek = $korekid ";
        // $wherekorekobjek = " where idkorek_objek = $korekid ";
        // endif;
        // $tglmasa = explode("-", $tgl);
        // $sql = "select * from (select t_nama, s_namajenis, t_npwpd, t_nop, t_namaobjek,
        // (select tt.t_idkorek from t_transaksi tt where tt.t_idwpobjek=b.t_idobjek limit 1 ) as idkorek_objek,
        // (
        // select t_jmlhpajak from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '01' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as data_jan, 
        // (
        // select t_jmlhpembayaran from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '01' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as bayar_jan, 
        // (
        // select t_jmlhpajak from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '02' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as data_feb, 
        // (
        // select t_jmlhpembayaran from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '02' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as bayar_feb, 
        // (
        // select t_jmlhpajak from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '03' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as data_mar, 
        // (
        // select t_jmlhpembayaran from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '03' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as bayar_mar, 
        // (
        // select t_jmlhpajak from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '" . $tglmasa[1] . "' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as data_apr, 
        // (
        // select t_jmlhpembayaran from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '" . $tglmasa[1] . "' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as bayar_apr, 
        // (
        // select t_jmlhpajak from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '05' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as data_mei, 
        // (
        // select t_jmlhpembayaran from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '05' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as bayar_mei, 
        // (
        // select t_jmlhpajak from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '06' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as data_jun, 
        // (
        // select t_jmlhpembayaran from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '06' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as bayar_jun, 
        // (
        // select t_jmlhpajak from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '07' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as data_jul, 
        // (
        // select t_jmlhpembayaran from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '07' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as bayar_jul, 
        // (
        // select t_jmlhpajak from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '08' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as data_agu, 
        // (
        // select t_jmlhpembayaran from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '08' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as bayar_agu, 
        // (
        // select t_jmlhpajak from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '09' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as data_sep, 
        // (
        // select t_jmlhpembayaran from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '09' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as bayar_sep, 
        // (
        // select t_jmlhpajak from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '10' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as data_okt, 
        // (
        // select t_jmlhpembayaran from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '10' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as bayar_okt, 
        // (
        // select t_jmlhpajak from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '11' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as data_nov, 
        // (
        // select t_jmlhpembayaran from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '11' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as bayar_nov, 
        // (
        // select t_jmlhpajak from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '12' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as data_des, 
        // (
        // select t_jmlhpembayaran from t_transaksi c
        // where c.t_idwpobjek = b.t_idobjek $wherekorekid and extract(month from t_masaawal) = '12' and extract(year from t_masaawal) = '" . $tglmasa[2] . "'
        // ) as bayar_des
        // from view_wp a
        // left join view_wpobjek b on a.t_idwp = b.t_idwp
        // " . $wherejenisobj . "
        // order by b.s_idjenis asc ) ketset $wherekorekobjek ";
        //        die($sql);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getDataTransaksiByMasaPajak($masaawaltrans, $periodetrans, $jenisobjtrans) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "s_namajenis"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_tglpendataan", "t_jmlhpajak", "t_tglpembayaran", "t_jmlhpembayaran", "t_tgljatuhtempo", "t_masaawal", "t_masaakhir", "t_periodepajak"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "c.t_idkorek = d.s_idkorek", array(
            "s_namakorek", "korek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal("extract(month from c.t_masaawal) in (" . $masaawaltrans . ")");
        $where->equalTo('t_periodepajak', $periodetrans);
        if ($jenisobjtrans != null) {
            $where->literal("b.t_jenisobjek in (" . $jenisobjtrans . ")");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getdataKetetapanSetoranBulanan($tglawal, $tglakhir, $jenisobj, $kecamatan) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "s_namajenis", "t_namaobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_tglpendataan", "t_jmlhpajak", "t_tglpembayaran", "t_jmlhpembayaran", "t_tgljatuhtempo", "t_masaawal", "t_masaakhir",
            "t_periodepajak"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "c.t_idkorek = d.s_idkorek", array(
            "s_namakorek", "korek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->between("c.t_tglpendataan", date('Y-m-d', strtotime($tglawal)), date('Y-m-d', strtotime($tglakhir)));
        if ($jenisobj != null) {
            $where->literal("b.t_jenisobjek in (" . $jenisobj . ")");
        }
        if (!empty($kecamatan)) {
            $where->literal("b.t_kecamatanobjek = " . $kecamatan . " ");
        }

        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getdataRealisasi($tglawal, $tglakhir, $idrealisasi) {
        $bulan_desember = date('m', strtotime($tglakhir));
        if ($bulan_desember == 12) {
            $tgl_akhir = date('Y-11-30', strtotime($tglakhir));
        } else {
            $tgl_akhir = date('Y-m-t', strtotime($tglakhir . " -1 months"));
        }

        /* AUTHOR : RONI MUSTAPA
          email : ronimustapa@gmail.com
         */
        $connect_db_bphtb = "host=localhost user=postgres password=postgres dbname=bphtboki"; //KONEKSI DB BPHTB

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_rekening");
        $select->columns(array(
            "s_tipekorek",
            "s_kelompokkorek",
            "s_jeniskorek",
            "s_objekkorek",
            "s_rinciankorek",
            "s_sub1korek",
            "s_sub2korek",
            "s_sub3korek",
            "korek" => new \Zend\Db\Sql\Expression("(CASE WHEN " .
                    "s_kelompokkorek = '0' THEN s_tipekorek " .
                    "WHEN s_jeniskorek = '0' THEN s_tipekorek || '.' || s_kelompokkorek " .
                    "WHEN s_objekkorek = '0' THEN s_tipekorek || '.' || s_kelompokkorek || '.' || s_jeniskorek " .
                    "WHEN s_rinciankorek = '00' THEN s_tipekorek || '.' || s_kelompokkorek || '.' || s_jeniskorek || '.' || s_objekkorek " .
                    "WHEN s_sub1korek = '00' THEN " .
                    "s_tipekorek || '.' || s_kelompokkorek || '.' || s_jeniskorek || '.' || s_objekkorek || '.' || s_rinciankorek " .
                    "WHEN s_sub2korek = '00' THEN " .
                    "s_tipekorek || '.' || s_kelompokkorek || '.' || s_jeniskorek || '.' || s_objekkorek || '.' || s_rinciankorek || '.' || s_sub1korek " .
                    "WHEN s_sub3korek = '' THEN " .
                    "s_tipekorek || '.' || s_kelompokkorek || '.' || s_jeniskorek || '.' || s_objekkorek || '.' || s_rinciankorek || '.' || s_sub1korek || '.' || s_sub2korek " .
                    "ELSE " .
                    "s_tipekorek || '.' || s_kelompokkorek || '.' || s_jeniskorek || '.' || s_objekkorek || '.' || s_rinciankorek || '.' || s_sub1korek || '.' || s_sub2korek || '.' || s_sub3korek " .
                    "END )"),
            "s_namakorek",
            "target" => new \Zend\Db\Sql\Predicate\Expression("(SELECT coalesce(s_targetjumlah,0) FROM s_target " .
                    "LEFT JOIN s_targetdetail on s_target.s_idtarget = s_targetdetail.s_idtargetheader " .
                    "WHERE s_target.s_idtarget = " . $idrealisasi . " and s_targetdetail.s_targetrekening = s_rekening.s_idkorek)"),
            "transaksi_blnlalu" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END )"),
            "transaksi_blnini" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END )"),
            "transaksi_sdblnini" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(aa.t_jmlhpembayaran), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorek = za.s_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND aa.t_jenispajak != 6 " .
                    ") " .
                    "END )"),
            "minerba_blnlalu" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END )"),
            "minerba_blnini" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END )"),
            "minerba_sdblnini" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bb.t_pajak),0) FROM t_detailminerba bb " .
                    "LEFT JOIN t_transaksi aa on bb.t_idtransaksi = aa.t_idtransaksi " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = bb.t_idkorek " .
                    "WHERE aa.t_tglpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND aa.t_idtransaksi is not null " .
                    ") " .
                    "END )"),
            "dendatransaksi_blnlalu" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    ") " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    ") " .
                    "END )"),
            "dendatransaksi_blnini" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    ") " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    ") " .
                    "END )"),
            "dendatransaksi_sdblnini" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    ") " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jmlhbayardenda), 0) FROM t_transaksi aa " .
                    "LEFT JOIN s_rekening za on aa.t_idkorekdenda = za.s_idkorek " .
                    "WHERE aa.t_tglbayardenda >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglbayardenda <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    ") " .
                    "END )"),
            "setoranlain_blnlalu" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    ") " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    ") " .
                    "END )"),
            "setoranlain_blnini" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    ") " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    ") " .
                    "END )"),
            "setoranlain_sdblnini" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    ") " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(t_jumlahsetor),0) FROM t_setoranlain aa " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = aa.t_idrekening " .
                    "WHERE aa.t_idrekening != 176 AND aa.t_issetorandeleted=0 AND aa.t_tglsetor >= '" . date('Y-m-d', strtotime($tglawal)) . "' and aa.t_tglsetor <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    ") " .
                    "END )"),
            "bphtb_blnlalu" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    ") " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . $tgl_akhir . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    ") " .
                    "END )"),
            "bphtb_blnini" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    ") " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-01', strtotime($tglakhir)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    ") " .
                    "END )"),
            "bphtb_sdblnini" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '0' THEN " .
                    "case when s_rekening.s_objekkorek != '0' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' THEN " .
                    "case when s_rekening.s_sub3korek != '0' THEN " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    ") " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT COALESCE(SUM(bphtb.t_nilaipembayaranspt),0) 
												FROM dblink('" . $connect_db_bphtb . "','SELECT t_tanggalpembayaran,t_nilaipembayaranspt,t_idkorekspt FROM t_pembayaranspt ') AS bphtb(t_tanggalpembayaran date, t_nilaipembayaranspt bigint, t_idkorekspt integer) " .
                    "LEFT JOIN s_rekening za on za.s_idkorek = 176 " .
                    "WHERE bphtb.t_tanggalpembayaran >= '" . date('Y-m-d', strtotime($tglawal)) . "' and bphtb.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tglakhir)) . "' " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    ") " .
                    "END )"),
        ));
        $where = new Where();
        $select->where($where);
        $select->order(new \Zend\Db\Sql\Expression("s_rekening.s_tipekorek, " .
                "s_rekening.s_kelompokkorek, " . "s_rekening.s_jeniskorek, " . "s_rekening.s_objekkorek, " .
                "s_rekening.s_rinciankorek, " . "s_rekening.s_sub1korek"));
//        echo $select->getSqlString();exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataTransaksiByPerMasaPajak($masaawaltrans, $periodetrans, $jenisobjtrans, $korekid) {
//        $sql = new Sql($this->adapter);
//        $select = $sql->select();
//        $select->from(array(
//            "a" => "view_wp"
//        ));
//        $select->join(array(
//            "b" => "view_wpobjek"
//                ), "a.t_idwp = b.t_idwp", array(
//            "t_nop", "s_namajenis"
//                ), $select::JOIN_LEFT);
//        $select->join(array(
//            "c" => "t_transaksi"
//                ), "b.t_idobjek = c.t_idwpobjek", array(
//            "t_tglpendataan","t_nourut", "t_jmlhpajak", "t_tglpembayaran", "t_jmlhpembayaran", "t_tgljatuhtempo", "t_masaawal", "t_masaakhir", "t_periodepajak"
//                ), $select::JOIN_LEFT);
//        $select->join(array(
//            "d" => "view_rekening"
//                ), "c.t_idkorek = d.s_idkorek", array(
//            "s_namakorek", "korek"
//                ), $select::JOIN_LEFT);
//        $where = new Where();
//        $where->literal("extract(month from c.t_masaawal) in (" . $masaawaltrans . ")");
//        $where->equalTo('t_periodepajak', $periodetrans);
//        if ($jenisobjtrans != null) {
//            $where->literal("b.t_jenisobjek in (" . $jenisobjtrans . ")");
//        }
//        $select->where($where);
//        $select->order('d.s_idkorek asc');
//        $state = $sql->prepareStatementForSqlObject($select);
//        $res = $state->execute();
//        return $res;
        if ($jenisobjtrans != null) {
            $where_jenisobjek = " AND b.t_jenisobjek=$jenisobjtrans ";
        } else {
            $where_jenisobjek = " ";
        }

        if ($korekid != null):
            $wherekorekid = " AND c.t_idkorek = $korekid ";
        endif;

        $sql = "SELECT c.t_tglpendataan,c.t_nourut,a.t_npwpd,a.t_nama,a.t_alamat, c.t_jmlhpajak, extract(MONTH FROM c.t_masaawal) as masapajak, extract(YEAR FROM c.t_masaawal) as periodepajak, d.s_namakorek 
                FROM view_wp a
                    LEFT JOIN view_wpobjek b ON a.t_idwp = b.t_idwp
                    LEFT JOIN t_transaksi c ON b.t_idobjek = c.t_idwpobjek
                    LEFT JOIN view_rekening d ON c.t_idkorek = d.s_idkorek
                WHERE extract(month from c.t_masaawal)='$masaawaltrans' AND extract(YEAR FROM c.t_masaawal)='$periodetrans' $where_jenisobjek $wherekorekid 
                ORDER BY d.s_idkorek asc";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getDataTotalTransaksiByPerMasaPajak($masaawaltrans, $periodetrans, $jenisobjtrans, $korekid) {
        if ($jenisobjtrans != null) {
            $where_jenisobjek = " AND c.t_jenispajak=$jenisobjtrans ";
        } else {
            $where_jenisobjek = " ";
        }

        if ($korekid != NULL):
            $wherekorekid = " and c.t_idkorek = $korekid ";
        endif;

        $bulan_lalu = ($masaawaltrans) - 1;

        $sql = "SELECT  (
				select sum(c.t_jmlhpajak) from t_transaksi c
                WHERE extract(month from c.t_masaawal)='$masaawaltrans' AND extract(YEAR FROM c.t_masaawal)='$periodetrans' $where_jenisobjek $wherekorekid)
				as total_bulanini,
				(
				select sum(c.t_jmlhpajak) from t_transaksi c
                WHERE (extract(month from c.t_masaawal) between '01' and '$bulan_lalu') AND extract(YEAR FROM c.t_masaawal)='$periodetrans' $where_jenisobjek $wherekorekid)
				as total_sdbulanlalu,
				(
				select sum(c.t_jmlhpajak) from t_transaksi c
                WHERE (extract(month from c.t_masaawal) between '01' and '$masaawaltrans') AND extract(YEAR FROM c.t_masaawal)='$periodetrans' $where_jenisobjek $wherekorekid)
				as total_sdbulanini";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();
        return $res;
    }

    public function getDataTransaksiByPerMasaAkhirpajak($tglcetak, $periodedimuka, $jenisobjdimuka, $korekid) {
        $tglcetak_thn = date('Y', strtotime($tglcetak));
        if ($jenisobjdimuka != null) {
            $where_jenisobjek = " AND b.t_jenisobjek=$jenisobjdimuka ";
        } else {
            $where_jenisobjek = " ";
        }

        if ($korekid != null):
            $wherekorekid = " AND c.t_idkorek = $korekid ";
        endif;

        $sql = "SELECT c.t_tglpendataan,c.t_nourut,a.t_npwpd,a.t_nama,b.t_namaobjek, a.t_alamat, c.t_jmlhpajak, c.t_nopembayaran, c.t_tglpembayaran, c.t_jmlhpembayaran, c.t_jenispajak, c.t_masaawal, c.t_masaakhir, extract(MONTH FROM c.t_masaawal) as masapajak, extract(YEAR FROM c.t_masaawal) as periodepajak, d.s_namakorek 
                FROM view_wp a
                    LEFT JOIN view_wpobjek b ON a.t_idwp = b.t_idwp
                    LEFT JOIN t_transaksi c ON b.t_idobjek = c.t_idwpobjek
                    LEFT JOIN view_rekening d ON c.t_idkorek = d.s_idkorek
                WHERE c.t_periodepajak='$periodedimuka' and extract(year from c.t_masaakhir)='$tglcetak_thn' $where_jenisobjek $wherekorekid 
                ORDER BY d.s_idkorek asc";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getdatagridBKU($bulan, $tahun) {
        $sql = "SELECT * FROM 
                ((
                        SELECT
                            t0.t_tglpembayaran AS tglbayar,
                            t0.t_jmlhpembayaran AS pembayaran,
                            t0.t_jmlhpembayaran AS penyetoran,
                            t_rek.korek,
                            t_rek.s_namakorek
                        FROM
                            t_transaksi t0
                        LEFT JOIN view_rekening t_rek ON t_rek.s_idkorek = t0.t_idkorek
                        WHERE t0.t_tglpembayaran IS NOT NULL 
                        
                        ORDER BY t0.t_tglpembayaran
                    )
                UNION
                    (
                        SELECT
                            t0.t_tglbayardenda AS tglbayar,
                            t0.t_jmlhbayardenda AS pembayaran,
                            t0.t_jmlhbayardenda AS penyetoran,
                            t_rekbunga.korek,
                            t_rekbunga.s_namakorek
                        FROM
                            t_transaksi t0
                        LEFT JOIN view_rekening t_rekbunga ON t_rekbunga.s_idkorek = t0.t_idkorekdenda
                        WHERE t0.t_tglbayardenda IS NOT NULL 
                        
                        ORDER BY t0.t_tglbayardenda
                    )
                UNION
                    (
                        SELECT 
                            bb.t_tglsbh AS tglbayar,
                            0 AS pembayaran,
                            sum(aa.t_jmlhsbd) AS penyetoran,
                            t_rek.korek,
                            'SETOR '||t_rek.s_namakorek
                        FROM t_setorbankdetail aa 
                            LEFT JOIN t_setorbankheader bb ON aa.t_idsbh=bb.t_idsbh
                            LEFT JOIN view_rekening t_rek ON t_rek.s_idkorek=aa.t_idkoreksbd
                        GROUP BY bb.t_tglsbh, t_rek.korek, t_rek.s_namakorek
                        ORDER BY bb.t_tglsbh
                )) AS bku 
                WHERE pembayaran <> 0 
                and penyetoran <> 0 
                and extract(month from tglbayar)::int = " . $bulan . " 
                AND extract(year from tglbayar)='" . $tahun . "' 
                ORDER BY tglbayar asc, korek asc";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }
    
    public function getdataBPS($post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "b.t_idobjek = a.t_idwpobjek", array(
            "t_nop", "t_namaobjek", "t_namawp", "t_npwpdwp"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_rekening"
                ), "c.s_idkorek = a.t_idkorek", array(
            "korek", "s_namakorek", "s_persentarifkorek", "s_namajenis"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal("t_tglpembayaran >= '" . date('Y-m-d', strtotime($post->tgl_bps1)) . "'");
        $where->literal("t_tglpembayaran <= '" . date('Y-m-d', strtotime($post->tgl_bps2)) . "'");
        if (!empty($post->jenis_bps)) {
            $where->literal("t_jenispajak = " . $post->jenis_bps . "");
        }
		if (!empty($post->rekening)) {
            $where->literal("a.t_idkorek = " . $post->rekening . "");
        }
		if (!empty($post->kecamatan)) {
            $where->literal("b.t_kecamatanobjek = " . $post->kecamatan . "");
        }
		if (!empty($post->kelurahan)) {
            $where->literal("b.t_kelurahanobjek = " . $post->kelurahan . "");
        }
        if (!empty($post->via_bps)) {
            $where->literal("t_viapembayaran = " . $post->via_bps . "");
        }
        $select->where($where);
        $select->order('t_jenispajak asc, t_tglpembayaran asc, korek asc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function getdataRealisasiTgl($tgl1, $tgl2, $periodetargetreal, $jenistargetreal, $tiperekreal) {
        $awaltahun = date("Y-01-01", strtotime($tgl1));
        $akhirtahun = date("Y-12-31", strtotime($tgl2));
        $tglreal1 = date("Y-m-d", strtotime($tgl1));
        $tglreal2 = date("Y-m-d", strtotime($tgl2));
        $tahun = date("Y", strtotime($tgl1));
        $where = " ";
        if ($tiperekreal == 'pokok') {
            $where .= " AND C.s_kelompokkorek != '0' AND C.s_jeniskorek != '0' AND C.s_rinciankorek = '00' ";
        }elseif ($tiperekreal == 'induk') {
			$where .= " AND C.s_kelompokkorek != '0' AND C.s_objekkorek = '00' AND C.s_rinciankorek = '00' ";
		}
        $sql = "SELECT C
        .s_idkorek,
	C.korek,
	C.s_namakorek,
	C.s_jeniskorek,
	C.s_objekkorek,
	C.s_rinciankorek,
	(
	SELECT
	CASE
			
		WHEN C
			.s_objekkorek = '00' THEN
				(
				SELECT SUM
					( s_targetjumlah ) 
				FROM
					s_targetdetail d
					LEFT JOIN s_target e ON e.s_idtarget = d.s_idtargetheader
					LEFT JOIN view_rekening f ON f.s_idkorek = d.s_targetrekening 
				WHERE
					f.s_tipekorek = C.s_tipekorek 
					AND f.s_kelompokkorek = C.s_kelompokkorek 
					AND f.s_jeniskorek = C.s_jeniskorek 
					AND e.s_tahuntarget = '" . $periodetargetreal . "' 
					AND e.s_statustarget = " . $jenistargetreal . "
				) 
				WHEN C.s_rinciankorek = '00' THEN
				(
				SELECT SUM
					( s_targetjumlah ) 
				FROM
					s_targetdetail d
					LEFT JOIN s_target e ON e.s_idtarget = d.s_idtargetheader
					LEFT JOIN view_rekening f ON f.s_idkorek = d.s_targetrekening 
				WHERE
					f.s_tipekorek = C.s_tipekorek 
					AND f.s_kelompokkorek = C.s_kelompokkorek 
					AND f.s_jeniskorek = C.s_jeniskorek 
					AND f.s_objekkorek = C.s_objekkorek 
					AND e.s_tahuntarget = '" . $periodetargetreal . "' 
					AND e.s_statustarget = " . $jenistargetreal . "
					) ELSE (
				SELECT
					s_targetjumlah 
				FROM
					s_targetdetail d
					LEFT JOIN s_target e ON e.s_idtarget = d.s_idtargetheader 
				WHERE
					d.s_targetrekening = C.s_idkorek 
					AND e.s_tahuntarget = '" . $periodetargetreal . "' 
					AND e.s_statustarget = " . $jenistargetreal . "
				) 
			END AS jml 
		) AS target,
		(
		SELECT
		CASE
				
			WHEN C
				.s_kelompokkorek = '0' THEN
					(
					SELECT SUM
						( t_jmlhpembayaran ) 
					FROM
						t_transaksi
						A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek 
					WHERE
						f.s_tipekorek = C.s_tipekorek 
						AND A.t_tglpembayaran >= '" . $awaltahun . "'
						AND A.t_tglpembayaran < '" . $tglreal1 . "'
						AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "' 
					) 
					WHEN C.s_jeniskorek = '0' THEN
					(
					SELECT SUM
						( t_jmlhpembayaran ) 
					FROM
						t_transaksi
						A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek 
					WHERE
						f.s_tipekorek = C.s_tipekorek 
						AND f.s_kelompokkorek = C.s_kelompokkorek 
						AND A.t_tglpembayaran >= '" . $awaltahun . "'
						AND A.t_tglpembayaran < '" . $tglreal1 . "'
						AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "' 
					) 
					WHEN C.s_objekkorek = '00' THEN
					(
					SELECT SUM
						( t_jmlhpembayaran ) 
					FROM
						t_transaksi
						A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek 
					WHERE
						f.s_tipekorek = C.s_tipekorek 
						AND f.s_kelompokkorek = C.s_kelompokkorek 
						AND f.s_jeniskorek = C.s_jeniskorek 
						AND A.t_tglpembayaran >= '" . $awaltahun . "'
						AND A.t_tglpembayaran < '" . $tglreal1 . "'
						AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "' 
					) 
					WHEN C.s_rinciankorek = '00' THEN
					(
					SELECT SUM
						( t_jmlhpembayaran ) 
					FROM
						t_transaksi
						A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek 
					WHERE
						f.s_tipekorek = C.s_tipekorek 
						AND f.s_kelompokkorek = C.s_kelompokkorek 
						AND f.s_jeniskorek = C.s_jeniskorek 
						AND f.s_objekkorek = C.s_objekkorek 
						AND A.t_tglpembayaran >= '" . $awaltahun . "'
						AND A.t_tglpembayaran < '" . $tglreal1 . "'
						AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "' 
					) 
					WHEN C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek || C.s_objekkorek = '41106' THEN
					(
					SELECT COALESCE
						( SUM ( bb.t_pajak ), 0 ) 
					FROM
						t_detailminerba bb
						LEFT JOIN t_transaksi A ON bb.t_idtransaksi = A.t_idtransaksi
						LEFT JOIN view_rekening f ON f.s_idkorek = bb.t_idkorek 
					WHERE
						f.s_tipekorek = C.s_tipekorek 
						AND f.s_kelompokkorek = C.s_kelompokkorek 
						AND f.s_jeniskorek = C.s_jeniskorek 
						AND f.s_objekkorek = C.s_objekkorek 
						AND f.s_rinciankorek = C.s_rinciankorek 
						AND A.t_tglpembayaran >= '" . $awaltahun . "'
						AND A.t_tglpembayaran < '" . $tglreal1 . "'
						AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "' 
						AND A.t_jenispajak = 6 
					) 
					WHEN C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek || C.s_objekkorek = '41407' THEN
					(
					SELECT SUM
						( t_jmlhbayardenda ) 
					FROM
						t_transaksi
						A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorekdenda 
					WHERE
						f.s_idkorek = C.s_idkorek 
						AND A.t_tglbayardenda >= '" . $awaltahun . "'
						AND A.t_tglbayardenda < '" . $tglreal1 . "'
						AND EXTRACT(YEAR FROM A.t_tglbayardenda) = '" . $tahun . "' 
						) ELSE (
					SELECT SUM
						( t_jmlhpembayaran ) 
					FROM
						t_transaksi A 
					WHERE
						C.s_idkorek = A.t_idkorek 
						AND A.t_tglpembayaran >= '" . $awaltahun . "'
						AND A.t_tglpembayaran < '" . $tglreal1 . "'
						AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "' 
					) 
				END AS jml 
			) AS pokok_sebelum,
			(
			SELECT
			CASE
					
					WHEN C
					.s_kelompokkorek = '0' THEN
						(
						SELECT SUM
							( t_jmlhpembayaran ) 
						FROM
							t_transaksi
							A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek 
						WHERE
							f.s_tipekorek = C.s_tipekorek 
							AND A.t_tglpembayaran >= '" . $tglreal1 . "'
							AND A.t_tglpembayaran <= '" . $tglreal2 . "'
							AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "'
						) 
						WHEN C.s_jeniskorek = '0' THEN
						(
						SELECT SUM
							( t_jmlhpembayaran ) 
						FROM
							t_transaksi
							A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek 
						WHERE
							f.s_tipekorek = C.s_tipekorek 
							AND f.s_kelompokkorek = C.s_kelompokkorek 
							AND A.t_tglpembayaran >= '" . $tglreal1 . "'
							AND A.t_tglpembayaran <= '" . $tglreal2 . "'
							AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "'
						) 
						WHEN C.s_objekkorek = '00' THEN
						(
						SELECT SUM
							( t_jmlhpembayaran ) 
						FROM
							t_transaksi
							A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek 
						WHERE
							f.s_tipekorek = C.s_tipekorek 
							AND f.s_kelompokkorek = C.s_kelompokkorek 
							AND f.s_jeniskorek = C.s_jeniskorek 
							AND A.t_tglpembayaran >= '" . $tglreal1 . "'
							AND A.t_tglpembayaran <= '" . $tglreal2 . "'
							AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "'
						) 
						WHEN C.s_rinciankorek = '00' THEN
						(
						SELECT SUM
							( t_jmlhpembayaran ) 
						FROM
							t_transaksi
							A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek 
						WHERE
							f.s_tipekorek = C.s_tipekorek 
							AND f.s_kelompokkorek = C.s_kelompokkorek 
							AND f.s_jeniskorek = C.s_jeniskorek 
							AND f.s_objekkorek = C.s_objekkorek 
							AND A.t_tglpembayaran >= '" . $tglreal1 . "'
							AND A.t_tglpembayaran <= '" . $tglreal2 . "'
							AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "'
						) 
						WHEN C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek || C.s_objekkorek = '41106' THEN
						(
						SELECT COALESCE
							( SUM ( bb.t_pajak ), 0 ) 
						FROM
							t_detailminerba bb
							LEFT JOIN t_transaksi A ON bb.t_idtransaksi = A.t_idtransaksi
							LEFT JOIN view_rekening f ON f.s_idkorek = bb.t_idkorek 
						WHERE
							f.s_tipekorek = C.s_tipekorek 
							AND f.s_kelompokkorek = C.s_kelompokkorek 
							AND f.s_jeniskorek = C.s_jeniskorek 
							AND f.s_objekkorek = C.s_objekkorek 
							AND f.s_rinciankorek = C.s_rinciankorek 
							AND A.t_tglpembayaran >= '" . $tglreal1 . "'
							AND A.t_tglpembayaran <= '" . $tglreal2 . "'
							AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "'
							AND A.t_jenispajak = 6 
						) 
						WHEN C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek || C.s_objekkorek = '41407' THEN
						(
						SELECT SUM
							( t_jmlhbayardenda ) 
						FROM
							t_transaksi
							A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorekdenda 
						WHERE
							f.s_idkorek = C.s_idkorek 
							AND A.t_tglbayardenda >= '" . $tglreal1 . "'
							AND A.t_tglbayardenda <= '" . $tglreal2 . "'
							AND EXTRACT(YEAR FROM A.t_tglbayardenda) = '" . $tahun . "' 
							) ELSE (
						SELECT SUM
							( t_jmlhpembayaran ) 
						FROM
							t_transaksi A 
						WHERE
							C.s_idkorek = A.t_idkorek 
							AND A.t_tglpembayaran >= '" . $tglreal1 . "'
							AND A.t_tglpembayaran <= '" . $tglreal2 . "'
							AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "'
						) 
					END AS jml 
				) AS pokok_sekarang,
			(
			SELECT
			CASE
					WHEN C.s_kelompokkorek = '0' THEN
						(
						SELECT SUM
							( t_jmlhpembayaran ) 
						FROM
							t_transaksi
							A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek 
						WHERE
							f.s_tipekorek = C.s_tipekorek
							AND A.t_tglpembayaran >= '" . $awaltahun . "'
                            AND A.t_tglpembayaran <= '" . $akhirtahun . "'
							AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "'
						)
						WHEN C.s_jeniskorek = '0' THEN
						(
						SELECT SUM
							( t_jmlhpembayaran ) 
						FROM
							t_transaksi
							A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek 
						WHERE
							f.s_tipekorek = C.s_tipekorek 
							AND f.s_kelompokkorek = C.s_kelompokkorek 
							AND A.t_tglpembayaran >= '" . $awaltahun . "'
                            AND A.t_tglpembayaran <= '" . $akhirtahun . "'
							AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "'
						)	
						WHEN C.s_objekkorek = '00' THEN
						(
						SELECT SUM
							( t_jmlhpembayaran ) 
						FROM
							t_transaksi
							A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek 
						WHERE
							f.s_tipekorek = C.s_tipekorek 
							AND f.s_kelompokkorek = C.s_kelompokkorek 
							AND f.s_jeniskorek = C.s_jeniskorek 
							AND A.t_tglpembayaran >= '" . $awaltahun . "'
							AND A.t_tglpembayaran <= '" . $akhirtahun . "'
							AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "'
						) 
						WHEN C.s_rinciankorek = '00' THEN
						(
						SELECT SUM
							( t_jmlhpembayaran ) 
						FROM
							t_transaksi
							A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorek 
						WHERE
							f.s_tipekorek = C.s_tipekorek 
							AND f.s_kelompokkorek = C.s_kelompokkorek 
							AND f.s_jeniskorek = C.s_jeniskorek 
							AND f.s_objekkorek = C.s_objekkorek 
							AND A.t_tglpembayaran >= '" . $awaltahun . "'
                                                        AND A.t_tglpembayaran <= '" . $akhirtahun . "'
							AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "'
						)
						WHEN C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek || C.s_objekkorek = '41106' THEN
						(
						SELECT COALESCE
							( SUM ( bb.t_pajak ), 0 ) 
						FROM
							t_detailminerba bb
							LEFT JOIN t_transaksi A ON bb.t_idtransaksi = A.t_idtransaksi
							LEFT JOIN view_rekening f ON f.s_idkorek = bb.t_idkorek 
						WHERE
							f.s_tipekorek = C.s_tipekorek 
							AND f.s_kelompokkorek = C.s_kelompokkorek 
							AND f.s_jeniskorek = C.s_jeniskorek 
							AND f.s_objekkorek = C.s_objekkorek 
							AND f.s_rinciankorek = C.s_rinciankorek 
							AND A.t_tglpembayaran >= '" . $awaltahun . "'
                                                        AND A.t_tglpembayaran <= '" . $akhirtahun . "'
							AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "'
							AND A.t_jenispajak = 6 
						) 
						WHEN C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek || C.s_objekkorek = '41407' THEN
						(
						SELECT SUM
							( t_jmlhbayardenda ) 
						FROM
							t_transaksi
							A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idkorekdenda 
						WHERE
							f.s_idkorek = C.s_idkorek 
                                                        AND A.t_tglbayardenda >= '" . $awaltahun . "'
                                                        AND A.t_tglbayardenda <= '" . $akhirtahun . "'
							AND EXTRACT(YEAR FROM A.t_tglbayardenda) = '" . $tahun . "' 
							) ELSE (
						SELECT SUM
							( t_jmlhpembayaran ) 
						FROM
							t_transaksi A 
						WHERE
							C.s_idkorek = A.t_idkorek 
							AND A.t_tglpembayaran >= '" . $awaltahun . "'
                                                        AND A.t_tglpembayaran <= '" . $akhirtahun . "'
							AND EXTRACT ( YEAR FROM A.t_tglpembayaran ) = '" . $tahun . "'
						) 
					END AS jml 
				) AS pokok_sesudah,
					(
					SELECT
					CASE
							
						WHEN C
							.s_objekkorek = '00' THEN
								(
								SELECT SUM
									( t_jumlahsetor ) 
								FROM
									t_setoranlain
									A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idrekening 
								WHERE
									f.s_tipekorek = C.s_tipekorek 
									AND C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek != '411'
									AND f.s_kelompokkorek = C.s_kelompokkorek 
									AND f.s_jeniskorek = C.s_jeniskorek 
									AND A.t_tglsetor >= '".$awaltahun."' 
									AND A.t_tglsetor < '".$tglreal1."' 
									AND EXTRACT ( YEAR FROM A.t_tglsetor ) = '" . $tahun . "' 
                                                                            AND A.t_issetorandeleted = 0
								) 
								WHEN C.s_rinciankorek = '00' THEN
								(
								SELECT SUM
									( t_jumlahsetor ) 
								FROM
									t_setoranlain
									A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idrekening 
								WHERE
									f.s_tipekorek = C.s_tipekorek 
									AND C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek != '411'
									AND f.s_kelompokkorek = C.s_kelompokkorek 
									AND f.s_jeniskorek = C.s_jeniskorek 
									AND f.s_objekkorek = C.s_objekkorek 
									AND A.t_tglsetor >= '".$awaltahun."' 
									AND A.t_tglsetor < '".$tglreal1."' 
									AND EXTRACT ( YEAR FROM A.t_tglsetor ) = '" . $tahun . "' 
                                                                            AND A.t_issetorandeleted = 0
									) ELSE (
								SELECT SUM
									( t_jumlahsetor ) 
								FROM
									t_setoranlain A 
								WHERE
									C.s_idkorek = A.t_idrekening 
									AND C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek != '411'
									AND A.t_tglsetor >= '".$awaltahun."' 
									AND A.t_tglsetor < '".$tglreal1."' 
									AND EXTRACT ( YEAR FROM A.t_tglsetor ) = '" . $tahun . "'
                                                                            AND A.t_issetorandeleted = 0
								) 
							END AS jml 
						) AS setorlain_sebelum,
						(
						SELECT
						CASE
								
							WHEN C
								.s_objekkorek = '00' THEN
									(
									SELECT SUM
										( t_jumlahsetor ) 
									FROM
										t_setoranlain
										A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idrekening 
									WHERE
										f.s_tipekorek = C.s_tipekorek 
										AND C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek != '411'
										AND f.s_kelompokkorek = C.s_kelompokkorek 
										AND f.s_jeniskorek = C.s_jeniskorek 
										AND A.t_tglsetor >= '".$tglreal1."' 
										AND A.t_tglsetor <= '".$tglreal2."' 
										AND EXTRACT ( YEAR FROM A.t_tglsetor ) = '" . $tahun . "' 
                                                                                    AND A.t_issetorandeleted = 0
									) 
									WHEN C.s_rinciankorek = '00' THEN
									(
									SELECT SUM
										( t_jumlahsetor ) 
									FROM
										t_setoranlain
										A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idrekening 
									WHERE
										f.s_tipekorek = C.s_tipekorek 
										AND C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek != '411'
										AND f.s_kelompokkorek = C.s_kelompokkorek 
										AND f.s_jeniskorek = C.s_jeniskorek 
										AND f.s_objekkorek = C.s_objekkorek 
										AND A.t_tglsetor >= '".$tglreal1."' 
										AND A.t_tglsetor <= '".$tglreal2."' 
										AND EXTRACT ( YEAR FROM A.t_tglsetor ) = '" . $tahun . "' 
                                                                                    AND A.t_issetorandeleted = 0
										) ELSE (
									SELECT SUM
										( t_jumlahsetor ) 
									FROM
										t_setoranlain A 
									WHERE
										C.s_idkorek = A.t_idrekening 
										AND C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek != '411'
										AND A.t_tglsetor >= '".$tglreal1."' 
										AND A.t_tglsetor <= '".$tglreal2."' 
										AND EXTRACT ( YEAR FROM A.t_tglsetor ) = '" . $tahun . "' 
                                                                                    AND A.t_issetorandeleted = 0
									) 
								END AS jml 
							) AS setorlain_sekarang,
						(
						SELECT
						CASE
								
							WHEN C
								.s_objekkorek = '00' THEN
									(
									SELECT SUM
										( t_jumlahsetor ) 
									FROM
										t_setoranlain
										A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idrekening 
									WHERE
										f.s_tipekorek = C.s_tipekorek 
										AND C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek != '411'
										AND f.s_kelompokkorek = C.s_kelompokkorek 
										AND f.s_jeniskorek = C.s_jeniskorek 
										AND A.t_tglsetor >= '".$awaltahun."' 
										AND A.t_tglsetor <= '".$akhirtahun."' 
										AND EXTRACT ( YEAR FROM A.t_tglsetor ) = '" . $tahun . "'
                                                                                    AND A.t_issetorandeleted = 0
									) 
									WHEN C.s_rinciankorek = '00' THEN
									(
									SELECT SUM
										( t_jumlahsetor ) 
									FROM
										t_setoranlain
										A LEFT JOIN view_rekening f ON f.s_idkorek = A.t_idrekening 
									WHERE
										f.s_tipekorek = C.s_tipekorek 
										AND C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek != '411'
										AND f.s_kelompokkorek = C.s_kelompokkorek 
										AND f.s_jeniskorek = C.s_jeniskorek 
										AND f.s_objekkorek = C.s_objekkorek 
										AND A.t_tglsetor >= '".$awaltahun."' 
										AND A.t_tglsetor <= '".$akhirtahun."' 
										AND EXTRACT ( YEAR FROM A.t_tglsetor ) = '" . $tahun . "' 
                                                                                AND A.t_issetorandeleted = 0
										) ELSE (
									SELECT SUM
										( t_jumlahsetor ) 
									FROM
										t_setoranlain A 
									WHERE
										C.s_idkorek = A.t_idrekening 
										AND C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek != '411'
										AND A.t_tglsetor >= '".$awaltahun."' 
										AND A.t_tglsetor <= '".$akhirtahun."' 
										AND EXTRACT ( YEAR FROM A.t_tglsetor ) = '" . $tahun . "' 
                                                                                AND A.t_issetorandeleted = 0
									) 
								END AS jml 
							) AS setorlain_sesudah 
							FROM
								view_rekening C 
							WHERE
								C.s_tipekorek = '4' $where
						ORDER BY
	C.korek ASC";
//        (C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek = '411' 
//	OR C.s_tipekorek || C.s_kelompokkorek || C.s_jeniskorek || C.s_objekkorek = '41407') 
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

}
