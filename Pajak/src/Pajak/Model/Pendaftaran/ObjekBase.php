<?php

namespace Pajak\Model\Pendaftaran;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ObjekBase implements InputFilterAwareInterface {

    public $t_idobjek, $t_idwp, $t_noobjek, $t_korekobjek, $t_tgldaftarobjek, $t_namaobjek, $t_alamatobjek, $t_rtobjek, $t_rwobjek, $t_kelurahanobjek, $t_kecamatanobjek, $t_kabupatenobjek;
    public $t_notelpobjek, $t_jenisobjek, $t_kodeposobjek, $t_latitudeobjek, $t_longitudeobjek, $t_namaobjekpj, $t_alamatobjekpj, $t_gambarobjek, $t_tipeusaha;
    public $t_kelashotel, $t_jumlahkamar, $t_ratapengunjunghotel, $t_jumlahkursiresto, $t_jumlahmejaresto, $t_ratapengunjungresto, $t_jumlahkaryawanresto, $t_jenishiburan, $t_jumlahalathiburan;
    public $t_ratapengunjunghiburan, $t_jenisreklame, $t_titiklokasipemasanganreklame, $t_ratajumlahpasangreklame, $t_dayagenset, $t_jumlahgenset, $t_ratapemakaianppj, $t_jenispemakaianppj, $t_luaslahan;
    public $t_jenisminerba, $t_ratapengambilan, $t_sistemparkir, $t_luasparkirmotor, $t_luasparkirmobil, $t_ratakunjunganmotor, $t_ratakunjunganmobil, $t_volumeair, $t_watermeter, $t_jenispenggunaanair, $t_ratapemakaianair, $t_jumlahkaryawanhotel;

    public $page, $direction, $combocari, $kolomcari, $combosorting, $sortasc, $sortdesc, $combooperator;
    public $rows;
    public $sidx;
    public $sord;
    protected $inputFilter;

    public function exchangeArray($data) {
        $this->t_idobjek = (isset($data['t_idobjek'])) ? $data['t_idobjek'] : null;
        $this->t_idwp = (isset($data['t_idwp'])) ? $data['t_idwp'] : null;
        $this->t_noobjek = (isset($data['t_noobjek'])) ? $data['t_noobjek'] : null;
        $this->t_korekobjek = (isset($data['t_korekobjek'])) ? $data['t_korekobjek'] : null;
        $this->t_tgldaftarobjek = (isset($data['t_tgldaftarobjek'])) ? $data['t_tgldaftarobjek'] : null;
        $this->t_namaobjek = (isset($data['t_namaobjek'])) ? $data['t_namaobjek'] : null;
        $this->t_alamatobjek = (isset($data['t_alamatobjek'])) ? $data['t_alamatobjek'] : null;
        $this->t_rtobjek = (isset($data['t_rtobjek'])) ? $data['t_rtobjek'] : null;
        $this->t_rwobjek = (isset($data['t_rwobjek'])) ? $data['t_rwobjek'] : null;
        $this->t_kelurahanobjek = (isset($data['t_kelurahanobjek'])) ? $data['t_kelurahanobjek'] : null;
        $this->t_kecamatanobjek = (isset($data['t_kecamatanobjek'])) ? $data['t_kecamatanobjek'] : null;
        $this->t_kabupatenobjek = (isset($data['t_kabupatenobjek'])) ? $data['t_kabupatenobjek'] : null;
        $this->t_notelpobjek = (isset($data['t_notelpobjek'])) ? $data['t_notelpobjek'] : null;
        $this->t_jenisobjek = (isset($data['t_jenisobjek'])) ? $data['t_jenisobjek'] : null;
        $this->t_kodeposobjek = (isset($data['t_kodeposobjek'])) ? $data['t_kodeposobjek'] : null;
        $this->t_latitudeobjek = (isset($data['t_latitudeobjek'])) ? $data['t_latitudeobjek'] : null;
        $this->t_longitudeobjek = (isset($data['t_longitudeobjek'])) ? $data['t_longitudeobjek'] : null;
        $this->t_gambarobjek = (isset($data['t_gambarobjek'])) ? $data['t_gambarobjek'] : null;
        $this->t_namaobjekpj = (isset($data['t_namaobjekpj'])) ? $data['t_namaobjekpj'] : null;
        $this->t_alamatobjekpj = (isset($data['t_alamatobjekpj'])) ? $data['t_alamatobjekpj'] : null;
        $this->t_tipeusaha = (isset($data['t_tipeusaha'])) ? $data['t_tipeusaha'] : null;
        
        $this->t_kelashotel = (isset($data['t_kelashotel'])) ? $data['t_kelashotel'] : null;
        $this->t_jumlahkamar = (isset($data['t_jumlahkamar'])) ? $data['t_jumlahkamar'] : null;
        $this->t_ratapengunjunghotel = (isset($data['t_ratapengunjunghotel'])) ? $data['t_ratapengunjunghotel'] : null;
        $this->t_jumlahkursiresto = (isset($data['t_jumlahkursiresto'])) ? $data['t_jumlahkursiresto'] : null;
        $this->t_jumlahmejaresto = (isset($data['t_jumlahmejaresto'])) ? $data['t_jumlahmejaresto'] : null;
        $this->t_ratapengunjungresto = (isset($data['t_ratapengunjungresto'])) ? $data['t_ratapengunjungresto'] : null;
        $this->t_jumlahkaryawanresto = (isset($data['t_jumlahkaryawanresto'])) ? $data['t_jumlahkaryawanresto'] : null;
        $this->t_jenishiburan = (isset($data['t_jenishiburan'])) ? $data['t_jenishiburan'] : null;
        $this->t_jumlahalathiburan = (isset($data['t_jumlahalathiburan'])) ? $data['t_jumlahalathiburan'] : null;
        $this->t_ratapengunjunghiburan = (isset($data['t_ratapengunjunghiburan'])) ? $data['t_ratapengunjunghiburan'] : null;
        $this->t_jenisreklame = (isset($data['t_jenisreklame'])) ? $data['t_jenisreklame'] : null;
        $this->t_titiklokasipemasanganreklame = (isset($data['t_titiklokasipemasanganreklame'])) ? $data['t_titiklokasipemasanganreklame'] : null;
        $this->t_ratajumlahpasangreklame = (isset($data['t_ratajumlahpasangreklame'])) ? $data['t_ratajumlahpasangreklame'] : null;
        $this->t_dayagenset = (isset($data['t_dayagenset'])) ? $data['t_dayagenset'] : null;
        $this->t_jumlahgenset = (isset($data['t_jumlahgenset'])) ? $data['t_jumlahgenset'] : null;
        $this->t_ratapemakaianppj = (isset($data['t_ratapemakaianppj'])) ? $data['t_ratapemakaianppj'] : null;
        $this->t_jenispemakaianppj = (isset($data['t_jenispemakaianppj'])) ? $data['t_jenispemakaianppj'] : null;
        $this->t_luaslahan = (isset($data['t_luaslahan'])) ? $data['t_luaslahan'] : null;
        $this->t_jenisminerba = (isset($data['t_jenisminerba'])) ? $data['t_jenisminerba'] : null;
        $this->t_ratapengambilan = (isset($data['t_ratapengambilan'])) ? $data['t_ratapengambilan'] : null;
        $this->t_sistemparkir = (isset($data['t_sistemparkir'])) ? $data['t_sistemparkir'] : null;
        $this->t_luasparkirmotor = (isset($data['t_luasparkirmotor'])) ? $data['t_luasparkirmotor'] : null;
        $this->t_luasparkirmobil = (isset($data['t_luasparkirmobil'])) ? $data['t_luasparkirmobil'] : null;
        $this->t_ratakunjunganmotor = (isset($data['t_ratakunjunganmotor'])) ? $data['t_ratakunjunganmotor'] : null;
        $this->t_ratakunjunganmobil = (isset($data['t_ratakunjunganmobil'])) ? $data['t_ratakunjunganmobil'] : null;
        $this->t_volumeair = (isset($data['t_volumeair'])) ? $data['t_volumeair'] : null;
        $this->t_watermeter = (isset($data['t_watermeter'])) ? $data['t_watermeter'] : null;
        $this->t_jenispenggunaanair = (isset($data['t_jenispenggunaanair'])) ? $data['t_jenispenggunaanair'] : null;
        $this->t_ratapemakaianair = (isset($data['t_ratapemakaianair'])) ? $data['t_ratapemakaianair'] : null;
        $this->t_jumlahkaryawanhotel = (isset($data['t_jumlahkaryawanhotel'])) ? $data['t_jumlahkaryawanhotel'] : null;

        $this->page = (isset($data['page'])) ? $data['page'] : null;
        $this->direction = (isset($data['direction'])) ? $data['direction'] : null;
        $this->combocari = (isset($data['combocari'])) ? $data['combocari'] : null;
        $this->kolomcari = (isset($data['kolomcari'])) ? $data['kolomcari'] : null;
        $this->combosorting = (isset($data['combosorting'])) ? $data['combosorting'] : null;
        $this->sortasc = (isset($data['sortasc'])) ? $data['sortasc'] : null;
        $this->sortdesc = (isset($data['sortdesc'])) ? $data['sortdesc'] : null;
        $this->combooperator = (isset($data['combooperator'])) ? $data['combooperator'] : null;

        $this->rows = (isset($data['rows'])) ? $data['rows'] : null;
        $this->sidx = (isset($data['sidx'])) ? $data['sidx'] : null;
        $this->sord = (isset($data['sord'])) ? $data['sord'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 't_namaobjek',
                        'required' => true
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
