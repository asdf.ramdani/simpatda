<?php

namespace Pajak\Model\Pendaftaran;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class ObjekTable extends AbstractTableGateway {

    protected $table = 't_wpobjek';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new ObjekBase());
        $this->initialize();
    }

    public function getDataObjekById($t_idobjek) {
        $rowset = $this->select(array('t_idobjek' => $t_idobjek));
        $row = $rowset->current();
        return $row;
    }

    public function simpanobjek(ObjekBase $base, $session, $fileUpload) {
        $no = $this->noobjek($base->t_jenisobjek);
        $t_noobjek = (int) $no['t_noobjek'] + 1;
        $data = array(
            't_idwp' => $base->t_idwp,
            't_noobjek' => $t_noobjek,
            't_tgldaftarobjek' => date('Y-m-d', strtotime($base->t_tgldaftarobjek)),
            't_namaobjek' => $base->t_namaobjek,
            't_alamatobjek' => $base->t_alamatobjek,
            't_rtobjek' => $base->t_rtobjek,
            't_rwobjek' => $base->t_rwobjek,
            't_kelurahanobjek' => $base->t_kelurahanobjek,
            't_kecamatanobjek' => $base->t_kecamatanobjek,
            't_kabupatenobjek' => $base->t_kabupatenobjek,
            't_notelpobjek' => $base->t_notelpobjek,
            't_jenisobjek' => $base->t_jenisobjek,
            't_kodeposobjek' => $base->t_kodeposobjek,
            't_latitudeobjek' => $base->t_latitudeobjek,
            't_longitudeobjek' => $base->t_longitudeobjek,
            't_namaobjekpj' => $base->t_namaobjekpj,
            't_alamatobjekpj' => $base->t_alamatobjekpj,
            't_gambarobjek' => $fileUpload,
            't_operatorid' => $session['s_iduser'],
            't_tipeusaha' => $base->t_tipeusaha,
            't_statusobjek' => true,
            't_kelashotel' => $base->t_kelashotel,
            't_jumlahkamar' => (!empty($base->t_jumlahkamar)) ? $base->t_jumlahkamar : null,
            't_ratapengunjunghotel' => (!empty($base->t_ratapengunjunghotel)) ? $base->t_ratapengunjunghotel : null,
            't_jumlahkursiresto' => (!empty($base->t_jumlahkursiresto)) ? $base->t_jumlahkursiresto : null,
            't_jumlahmejaresto' => (!empty($base->t_jumlahmejaresto)) ? $base->t_jumlahmejaresto : null,
            't_ratapengunjungresto' => (!empty($base->t_ratapengunjungresto)) ? $base->t_ratapengunjungresto : null,
            't_jumlahkaryawanresto' => (!empty($base->t_jumlahkaryawanresto)) ? $base->t_jumlahkaryawanresto : null,
            't_jenishiburan' => $base->t_jenishiburan,
            't_jumlahalathiburan' => (!empty($base->t_jumlahalathiburan)) ? $base->t_jumlahalathiburan : null,
            't_ratapengunjunghiburan' => (!empty($base->t_ratapengunjunghiburan)) ? $base->t_ratapengunjunghiburan : null,
            't_jenisreklame' => $base->t_jenisreklame,
            't_titiklokasipemasanganreklame' => $base->t_titiklokasipemasanganreklame,
            't_ratajumlahpasangreklame' => (!empty($base->t_ratajumlahpasangreklame)) ? $base->t_ratajumlahpasangreklame : null,
            't_dayagenset' => (!empty($base->t_dayagenset)) ? $base->t_dayagenset : null,
            't_jumlahgenset' => (!empty($base->t_jumlahgenset)) ? $base->t_jumlahgenset : null,
            't_ratapemakaianppj' => (!empty($base->t_ratapemakaianppj)) ? $base->t_ratapemakaianppj : null,
            't_jenispemakaianppj' => $base->t_jenispemakaianppj,
            't_luaslahan' => (!empty($base->t_luaslahan)) ? $base->t_luaslahan : null,
            't_jenisminerba' => $base->t_jenisminerba,
            't_ratapengambilan' => (!empty($base->t_ratapengambilan)) ? $base->t_ratapengambilan : null,
            't_sistemparkir' => $base->t_sistemparkir,
            't_luasparkirmotor' => (!empty($base->t_luasparkirmotor)) ? $base->t_luasparkirmotor : null,
            't_luasparkirmobil' => (!empty($base->t_luasparkirmobil)) ? $base->t_luasparkirmobil : null,
            't_ratakunjunganmotor' => (!empty($base->t_ratakunjunganmotor)) ? $base->t_ratakunjunganmotor : null,
            't_ratakunjunganmobil' => (!empty($base->t_ratakunjunganmobil)) ? $base->t_ratakunjunganmobil : null,
            't_volumeair' => (!empty($base->t_volumeair)) ? $base->t_volumeair : null,
            't_watermeter' => $base->t_watermeter,
            't_jenispenggunaanair' => $base->t_jenispenggunaanair,
            't_ratapemakaianair' => (!empty($base->t_ratapemakaianair)) ? $base->t_ratapemakaianair : Null,
            't_jumlahkaryawanhotel' => (!empty($base->t_jumlahkaryawanhotel)) ? $base->t_jumlahkaryawanhotel : null
        );
        if (!empty($base->t_korekobjek)) {
            $data['t_korekobjek'] = $base->t_korekobjek;
        } else {
            if ($base->t_jenisobjek == 4) {
                $data['t_korekobjek'] = 42;
            } elseif ($base->t_jenisobjek == 6) {
                $data['t_korekobjek'] = 58;
            } else {
                $data['t_korekobjek'] = '';
            }
        }
        if (!empty($base->t_idobjek)) {
            $data['t_idobjek'] = $base->t_idobjek;
            $data['t_noobjek'] = $base->t_noobjek;
            $result = $this->update($data, array('t_idobjek' => $base->t_idobjek));
        } else {
            $result = $this->insert($data);
        }
        $returnval = array(
            'result' => $result
        );
        return $returnval;
    }

    public function SaveDataKorek($post) {
        for ($i = 0; $i < count($post->t_idobjek); $i++) {
            $data['t_korekobjek'] = $post->t_idkorek[$i];
            $this->update($data, array('t_idobjek' => $post->t_idobjek[$i]));
        }
    }

    public function getGridCountOp(ObjekBase $base, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_korekobjek = b.s_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $select->columns(array('t_idwp'));
        $where = new Where();
        if ($post->t_tgldaftarobjek != '') {
            $t_tgl = explode(' - ', $post->t_tgldaftarobjek);
            $where->literal("t_tgldaftarobjek between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        if ($post->t_npwpdwp != '') {
            $where->literal("(t_npwpdwp ILIKE '%$post->t_npwpdwp%' or t_namawp ILIKE '%$post->t_npwpdwp%')");
        }
        if ($post->t_namapemilik != '')
            $where->literal("t_namapemilik ILIKE '%$post->t_namapemilik%'");
        if ($post->t_nop != '') {
            $where->literal("(t_nop ILIKE '%$post->t_nop%' or t_namaobjek ILIKE '%$post->t_nop%')");
        }
        if ($post->t_alamatobjek != '')
            $where->literal("t_alamatlengkapobjek ILIKE '%$post->t_alamatobjek%'");
        if ($post->t_kecamatanobjek != '')
            $where->literal("a.s_namakec ILIKE '%$post->t_kecamatanobjek%'");
        if ($post->t_kelurahanobjek != '')
            $where->literal("a.s_namakel ILIKE '%$post->t_kelurahanobjek%'");
        if ($post->t_jenisobjek != '')
            $where->literal("a.s_namajenis ILIKE '%$post->t_jenisobjek%'");
        if ($post->t_rekening != '')
            $where->literal("(korek ILIKE '%$post->t_rekening%' or s_namakorek ILIKE '%$post->t_rekening%')");
        if ($post->t_tipeusaha != '')
            $where->literal("(s_kodeusaha ILIKE '%$post->t_tipeusaha%' or s_namausaha ILIKE '%$post->t_tipeusaha%')");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataOp(ObjekBase $base, $offset, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_korekobjek = b.s_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        if ($post->t_tgldaftarobjek != '') {
            $t_tgl = explode(' - ', $post->t_tgldaftarobjek);
            $where->literal("t_tgldaftarobjek between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        if ($post->t_npwpdwp != '') {
            $where->literal("(t_npwpdwp ILIKE '%$post->t_npwpdwp%' or t_namawp ILIKE '%$post->t_npwpdwp%')");
        }
        if ($post->t_namapemilik != '')
            $where->literal("t_namapemilik ILIKE '%$post->t_namapemilik%'");
        if ($post->t_nop != '') {
            $where->literal("(t_nop ILIKE '%$post->t_nop%' or t_namaobjek ILIKE '%$post->t_nop%')");
        }
        if ($post->t_alamatobjek != '')
            $where->literal("t_alamatlengkapobjek ILIKE '%$post->t_alamatobjek%'");
        if ($post->t_kecamatanobjek != '')
            $where->literal("a.s_namakec ILIKE '%$post->t_kecamatanobjek%'");
        if ($post->t_kelurahanobjek != '')
            $where->literal("a.s_namakel ILIKE '%$post->t_kelurahanobjek%'");
        if ($post->t_jenisobjek != '')
            $where->literal("a.s_namajenis ILIKE '%$post->t_jenisobjek%'");
        if ($post->t_rekening != '')
            $where->literal("(korek ILIKE '%$post->t_rekening%' or s_namakorek ILIKE '%$post->t_rekening%')");
        if ($post->t_tipeusaha != '')
            $where->literal("(s_kodeusaha ILIKE '%$post->t_tipeusaha%' or s_namausaha ILIKE '%$post->t_tipeusaha%')");
        $select->where($where);
        $select->order("t_tgldaftarobjek desc");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getcomboIdJenis($t_jenispendaftaran) {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_jenisobjek');
        $where = new Where();
        // if ($t_jenispendaftaran == 'P') {
        //     $where->equalTo('s_jenispungutan', 'Pajak');
        // } elseif ($t_jenispendaftaran == 'R') {
        //     $where->equalTo('s_jenispungutan', 'Retribusi');
        // }
        $select->where($where);
        $select->order('s_idjenis asc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $selectData = array();
        foreach ($res as $row) {
            $selectData[$row['s_idjenis']] = str_pad($row['s_idjenis'], 2, "0", STR_PAD_LEFT) . " || " . $row['s_namajenis'];
        }
        return $selectData;
    }

    public function getcomboIdJenisRek() {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_jenisobjek');
        $select->order('s_idjenis asc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $selectData = array();
        foreach ($res as $row) {
            $selectData[$row['s_idjenis']] = str_pad($row['s_idjenis'], 2, "0", STR_PAD_LEFT) . " || " . $row['s_namajenis'];
        }
        return $selectData;
    }

    public function noobjek($t_jenisobjek) {
        $sql = "select max(t_noobjek) as t_noobjek from t_wpobjek where t_jenisobjek=" . $t_jenisobjek;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getDataObjek($t_idwp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_wpobjek');
        $where = new Where();
        $where->equalTo('t_idwp', (int) $t_idwp);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataObjekKorek($t_idwp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            'a' => 'view_wpobjek'
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "b.s_idkorek = a.t_korekobjek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('t_idwp', (int) $t_idwp);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataObjekId($t_idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wpobjek'
        ));
        $where = new Where();
//        $where->isNull('a.is_deluser');
        $where->equalTo('a.t_idobjek', (int) $t_idobjek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getJmlOP($t_idwp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('t_wpobjek');
        $where = new Where();
//        $where->isNull('a.is_deluser');
        $where->equalTo('t_idwp', (int) $t_idwp);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->count();
        return $res;
    }

    public function getAllDataObjek() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_jenisobjek');
        $where = new Where();
        $select->where($where);
        $select->order("s_idjenis asc");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getObjekPajakbyNPWPD($npwpd) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_npwpd', $npwpd);
        $select->where($where);
        $select->order("b.t_idobjek asc");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getObjekPajakbyIdObjek($idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "t_jenisobjek", "t_alamatlengkapobjek", "s_namajenis"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('b.t_idobjek', $idobjek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getTransaksibyIdObjek($idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_tglpendataan", "t_jmlhpajak", "t_tglpenetapan", "t_tglpembayaran", "t_jmlhpembayaran", "t_masaawal", "t_masaakhir"
            , "t_jmlhdendapembayaran", "t_tgldendapembayaran", "t_jmlhbayardenda", "t_tglbayardenda"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "d.s_idkorek = c.t_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idwpobjek', $idobjek);
        $select->where($where);
        $select->order("c.t_masaawal asc");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getPendataanAwalbyIdObjek($idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_masaawal", "t_idkorek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "d.s_idkorek = c.t_idkorek", array(
            "s_jenisobjek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idwpobjek', $idobjek);
        $select->where($where);
        $select->order("c.t_masaawal asc");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPendataanbyMasa($month, $periodepajak, $idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_tglpenetapan", "t_tglpembayaran", "t_jmlhpembayaran"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "d.s_idkorek = c.t_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idwpobjek', $idobjek);
        $where->literal("extract(month from c.t_masaawal) ='" . str_pad($month, 2, '0', STR_PAD_LEFT) . "' and extract(year from c.t_masaawal) ='" . $periodepajak . "'");
        $select->where($where);
        $select->order("c.t_masaawal asc");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPendataanbyMasaHistori($month, $periodepajak, $idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_tglpenetapan", "t_tglpembayaran", "t_jmlhpembayaran"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "d.s_idkorek = c.t_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idwpobjek', $idobjek);
        $where->literal("extract(month from c.t_masaawal) ='" . str_pad($month, 2, '0', STR_PAD_LEFT) . "' and extract(year from c.t_masaawal) ='" . $periodepajak . "'");
        $select->where($where);
        $select->order("c.t_masaawal asc");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getPendataanKatering($periodepajak, $idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_tglpenetapan", "t_tglpembayaran", "t_jmlhpembayaran"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "d.s_idkorek = c.t_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idwpobjek', $idobjek);
        $where->literal("extract(year from c.t_masaawal) ='" . $periodepajak . "'");
        $select->where($where);
        $select->order("c.t_tglpendataan asc");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getviewobjek($tgldaftar0, $tgldaftar1, $t_kecamatan, $t_kelurahan, $t_jenispajakop, $idkorek, $statusobjek, $tipeusaha) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wpobjek'
        ));
        $where = new Where();
        $where->between("a.t_tgldaftarobjek", date('Y-m-d', strtotime($tgldaftar0)), date('Y-m-d', strtotime($tgldaftar1)));
        if (!empty($t_jenispajakop)) {
            $where->equalTo("a.t_jenisobjek", $t_jenispajakop);
        }
        if (!empty($idkorek)) {
            $where->equalTo("a.t_korekobjek", $idkorek);
        }
        if (!empty($t_kecamatan)) {
            $where->equalTo("a.t_kecamatanobjek", $t_kecamatan);
        }
        if (!empty($t_kelurahan)) {
            $where->equalTo("a.t_kelurahanobjek", $t_kelurahan);
        }
        if (!empty($statusobjek)) {
            if ($statusobjek == 'true') {
                $where->literal("a.t_statusobjek = true ");
            } else {
                $where->literal("a.t_statusobjek = false ");
            }
        }
        if (!empty($tipeusaha)) {
            $where->literal("a.t_tipeusaha = '" . $tipeusaha . "' ");
        }
        $select->where($where);
        $select->order(["a.t_tgldaftarobjek asc", "a.t_idwp asc"]);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getHistoryReklame($idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_tglpenetapan", "t_tglpembayaran", "t_jmlhpembayaran"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "d.s_idkorek = c.t_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idwpobjek', $idobjek);
        $select->where($where);
        $select->order("c.t_masaawal desc");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataObjekLeftMenu($t_idwp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_wpobjek');
        $select->columns(array(
            "t_jenisobjek", "t_namaobjek", "t_idobjek"
        ));
        $where = new Where();
        $where->equalTo('t_idwp', (int) $t_idwp);
        $where->literal('t_jenisobjek not in(4,8)');
        $select->where($where);
        $select->order('t_jenisobjek ASC');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function hapusPendaftaranObjek($id) {
        $this->delete(array('t_idobjek' => $id));
    }

    public function getJenisObjek($jenisobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_jenisobjek');
        $where = new Where();
        if (!empty($jenisobjek)) {
            $where->equalTo('s_idjenis', $jenisobjek);
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getObjekPajakbyIdTeguranArray($array_idteguran) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_teguranlaporpajak"
        ));
        $select->columns(array(
            "t_noteguran", "t_tglteguran", "t_bulanpajak", "t_tahunpajak", "t_idteguran"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "b.t_idobjek = a.t_idobjekteguran", array(
            't_npwpdwp', 't_namawp', "t_idobjek", "t_nop", "t_namaobjek", "t_alamatlengkapobjek", "t_alamat_lengkapwp", "s_namajenis", "t_jenisobjek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('a.t_idteguran in (' . $array_idteguran . ')');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

}
