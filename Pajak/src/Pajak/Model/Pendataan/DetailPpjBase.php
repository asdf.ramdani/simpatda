<?php

namespace Pajak\Model\Pendataan;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DetailPpjBase implements InputFilterAwareInterface {

    public $t_iddetailppj, $t_idtransaksi;
    public $t_tarifdasar, $t_idkorek, $t_jmlhkwh, $t_jmlhpajak;
    protected $inputFilter;

    public function exchangeArray($data) {
        $this->t_iddetailppj = (isset($data['t_iddetailppj'])) ? $data['t_iddetailppj'] : null;
        $this->t_idtransaksi = (isset($data['t_idtransaksi'])) ? $data['t_idtransaksi'] : null;
        $this->t_idkorek = (isset($data['t_idkorek'])) ? $data['t_idkorek'] : null;
        $this->t_tarifdasar = (isset($data['t_tarifdasar'])) ? $data['t_tarifdasar'] : null;
        $this->t_jmlhkwh = (isset($data['t_jmlhkwh'])) ? $data['t_jmlhkwh'] : null;
        $this->t_jmlhpajak = (isset($data['t_jmlhpajak'])) ? $data['t_jmlhpajak'] : null;
        
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
