<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class DetailPpjTable extends AbstractTableGateway {

    protected $table = 't_detailppj';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new DetailPpjBase());
        $this->initialize();
    }

    public function simpanpendataanppj($post, $dataparent) {
        $data = array(
            't_idkorek'             => $post['t_idkorek'],
            't_golongantarif'       => ((!empty($post['t_golongantarif'])) ? $post['t_golongantarif'] : 0),
            't_jmlhkwh'             => ((!empty($post['t_jmlhkwh'])) ? str_ireplace('.', '', $post['t_jmlhkwh']) : 0),
            't_nilailistrik_rt'     => ((!empty($post['t_nilailistrik_rt'])) ? str_ireplace('.', '', $post['t_nilailistrik_rt']) : 0),
            't_tarif_rt'            => ((!empty($post['t_nilailistrik_rt'])) ? $post['t_tarif_rt'] : 0),
            't_pajakterhutang_rt'   => ((!empty($post['t_pajakterhutang_rt'])) ? str_ireplace('.', '', $post['t_pajakterhutang_rt']) : 0),
            't_nilailistrik_nonrt'  => ((!empty($post['t_nilailistrik_nonrt'])) ? str_ireplace('.', '', $post['t_nilailistrik_nonrt']) : 0),
            't_tarif_nonrt'         => ((!empty($post['t_nilailistrik_nonrt'])) ? $post['t_tarif_nonrt'] : 0),
            't_pajakterhutang_nonrt'    => ((!empty($post['t_pajakterhutang_nonrt'])) ? str_ireplace('.', '', $post['t_pajakterhutang_nonrt']) : 0),
            't_nilailistrik_industri'   => ((!empty($post['t_nilailistrik_industri'])) ? str_ireplace('.', '', $post['t_nilailistrik_industri']) : 0),
            't_tarif_industri'          => ((!empty($post['t_nilailistrik_industri'])) ? $post['t_tarif_industri'] : 0),
            't_pajakterhutang_industri' => ((!empty($post['t_pajakterhutang_industri'])) ? str_ireplace('.', '', $post['t_pajakterhutang_industri']) : 0),
        );
        $t_iddetailppj = $post['t_iddetailppj'];
        if (empty($t_iddetailppj)) {
            $data['t_idtransaksi'] = $dataparent['t_idtransaksi'];
            $this->insert($data);
        } else {
            $this->update($data, array('t_iddetailppj' => $t_iddetailppj));
        }
//        return $data;
    }

    public function getDetailByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getPendataanByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_idkorek = b.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $select->order(['s_tipekorek' => 'asc', 's_kelompokkorek' => 'asc', 's_jeniskorek' => 'asc', 's_objekkorek' => 'asc', 's_rinciankorek' => 'asc', 's_sub1korek' => 'asc']);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }
    
    public function getDataTarifListrik() {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_tarif_listrik");
        $select->order('s_idtarif');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $selectData = array();
        foreach ($res as $row) {
            $selectData[$row['s_idtarif']] = $row['s_keterangan']." ";
        }
        return $selectData;
    }
    
    public function getDataTarifListrikById($id) {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_tarif_listrik");
        $where = new Where();
        $where->equalTo('s_idtarif', (int) $id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }

}
