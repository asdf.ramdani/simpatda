<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class DetailkekayaanTable extends AbstractTableGateway {

    protected $table = 't_detailkekayaan';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->initialize();
    }

    public function simpandetailkekayaan($post, $dataparent) {
        $data = array(
            't_idtransaksi' => $dataparent['t_idtransaksi'],
            't_idklasifikasi' => $post['t_klasifikasi'],
            't_luastanah' => $post['t_luastanah'],
            't_luasbangunan' => $post['t_luasbangunan'],
            't_nilailuastanah' => str_ireplace(".", "", $post['t_nilailuastanah']),
            't_nilailuasbangunan' => str_ireplace(".", "", $post['t_nilailuasbangunan']),
            't_jmlhbln' => $post['t_jmlhbln']
            // 't_potongan' => str_ireplace(".", "", $post['t_potongan'])
        );
        $t_idkekayaan = $post['t_idkekayaan'];
        if (empty($t_idkekayaan)) {
            $this->insert($data);
        } else {
            $this->update($data, array('t_idkekayaan' => $t_idkekayaan));
        }
        return $data;
    }

    public function getPendataanKekayaanByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi"
        ));
        $select->join(array(
            "b" => "t_detailkekayaan"
                ), "a.t_idtransaksi = b.t_idtransaksi", 
                array("*"), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_rekening"
                ), "a.t_idkorek = c.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('b.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

}
