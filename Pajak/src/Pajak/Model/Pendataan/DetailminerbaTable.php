<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class DetailminerbaTable extends AbstractTableGateway {

    protected $table = 't_detailminerba';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->initialize();
    }

    public function simpanpendataanminerba($datapost, $dataparent) {
//        $this->delete(array('t_idtransaksi' => $dataparent['t_idtransaksi']));
//        for ($i = 0; $i < count($datapost['t_idkorek']); $i++) {
//            if (!empty($datapost['t_idkorek'][$i])) {
                $data = array(
                    't_idkorek' => $datapost['t_idkorek'],
                    't_volume' => str_ireplace(",", ".", $datapost['t_volume']),
                    't_hargapasaran' => str_ireplace(".", "", $datapost['t_hargapasaran']),
                    't_jumlah' => str_ireplace(".", "", $datapost['t_dasarpengenaan']),
                    't_tarifpersen' => str_ireplace(".", "", $datapost['t_tarifpajak']),
                    't_pajak' => str_ireplace(".", "", $datapost['t_jmlhpajak']),
                );
                
                if(empty($datapost['t_idminerba'])){
                    $data['t_idtransaksi'] = $dataparent['t_idtransaksi'];
                    $this->insert($data);
                }else{
                    $this->update($data, array('t_idminerba' => $datapost['t_idminerba']));
                }
//            }
//        }
    }

    public function getPendataanMinerbaByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi"
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_idkorek = b.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDetailMinerbaByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_detailminerba"
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

}
