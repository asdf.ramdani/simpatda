<?php

namespace Pajak\Model\Pendataan;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DetailreklameBase implements InputFilterAwareInterface {

    public $t_iddetailreklame, $t_idtransaksi, $t_jenisreklame,
            $t_naskah, $t_lokasi, $t_panjang, $t_lebar, $t_tinggi, $t_tarifreklame;
    public $t_jumlah, $t_jangkawaktu, $t_tipewaktu,$t_kelasjalan, $t_sudutpandang, $t_nsr, $t_jmlhpajak;
    public $t_jenisselebaran, $t_jeniskendaraan, $t_jenissuara, $t_jenisfilm, $t_jenisperagaan, $t_nsl;
    public $t_pemasangan, $t_konstruksi, $t_bentukreklame, $t_jenislokasi, $t_lebarjalan, $t_ketinggian, $t_hargasatuan;
    
    public $page, $direction, $combocari, $kolomcari, $combosorting, $sortasc, $sortdesc, $combooperator;
    public $rows;
    public $sidx;
    public $sord;
    protected $inputFilter;

    public function exchangeArray($data) {
        $this->t_iddetailreklame = (isset($data['t_iddetailreklame'])) ? $data['t_iddetailreklame'] : null;
        $this->t_idtransaksi = (isset($data['t_idtransaksi'])) ? $data['t_idtransaksi'] : null;
        $this->t_jenisreklame = ($data['t_jenisreklame'] != NULL) ? $data['t_jenisreklame'] : null;
        $this->t_naskah = (isset($data['t_naskah'])) ? $data['t_naskah'] : null;
        $this->t_lokasi = (isset($data['t_lokasi'])) ? $data['t_lokasi'] : null;
        $this->t_panjang = ($data['t_panjang'] != NULL) ? $data['t_panjang'] : null;
        $this->t_lebar = ($data['t_lebar'] != NULL) ? $data['t_lebar'] : null;
        $this->t_tinggi = (isset($data['t_tinggi'])) ? $data['t_tinggi'] : null;
        $this->t_tarifreklame = (isset($data['t_tarifreklame'])) ? $data['t_tarifreklame'] : null;
        $this->t_jumlah = (isset($data['t_jumlah'])) ? $data['t_jumlah'] : null;
        $this->t_jangkawaktu = ($data['t_jangkawaktu'] != NULL) ? $data['t_jangkawaktu'] : null;
        $this->t_tipewaktu = ($data['t_tipewaktu'] != NULL) ? $data['t_tipewaktu'] : null;
        $this->t_sudutpandang = ($data['t_sudutpandang'] != NULL) ? $data['t_sudutpandang'] : null;
        $this->t_kelasjalan = ($data['t_kelasjalan'] != NULL) ? $data['t_kelasjalan'] : null;
        $this->t_jenisselebaran = ($data['t_jenisselebaran'] != NULL) ? $data['t_jenisselebaran'] : NULL;
        $this->t_jeniskendaraan = ($data['t_jeniskendaraan'] != NULL) ? $data['t_jeniskendaraan'] : NULL;
        $this->t_jenissuara = ($data['t_jenissuara'] != NULL) ? $data['t_jenissuara'] : NULL;
        $this->t_jenisfilm = ($data['t_jenisfilm'] != NULL) ? $data['t_jenisfilm'] : NULL;
        $this->t_jenisperagaan = ($data['t_jenisperagaan'] != NULL) ? $data['t_jenisperagaan'] : null;
        $this->t_nsr = ($data['t_nsr'] != NULL) ? $data['t_nsr'] : NULL;
        $this->t_nsl = ($data['t_nsl'] != NULL) ? $data['t_nsl'] : NULL;
        $this->t_jmlhpajak = ($data['t_jmlhpajak'] != NULL) ? $data['t_jmlhpajak'] : null;
        $this->t_pemasangan = ($data['t_pemasangan'] != NULL) ? $data['t_pemasangan'] : null;
        $this->t_konstruksi = ($data['t_konstruksi'] != NULL) ? $data['t_konstruksi'] : null;
        $this->t_bentukreklame = ($data['t_bentukreklame'] != NULL) ? $data['t_bentukreklame'] : null;
        $this->t_jenislokasi = ($data['t_jenislokasi'] != NULL) ? $data['t_jenislokasi'] : null;
        $this->t_lebarjalan = ($data['t_lebarjalan'] != NULL) ? $data['t_lebarjalan'] : null;
        $this->t_ketinggian = ($data['t_ketinggian'] != NULL) ? $data['t_ketinggian'] : null;
        $this->t_hargasatuan = ($data['t_hargasatuan'] != NULL) ? $data['t_hargasatuan'] : null;

        $this->page = (isset($data['page'])) ? $data['page'] : null;
        $this->direction = (isset($data['direction'])) ? $data['direction'] : null;
        $this->combocari = (isset($data['combocari'])) ? $data['combocari'] : null;
        $this->kolomcari = (isset($data['kolomcari'])) ? $data['kolomcari'] : null;
        $this->combosorting = (isset($data['combosorting'])) ? $data['combosorting'] : null;
        $this->sortasc = (isset($data['sortasc'])) ? $data['sortasc'] : null;
        $this->sortdesc = (isset($data['sortdesc'])) ? $data['sortdesc'] : null;
        $this->combooperator = (isset($data['combooperator'])) ? $data['combooperator'] : null;

        $this->rows = (isset($data['rows'])) ? $data['rows'] : null;
        $this->sidx = (isset($data['sidx'])) ? $data['sidx'] : null;
        $this->sord = (isset($data['sord'])) ? $data['sord'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
