<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class DetailreklameTable extends AbstractTableGateway {

    protected $table = 't_detailreklame';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new DetailreklameBase());
        $this->initialize();
    }

    public function simpanpendataanreklame(DetailreklameBase $base, $dataparent) {
        $data = array(
            't_jenisreklame' => $base->t_jenisreklame,
            't_kelasjalan' => $base->t_kelasjalan,
            't_jenisselebaran' => $base->t_jenisselebaran,
            't_jeniskendaraan' => $base->t_jeniskendaraan,
            't_jenissuara' => $base->t_jenissuara,
            't_jenisfilm' => $base->t_jenisfilm,
            't_jenisperagaan' => $base->t_jenisperagaan,
            't_naskah' => $base->t_naskah,
            't_lokasi' => $base->t_lokasi,
            't_tarifreklame' => str_ireplace(".", "", $base->t_tarifreklame),
            't_panjang' => $base->t_panjang,
            't_lebar' => $base->t_lebar,
            't_tinggi' => $base->t_tinggi,
            't_jumlah' => $base->t_jumlah,
            't_jangkawaktu' => $base->t_jangkawaktu,
            't_sudutpandang' => $base->t_sudutpandang,
            't_pemasangan' => $base->t_pemasangan,
            't_konstruksi' => $base->t_konstruksi,
            't_bentukreklame' => $base->t_bentukreklame,
            't_jenislokasi' => $base->t_jenislokasi,
            't_lebarjalan' => $base->t_lebarjalan,
            't_ketinggian' => $base->t_ketinggian,
            't_nsr' => str_ireplace(".", "", $base->t_nsr),
            't_nsl' => $base->t_nsl,
            't_jmlhpajak' => str_ireplace(".", "", $base->t_jmlhpajak),
            't_hargasatuan' => str_ireplace(".", "", $base->t_hargasatuan),
        );
        $t_iddetailreklame = $base->t_iddetailreklame;
//       var_dump($data);
//       die();
        if (empty($t_iddetailreklame)) {
            $data['t_idtransaksi'] = $dataparent['t_idtransaksi'];
            $this->insert($data);
        } else {
            $data['t_idtransaksi'] = $base->t_idtransaksi;
            $this->update($data, array('t_iddetailreklame' => $t_iddetailreklame));
        }
        return $data;
    }

    public function getDetailReklameByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        $select->join(array(
            "b" => "s_reklame"
                ), "a.t_iddetailreklame = b.s_idreklame", array(
            "s_namareklame"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

}
