<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

class PendataanTable extends AbstractTableGateway {

    protected $table = 't_transaksi';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new PendataanBase());
        $this->initialize();
    }

    public function getPendataanId($t_idtransaksi) {
        $rowset = $this->select(array('t_idtransaksi' => $t_idtransaksi));
        $row = $rowset->current();
        return $row;
    }

    // Pendataan SPTPD Self Assesment
    public function simpanpendataanself(PendataanBase $base, $session, $post) {
        // Kode Provinsi dan Kab/Kota
        $kdProvkabkota = '2';//'1602'

        // Jika Inputan Berupa SKPD Jabatan
        if (!empty($post['t_tarifkenaikan'])) {
            $t_tarifkenaikan = $post['t_tarifkenaikan'];
            $t_jmlhkenaikan = str_ireplace(".", "", $post['t_jmlhkenaikan']);
        } else {
            $t_tarifkenaikan = 0;
            $t_jmlhkenaikan = 0;
        }

        // Pajak Minerba = 6
//        if ($post['t_jenisobjekpajak'] == 6) {
//            $base->t_idkorek = 34;
//        }

        $delay = \Zend\Math\Rand::getString(1, '12345', true);
        sleep($delay);

        // Jatuh Tempo Self Assesment
        $t_tgljatuhtempo = $this->geTglJatuhTempo($post['t_jenisobjekpajak']);
        $t_tgljatuhtempofix = date('Y-m-' . $t_tgljatuhtempo['t_akhirbayar'], strtotime("+1 month" . $base->t_masaawal));

        $data = array(
            't_idwpobjek' => $base->t_idobjek,
            't_idkorek' => $base->t_idkorek,
            't_jenispajak' => (float) $base->t_jenispajak,
            't_periodepajak' => $base->t_periodepajak,
            't_tglpendataan' => date('Y-m-d', strtotime($base->t_tglpendataan)).date(' H:i:s'),
            't_masaawal' => date('Y-m-d', strtotime($base->t_masaawal)),
            't_masaakhir' => date('Y-m-d', strtotime($base->t_masaakhir)),
            't_dasarpengenaan' => (float) str_ireplace(".", "", $base->t_dasarpengenaan),
            't_tarifpajak' => (float) $base->t_tarifpajak,
            't_jmlhpajak' => (float) str_ireplace(".", "", $base->t_jmlhpajak),
            't_namakegiatan' => $base->t_namakegiatan,
            't_operatorpendataan' => $session['s_iduser'],
            't_tgljatuhtempo' => $t_tgljatuhtempofix,
            't_tarifkenaikan' => (float) $t_tarifkenaikan,
            't_jmlhkenaikan' => (float) $t_jmlhkenaikan,
            'is_deluserpendataan' => 0,
            't_volume' => (!empty($base->t_volume)) ? (float) str_ireplace(".", "", $base->t_volume) : null,
            't_hargapasaran' => (!empty($base->t_hargapasaran)) ? (float) str_ireplace(".", "", $base->t_hargapasaran) : null,
        );
//        var_dump($data);
//        die();
        $t_idtransaksi = $base->t_idtransaksi;
        if (empty($t_idtransaksi)) {
            if (!empty($post['t_tarifkenaikan'])) {
                // Kode Bayar 4-SKPD JABATAN
                $jenissurat = $this->getjenissurat(4);
                // No. SKPD Jabatan
                $no = $this->getSkpdjabMax();
                $t_noskpdjab = (int) $no['t_noskpdjab'] + 1;
                $data['t_noskpdjab'] = $t_noskpdjab;
            } else {
                // Kode Bayar Self => 1-SPTPD
                $jenissurat = $this->getjenissurat(1);
            }
            $data['t_jenisketetapan'] = $jenissurat['s_idsurat'];
            // No. SPTPD
            $no = $this->getPendataanMax($base->t_periodepajak);
            $t_nourut = (int) $no['t_nourut'] + 1;
            $data['t_nourut'] = $t_nourut;
            $data['t_kodebayar'] = date('Y', strtotime($base->t_tglpendataan)). str_pad($jenissurat['s_idsurat'], 2, "0", STR_PAD_LEFT).str_pad($t_nourut, 5, "0", STR_PAD_LEFT);
            $this->insert($data);
        } else {
            $this->update($data, array('t_idtransaksi' => $t_idtransaksi));
        }
        
        if ($post['t_jenisobjekpajak'] == 5) {
            $sql = new Sql($this->adapter);
            $select = $sql->select();
            $select->from("t_transaksi");
            $select->columns(array(
                "t_idtransaksi"
            ));
            $where = new Where();
            if (!empty($post['t_tarifkenaikan'])) {
                $where->equalTo('t_noskpdjab', $t_noskpdjab);
            } else {
                $where->equalTo('t_nourut', $t_nourut);
            }
            $where->equalTo('t_tglpendataan', date('Y-m-d', strtotime($base->t_tglpendataan)).date(' H:i:s'));
            $where->equalTo('t_masaawal', date('Y-m-d', strtotime($base->t_masaawal)));
            $where->equalTo('t_masaakhir', date('Y-m-d', strtotime($base->t_masaakhir)));
            $select->where($where);
            $state = $sql->prepareStatementForSqlObject($select);
            $res = $state->execute()->current();
            return $res;
        }
    }

    public function simpanpendataanwalet(PendataanBase $base, $session, $post) {
        // Kode Provinsi dan Kab/Kota
        $kdProvkabkota = '2';//'1602'

        // Jika Inputan Berupa SKPD Jabatan
        if (!empty($post['t_tarifkenaikan'])) {
            $t_tarifkenaikan = $post['t_tarifkenaikan'];
            $t_jmlhkenaikan = str_ireplace(".", "", $post['t_jmlhkenaikan']);
        } else {
            $t_tarifkenaikan = 0;
            $t_jmlhkenaikan = 0;
        }

        $delay = \Zend\Math\Rand::getString(1, '12345', true);
        sleep($delay);
        // Jatuh Tempo Self Assesment
        $t_tgljatuhtempo = $this->geTglJatuhTempo($post['t_jenisobjekpajak']);
        $t_tgljatuhtempofix = date('Y-m-' . $t_tgljatuhtempo['t_akhirbayar'], strtotime("+1 month" . $base->t_masaawal));


        $data = array(
            't_idwpobjek' => $base->t_idobjek,
            't_idkorek' => $base->t_idkorek,
            't_jenispajak' => $base->t_jenispajak,
            't_periodepajak' => $base->t_periodepajak,
            't_tglpendataan' => date('Y-m-d', strtotime($base->t_tglpendataan)).date(' H:i:s'),
            't_masaawal' => date('Y-m-d', strtotime($base->t_masaawal)),
            't_masaakhir' => date('Y-m-d', strtotime($base->t_masaakhir)),
            't_tarifdasarkorek' => str_ireplace(".", "", $base->t_tarifdasarkorek),
            't_nilaiperolehan' => $base->t_nilaiperolehan,
            't_dasarpengenaan' => str_ireplace(".", "", $base->t_dasarpengenaan),
            't_tarifpajak' => $base->t_tarifpajak,
            't_jmlhpajak' => str_ireplace(".", "", $base->t_jmlhpajak),
            't_operatorpendataan' => $session['s_iduser'],
            't_tgljatuhtempo' => $t_tgljatuhtempofix,
            't_tarifkenaikan' => $t_tarifkenaikan,
            't_jmlhkenaikan' => $t_jmlhkenaikan,
            'is_deluserpendataan' => 0
        );
        $t_idtransaksi = $base->t_idtransaksi;
        if (empty($t_idtransaksi)) {
            if (!empty($post['t_tarifkenaikan'])) {
                // Kode Bayar 4-SKPD JABATAN
                $jenissurat = $this->getjenissurat(4);
                // No. SKPD Jabatan
                $no = $this->getSkpdjabMax();
                $t_noskpdjab = (int) $no['t_noskpdjab'] + 1;
                $data['t_noskpdjab'] = $t_noskpdjab;
            } else {
                // Kode Bayar Self => 1-SPTPD
                $jenissurat = $this->getjenissurat(1);
            }
            // No. SPTPD
            $no = $this->getPendataanMax($base->t_periodepajak);
            $t_nourut = (int) $no['t_nourut'] + 1;
            $data['t_nourut'] = $t_nourut;
            $data['t_kodebayar'] = date('Y', strtotime($base->t_tglpendataan)). str_pad($jenissurat['s_idsurat'], 2, "0", STR_PAD_LEFT).str_pad($t_nourut, 5, "0", STR_PAD_LEFT);
            $this->insert($data);
        } else {
            $this->update($data, array('t_idtransaksi' => $t_idtransaksi));
        }
        
    }

    // Pendataan SPTPD Official Assesment
    public function simpanpendataanofficial(PendataanBase $base, $session, $post) {
        // Kode Provinsi dan Kab/Kota
        $kdProvkabkota = '2';//'1602'

        if (!empty($post['t_tarifkenaikan'])) {
            $t_tarifkenaikan = $post['t_tarifkenaikan'];
            $t_jmlhkenaikan = str_ireplace(".", "", $post['t_jmlhkenaikan']);
        } else {
            $t_tarifkenaikan = 0;
            $t_jmlhkenaikan = 0;
        }
        $delay = \Zend\Math\Rand::getString(1, '12345', true);
        sleep($delay);

        // Jatuh Tempo Self Assesment
        $t_tgljatuhtempo = $this->geTglJatuhTempoofficial($post['t_jenisobjekpajak']);
        $t_tgljatuhtempofix = date('Y-m-d', strtotime("+" . $t_tgljatuhtempo['t_jmlharitempo'] . " days" . $base->t_tglpenetapan));

        if (!empty($base->t_dasarpengenaan)) {
            $dasar_pengenaan = str_ireplace(".", "", $base->t_dasarpengenaan);
        } else {
            $dasar_pengenaan = 0;
        }

        // Kode Bayar Official => 2-SKPD
        $jenissurat = $this->getjenissurat(2);
        $data = array(
            't_idwpobjek' => $base->t_idobjek,
            't_idkorek' => $base->t_idkorek,
            't_jenispajak' => $base->t_jenispajak,
            't_periodepajak' => $base->t_periodepajak,
            't_tglpendataan' => date('Y-m-d', strtotime($base->t_tglpendataan)).date(' H:i:s'),
            't_masaawal' => date('Y-m-d', strtotime($base->t_masaawal)),
            't_masaakhir' => date('Y-m-d', strtotime($base->t_masaakhir)),
            't_dasarpengenaan' => $dasar_pengenaan, //str_ireplace(".", "", $base->t_dasarpengenaan),
            't_tarifpajak' => $base->t_tarifpajak,
            't_jmlhpajak' => str_ireplace(".", "", $base->t_jmlhpajak),
            't_operatorpendataan' => $session['s_iduser'],
            't_tarifkenaikan' => $t_tarifkenaikan,
            't_jmlhkenaikan' => $t_jmlhkenaikan,
            'is_deluserpendataan' => 0
        );
        $t_idtransaksi = $base->t_idtransaksi;
        if (empty($t_idtransaksi)) {
            if (!empty($post['t_tarifkenaikan'])) {
                // Kode Bayar 4-SKPD JABATAN
                $jenissurat = $this->getjenissurat(4);
                // No. SKPD Jabatan
                $no = $this->getSkpdjabMax();
                $t_noskpdjab = (int) $no['t_noskpdjab'] + 1;
                $data['t_noskpdjab'] = $t_noskpdjab;
            } else {
                // Kode Bayar Official => 1-SPTPD
                $jenissurat = $this->getjenissurat(2);
            }
            // No. SPTPD
            $no = $this->getPendataanMax($base->t_periodepajak);
            $t_nourut = (int) $no['t_nourut'] + 1;
            $data['t_nourut'] = $t_nourut;
            $this->insert($data);
        } else {
            $this->update($data, array('t_idtransaksi' => $t_idtransaksi));
        }
        
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_transaksi");
        $select->columns(array(
            "t_idtransaksi"
        ));
        $where = new Where();
        $where->equalTo('t_nourut', $data['t_nourut']);
        $where->equalTo('t_tglpendataan', date('Y-m-d', strtotime($base->t_tglpendataan)).date(' H:i:s'));
        $where->equalTo('t_masaawal', date('Y-m-d', strtotime($base->t_masaawal)));
        $where->equalTo('t_masaakhir', date('Y-m-d', strtotime($base->t_masaakhir)));
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }


    public function temukanPendataan($t_idwp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_wp"
        ));
        $where = new Where();
        $where->equalTo('a.t_idwp', $t_idwp);
        $select->where($where);
        $select->order("a.t_tgldaftar DESC");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountSudah(PendataanBase $base, $s_idjenis, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak","t_tglpenetapan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "e" => "t_detailreklame"
                ), "e.t_idtransaksi = c.t_idtransaksi", array(
            "t_lokasi"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "f" => "view_rekening"
                ), "f.s_idkorek = c.t_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('b.t_jenisobjek = ' . $s_idjenis . ' and c.t_tglpendataan is not null');
        $where->notEqualTo("t_nourut", 0);
        if ($post->t_nourut != '')
            $where->literal("c.t_nourut::text LIKE '%$post->t_nourut%'");
        if ($post->t_tglpendataan != '') {
            $t_tgl = explode(' - ', $post->t_tglpendataan);
            $where->literal("TO_CHAR(c.t_tglpendataan, 'yyyy-mm-dd') between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        if ($post->t_masapajak != '') {
            $t_tgl1 = explode(' - ', $post->t_masapajak);
            $where->literal("c.t_masaawal >= '" . date('Y-m-d', strtotime($t_tgl1[0])) . "' and c.t_masaakhir <= '" . date('Y-m-d', strtotime($t_tgl1[1])) . "'");
        }
        if ($post->t_npwpd != '')
            $where->literal("a.t_npwpd ILIKE '%$post->t_npwpd%'");
        if ($post->t_nama != '')
            $where->literal("a.t_nama ILIKE '%$post->t_nama%'");
        if ($post->t_niop != '')
            $where->literal("b.t_nop ILIKE '%$post->t_niop%'");
        if ($post->t_namaobjek != '')
            $where->literal("b.t_namaobjek ILIKE '%$post->t_namaobjek%'");
        if ($post->t_lokasipemasangan != '')
            $where->literal("e.t_lokasi ILIKE '%$post->t_lokasipemasangan%'");
        if ($post->t_kelurahan != '')
            $where->literal("b.s_namakel ILIKE '%$post->t_kelurahan%'");
        if ($post->t_kecamatan != '')
            $where->literal("b.s_namakec ILIKE '%$post->t_kecamatan%'");
        if ($post->t_namanaskah != '')
            $where->literal("e.t_naskah ILIKE '%$post->t_namanaskah%'");
        if ($post->t_kodebayar != '')
            $where->literal("c.t_kodebayar like '%$post->t_kodebayar%'");
        if ($post->t_rekening != '')
            $where->literal("f.korek ilike '%$post->t_rekening%' OR f.s_namakorek ilike '%$post->t_rekening%'");
        if ($post->t_jmlhpajak != '')
            $where->literal("c.t_jmlhpajak::text like '$post->t_jmlhpajak%'");
        if ($post->t_statusbayar != '') {
            if ($post->t_statusbayar == 1) {
                $where->isNotNull("c.t_tglpembayaran");
            } else {
                $where->isNull("c.t_tglpembayaran");
            }
        }
        if ($post->t_pelaporan != '') {
            if ($post->t_pelaporan == 1) {
                $where->equalTo("c.is_esptpd", 1);
            } else {
                $where->equalTo("c.is_esptpd", 0);
            }
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataSudah(PendataanBase $base, $offset, $s_idjenis, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_jenispungutan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak", "t_tglpembayaran", "t_jenispajak", "t_masaawal", "t_masaakhir", "is_esptpd", "t_kodebayar", "t_tglpenetapan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "s_users"
                ), "c.t_operatorpendataan = d.s_iduser", array(
            "s_nama"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "e" => "t_detailreklame"
                ), "e.t_idtransaksi = c.t_idtransaksi", array(
            "t_naskah", "t_lokasi"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "f" => "view_rekening"
                ), "f.s_idkorek = c.t_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('b.t_jenisobjek = ' . $s_idjenis . ' and c.t_tglpendataan is not null');
        $where->notEqualTo("t_nourut", 0);
        if ($post->t_nourut != '')
            $where->literal("c.t_nourut::text LIKE '%$post->t_nourut%'");
        if ($post->t_tglpendataan != '') {
            $t_tgl = explode(' - ', $post->t_tglpendataan);
            $where->literal("TO_CHAR(c.t_tglpendataan, 'yyyy-mm-dd') between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        if ($post->t_masapajak != '') {
            $t_tgl1 = explode(' - ', $post->t_masapajak);
            $where->literal("c.t_masaawal >= '" . date('Y-m-d', strtotime($t_tgl1[0])) . "' and c.t_masaakhir <= '" . date('Y-m-d', strtotime($t_tgl1[1])) . "'");
        }
        if ($post->t_npwpd != '')
            $where->literal("a.t_npwpd ILIKE '%$post->t_npwpd%'");
        if ($post->t_nama != '')
            $where->literal("a.t_nama ILIKE '%$post->t_nama%'");
        if ($post->t_niop != '')
            $where->literal("b.t_nop ILIKE '%$post->t_niop%'");
        if ($post->t_namaobjek != '')
            $where->literal("b.t_namaobjek ILIKE '%$post->t_namaobjek%'");
        if ($post->t_namanaskah != '')
            $where->literal("e.t_naskah ILIKE '%$post->t_namanaskah%'");
        if ($post->t_lokasipemasangan != '')
            $where->literal("e.t_lokasi ILIKE '%$post->t_lokasipemasangan%'");
        if ($post->t_kelurahan != '')
            $where->literal("b.s_namakel ILIKE '%$post->t_kelurahan%'");
        if ($post->t_kecamatan != '')
            $where->literal("b.s_namakec ILIKE '%$post->t_kecamatan%'");
        if ($post->t_kodebayar != '')
            $where->literal("c.t_kodebayar like '%$post->t_kodebayar%'");
        if ($post->t_rekening != '')
            $where->literal("f.korek ilike '%$post->t_rekening%' OR f.s_namakorek ilike '%$post->t_rekening%'");
        if ($post->t_jmlhpajak != '')
            $where->literal("c.t_jmlhpajak::text like '$post->t_jmlhpajak%'");
        if ($post->t_statusbayar != '') {
            if ($post->t_statusbayar == 1) {
                $where->isNotNull("c.t_tglpembayaran");
            } else {
                $where->isNull("c.t_tglpembayaran");
            }
        }
        if ($post->t_pelaporan != '') {
            if ($post->t_pelaporan == 1) {
                $where->equalTo("c.is_esptpd", 1);
            } else {
                $where->equalTo("c.is_esptpd", 0);
            }
        }
        $select->where($where);
        $select->order('c.t_tglpendataan desc');
        $select->order('c.t_nourut desc');
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountMasahabis(PendataanBase $base, $s_idjenis, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak"
                ), $select::JOIN_LEFT);
        $where = new Where();
//        $where->literal("b.t_jenisobjek = " . $s_idjenis . " and c.t_tglpendataan is not null and c.t_masaakhir < '" . date('Y-m-d') . "' ");
        $where->literal("b.t_jenisobjek = " . $s_idjenis . " and c.t_tglpendataan is not null and c.t_masaakhir between now() and (now() + interval '20 day')");
        if ($post->t_nourut != '')
            $where->literal("c.t_nourut::text LIKE '%$post->t_nourut%'");
        if ($post->t_tglpendataan != '') {
            $t_tgl = explode(' - ', $post->t_tglpendataan);
            $where->literal("TO_CHAR(c.t_tglpendataan,'yyyy-mm-dd') between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        if ($post->t_masapajak != '') {
            $t_tgl1 = explode(' - ', $post->t_masapajak);
            $where->literal("c.t_masaawal >= '" . date('Y-m-d', strtotime($t_tgl1[0])) . "' and c.t_masaakhir <= '" . date('Y-m-d', strtotime($t_tgl1[1])) . "'");
        }
        if ($post->t_npwpd != '')
            $where->literal("a.t_npwpd ILIKE '%$post->t_npwpd%'");
        if ($post->t_nama != '')
            $where->literal("a.t_nama ILIKE '%$post->t_nama%'");
        if ($post->t_niop != '')
            $where->literal("b.t_nop ILIKE '%$post->t_niop%'");
        if ($post->t_namaobjek != '')
            $where->literal("b.t_namaobjek ILIKE '%$post->t_namaobjek%'");
        $select->where($where);
        $select->order('t_nourut desc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataMasahabis(PendataanBase $base, $offset, $s_idjenis, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir"
                ), $select::JOIN_LEFT);
        $where = new Where();
//        $where->literal("b.t_jenisobjek = " . $s_idjenis . " and c.t_tglpendataan is not null and c.t_masaakhir < '" . date('Y-m-d') . "' ");
        $where->literal("b.t_jenisobjek = " . $s_idjenis . " and c.t_tglpendataan is not null and c.t_masaakhir between now() and (now() + interval '20 day')");
        if ($post->t_nourut != '')
            $where->literal("c.t_nourut::text LIKE '%$post->t_nourut%'");
        if ($post->t_tglpendataan != '') {
            $t_tgl = explode(' - ', $post->t_tglpendataan);
            $where->literal("TO_CHAR(c.t_tglpendataan,'yyyy-mm-dd') between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        if ($post->t_masapajak != '') {
            $t_tgl1 = explode(' - ', $post->t_masapajak);
            $where->literal("c.t_masaawal >= '" . date('Y-m-d', strtotime($t_tgl1[0])) . "' and c.t_masaakhir <= '" . date('Y-m-d', strtotime($t_tgl1[1])) . "'");
        }
        if ($post->t_npwpd != '')
            $where->literal("a.t_npwpd ILIKE '%$post->t_npwpd%'");
        if ($post->t_nama != '')
            $where->literal("a.t_nama ILIKE '%$post->t_nama%'");
        if ($post->t_niop != '')
            $where->literal("b.t_nop ILIKE '%$post->t_niop%'");
        if ($post->t_namaobjek != '')
            $where->literal("b.t_namaobjek ILIKE '%$post->t_namaobjek%'");
        $select->where($where);
        $select->order("t_nourut desc");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridDataBelumLapor(PendataanBase $base, $s_idjenis, $bln, $thn) {
        $tgl = $thn . "-" . $bln . "-01";
        $sql = "
        SELECT t_idobjek,t_npwpdwp,t_namawp,t_nop,t_namaobjek,s_jenisobjek.s_namajenis,t_alamatlengkapobjek, t_notelpobjek from view_wpobjek 
                left join s_jenisobjek on view_wpobjek.t_jenisobjek=s_jenisobjek.s_idjenis
                where t_idobjek not in ((
                                        SELECT a.t_idobjek from t_wpobjek a
                                            left join t_transaksi b on b.t_idwpobjek=a.t_idobjek
                                            WHERE a.t_jenisobjek=$s_idjenis 
                                            and EXTRACT(MONTH from b.t_masaawal) = '$bln' 
                                            and EXTRACT(YEAR FROM b.t_masaawal) = '$thn'
                                        )) 
                and view_wpobjek.t_jenisobjek=$s_idjenis and view_wpobjek.t_statusobjek = true
                ORDER BY view_wpobjek.t_idobjek
        ";
        // SELECT t_idobjek,t_npwpdwp,t_namawp,t_nop,t_namaobjek,s_jenisobjek.s_namajenis,t_alamatlengkapobjek, t_notelpobjek,t_nohpobjek from view_wpobjek 
        //         left join s_jenisobjek on view_wpobjek.t_jenisobjek=s_jenisobjek.s_idjenis
        //         where t_idobjek not in ((
        //                                 SELECT a.t_idobjek from t_wpobjek a
        //                                     left join t_transaksi b on b.t_idwpobjek=a.t_idobjek
        //                                     WHERE a.t_jenisobjek=$s_idjenis 
        //                                     and MONTH(b.t_masaawal) = '$bln' 
        //                                     and YEAR(b.t_masaawal) = '$thn'
        //                                 )) 
        //         and view_wpobjek.t_jenisobjek=$s_idjenis 
        //         and (view_wpobjek.t_objektutup is null or view_wpobjek.t_tgltutupobjek >'$tgl') 
        //         ORDER BY view_wpobjek.t_idobjek
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getPendataan($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "*"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPendataanAirByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek", "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "t_jenisobjek", "s_namajenis"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_tglpenetapan", "t_jmlhpajak", "t_nourut", "t_tarifpajak","t_masaawal", "t_masaakhir", "t_periodepajak", "t_tglpendataan", "t_nopenetapan", "t_tgljatuhtempo", "t_kodebayar"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "d.s_idkorek = c.t_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_namajenis", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "e" => "t_detailair"
                ), "e.t_idtransaksi = c.t_idtransaksi", ['*'], $select::JOIN_LEFT);
        $select->join(array(
            "f" => "s_air_zona"
                ), "f.s_idzona= e.t_zona", ['s_kodezona' => 's_kode','s_namazona' => 's_deskripsi'], $select::JOIN_LEFT);
        $select->join(array(
            "g" => "s_air_kelompok"
                ), "g.s_id = e.t_kodekelompok", ['s_kodekelompok' => 's_kode','s_namakelompok' => 's_deskripsi'], $select::JOIN_LEFT);
        $select->join(array(
            "h" => "s_air_jenis"
                ), "h.s_id= e.t_kodejenis", ['s_kodejenis' => 's_nourut','s_namakodejenis' => 's_deskripsi'], $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPendataanReklameByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel"
            , "t_kabupatenobjek", "t_rtobjek", "t_rwobjek", "t_latitudeobjek", "t_longitudeobjek", "t_jenisobjek", "s_namajenis"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_tglpenetapan", "t_jmlhpajak", "t_nourut", "t_tarifpajak","t_masaawal", "t_masaakhir", "t_periodepajak", "t_tglpendataan", "t_nopenetapan", "t_tgljatuhtempo", "t_kodebayar"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "t_detailreklame"
                ), "c.t_idtransaksi = d.t_idtransaksi", array('*'), $select::JOIN_LEFT);
        $select->join(array(
            "e" => "s_reklame"
                ), "e.s_idreklame = d.t_jenisreklame", array(
            "s_namareklame"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "f" => "view_rekening"
                ), "c.t_idkorek = f.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function hapusPendataan($id) {
        $this->delete(array('t_idtransaksi' => $id));
    }

    public function hapusSkpdkbJabaran($id) {
        $this->delete(array('t_idtransaksi' => $id));
    }

    public function getPendataanSeMasa(PendataanBase $base, $t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wpobjek'
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "b.t_idwpobjek = a.t_idobjek", array(
            "t_idwpobjek" => "t_idwpobjek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal("extract(month from b.t_masaawal) = '" . date('m', strtotime($base->t_masaawal)) . "' and extract(year from b.t_masaawal) = '" . date('Y', strtotime($base->t_masaawal)) . "' and b.t_idwpobjek= " . $base->t_idobjek);
        $where->notEqualTo("b.t_idtransaksi", $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPendataanMasapajakAwal(PendataanBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wpobjek'
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "b.t_idwpobjek = a.t_idobjek", array(
            "t_idwpobjek" => "t_idwpobjek", "t_idkorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        if($base->t_jenispajak == 2 || $base->t_jenispajak == 6){
            $where->equalTo('b.t_idkorek', $base->t_idkorek);
        }
        $where->literal("extract(month from b.t_masaawal) = '" . date('m', strtotime($base->t_masaawal)) . "' and extract(year from b.t_masaawal) = '" . date('Y', strtotime($base->t_masaawal)) . "' and b.t_idwpobjek= " . $base->t_idobjek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataPendataan($tglpendataan0, $tglpendataan1, $t_kecamatan, $t_kelurahan, $t_idkorek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel"
            , "t_kabupatenobjek", "t_rtobjek", "t_rwobjek", "t_latitudeobjek", "t_longitudeobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_tarifpajak",
            "t_jmlhpajak", "t_volume"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->isNull('e.is_deluserpendataan');
        $where->literal("TO_CHAR(e.t_tglpendataan, 'yyyy-mm-dd') BETWEEN '".date('Y-m-d', strtotime($tglpendataan0))."' AND '".date('Y-m-d', strtotime($tglpendataan1))."' ");
//        $where->between("e.t_tglpendataan", date('Y-m-d', strtotime($tglpendataan0)), date('Y-m-d', strtotime($tglpendataan1)));
        if (!empty($t_kecamatan)) {
            $where->equalTo("a.t_kecamatan", $t_kecamatan);
        }
        if (!empty($t_kelurahan)) {
            $where->equalTo("a.t_kelurahan", $t_kelurahan);
        }
        if (!empty($t_idkorek)) {
            $where->equalTo("a.t_idkorek", $t_idkorek);
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getPendataanPembayaran($t_idwp, $month, $tahun) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_wp'
        ));
        $select->join(array(
            "e" => "t_transaksi"
                ), "a.t_idwp = e.t_idwp", array(
            "t_tglpendataan", "t_jmlhpajak", "t_tglpembayaran", "t_jmlhpembayaran"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->isNull('e.is_deluserpendataan');
        $where->isNull('e.is_deluserpembayaran');
        $where->equalTo("e.t_idwp", $t_idwp);
        $where->literal("extract(month from e.t_tglpendataan) ='" . str_pad($month, 2, '0', STR_PAD_LEFT) . "' and extract(year from e.t_tglpendataan) ='" . $tahun . "'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataPendataanID($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idkorek", "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan"
            , "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak"
            , "t_dasarpengenaan", "t_tarifpajak", "t_kodebayar", "t_operatorpendataan"
            , "t_namakegiatan", "t_tglpenetapan", "t_volume", "t_hargapasaran"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_wp"
                ), "a.t_idwp = c.t_idwp", array(
            "t_nama", "t_alamat", "t_alamat_lengkap", "t_npwpd", "t_kabupaten", "t_rt", "t_rw", "t_alamat"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "b.t_idkorek = d.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek", "s_rinciankorek", "s_sub1korek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('b.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataWPObjek($t_idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idkorek", "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan"
            , "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak"
            , "t_dasarpengenaan", "t_tarifpajak", "t_kodebayar", "t_operatorpendataan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_wp"
                ), "a.t_idwp = c.t_idwp", array(
            "t_nama", "t_alamat", "t_alamat_lengkap", "t_npwpd", "t_kabupaten", "t_rt", "t_rw", "t_alamat"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "b.t_idkorek = d.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek", "s_rinciankorek", "s_sub1korek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idobjek', (int) $t_idobjek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataDetailMinerba($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_detailminerba"
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_idkorek = b.s_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataDetailPPJ($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_detailppj"
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_idkorek = b.s_idkorek", array(
            "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataDetailParkir($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_detailparkir"
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_idkorek = b.s_idkorek", array(
            "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataPendataanSebelumnya($t_jenisobjek, $t_idwpobjek, $t_masaawal) {
        $date = explode("-", date("Y-m-d", strtotime($t_masaawal . "-1 month")));
        $tgl = $date[2];
        $bln = $date[1];
        $thn = $date[0];
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak", "t_dasarpengenaan", "t_tarifpajak"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_jenisobjek', (int) $t_jenisobjek);
        $where->equalTo('b.t_idwpobjek', (int) $t_idwpobjek);
        $where->literal("extract(month from b.t_masaawal) = '" . $bln . "' and extract(year from b.t_masaawal) = '" . $thn . "'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPendataanMax($t_periodepajak) {
        $sql = "select max(t_nourut) as t_nourut from t_transaksi where t_periodepajak='" . $t_periodepajak . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getSkpdjabMax() {
        $sql = "select max(t_noskpdjab) as t_noskpdjab from t_transaksi";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getPenetapanMax() {
        $sql = "select max(t_nopenetapan) as t_nopenetapan from t_transaksi";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function geTglJatuhTempo($t_jenisobjekpajak) {
        $sql = "select t_akhirbayar from s_jenisobjek where s_idjenis='" . $t_jenisobjekpajak . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function geTglJatuhTempoofficial($t_jenisobjekpajak) {
        $sql = "select t_jmlharitempo from s_jenisobjek where s_idjenis='" . $t_jenisobjekpajak . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function geTglJatuhTempoRetribusi($t_jenisobjekpajak) {
        $sql = "select t_jmlharitempo from s_jenisobjek where s_idjenis='" . $t_jenisobjekpajak . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getjenissurat($s_idsurat) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_jenissurat");
        $where = new Where();
        $where->equalTo("s_idsurat", $s_idsurat);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getjmlpendataantahun() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_transaksi");
        $where = new Where();
        $where->equalTo('is_deluserpendataan', 0);
        $where->literal("extract(year from t_tglpendataan) = " . date('Y'));
        $where->literal('t_tglpendataan is not null');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getjmlpendataan() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_transaksi");
        $where = new Where();
        $where->equalTo('is_deluserpendataan', 0);
        $where->literal('t_tglpendataan is not null');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getPendataanSebelumnya($t_idwpobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_rekening"
                ), "b.t_idkorek = c.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek", "t_berdasarmasa"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idobjek', (int) $t_idwpobjek);
        $select->where($where);
        $select->order("t_idtransaksi desc");
        $select->limit(1);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPendataanSebelumnyaABT($t_idwpobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_tglpenetapan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_detailair"
                ), "c.t_idtransaksi = b.t_idtransaksi", array(
            't_volume',
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "b.t_idkorek = d.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idobjek', (int) $t_idwpobjek);
        $select->where($where);
        $select->order("t_idtransaksi desc");
        $select->limit(1);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataPokokPajak($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi",
        ));
        $select->columns(array(
            "t_idtransaksi" => "t_idtransaksi",
            "t_tglpendataan" => "t_tglpendataan",
            "t_dasarpengenaan" => "t_dasarpengenaan",
            "t_tarifpajak" => "t_tarifpajak",
            "t_jmlhpajak" => "t_jmlhpajak",
            "t_tgljatuhtempo" => "t_tgljatuhtempo",
            "t_jenispajak" => "t_jenispajak"
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        if ($res == false) {
            $data = array(
                "t_idtransaksi" => null,
                "t_tglpendataan" => null,
                "t_dasarpengenaan" => null,
                "t_tarifpajak" => null,
                "t_jmlhpajak" => null,
                "t_jenisketetapan" => "SPTPD",
                "t_idketetapan" => 1
            );
        } else {
            $tambahan = array(
                "t_jenisketetapan" => "SPTPD",
                "t_idketetapan" => 1
            );
            $data = array_merge($res, $tambahan);
        }
        return $data;
    }

    public function getDataNppdObjek($t_idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_tglpendataan", "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak", "t_nourut"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_wp"
                ), "a.t_idwp = c.t_idwp", array(
            "t_nama", "t_alamat", "t_alamat_lengkap", "t_npwpd", "t_kabupaten"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idobjek', (int) $t_idobjek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataNppdSelf($t_idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan"
            , "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak"
            , "t_dasarpengenaan", "t_tarifpajak", "t_kodebayar", "t_jmlhpembayaran", "t_jmlhdendapembayaran", "t_jmlhbayardenda"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_wp"
                ), "a.t_idwp = c.t_idwp", array(
            "t_nama", "t_alamat", "t_alamat_lengkap", "t_npwpd", "t_kabupaten"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "b.t_idkorek = d.s_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idobjek', (int) $t_idobjek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataNppdOfficial($t_idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan"
            , "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak"
            , "t_dasarpengenaan", "t_tarifpajak", "t_kodebayar", "t_jmlhpembayaran", "t_jmlhdendapembayaran", "t_jmlhbayardenda"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_wp"
                ), "a.t_idwp = c.t_idwp", array(
            "t_nama", "t_alamat", "t_alamat_lengkap", "t_npwpd", "t_kabupaten"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "b.t_idkorek = d.s_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idobjek', (int) $t_idobjek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountSkpdJabatan(PendataanBase $base, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('c.t_tarifkenaikan != 0');
        if ($post->t_tglpendataan != '') {
            $t_tgl = explode(' - ', $post->t_tglpendataan);
            $where->literal("TO_CHAR(c.t_tglpendataan, 'yyyy-mm-dd') between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        if ($post->t_npwpd != '')
            $where->literal("a.t_npwpd ILIKE '%$post->t_npwpd%'");
        if ($post->t_nama != '')
            $where->literal("a.t_nama ILIKE '%$post->t_nama%'");
        if ($post->t_niop != '')
            $where->literal("b.t_nop ILIKE '%$post->t_niop%'");
        if ($post->t_namaobjek != '')
            $where->literal("b.t_namaobjek ILIKE '%$post->t_namaobjek%'");
        if ($post->t_nopenetapan != '')
            $where->literal("c.t_noskpdjab::text LIKE '%$post->t_nopenetapan%' ");
        if ($post->t_jmlhkenaikan != '')
            $where->literal("c.t_jmlhkenaikan::text LIKE '%$post->t_jmlhkenaikan%'");
        if ($post->t_jmlhpajak != '')
            $where->literal("c.t_jmlhpajak::text LIKE '%$post->t_jmlhpajak%'");
        
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataSkpdJabatan(PendataanBase $base, $offset, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_noskpdjab", "t_tglpendataan", "t_jmlhpajak", "t_jmlhkenaikan"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('c.t_tarifkenaikan != 0');
        if ($post->t_tglpendataan != '') {
            $t_tgl = explode(' - ', $post->t_tglpendataan);
            $where->literal("TO_CHAR(c.t_tglpendataan, 'yyyy-mm-dd') between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        if ($post->t_npwpd != '')
            $where->literal("a.t_npwpd ILIKE '%$post->t_npwpd%'");
        if ($post->t_nama != '')
            $where->literal("a.t_nama ILIKE '%$post->t_nama%'");
        if ($post->t_niop != '')
            $where->literal("b.t_nop ILIKE '%$post->t_niop%'");
        if ($post->t_namaobjek != '')
            $where->literal("b.t_namaobjek ILIKE '%$post->t_namaobjek%'");
        if ($post->t_nopenetapan != '')
            $where->literal("c.t_noskpdjab::text LIKE '%$post->t_nopenetapan%' ");
        if ($post->t_jmlhkenaikan != '')
            $where->literal("c.t_jmlhkenaikan::text LIKE '%$post->t_jmlhkenaikan%'");
        if ($post->t_jmlhpajak != '')
            $where->literal("c.t_jmlhpajak::text LIKE '%$post->t_jmlhpajak%'");
        $select->where($where);
        $select->order("t_noskpdjab desc");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataPendataanSKPDJab($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $select->join(array(
            "b" => "t_transaksi"
                ), "a.t_idobjek = b.t_idwpobjek", array(
            "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan", "t_tgljatuhtempo"
            , "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak", "t_jmlhdendapembayaran"
            , "t_dasarpengenaan", "t_tarifpajak", "t_kodebayar", "t_noskpdjab", "t_tarifkenaikan", "t_jmlhkenaikan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_wp"
                ), "a.t_idwp = c.t_idwp", array(
            "t_nama", "t_alamat", "t_alamat_lengkap", "t_npwpd", "t_kabupaten"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "b.t_idkorek = d.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('b.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPendataanRetribusi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_namajenis"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak", "t_tglpenetapan", "t_tgljatuhtempo", "t_masaawal", "t_masaakhir", "t_kodebayar"
            , "t_periodepajak", "t_nopenetapan", "t_jenispajak"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "c.t_idkorek = d.s_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getSewaDinas($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_detailrumahdinas'
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPanggungReklame($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_detailpanggungreklame'
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getTanahReklame($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_detailtanahreklame'
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getTanahLain($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_detailtanahlain'
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getGedungOlahraga($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_detailgedung'
        ));
        $select->join(array(
            "b" => "s_tarifgedung"
                ), "a.t_jenis = b.s_idtarif", array(
            "s_namatarif"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getJenisPenetapan($t_jenispajak) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 's_jenisobjek'
        ));
        $select->columns(array(
            "s_jenispungutan"
        ));
        $where = new Where();
        $where->equalTo('a.s_idjenis', (int) $t_jenispajak);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getNoPenetapan($t_jenispungutan) {
        if ($t_jenispungutan == 'Pajak') {
            $sql = "select max(t_nopenetapan) as t_nopenetapan from t_transaksi where t_jenispajak in (4,8) and t_periodepajak='" . date('Y') . "'";
        } elseif ($t_jenispungutan == 'Retribusi') {
            $sql = "select max(t_nopenetapan) as t_nopenetapan from t_transaksi where t_jenispajak in (15,19,22,23,25) and t_periodepajak='" . date('Y') . "'";
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getGridCountReklame(PendataanBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi"
                ), $select::JOIN_LEFT);
        $where = new Where();
        // $where->literal('b.t_jenisobjek=4 and c.t_tglpendataan is not null');
        $where->literal('c.t_tglpendataan is not null');
        $where->notEqualTo("t_nourut", 0);
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("$base->combocari LIKE '%$base->kolomcari%'");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        $select->order('t_nourut desc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataReklame(PendataanBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_jenispungutan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_nopenetapan", "t_tglpenetapan", "t_jmlhpajak", "t_tglpembayaran", "t_jenispajak", "t_masaawal", "t_masaakhir", "t_tgljatuhtempo", "t_tglpembayaran"
                ), $select::JOIN_LEFT);
        $where = new Where();
        // $where->literal('b.t_jenisobjek=4 and c.t_tglpendataan is not null');
        $where->literal('c.t_tglpendataan is not null');
        $where->notEqualTo("t_nourut", 0);
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("$base->combocari LIKE '%$base->kolomcari%'");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        if ($base->sortasc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortasc");
            } else {
                $select->order("t_nourut desc");
            }
        } elseif ($base->sortdesc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortdesc");
            } else {
                $select->order("t_nourut desc");
            }
        }
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataReklame($t_kecamatan) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_jenispungutan", "t_alamatlengkapobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nopenetapan", "t_tglpenetapan", "t_jmlhpajak", "t_tglpembayaran", "t_jenispajak", "t_masaawal", "t_masaakhir", "t_tgljatuhtempo", "t_tglpembayaran"
                ), $select::JOIN_LEFT);
        $where = new Where();
        // $where->literal('b.t_jenisobjek=4 and c.t_tglpendataan is not null');
        $where->literal('c.t_tglpendataan is not null');
        $where->notEqualTo("t_nourut", 0);
        if (!empty($t_kecamatan)) {
            $where->equalTo('b.t_kecamatanobjek', $t_kecamatan);
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataPendataanIDTrans($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_jenisobjek", "t_nop", "t_namaobjek", "t_rtobjek", "t_rwobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_namajenis", "t_latitudeobjek", "t_longitudeobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan"
            , "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak"
            , "t_dasarpengenaan", "t_tarifpajak", "t_kodebayar", "t_tglpenetapan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "c.t_idkorek = d.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek", "s_rinciankorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getGridCountAllPajak(PendataanBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('c.t_tglpendataan is not null');
        $where->notEqualTo("t_nourut", 0);
        $where->equalTo("b.s_jenispungutan", 'Pajak');
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("$base->combocari LIKE '%$base->kolomcari%'");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        $select->order('t_nourut desc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataAllPajak(PendataanBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_jenispungutan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak", "t_tglpembayaran", "t_jenispajak", "t_masaawal", "t_masaakhir", "t_tgljatuhtempo", "t_tglpembayaran"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('c.t_tglpendataan is not null');
        $where->notEqualTo("t_nourut", 0);
        $where->equalTo("b.s_jenispungutan", 'Pajak');
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("$base->combocari LIKE '%$base->kolomcari%'");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        if ($base->sortasc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortasc");
            } else {
                $select->order("t_nourut desc");
            }
        } elseif ($base->sortdesc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortdesc");
            } else {
                $select->order("t_nourut desc");
            }
        }
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataAllPajak($t_kecamatan, $jenispajak) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->columns(array(
            "t_npwpd", "t_nama"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatlengkapobjek", "s_namajenissingkat"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_jmlhpajak", "t_tglpembayaran", "t_masaawal", "t_masaakhir", "t_tgljatuhtempo", "t_tglpembayaran"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('c.t_tglpendataan is not null');
        if (!empty($t_kecamatan)) {
            $where->equalTo('b.t_kecamatanobjek', $t_kecamatan);
        }
        if (!empty($jenispajak)) {
            $where->literal("b.t_jenisobjek in (" . $jenispajak . ")");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataPendataanIDTransAll($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wp'
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_idobjek", "t_jenisobjek", "t_nop", "t_namaobjek", "t_rtobjek", "t_rwobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_namajenis", "t_latitudeobjek", "t_longitudeobjek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idkorek", "t_idtransaksi", "t_idwpobjek", "t_nourut", "t_tglpendataan"
            , "t_jmlhpajak", "t_masaawal", "t_masaakhir", "t_periodepajak"
            , "t_dasarpengenaan", "t_tarifpajak", "t_kodebayar", "t_tglpenetapan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_rekening"
                ), "c.t_idkorek = d.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek", "s_rinciankorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('c.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDaftarReklame(PendataanBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_jenispungutan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array("*"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "s_users"
                ), "c.t_operatorpendataan = d.s_iduser", array(
            "s_nama"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "e" => "t_detailreklame"
                ), "e.t_idtransaksi = c.t_idtransaksi", array(
            "*"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('b.t_jenisobjek = 4 and c.t_tglpendataan is not null');
        $where->notEqualTo("t_nourut", 0);
        if ($base->kolomcari != 'undefined') {
            if ($base->combocari != "undefined") {
                if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
                    $where->literal("$base->combocari LIKE '%$base->kolomcari%'");
                } elseif ($base->combooperator == "carisama") {
                    $where->equalTo($base->combocari, $base->kolomcari);
                }
            }
        }
        $select->where($where);
        if ($base->sortasc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortasc");
            } else {
                $select->order("t_nourut desc");
            }
        } elseif ($base->sortdesc != 'undefined') {
            if ($base->combosorting != "undefined") {
                $select->order("$base->combosorting $base->sortdesc");
            } else {
                $select->order("t_nourut desc");
            }
        }
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDaftarRegisterPajak($tglawal, $tglakhir, $jenispajak, $rekening, $kecamatan, $kelurahan, $statubayar, $viabayar) {
        // if (!empty($statubayar)) {
        //     if ($statubayar == 1) {
        //         $where = 'and a.t_tglpembayaran is null ';
        //     } elseif ($statubayar == 2) {
        //         $where = 'and a.t_tglpembayaran is not null ';
        //     }
        // } else {
        //     $where = '';
        // }

        // if (!empty($jenispajak)) {
        //     $where_jenis = "and a.t_jenispajak=" . $jenispajak . " ";
        //     if ($jenispajak == 4 || $jenispajak == 8) {
        //         $orderby = 'order by a.t_tglpenetapan,a.t_nopenetapan asc';
        //     } else {
        //         $orderby = 'order by a.t_tglpendataan,a.t_nourut asc';
        //     }
        // } else {
        //     $where_jenis = "";
        //     $orderby = 'order by a.t_tglpendataan,a.t_nourut asc';
        // }

        // $where_korek = (!empty($rekening)) ? ' and a.t_idkorek='.$rekening.' ' : '';
        // $where_kecamatan = (!empty($kecamatan)) ? ' and b.t_kecamatanobjek='.$kecamatan.' ' : '';
        // $where_kelurahan = (!empty($kelurahan)) ? ' and b.t_kelurahanobjek='.$kelurahan.' ' : '';
        // $where_viabayar = (!empty($viabayar)) ? ' and a.t_viapembayaran='.$viabayar.' ' : '';
        // $sql = "select a.t_periodepajak, a.t_nourut,a.t_jenispajak, b.t_namawp,b.t_namaobjek,a.t_tglpendataan,a.t_tglpenetapan,a.t_nopenetapan,a.t_masaawal,a.t_masaakhir,a.t_jmlhpajak,a.t_tglpembayaran,a.t_tgljatuhtempo,
        //                 c.t_naskah, c.t_lokasi, b.s_namakec, b.s_namakel,a.t_tglbayardenda,a.t_jmlhbayardenda
		// 		from t_transaksi a
		// 		left join view_wpobjek b on b.t_idobjek=a.t_idwpobjek
        //                         left join t_detailreklame c on c.t_idtransaksi=a.t_idtransaksi
		// 		where a.t_tglpendataan between '" . date('Y-m-d', strtotime($tglawal)) . "' and '" . date('Y-m-d', strtotime($tglakhir)) . "'
		// 		" . $where_jenis . " ".$where_korek." ".$where_kecamatan." ".$where_kelurahan." ".$where_viabayar." " . $where . "
		// 		" . $orderby . " ";
        // $statement = $this->adapter->query($sql);
        // return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('a' => 't_transaksi'));
        $select->columns(array(
            't_periodepajak', 't_nourut', 't_jenispajak', 't_tglpendataan', 't_tglpenetapan', 't_nopenetapan',
            't_masaawal', 't_masaakhir', 't_jmlhpajak', 't_tglpembayaran', 't_tgljatuhtempo', 
            't_tglbayardenda', 't_jmlhbayardenda'
        ));
        $select->join(array('b' => 'view_wpobjek'), new Expression('b.t_idobjek=a.t_idwpobjek'), array(
            't_namawp', 't_namaobjek', 's_namakec', 's_namakel', 't_alamatlengkapobjek', 't_npwpdwp'
        ), 'LEFT');
        $select->join(array('c' => 't_detailreklame'), new Expression('c.t_idtransaksi=a.t_idtransaksi'), array(
            't_naskah', 't_lokasi'
        ), 'LEFT');
        $where = new Where();
        $where->literal('TO_CHAR(a.t_tglpendataan, "yyyy-mm-dd") BETWEEN \''. date('Y-m-d', strtotime($tglawal))  .'\' AND  \''. date('Y-m-d', strtotime($tglakhir)) .'\'');
        if($jenispajak != ''){
            $where->literal('a.t_jenispajak ='. (int) $jenispajak .'');
        }
        if($rekening != ''){
            $where->literal('a.t_idkorek='. $rekening .'');
        }
        if($statubayar != ''){
            if ($statubayar == 1) {
                $where->literal('a.t_tglpembayaran is null');
            } elseif ($statubayar == 2) {
                $where->literal('a.t_tglpembayaran is not null');
            }
        }
        if($kecamatan != ''){
            $where->literal('b.t_kecamatanobjek='.$kecamatan.'');
        }
        if($kelurahan != ''){
            $where->literal('b.t_kelurahanobjek='.$kelurahan.'');
        }
        if($viabayar != ''){
            $where->literal('a.t_viapembayaran='.$viabayar.'');
        }
        $select->where($where);
        if($jenispajak == 4 || $jenispajak == 8){
            $orderby = 'a.t_tglpenetapan asc, a.t_nopenetapan asc';
        }else{
            $orderby = 'a.t_tglpendataan asc, a.t_nourut asc';
        }
        $select->order($orderby);
        // die($select->getSqlString());
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function getDataTransaksiByPerMasaAkhirpajak($tglcetak, $periode, $jenispajak) {
        $tglcetak_thn = date('Y', strtotime($tglcetak));
        $sql = "SELECT c.t_tglpendataan,c.t_nourut,a.t_npwpd,a.t_nama,b.t_namaobjek, a.t_alamat, c.t_jmlhpajak, c.t_nopembayaran, c.t_tglpembayaran, c.t_jmlhpembayaran, c.t_jenispajak, c.t_masaawal, c.t_masaakhir, extract(MONTH FROM c.t_masaawal) as masapajak, extract(YEAR FROM c.t_masaawal) as periodepajak, d.s_namakorek 
                FROM view_wp a
                    LEFT JOIN view_wpobjek b ON a.t_idwp = b.t_idwp
                    LEFT JOIN t_transaksi c ON b.t_idobjek = c.t_idwpobjek
                    LEFT JOIN view_rekening d ON c.t_idkorek = d.s_idkorek
                WHERE c.t_periodepajak='" . $periode . "' and extract(year from c.t_masaakhir)='" . $tglcetak_thn . "' and b.t_jenisobjek=$jenispajak ORDER BY d.s_idkorek asc";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }

    public function getdaftarbelumlapor($t_idobjek) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wpobjek"
        ));
        $where = new Where();
        $where->literal('a.t_idobjek in(' . $t_idobjek . ')');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getJenisPajak($s_idjenis) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "s_jenisobjek"
        ));
        $where = new Where();
        $where->equalTo('a.s_idjenis', $s_idjenis);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataExport($s_idjenis, $post){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_wp"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwp = b.t_idwp", array(
            "t_nop", "t_namaobjek", "t_alamatobjek",
            "s_namakecobjek" => "s_namakec", "s_namakelobjek" => "s_namakel", "t_kabupatenobjek", "s_jenispungutan"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "t_transaksi"
                ), "b.t_idobjek = c.t_idwpobjek", array(
            "t_idtransaksi", "t_nourut", "t_tglpendataan", "t_jmlhpajak", "t_tglpembayaran", "t_jenispajak", "t_masaawal", "t_masaakhir", "is_esptpd", "t_kodebayar"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "s_users"
                ), "c.t_operatorpendataan = d.s_iduser", array(
            "s_nama"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "e" => "t_detailreklame"
                ), "e.t_idtransaksi = c.t_idtransaksi", array(
            "t_lokasi"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->literal('b.t_jenisobjek = ' . $s_idjenis . ' and c.t_tglpendataan is not null');
        $where->notEqualTo("t_nourut", 0);
        if ($post->t_nourut != '')
            $where->literal("c.t_nourut::text LIKE '%$post->t_nourut%'");
        if ($post->t_tglpendataan != '') {
            $t_tgl = explode(' - ', $post->t_tglpendataan);
            $where->literal("TO_CHAR(c.t_tglpendataan, 'yyyy-mm-dd') between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        if ($post->t_masapajak != '') {
            $t_tgl1 = explode(' - ', $post->t_masapajak);
            $where->literal("c.t_masaawal >= '" . date('Y-m-d', strtotime($t_tgl1[0])) . "' and c.t_masaakhir <= '" . date('Y-m-d', strtotime($t_tgl1[1])) . "'");
        }
        if ($post->t_npwpd != '')
            $where->literal("a.t_npwpd ILIKE '%$post->t_npwpd%'");
        if ($post->t_nama != '')
            $where->literal("a.t_nama ILIKE '%$post->t_nama%'");
        if ($post->t_niop != '')
            $where->literal("b.t_nop ILIKE '%$post->t_niop%'");
        if ($post->t_namaobjek != '')
            $where->literal("b.t_namaobjek ILIKE '%$post->t_namaobjek%'");
        if ($post->t_lokasipemasangan != 'undefined')
            $where->literal("e.t_lokasi ILIKE '%$post->t_lokasipemasangan%'");
        if ($post->t_kodebayar != '')
            $where->literal("c.t_kodebayar like '%$post->t_kodebayar%'");
        if ($post->t_jmlhpajak != '')
            $where->literal("c.t_jmlhpajak::text like '$post->t_jmlhpajak%'");
        if ($post->t_statusbayar != '') {
            if ($post->t_statusbayar == 1) {
                $where->isNotNull("c.t_tglpembayaran");
            } else {
                $where->isNull("c.t_tglpembayaran");
            }
        }
        if ($post->t_pelaporan != '') {
            if ($post->t_pelaporan == 1) {
                $where->equalTo("c.is_esptpd", 1);
            } else {
                $where->equalTo("c.is_esptpd", 0);
            }
        }
        $select->where($where);
        $select->order('c.t_tglpendataan desc');
        $select->order('c.t_nourut desc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
}
