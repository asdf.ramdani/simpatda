<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class TeguranTable extends AbstractTableGateway {

    protected $table = 't_teguranlaporpajak';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new TeguranBase());
        $this->initialize();
    }

    public function simpansuratteguran(TeguranBase $base, $post, $session, $i) {
        $data = array( 
            't_noteguran' => $post['t_noteguran'][$i],
            't_idobjekteguran' => $post['t_idobjekteguran'][$i],
            't_tglteguran' => date('Y-m-d', strtotime($base->t_tglteguran)),
            't_bulanpajak' => $base->t_bulanpajak,
            't_tahunpajak' => $base->t_tahunpajak,
            't_operatorinputteguran' => $session['s_iduser'],
        );
//        $t_idair = $base->t_idair;
//        if (empty($t_idair)) {
        $this->insert($data);
//        } else {
//            $this->update($data, array('t_idair' => $t_idair));
//        }
//        return $data;
    }

    public function getGridCountSuratTeguran(TeguranBase $base, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_teguranlaporpajak"
        ));
        $select->columns(array(
            "t_noteguran"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "b.t_idobjek = a.t_idobjekteguran", array(
            't_npwpdwp'
                ), $select::JOIN_LEFT);
        $where = new Where();
        $tglteguran = date('dmY', strtotime($post->t_noteguran));
        if ($post->t_niop != '')
            $where->literal("t_nop ILIKE '%$post->t_niop%'");
        if ($post->t_namaobjek != '')
            $where->literal("t_namaobjek ILIKE '%$post->t_namaobjek%'");
        if ($post->t_alamatobjek != '')
            $where->literal("t_alamatlengkapobjek ILIKE '%$post->t_alamatobjek%'");
        if ($post->t_npwpd != '')
            $where->literal("t_npwpdwp ILIKE '%$post->t_npwpd%'");
        if ($post->t_nama != '')
            $where->literal("t_namawp ILIKE '%$post->t_nama%'");
        if ($post->t_jenispajak != '')
            $where->literal("s_namajenis ILIKE '%$post->t_jenispajak%'");
        if ($post->t_masapajak != '')
            $where->literal("t_bulanpajak ILIKE '%$post->t_masapajak%' OR t_tahunpajak ILIKE '%$post->t_masapajak%'");
        if ($post->t_noteguran != '')
            $where->literal("t_noteguran ILIKE '%$post->t_noteguran%' OR t_tglteguran = '$tglteguran'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataSuratTeguran(TeguranBase $base, $offset, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_teguranlaporpajak"
        ));
        $select->columns(array(
            "t_noteguran", "t_tglteguran", "t_bulanpajak", "t_tahunpajak", "t_idteguran"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "b.t_idobjek = a.t_idobjekteguran", array(
            't_npwpdwp', 't_namawp', "t_idobjek", "t_nop", "t_namaobjek", "t_alamatlengkapobjek", "s_namajenis"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $tglteguran = date('dmY', strtotime($post->t_noteguran));
        if ($post->t_niop != '')
            $where->literal("t_nop ILIKE '%$post->t_niop%'");
        if ($post->t_namaobjek != '')
            $where->literal("t_namaobjek ILIKE '%$post->t_namaobjek%'");
        if ($post->t_alamatobjek != '')
            $where->literal("t_alamatlengkapobjek ILIKE '%$post->t_alamatobjek%'");
        if ($post->t_npwpd != '')
            $where->literal("t_npwpdwp ILIKE '%$post->t_npwpd%'");
        if ($post->t_nama != '')
            $where->literal("t_namawp ILIKE '%$post->t_nama%'");
        if ($post->t_jenispajak != '')
            $where->literal("s_namajenis ILIKE '%$post->t_jenispajak%'");
        if ($post->t_masapajak != '')
            $where->literal("t_bulanpajak ILIKE '%$post->t_masapajak%' OR t_tahunpajak ILIKE '%$post->t_masapajak%'");
        if ($post->t_noteguran != '')
            $where->literal("t_noteguran ILIKE '%$post->t_noteguran%' OR t_tglteguran = '$tglteguran'");
        $select->where($where);
        $select->order("a.t_idteguran desc");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridDataPenagihan($post) {
        $where = " ";
        if (!empty($post->t_jenispajak)) {
            $where .= " where t_jenisobjek=" . $post->t_jenispajak . "";
            if (!empty($post->t_tgldaftarobjek)) {
                $t_tgl = explode(' - ', $post->t_tgldaftarobjek);
                $where .= " and t_tgldaftarobjek between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'";
            }
            if (!empty($post->t_namawp)) {
                $where .= " and t_namawp ILIKE '%" . $post->t_namawp . "%'";
            }
            if (!empty($post->t_namaobjek)) {
                $where .= " and t_namaobjek ILIKE '%" . $post->t_namaobjek . "%'";
            }
            if (!empty($post->t_kecamatanobjek)) {
                $where .= " and t_kecamatanobjek=" . $post->t_kecamatanobjek . "";
            }
        }
        $sql = "SELECT t_idobjek, t_tgldaftarobjek,t_namawp, t_npwpdwp, t_nop, t_namaobjek, s_namajenis, '' as t_objektutup, '' as t_tgltutupobjek, s_namakec, s_namakel, t_notelpobjek, '' as t_nohpobjek, '' as t_notelpwp, '' as t_nohpwp,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='01' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as data_jan,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='02' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as data_feb,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='03' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as data_mar,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='04' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as data_apr,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='05' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as data_mei,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='06' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as data_jun,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='07' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as data_jul,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='08' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as data_agu,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='09' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as data_sep,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='10' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as data_okt,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='11' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as data_nov,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='12' and date_part('year',t_masaawal) =" . ($post->t_periodepajak) . ") as data_des,
                    (SELECT sum(t_jmlhpajak) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='01' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as bayar_jan,
                    (SELECT sum(t_jmlhpembayaran) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='02' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as bayar_feb,
                    (SELECT sum(t_jmlhpembayaran) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='03' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as bayar_mar,
                    (SELECT sum(t_jmlhpembayaran) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='04' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as bayar_apr,
                    (SELECT sum(t_jmlhpembayaran) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='05' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as bayar_mei,
                    (SELECT sum(t_jmlhpembayaran) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='06' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as bayar_jun,
                    (SELECT sum(t_jmlhpembayaran) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='07' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as bayar_jul,
                    (SELECT sum(t_jmlhpembayaran) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='08' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as bayar_agu,
                    (SELECT sum(t_jmlhpembayaran) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='09' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as bayar_sep,
                    (SELECT sum(t_jmlhpembayaran) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='10' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as bayar_okt,
                    (SELECT sum(t_jmlhpembayaran) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='11' and date_part('year',t_masaawal) =" . $post->t_periodepajak . ") as bayar_nov,
                    (SELECT sum(t_jmlhpembayaran) as jml FROM t_transaksi WHERE t_idwpobjek=a.t_idobjek and date_part('month',t_masaawal)='12' and date_part('year',t_masaawal) =" . ($post->t_periodepajak) . ") as bayar_des,
                    (SELECT count(t_idteguran) as jml FROM t_teguranlaporpajak WHERE t_idobjekteguran=a.t_idobjek and t_bulanpajak='01' and t_tahunpajak ='" . $post->t_periodepajak . "') as jml_jan,
                    (SELECT count(t_idteguran) as jml FROM t_teguranlaporpajak WHERE t_idobjekteguran=a.t_idobjek and t_bulanpajak='02' and t_tahunpajak ='" . $post->t_periodepajak . "') as jml_feb,
                    (SELECT count(t_idteguran) as jml FROM t_teguranlaporpajak WHERE t_idobjekteguran=a.t_idobjek and t_bulanpajak='03' and t_tahunpajak ='" . $post->t_periodepajak . "') as jml_mar,
                    (SELECT count(t_idteguran) as jml FROM t_teguranlaporpajak WHERE t_idobjekteguran=a.t_idobjek and t_bulanpajak='04' and t_tahunpajak ='" . $post->t_periodepajak . "') as jml_apr,
                    (SELECT count(t_idteguran) as jml FROM t_teguranlaporpajak WHERE t_idobjekteguran=a.t_idobjek and t_bulanpajak='05' and t_tahunpajak ='" . $post->t_periodepajak . "') as jml_mei,
                    (SELECT count(t_idteguran) as jml FROM t_teguranlaporpajak WHERE t_idobjekteguran=a.t_idobjek and t_bulanpajak='06' and t_tahunpajak ='" . $post->t_periodepajak . "') as jml_jun,
                    (SELECT count(t_idteguran) as jml FROM t_teguranlaporpajak WHERE t_idobjekteguran=a.t_idobjek and t_bulanpajak='07' and t_tahunpajak ='" . $post->t_periodepajak . "') as jml_jul,
                    (SELECT count(t_idteguran) as jml FROM t_teguranlaporpajak WHERE t_idobjekteguran=a.t_idobjek and t_bulanpajak='08' and t_tahunpajak ='" . $post->t_periodepajak . "') as jml_agu,
                    (SELECT count(t_idteguran) as jml FROM t_teguranlaporpajak WHERE t_idobjekteguran=a.t_idobjek and t_bulanpajak='09' and t_tahunpajak ='" . $post->t_periodepajak . "') as jml_sep,
                    (SELECT count(t_idteguran) as jml FROM t_teguranlaporpajak WHERE t_idobjekteguran=a.t_idobjek and t_bulanpajak='10' and t_tahunpajak ='" . $post->t_periodepajak . "') as jml_okt,
                    (SELECT count(t_idteguran) as jml FROM t_teguranlaporpajak WHERE t_idobjekteguran=a.t_idobjek and t_bulanpajak='11' and t_tahunpajak ='" . $post->t_periodepajak . "') as jml_nov,
                    (SELECT count(t_idteguran) as jml FROM t_teguranlaporpajak WHERE t_idobjekteguran=a.t_idobjek and t_bulanpajak='12' and t_tahunpajak ='" . ($post->t_periodepajak) . "') as jml_des
                from view_wpobjek a
                $where
                GROUP BY t_idobjek, t_tgldaftarobjek,t_namawp, t_npwpdwp, t_nop, t_namaobjek, s_namajenis, t_objektutup, t_tgltutupobjek, s_namakec, s_namakel, t_notelpobjek, t_nohpobjek, t_notelpwp, t_nohpwp
                ORDER BY t_tgldaftarobjek desc";
//        print_r($sql);
//        exit();
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res;
    }
}
