<?php

namespace Pajak\Model\Secure;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

class ActivityTable extends AbstractTableGateway {
    
    public $table = 'history_log';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }
    
    public function getdata() {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }
    
    public function save($base, $session) {
        $data = array(
            'hislog_opr_id' => $session['s_iduser'],
            'hislog_opr_user' => $session['s_username'],
            'hislog_opr_nama' => $session['s_namauserrole'],
            'hislog_action' => $base,
            'hislog_time' => date('Y-m-d H:s:i')
        );
        $this->insert($data);
    }
    
    public function saveLogin($message) {
        $data = array(
            'hislog_opr_id' => 1,
            'hislog_opr_user' => 'roni',
            'hislog_opr_nama' => 'roni',
            'hislog_action' => $message,
            'hislog_time' => date('Y-m-d H:s:i')
        );
        $table_hislog = new \Zend\Db\TableGateway\TableGateway('history_log', $this->adapter);
        $table_hislog->insert($data);
    }
    
}

