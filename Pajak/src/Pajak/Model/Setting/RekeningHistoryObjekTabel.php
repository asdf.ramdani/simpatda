<?php

namespace Pajak\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class RekeningHistoryObjekTabel extends AbstractTableGateway
{
    protected $table = 's_rekening_history_objek';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function save($data)
    {
        if (!empty($data['t_idobjek'])) {
            $this->insert($data);
        }
    }

    public function getdataObjek($idkorek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_wpobjek"
        ));
        $where = new Where();
        $where->equalTo('a.t_korekobjek', (int) $idkorek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getRekeningIdmax()
    {
        $sql = "select max(s_idkorek) from s_rekening";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getGridCountObjek($base, $idkorek, $parametercari)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array(
            "a" => "t_wpobjek"
        ));
        $select->join(array(
            "b" => "view_rekening"
        ), "a.t_korekobjek = b.s_idkorek", array(
            "korek", "s_namakorek", "s_tglawalkorek", "s_tglakhirkorek"
        ));

        $where = new Where();
        if ($parametercari->korek != '') {
            $where->literal("korek LIKE '%$parametercari->korek%'");
        }
        if ($parametercari->s_namakorek != '') {
            $where->literal("s_namakorek LIKE '%$parametercari->s_namakorek%'");
        }
        if ($parametercari->s_persentarifkorek != '') {
            $where->literal("s_persentarifkorek LIKE '%$parametercari->s_persentarifkorek%'");
        }
        if (!empty($parametercari->t_jenisobjek)) {
            $where->equalTo('s_jenisobjek', $parametercari->t_jenisobjek);
        }
        $where->equalTo('t_korekobjek', $idkorek);
        $select->where($where);
        // die($select->getSqlString());
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataObjek($base, $start, $idkorek, $parametercari)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array(
            "a" => "t_wpobjek"
        ));
        $select->join(array(
            "b" => "view_rekening"
        ), "a.t_korekobjek = b.s_idkorek", array(
            "korek", "s_namakorek", "s_tglawalkorek", "s_tglakhirkorek"
        ));

        $where = new Where();
        if ($parametercari->korek != '') {
            $where->literal("korek LIKE '%$parametercari->korek%'");
        }
        if ($parametercari->s_namakorek != '') {
            $where->literal("s_namakorek LIKE '%$parametercari->s_namakorek%'");
        }
        if ($parametercari->s_persentarifkorek != '') {
            $where->literal("s_persentarifkorek LIKE '%$parametercari->s_persentarifkorek%'");
        }
        if (!empty($parametercari->t_jenisobjek)) {
            $where->equalTo('s_jenisobjek', $parametercari->t_jenisobjek);
        }
        $where->equalTo('t_korekobjek', $idkorek);
        $select->where($where);
        $select->order('s_rinciankorek asc');
        $select->order('s_sub1korek asc');
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountMigrasiRekening(RekeningBase $base, $parametercari)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array(
            "a" => "t_wpobjek"
        ));
        $select->join(array(
            "b" => "s_rekening_history_objek"
        ), "b.t_idobjek = a.t_idobjek");
        $select->join(array(
            "c" => "view_rekening"
        ), "c.s_idkorek = b.t_idkorek_lama", array(
            "korek_lama" => "korek", "s_namakorek_lama" => "s_namakorek", "s_tglawalkorek_lama" => "s_tglawalkorek", "s_tglakhirkorek_lama" => "s_tglakhirkorek"
        ));
        $select->join(array(
            "d" => "view_rekening"
        ), "d.s_idkorek = b.t_idkorek_baru", array(
            "korek_baru" => "korek", "s_namakorek_baru" => "s_namakorek", "s_tglawalkorek_baru" => "s_tglawalkorek", "s_tglakhirkorek_baru" => "s_tglakhirkorek"
        ));

        $where = new Where();
        if ($parametercari->korek != '') {
            $where->literal("korek LIKE '%$parametercari->korek%'");
        }
        if ($parametercari->s_namakorek != '') {
            $where->literal("s_namakorek LIKE '%$parametercari->s_namakorek%'");
        }
        if ($parametercari->s_persentarifkorek != '') {
            $where->literal("s_persentarifkorek LIKE '%$parametercari->s_persentarifkorek%'");
        }
        if (!empty($parametercari->t_jenisobjek)) {
            $where->equalTo('s_jenisobjek', $parametercari->t_jenisobjek);
        }
        
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridMigrasiRekening(RekeningBase $base, $start, $parametercari)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array(
            "a" => "t_wpobjek"
        ));
        $select->join(array(
            "b" => "s_rekening_history_objek"
        ), "b.t_idobjek = a.t_idobjek");
        $select->join(array(
            "c" => "view_rekening"
        ), "c.s_idkorek = b.t_idkorek_lama", array(
            "korek_lama" => "korek", "s_namakorek_lama" => "s_namakorek", "s_tglawalkorek_lama" => "s_tglawalkorek", "s_tglakhirkorek_lama" => "s_tglakhirkorek"
        ));
        $select->join(array(
            "d" => "view_rekening"
        ), "d.s_idkorek = b.t_idkorek_baru", array(
            "korek_baru" => "korek", "s_namakorek_baru" => "s_namakorek", "s_tglawalkorek_baru" => "s_tglawalkorek", "s_tglakhirkorek_baru" => "s_tglakhirkorek"
        ));

        $where = new Where();
        if ($parametercari->korek != '') {
            $where->literal("korek LIKE '%$parametercari->korek%'");
        }
        if ($parametercari->s_namakorek != '') {
            $where->literal("s_namakorek LIKE '%$parametercari->s_namakorek%'");
        }
        if ($parametercari->s_persentarifkorek != '') {
            $where->literal("s_persentarifkorek LIKE '%$parametercari->s_persentarifkorek%'");
        }
        if (!empty($parametercari->t_jenisobjek)) {
            $where->equalTo('s_jenisobjek', $parametercari->t_jenisobjek);
        }
        $select->where($where);
        $select->order('korek_lama asc');
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
}
