<?php

namespace Pajak\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class RekeningHistoryTable extends AbstractTableGateway
{
    protected $table = 's_rekening_history';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function save($data)
    {
        // var_dump($data); die;
        if (!empty($data['s_idkorek_lama'])) {
            $this->insert($data);
        }
    }

    public function getdataRekeningLama($s_idkorek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "s_rekening"
        ));
        $select->join(array(
            "b" => "s_rekening_history"
        ), "a.s_idkorek = b.s_idkorek_lama", array(
            "idkorek_lama" => "s_idkorek_lama", "idkorek_baru" => "s_idkorek_baru"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "s_rekening"
        ), "c.s_idkorek = b.s_idkorek_baru", array(
            "s_idkorek_baru" => "s_idkorek", "s_tipekorek_baru" => "s_tipekorek", "s_kelompokkorek_baru" => "s_kelompokkorek", "s_jeniskorek_baru" => "s_jeniskorek", "s_objekkorek_baru" => "s_objekkorek",
            "s_rinciankorek_baru" => "s_rinciankorek", "s_sub1korek_baru" => "s_sub1korek", "s_sub2korek_baru" => "s_sub2korek", "s_sub3korek_baru" => "s_sub3korek", "s_namakorek_baru" => "s_namakorek",
            "s_tglawalkorek_baru" => "s_tglawalkorek", "s_tglakhirkorek_baru" => "s_tglakhirkorek", "s_jenisdenda_baru" => "s_jenisdenda"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.s_idkorek', (int) $s_idkorek);
        $select->where($where);
        // die($select->getSqlString());
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getGridCountRekening(RekeningBase $base, $parametercari)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_rekening');
        $where = new Where();
        if ($parametercari->korek != '')
            $where->literal("korek LIKE '%$parametercari->korek%'");
        if ($parametercari->s_namakorek != '')
            $where->literal("s_namakorek ILIKE '%$parametercari->s_namakorek%'");
        if ($parametercari->s_persentarifkorek != '')
            $where->literal("s_persentarifkorek LIKE '%$parametercari->s_persentarifkorek%'");
        if (!empty($parametercari->t_jenisobjek)) {
            $where->equalTo('s_jenisobjek', $parametercari->t_jenisobjek);
        }
        $where->notEqualTo('s_rinciankorek', '00');
        $where->notEqualTo('s_jeniskorek', '4');
        $select->where($where);
        // die($select->getSqlString());
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataRekening(RekeningBase $base, $start, $parametercari)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_rekening');
        $where = new Where();
        if ($parametercari->korek != '')
            $where->literal("korek LIKE '%$parametercari->korek%'");
        if ($parametercari->s_namakorek != '')
            $where->literal("s_namakorek LIKE '%$parametercari->s_namakorek%'");
        if ($parametercari->s_persentarifkorek != '')
            $where->literal("s_persentarifkorek LIKE '%$parametercari->s_persentarifkorek%'");
        if (!empty($parametercari->t_jenisobjek)) {
            $where->equalTo('s_jenisobjek', $parametercari->t_jenisobjek);
        }
        $where->notEqualTo('s_rinciankorek', '00');
        $where->notEqualTo('s_jeniskorek', '4');
        $select->where($where);
        $select->order('s_rinciankorek asc');
        $select->order('s_sub1korek asc');
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getdataObjek($idkorek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_wpobjek"
        ));
        $where = new Where();
        $where->equalTo('a.t_korekobjek', (int) $idkorek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getRekeningIdmax()
    {
        $sql = "select max(s_idkorek) from s_rekening";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function updateKorekObjek($param)
    {
        $data = array(
            't_korekobjek' => $param['t_korekobjek']
        );

        $tabel = new \Zend\Db\TableGateway\TableGateway('t_wpobjek', $this->adapter);
        $tabel->update($data, array('t_idobjek' => $param['t_idobjek']));
    }

    public function getGridCountMigrasiRekening(RekeningBase $base, $parametercari)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array(
            "a" => "view_rekening"
        ));
        $select->join(array(
            "b" => "s_rekening_history"
        ), "a.s_idkorek = b.s_idkorek_lama");
        $select->join(array(
            "c" => "view_rekening"
        ), "c.s_idkorek = b.s_idkorek_baru", array(
            "korek_baru" => "korek", "s_namakorek_baru" => "s_namakorek", "s_tglawalkorek_baru" => "s_tglawalkorek", "s_tglakhirkorek_baru" => "s_tglakhirkorek"
        ));

        $where = new Where();
        if ($parametercari->korek != '') {
            $where->literal("korek LIKE '%$parametercari->korek%'");
        }
        if ($parametercari->s_namakorek != '') {
            $where->literal("s_namakorek LIKE '%$parametercari->s_namakorek%'");
        }
        if ($parametercari->s_persentarifkorek != '') {
            $where->literal("s_persentarifkorek LIKE '%$parametercari->s_persentarifkorek%'");
        }
        if (!empty($parametercari->t_jenisobjek)) {
            $where->equalTo('s_jenisobjek', $parametercari->t_jenisobjek);
        }
        
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridMigrasiRekening(RekeningBase $base, $start, $parametercari)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array(
            "a" => "view_rekening"
        ));
        $select->join(array(
            "b" => "s_rekening_history"
        ), "a.s_idkorek = b.s_idkorek_lama");
        $select->join(array(
            "c" => "view_rekening"
        ), "c.s_idkorek = b.s_idkorek_baru", array(
            "korek_baru" => "korek", "s_namakorek_baru" => "s_namakorek", "s_tglawalkorek_baru" => "s_tglawalkorek", "s_tglakhirkorek_baru" => "s_tglakhirkorek"
        ));

        $where = new Where();
        if ($parametercari->korek != '') {
            $where->literal("korek LIKE '%$parametercari->korek%'");
        }
        if ($parametercari->s_namakorek != '') {
            $where->literal("s_namakorek LIKE '%$parametercari->s_namakorek%'");
        }
        if ($parametercari->s_persentarifkorek != '') {
            $where->literal("s_persentarifkorek LIKE '%$parametercari->s_persentarifkorek%'");
        }
        if (!empty($parametercari->t_jenisobjek)) {
            $where->equalTo('s_jenisobjek', $parametercari->t_jenisobjek);
        }
        $select->where($where);
        $select->order('s_rinciankorek asc');
        $select->order('s_sub1korek asc');
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataDenda()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "view_rekening"
        ));
        $where = new Where();
        $where->equalTo('s_tipekorek', '4');
        $where->equalTo('s_kelompokkorek', '1');
        $where->equalTo('s_jeniskorek', '4');
        $select->where($where);
        // die($select->getSqlString());
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }   
}
